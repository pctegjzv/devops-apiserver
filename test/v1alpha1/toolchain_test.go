package v1alpha1_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = DescribeM("toolchain", func() {
	var (
		toolChain []*v1alpha1.ToolChainElement
	)
	DescribeF("GetDefaultToolChains", func() {
		JustBeforeEach(func() {
			toolChain = v1alpha1.GetDefaultToolChains()
		})
		It("should not error", func() {
			Expect(len(toolChain)).NotTo(Equal(0))
			found := false
			for _, p := range toolChain {
				if p.Name == "continuousIntegration" {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
})
