package v1alpha1_test

import (
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = DescribeM("Projects", func() {
	var (
		project          *v1alpha1.Project
		projectlist      *v1alpha1.ProjectList
		err              error
		output           []byte
		name             string
		namespace_name   string
		file, file_error string
		projectData      map[string]string
	)
	BeforeEach(func() {
		name = "e2etestprojectname" + fmt.Sprintf("%v", time.Now().UnixNano())
		file = "projects/test-project.yaml"
		projectData = map[string]string{
			"projectName":        name,
			"projectDisplayName": "e2eTestProjectDisplayName",
			"projectDescription": "e2eTestProjectDescription",
		}
		file = makeFile(file, projectData)
		kubectlApply(file, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})
	DescribeF("Creating a right project", func() {
		JustBeforeEach(func() {
			project, err = devopsClient.DevopsV1alpha1().Projects().Get(name, v1.GetOptions{})
			time.Sleep(time.Second * 1)
			namespace, _ := k8sClient.CoreV1().Namespaces().Get(name, v1.GetOptions{})
			namespace_name = namespace.GetName()
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(project.GetName()).To(Equal(name))
			Expect(namespace_name).To(Equal(name))
		})
	})

	DescribeF("Creating blacklist project", func() {
		JustBeforeEach(func() {
			file_error = "projects/alauda-system.yaml"
			output, err = kubectlApply(file_error)
		})
		// no need to delete
		It("Should return an error", func() {
			Expect(err).ToNot(BeNil())
			Expect(trim(output)).To(Equal(``))
		})
	})

	DescribeF("Creating invalid project", func() {
		JustBeforeEach(func() {
			file_error = "projects/invalid-project.yaml"
			output, err = kubectlApply(file_error)
		})
		It("Should return an error", func() {
			Expect(err).ToNot(BeNil())
			Expect(trim(output)).To(Equal(``))
		})

	})

	DescribeF("List project ", func() {
		JustBeforeEach(func() {
			projectlist, err = devopsClient.DevopsV1alpha1().Projects().List(v1.ListOptions{})
		})
		It("should contain "+name, func() {
			Expect(err).To(BeNil())
			Expect(projectlist).ToNot(BeNil())
			Expect(projectlist.Items).ToNot(BeEmpty())
			found := false
			for _, p := range projectlist.Items {
				if p.Name == name {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("Retrieve project ", func() {
		JustBeforeEach(func() {
			project, err = devopsClient.DevopsV1alpha1().Projects().Get(name, v1.GetOptions{})
		})
		It("get details", func() {
			Expect(err).To(BeNil())
			Expect(project.GetName()).To(Equal(name))
			Expect(project.Annotations["alauda.io/description"]).To(Equal(projectData["projectDescription"]))
		})
	})

	DescribeF("Update project ", func() {
		JustBeforeEach(func() {
			// controller will update the data quite quickly so we need to try a few times
			RetryExecution(func() error {
				project, err = devopsClient.DevopsV1alpha1().Projects().Get(name, v1.GetOptions{})
				project = project.DeepCopy()
				project.Annotations["alauda.io/description"] = "update"
				project, err = devopsClient.DevopsV1alpha1().Projects().Update(project)
				return err
			})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(project.Annotations["alauda.io/description"]).To(Equal("update"))
		})
	})

	DescribeF("Delete project", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().Projects().Delete(name, nil)
		})
		It("should not err", func() {
			Expect(err).To(BeNil())
			timer := time.NewTimer(time.Second * 30)
			for {
				select {
				case <-timer.C:
					Fail("timeout")
					return
				default:
					_, err = k8sClient.CoreV1().Namespaces().Get(name, v1.GetOptions{})
					if err != nil {
						namespacelist, _ := k8sClient.CoreV1().Namespaces().List(v1.ListOptions{})
						if !strings.Contains(namespacelist.String(), name) {
							Succeed()
							return
						}
					}
					time.Sleep(time.Second * 1)
				}
			}
		})
	})
})
