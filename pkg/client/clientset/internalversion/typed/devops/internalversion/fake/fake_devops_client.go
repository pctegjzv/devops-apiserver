/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	internalversion "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/typed/devops/internalversion"
	rest "k8s.io/client-go/rest"
	testing "k8s.io/client-go/testing"
)

type FakeDevops struct {
	*testing.Fake
}

func (c *FakeDevops) ClusterPipelineTaskTemplates() internalversion.ClusterPipelineTaskTemplateInterface {
	return &FakeClusterPipelineTaskTemplates{c}
}

func (c *FakeDevops) ClusterPipelineTemplates() internalversion.ClusterPipelineTemplateInterface {
	return &FakeClusterPipelineTemplates{c}
}

func (c *FakeDevops) CodeRepoBindings(namespace string) internalversion.CodeRepoBindingInterface {
	return &FakeCodeRepoBindings{c, namespace}
}

func (c *FakeDevops) CodeRepoServices() internalversion.CodeRepoServiceInterface {
	return &FakeCodeRepoServices{c}
}

func (c *FakeDevops) CodeRepositories(namespace string) internalversion.CodeRepositoryInterface {
	return &FakeCodeRepositories{c, namespace}
}

func (c *FakeDevops) ImageRegistries() internalversion.ImageRegistryInterface {
	return &FakeImageRegistries{c}
}

func (c *FakeDevops) ImageRegistryBindings(namespace string) internalversion.ImageRegistryBindingInterface {
	return &FakeImageRegistryBindings{c, namespace}
}

func (c *FakeDevops) ImageRepositories(namespace string) internalversion.ImageRepositoryInterface {
	return &FakeImageRepositories{c, namespace}
}

func (c *FakeDevops) Jenkinses() internalversion.JenkinsInterface {
	return &FakeJenkinses{c}
}

func (c *FakeDevops) JenkinsBindings(namespace string) internalversion.JenkinsBindingInterface {
	return &FakeJenkinsBindings{c, namespace}
}

func (c *FakeDevops) Pipelines(namespace string) internalversion.PipelineInterface {
	return &FakePipelines{c, namespace}
}

func (c *FakeDevops) PipelineConfigs(namespace string) internalversion.PipelineConfigInterface {
	return &FakePipelineConfigs{c, namespace}
}

func (c *FakeDevops) PipelineTaskTemplates(namespace string) internalversion.PipelineTaskTemplateInterface {
	return &FakePipelineTaskTemplates{c, namespace}
}

func (c *FakeDevops) PipelineTemplates(namespace string) internalversion.PipelineTemplateInterface {
	return &FakePipelineTemplates{c, namespace}
}

func (c *FakeDevops) PipelineTemplateSyncs(namespace string) internalversion.PipelineTemplateSyncInterface {
	return &FakePipelineTemplateSyncs{c, namespace}
}

func (c *FakeDevops) Projects() internalversion.ProjectInterface {
	return &FakeProjects{c}
}

func (c *FakeDevops) ProjectManagements() internalversion.ProjectManagementInterface {
	return &FakeProjectManagements{c}
}

func (c *FakeDevops) ProjectManagementBindings(namespace string) internalversion.ProjectManagementBindingInterface {
	return &FakeProjectManagementBindings{c, namespace}
}

func (c *FakeDevops) TestTools() internalversion.TestToolInterface {
	return &FakeTestTools{c}
}

func (c *FakeDevops) TestToolBindings(namespace string) internalversion.TestToolBindingInterface {
	return &FakeTestToolBindings{c, namespace}
}

// RESTClient returns a RESTClient that is used to communicate
// with API server by this client implementation.
func (c *FakeDevops) RESTClient() rest.Interface {
	var ret *rest.RESTClient
	return ret
}
