package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetImageRepos adding fake remote repository list method for testing
func (c *FakeImageRegistryBindings) GetImageRepos(name string) (result *devops.ImageRegistryBindingRepositories, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "repositories"
	obj, err := c.Fake.Invokes(action, &devops.ImageRegistryBindingRepositories{})
	result = obj.(*devops.ImageRegistryBindingRepositories)
	return result, err
}