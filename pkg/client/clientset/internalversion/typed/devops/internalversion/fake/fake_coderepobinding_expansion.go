package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetRemoteRepositories adding fake method for testing
func (c *FakeCodeRepoBindings) GetRemoteRepositories(name string, opts *devops.CodeRepoBindingRepositoryOptions) (result *devops.CodeRepoBindingRepositories, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = coderepobindingsResource
	action.Subresource = "remoterepositories"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoBindingRepositories{})
	result = obj.(*devops.CodeRepoBindingRepositories)
	return result, err
}
