package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetLogs adding fake log method for testing
func (c *FakePipelines) GetLogs(name string, opts *devops.PipelineLogOptions) (result *devops.PipelineLog, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "logs"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.PipelineLog{})
	result = obj.(*devops.PipelineLog)
	return result, err
}

// GetTasks adding fake log method for testing
func (c *FakePipelines) GetTasks(name string, opts *devops.PipelineTaskOptions) (result *devops.PipelineTask, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "tasks"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.PipelineTask{})
	result = obj.(*devops.PipelineTask)
	return result, err
}
