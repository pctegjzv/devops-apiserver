package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
)

// ImageRegistryBindingExpansion expansion methods for client
type ImageRegistryBindingExpansion interface {
	GetImageRepos(name string) (*devops.ImageRegistryBindingRepositories, error)
}

// Get takes name of the pipeline, and returns the remote repository list, and an error if there is any
func (c *imageRegistryBindings) GetImageRepos(name string) (result *devops.ImageRegistryBindingRepositories, err error) {
	result = &devops.ImageRegistryBindingRepositories{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("repositories").
		Do().
		Into(result)
	return
}