package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// PipelineExpansion expansion methods for client
type PipelineExpansion interface {
	GetLogs(name string, opts *v1alpha1.PipelineLogOptions) (*v1alpha1.PipelineLog, error)
	GetTasks(name string, opts *v1alpha1.PipelineTaskOptions) (result *v1alpha1.PipelineTask, err error)
}

// GetLogs takes name of the pipeline and get log options and return PipelineLog or any error if any
func (c *pipelines) GetLogs(name string, opts *v1alpha1.PipelineLogOptions) (result *v1alpha1.PipelineLog, err error) {
	result = &v1alpha1.PipelineLog{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("logs").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// GetTasks takes name of the pipeline, and returns the corresponding pipeline object, and an error if there is any.
func (c *pipelines) GetTasks(name string, opts *v1alpha1.PipelineTaskOptions) (result *v1alpha1.PipelineTask, err error) {
	result = &v1alpha1.PipelineTask{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("tasks").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
