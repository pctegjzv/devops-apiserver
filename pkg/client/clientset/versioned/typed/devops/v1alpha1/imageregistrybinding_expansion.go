package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

// ImageRegistryBindingExpansion expansion methods for client
type ImageRegistryBindingExpansion interface {
	GetImageRepos(name string) (*v1alpha1.ImageRegistryBindingRepositories, error)
}

// Get takes name of the pipeline, and returns the remote repository list, and an error if there is any
func (c *imageRegistryBindings) GetImageRepos(name string) (result *v1alpha1.ImageRegistryBindingRepositories, err error) {
	result = &v1alpha1.ImageRegistryBindingRepositories{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("repositories").
		Do().
		Into(result)
	return
}
