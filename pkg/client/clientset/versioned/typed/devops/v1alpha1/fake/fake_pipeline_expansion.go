package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// GetLogs adding fake log method for testing
func (c *FakePipelines) GetLogs(name string, opts *v1alpha1.PipelineLogOptions) (result *v1alpha1.PipelineLog, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "logs"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineLog{})
	result = obj.(*v1alpha1.PipelineLog)
	return result, err
}

// GetTasks adding fake log method for testing
func (c *FakePipelines) GetTasks(name string, opts *v1alpha1.PipelineTaskOptions) (result *v1alpha1.PipelineTask, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "tasks"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineTask{})
	result = obj.(*v1alpha1.PipelineTask)
	return result, err
}
