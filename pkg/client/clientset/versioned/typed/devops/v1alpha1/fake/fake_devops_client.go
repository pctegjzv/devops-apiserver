/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fake

import (
	v1alpha1 "alauda.io/devops-apiserver/pkg/client/clientset/versioned/typed/devops/v1alpha1"
	rest "k8s.io/client-go/rest"
	testing "k8s.io/client-go/testing"
)

type FakeDevopsV1alpha1 struct {
	*testing.Fake
}

func (c *FakeDevopsV1alpha1) ClusterPipelineTaskTemplates() v1alpha1.ClusterPipelineTaskTemplateInterface {
	return &FakeClusterPipelineTaskTemplates{c}
}

func (c *FakeDevopsV1alpha1) ClusterPipelineTemplates() v1alpha1.ClusterPipelineTemplateInterface {
	return &FakeClusterPipelineTemplates{c}
}

func (c *FakeDevopsV1alpha1) CodeRepoBindings(namespace string) v1alpha1.CodeRepoBindingInterface {
	return &FakeCodeRepoBindings{c, namespace}
}

func (c *FakeDevopsV1alpha1) CodeRepoServices() v1alpha1.CodeRepoServiceInterface {
	return &FakeCodeRepoServices{c}
}

func (c *FakeDevopsV1alpha1) CodeRepositories(namespace string) v1alpha1.CodeRepositoryInterface {
	return &FakeCodeRepositories{c, namespace}
}

func (c *FakeDevopsV1alpha1) ImageRegistries() v1alpha1.ImageRegistryInterface {
	return &FakeImageRegistries{c}
}

func (c *FakeDevopsV1alpha1) ImageRegistryBindings(namespace string) v1alpha1.ImageRegistryBindingInterface {
	return &FakeImageRegistryBindings{c, namespace}
}

func (c *FakeDevopsV1alpha1) ImageRepositories(namespace string) v1alpha1.ImageRepositoryInterface {
	return &FakeImageRepositories{c, namespace}
}

func (c *FakeDevopsV1alpha1) Jenkinses() v1alpha1.JenkinsInterface {
	return &FakeJenkinses{c}
}

func (c *FakeDevopsV1alpha1) JenkinsBindings(namespace string) v1alpha1.JenkinsBindingInterface {
	return &FakeJenkinsBindings{c, namespace}
}

func (c *FakeDevopsV1alpha1) Pipelines(namespace string) v1alpha1.PipelineInterface {
	return &FakePipelines{c, namespace}
}

func (c *FakeDevopsV1alpha1) PipelineConfigs(namespace string) v1alpha1.PipelineConfigInterface {
	return &FakePipelineConfigs{c, namespace}
}

func (c *FakeDevopsV1alpha1) PipelineTaskTemplates(namespace string) v1alpha1.PipelineTaskTemplateInterface {
	return &FakePipelineTaskTemplates{c, namespace}
}

func (c *FakeDevopsV1alpha1) PipelineTemplates(namespace string) v1alpha1.PipelineTemplateInterface {
	return &FakePipelineTemplates{c, namespace}
}

func (c *FakeDevopsV1alpha1) PipelineTemplateSyncs(namespace string) v1alpha1.PipelineTemplateSyncInterface {
	return &FakePipelineTemplateSyncs{c, namespace}
}

func (c *FakeDevopsV1alpha1) Projects() v1alpha1.ProjectInterface {
	return &FakeProjects{c}
}

func (c *FakeDevopsV1alpha1) ProjectManagements() v1alpha1.ProjectManagementInterface {
	return &FakeProjectManagements{c}
}

func (c *FakeDevopsV1alpha1) ProjectManagementBindings(namespace string) v1alpha1.ProjectManagementBindingInterface {
	return &FakeProjectManagementBindings{c, namespace}
}

func (c *FakeDevopsV1alpha1) TestTools() v1alpha1.TestToolInterface {
	return &FakeTestTools{c}
}

func (c *FakeDevopsV1alpha1) TestToolBindings(namespace string) v1alpha1.TestToolBindingInterface {
	return &FakeTestToolBindings{c, namespace}
}

// RESTClient returns a RESTClient that is used to communicate
// with API server by this client implementation.
func (c *FakeDevopsV1alpha1) RESTClient() rest.Interface {
	var ret *rest.RESTClient
	return ret
}
