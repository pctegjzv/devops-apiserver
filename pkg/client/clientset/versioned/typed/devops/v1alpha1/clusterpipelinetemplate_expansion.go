package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// ClusterPipelineTemplateExpansion expansion methods for client
type ClusterPipelineTemplateExpansion interface {
	Preview(name string, opts *v1alpha1.JenkinsfilePreviewOptions) (*v1alpha1.JenkinsfilePreview, error)
}

// Preview for ClusterPipelineTemplate
func (c *clusterPipelineTemplates) Preview(name string, opts *v1alpha1.JenkinsfilePreviewOptions) (result *v1alpha1.JenkinsfilePreview, err error) {
	result = &v1alpha1.JenkinsfilePreview{}
	err = c.client.Get().
		Resource("clusterpipelinetemplates").
		Name(name).
		SubResource("preview").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
