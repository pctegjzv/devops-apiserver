package main

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-bitbucket"
	"log"
)

func main() {
	ProjectExample()
}

func ProjectExample() {
	git, err := bitbucket.NewBasicAuthClient(nil, "", "wangsanyang", "yfsHTudqX7SLpaWZtDqZ")
	if err != nil {
		log.Fatal(err)
		return
	}

	opt := bitbucket.ListRepositoriesOptions{Role: bitbucket.Role(bitbucket.RoleValueCntributor)}
	repositoryList, _, err := git.Repositories.ListAll("", &opt)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i, repository := range repositoryList {
		log.Printf("Found %d repository: %s; owner: %s; type: %s", i, repository.FullName, repository.Owner.Username, repository.Owner.Type)
	}

	repositoryList, _, err = git.Repositories.ListAll("wangsanyang", &opt)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i, repository := range repositoryList {
		log.Printf("Found %d repository: %s; owner: %s; type: %s", i, repository.FullName, repository.Owner.Username, repository.Owner.Type)
	}
}
