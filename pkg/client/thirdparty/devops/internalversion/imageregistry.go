package internalversion

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
)

const (
	ImageDigestSignature     = "sha256:"
	ImageRepositoryAvailable = "available"
)

type ImageRegistryClient struct {
	Endpoint   string
	Username   string
	Credential string
}

type RegistryRepositoriesResponse struct {
	Repositories []string `json:"repositories"`
}

type RegistryRepositoryTagResponse struct {
	Tags []string `json:"tags"`
}

type TagDetails struct {
	History []History `json:"history"`
}

type History struct {
	V1Compatibility string `json:"v1Compatibility"`
}

type V1Compatibility struct {
	Created string `json:"created"`
}

type RegistryResponse struct {
	StatusCode int
	Header     RegistryHeader
	Body       []byte
}

type RegistryHeader struct {
	Digest       string
	Authenticate string
}

type AuthOpts struct {
	Realm   string
	Scope   string
	Service string
}

func NewImageRegistryClient(endpoint, username, credential string) ImageRegistryClient {
	client := ImageRegistryClient{
		endpoint,
		username,
		credential,
	}
	return client
}

// docker registry
type DockerClient struct {
	ImageRegistryClient
	// TODO: replace with http.RoundTripper and add unit test
	Client *http.Client
	Cxt    context.Context
}

func NewDockerClient(endpoint, username, credential string) *DockerClient {
	serviceClient := NewImageRegistryClient(endpoint, username, credential)
	client := &http.Client{}
	cxt := context.Background()
	client.Timeout = time.Second * 30
	return &DockerClient{serviceClient, client, cxt}
}

func (c *DockerClient) CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.Endpoint, nil)
}

func (c *DockerClient) CheckRepositoryAvailable(repositoryName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	glog.Infof("Check image repository: %s is available or not", repositoryName)
	var (
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	repository := strings.TrimLeft(repositoryName, "/")
	urlSuffix := fmt.Sprintf("v2/%s/tags/list", repository)
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	}
	status.StatusCode = resp.StatusCode
	status.Response = ImageRepositoryAvailable
	duration = time.Since(start)
	status.Delay = &duration
	return
}

func (c *DockerClient) GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error) {
	result = &devops.ImageRegistryBindingRepositories{}
	resp, err := c.RequestRegistry("v2/_catalog")
	if err != nil {
		return
	}
	response := new(RegistryRepositoriesResponse)
	json.Unmarshal([]byte(resp.Body), response)
	result.Items = response.Repositories

	return result, nil
}

func (c *DockerClient) GetImageTags(repositoryName string) (result []devops.ImageTag, err error) {
	repository := strings.TrimLeft(repositoryName, "/")
	urlSuffix := fmt.Sprintf("v2/%s/tags/list", repository)
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("Can't get repository: %s tag list", repository)
		return result, err
	}
	tagResp := new(RegistryRepositoryTagResponse)
	json.Unmarshal([]byte(resp.Body), tagResp)
	tags := tagResp.Tags
	glog.Infof("Repository(%s) tags is: %s", repositoryName, tags)
	wait := sync.WaitGroup{}
	wait.Add(len(tags))
	// TODO: support worker pool
	for _, tag := range tags {
		go func(tagStr string) {
			defer wait.Done()
			tagDetail, err := c.GetImageTagDetail(repository, tagStr)
			if err != nil {
				glog.Errorf("Can't get repository: (%s), tag: (%s) detail.", repository, tag)
			} else {
				result = append(result, tagDetail)
			}
		}(tag)
	}
	wait.Wait()

	return result, nil
}

func (c *DockerClient) GetImageTagDetail(repository, reference string) (result devops.ImageTag, err error) {
	urlSuffix := fmt.Sprintf("v2/%s/manifests/%s", repository, reference)
	result = devops.ImageTag{}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("Cat't get repository: (%s) tag: (%s) manifest.", repository, reference)
		result.Name = reference
		return result, err
	}
	glog.Infof("Get repository (%s) tag (%s) manifests", repository, reference)
	tagDetailResp := new(TagDetails)
	json.Unmarshal([]byte(resp.Body), tagDetailResp)
	firstV1Compatibility := tagDetailResp.History[0].V1Compatibility
	v1Compatibility := new(V1Compatibility)
	json.Unmarshal([]byte(firstV1Compatibility), v1Compatibility)
	t, err := time.Parse(time.RFC3339Nano, v1Compatibility.Created)
	if err != nil {
		glog.Errorf("Tag create time convert string to time.Time is failed: %s", err)
	}
	result.CreatedAt = &metav1.Time{Time: t}
	result.Name = reference
	result.Digest = strings.TrimPrefix(resp.Header.Digest, ImageDigestSignature)
	return result, nil
}

func (c *DockerClient) GetImageToken(opts AuthOpts) (result string, err error) {
	glog.Infof("Get registry token with %s", opts)
	req, err1 := http.NewRequest(http.MethodGet, opts.Realm, nil)
	if err1 != nil {
		err = err1
		return
	}

	query := req.URL.Query()
	query.Add("scope", opts.Scope)
	query.Add("service", opts.Service)
	req.URL.RawQuery = query.Encode()
	req.SetBasicAuth(c.Username, c.Credential)
	resp, err := c.DoRequest(req)
	if err != nil {
		glog.Errorf("Get registry token from %s failed, error is: %s", opts.Realm, err)
		return "", err
	}
	resMap := make(map[string]string)
	//bodyBytes, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal([]byte(resp.Body), &resMap)
	return resMap["token"], nil
}

func (c *DockerClient) Url(suffix string) string {
	url := fmt.Sprintf("%s/%s", strings.TrimRight(c.Endpoint, "/"),
		strings.TrimLeft(suffix, "/"))
	return url
}

func (c *DockerClient) RequestRegistry(suffix string) (resp *RegistryResponse, err error) {
	var (
		req   *http.Request
		token string
	)

	url := c.Url(suffix)
	req, err1 := http.NewRequest(http.MethodGet, url, nil)
	if err1 != nil {
		err = err1
		return
	}

	if c.Username != "" && c.Credential != "" {
		glog.Infof("DockerClient username: %s, credential: %s", c.Username, c.Credential)
		req.SetBasicAuth(c.Username, c.Credential)
	}
	resp, err = c.DoRequest(req)
	switch resp.StatusCode {
	case http.StatusOK:
		glog.Infof("Get %s is successful", suffix)
		return resp, nil
	case http.StatusUnauthorized:
		glog.Infof("Get registry response return 401 error, response is: %s", resp)
		auth := resp.Header.Authenticate
		opts := parseAuthenticateString(auth)
		token, err = c.GetImageToken(opts)
		if err != nil {
			return resp, err
		}
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		resp, err = c.DoRequest(req)
		if err != nil {
			return resp, err
		}
		return resp, nil
	default:
		return resp, err
	}
}

func (c *DockerClient) DoRequest(req *http.Request) (resp *RegistryResponse, err error) {
	glog.V(3).Infof("Request data is: %s", req)
	resp = new(RegistryResponse)
	response, err := c.Client.Do(req)
	if err != nil {
		glog.Errorf("Request url: %s failed, error: %v", req.URL, err)
		return resp, err
	} else {
		glog.V(3).Infof("Request url: %s successful", req.URL)
		defer response.Body.Close()
		resp.StatusCode = response.StatusCode
		resp.Header.Digest = response.Header.Get("docker-content-digest")
		resp.Header.Authenticate = response.Header.Get("www-authenticate")
		body, _ := ioutil.ReadAll(response.Body)
		resp.Body = body
		return resp, nil
	}
}

func parseAuthenticateString(auth string) (opts AuthOpts) {
	authSplit := strings.SplitN(auth, " ", 2)
	if len(authSplit) != 2 {
		return
	}
	parts := strings.Split(authSplit[1], ",")
	for _, part := range parts {
		valueList := strings.SplitN(part, "=", 2)
		if len(valueList) == 2 {
			value := strings.Trim(valueList[1], "\",")
			switch valueList[0] {
			case "realm":
				opts.Realm = value
			case "scope":
				opts.Scope = value
			case "service":
				opts.Service = value
			}
		}
	}
	return
}
