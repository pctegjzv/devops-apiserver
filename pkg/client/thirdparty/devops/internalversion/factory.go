package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
)

type ThirdParty struct {
	CodeRepoServiceClientFactory CodeRepoServiceClientFactory
	ImageRegistryClientFactory   ImageRegistryClientFactory
}

func NewThirdParty(codeRepoServiceClientFactory CodeRepoServiceClientFactory, imageRegistryClientFactory ImageRegistryClientFactory) ThirdParty {
	return ThirdParty{
		CodeRepoServiceClientFactory: codeRepoServiceClientFactory,
		ImageRegistryClientFactory:   imageRegistryClientFactory,
	}
}

type CodeRepoServiceClientFactory interface {
	GetCodeRepoServiceClient(service *devops.CodeRepoService, secret *corev1.Secret) (CodeRepoServiceClientInterface, error)
}

type CodeRepoServiceClientInterface interface {
	GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error)
	ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository)
	CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	CheckRepositoryAvailable(repoID, repoName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	AccessToken(redirectUrl string) (secret *corev1.Secret, err error)
	GetAuthorizeUrl(redirectUrl string) string
}

type AccessTokenResponseInterface interface {
	ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details
	GetAccessToken() string
}

func NewCodeRepoServiceClientFactory() CodeRepoServiceClientFactory {
	return codeRepoServiceClientFactory{}
}

type codeRepoServiceClientFactory struct{}

func (f codeRepoServiceClientFactory) GetCodeRepoServiceClient(service *devops.CodeRepoService, secret *corev1.Secret) (CodeRepoServiceClientInterface, error) {
	if service == nil {
		return nil, fmt.Errorf("CodeRepoService must not be empty")
	}

	var (
		err           error
		client        CodeRepoServiceClientInterface
		serviceClient = NewServiceClient(service, secret)
	)
	switch service.Spec.Type {
	case devops.CodeRepoServiceTypeGithub:
		client, err = NewGithubClient(serviceClient)
	case devops.CodeRepoServiceTypeGitlab:
		client, err = NewGitlabClient(serviceClient)
	case devops.CodeRepoServiceTypeGitee:
		client, err = NewGiteeClient(serviceClient)
	case devops.CodeRepoServiceTypeBitbucket:
		client, err = NewBitbucketClient(serviceClient)
	default:
		log.Println("Invalid Service type: ", service.Spec.Type)
		return nil, fmt.Errorf("Invalid service type %s", service.Spec.Type)
	}
	return client, err
}

type ImageRegistryClientFactory interface {
	GetImageRegistryClient(registryType devops.ImageRegistryType, endpoint, username, credential string) ImageRegistryClientInterface
}

type ImageRegistryClientInterface interface {
	GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error)
	GetImageTags(repository string) (result []devops.ImageTag, err error)
	GetImageToken(opts AuthOpts) (result string, err error)
	GetImageTagDetail(repository, reference string) (result devops.ImageTag, err error)
	CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	CheckRepositoryAvailable(repository string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
}

func NewImageRegistryClientFactory() ImageRegistryClientFactory {
	return imageRegistryClientFactory{}
}

type imageRegistryClientFactory struct{}

func (f imageRegistryClientFactory) GetImageRegistryClient(registryType devops.ImageRegistryType, endpoint, username, credential string) ImageRegistryClientInterface {
	var client ImageRegistryClientInterface
	switch registryType {
	case devops.RegistryTypeDocker:
		client = NewDockerClient(endpoint, username, credential)
	}
	return client
}
