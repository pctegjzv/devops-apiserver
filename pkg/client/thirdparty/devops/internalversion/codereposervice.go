/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"context"
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/golang/glog"
	"golang.org/x/oauth2"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"sync"
	"time"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-bitbucket"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-gitee"
	"github.com/google/go-github/github"
	"github.com/xanzy/go-gitlab"
)

const (
	GithubIfModifiedSince = "If-Modified-Since"
	GitlabPrivateTokenKey = "PRIVATE-TOKEN"
	PerPage               = 100
)

// region Github
type GithubClient struct {
	*ServiceClient
	Client *github.Client
	Cxt    context.Context
}

func NewGithubClient(serviceClient *ServiceClient) (*GithubClient, error) {
	var (
		cxt        = context.Background()
		err        error
		httpClient = &http.Client{}
	)

	if serviceClient.isBasicAuth() {
		tp := github.BasicAuthTransport{
			Username: serviceClient.BasicAuthInfo.Username,
			Password: serviceClient.BasicAuthInfo.Password,
		}
		httpClient = tp.Client()
	} else {
		serviceClient.OAuth2Info.AccessTokenKey = k8s.GithubAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.GithubAccessTokenScope

		ts := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: serviceClient.OAuth2Info.AccessToken},
		)
		httpClient = oauth2.NewClient(cxt, ts)
	}

	return &GithubClient{ServiceClient: serviceClient, Client: github.NewClient(httpClient), Cxt: cxt}, err
}

func (c *GithubClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *GithubClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser", c.getBaseUrl())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *GithubClient) CheckRepositoryAvailable(repoID, repoName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getRepoUrl(repoName), c.getHeader(lastModifyAt))
}

func (c *GithubClient) getAllRepos() (githubRepositories []*github.Repository, err error) {
	var (
		wg = sync.WaitGroup{}
	)

	glog.V(5).Infof("fetch data in page %d", 1)
	firstPageResult, response, err1 := c.Client.Repositories.List(c.Cxt, "", &github.RepositoryListOptions{ListOptions: github.ListOptions{Page: 1, PerPage: PerPage}})
	if err1 != nil {
		err = err1
		glog.Errorf("Error list repositories for the user %s. err: %v", c.getUsername(), err)
		return
	}

	if firstPageResult != nil {
		githubRepositories = append(githubRepositories, firstPageResult...)
	}

	if response == nil || response.LastPage < 2 {
		glog.V(5).Infof("No next page, return current result")
		return
	}

	for page := 2; page < response.LastPage+1; page++ {
		glog.V(5).Infof("fetch data in page %d", page)
		wg.Add(1)
		go func(page, perPage int) {
			defer wg.Done()

			pagedResult, _, err := c.Client.Repositories.List(c.Cxt, "", &github.RepositoryListOptions{ListOptions: github.ListOptions{Page: page, PerPage: perPage}})
			if err != nil {
				return
			}
			if pagedResult != nil {
				githubRepositories = append(githubRepositories, pagedResult...)
			}
		}(page, PerPage)
	}
	wg.Wait()

	return
}

func (c *GithubClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	result = &devops.CodeRepoBindingRepositories{
		Type:   devops.CodeRepoServiceTypeGithub,
		Owners: []devops.CodeRepositoryOwner{},
	}

	repositories, err := c.getAllRepos()
	if err != nil || repositories == nil {
		return
	}

	dictOwners := make(map[string]*devops.CodeRepositoryOwner)
	for _, githubRepo := range repositories {
		repositoryOwnerKey := githubRepo.Owner.GetLogin()

		if owner, ok := dictOwners[repositoryOwnerKey]; !ok {
			glog.Infof("Append owner (kind: %s, name: %s) to result", githubRepo.Owner.GetType(), githubRepo.Owner.GetLogin())
			owner = &devops.CodeRepositoryOwner{
				Type:         devops.GetOwnerType(githubRepo.Owner.GetType()),
				ID:           strconv.FormatInt(githubRepo.Owner.GetID(), 10),
				Name:         githubRepo.Owner.GetLogin(),
				Email:        githubRepo.Owner.GetEmail(),
				HTMLURL:      githubRepo.Owner.GetHTMLURL(),
				AvatarURL:    githubRepo.Owner.GetAvatarURL(),
				DiskUsage:    githubRepo.Owner.GetDiskUsage(),
				Repositories: make([]devops.OriginCodeRepository, 0, 10),
			}
			dictOwners[repositoryOwnerKey] = owner
		}
		log.Printf("Append repo (name: %s, id: %d) to owner %s", githubRepo.GetName(), githubRepo.GetID(), dictOwners[repositoryOwnerKey].Name)

		codeRepo := c.ConvertRemoteRepoToBindingRepo(githubRepo)
		dictOwners[repositoryOwnerKey].Repositories = append(dictOwners[repositoryOwnerKey].Repositories, codeRepo)
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return

}

func (c *GithubClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if githubRepo, ok := remoteRepo.(*github.Repository); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeGithub,
			ID:                  strconv.FormatInt(githubRepo.GetID(), 10),
			Name:                githubRepo.GetName(),
			FullName:            githubRepo.GetFullName(),
			Description:         githubRepo.GetDescription(),
			HTMLURL:             githubRepo.GetHTMLURL(),
			CloneURL:            githubRepo.GetCloneURL(),
			SSHURL:              githubRepo.GetSSHURL(),
			Language:            githubRepo.GetLanguage(),
			Owner: devops.OwnerInRepository{
				ID:   strconv.FormatInt(githubRepo.Owner.GetID(), 10),
				Name: githubRepo.GetOwner().GetLogin(),
				Type: devops.GetOwnerType(githubRepo.GetOwner().GetType()),
			},

			CreatedAt: ConvertToMetaTime(githubRepo.GetCreatedAt()),
			PushedAt:  ConvertToMetaTime(githubRepo.GetPushedAt()),
			UpdatedAt: ConvertToMetaTime(githubRepo.GetUpdatedAt()),

			Private:      githubRepo.GetPrivate(),
			Size:         githubRepo.GetSize() * 1000,
			SizeHumanize: humanize.Bytes(uint64(githubRepo.GetSize() * 1000)),
		}
	}

	return
}

// getRateLimitUrl access this url will not waste the rate limit
func (c *GithubClient) getRateLimitUrl() string {
	return fmt.Sprintf("%srate_limit", c.Client.BaseURL)
}

func (c *GithubClient) getRepoUrl(repoName string) string {
	return fmt.Sprintf("%srepos/%s", c.getBaseUrl(), repoName)
}

func (c *GithubClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/login/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.getHtml(), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GithubClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/login/oauth/access_token", c.getHtml())
}

func (c *GithubClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGithub{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *GithubClient) getBaseUrl() string {
	return c.Client.BaseURL.String()
}

// endregion

// region Gitlab
type GitlabClient struct {
	*ServiceClient
	Client *gitlab.Client
}

func NewGitlabClient(serviceClient *ServiceClient) (*GitlabClient, error) {
	var (
		client = &gitlab.Client{}
		err    error
	)

	if serviceClient.isBasicAuth() {
		client = gitlab.NewClient(nil, serviceClient.getPassword())
	} else {
		client = gitlab.NewOAuthClient(nil, serviceClient.OAuth2Info.AccessToken)

		serviceClient.OAuth2Info.AccessTokenKey = k8s.GitlabAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.GitlabAccessTokenScope
	}

	err = client.SetBaseURL(serviceClient.getHost())
	if err != nil {
		return nil, err
	}
	return &GitlabClient{ServiceClient: serviceClient, Client: client}, nil
}

func (c *GitlabClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *GitlabClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser", c.getBaseUrl())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *GitlabClient) CheckRepositoryAvailable(repoID, repoName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getRepoUrl(repoID), c.getHeader(lastModifyAt))
}

func (c *GitlabClient) getAllRepos() (repositories []*gitlab.Project, err error) {
	var (
		wg = sync.WaitGroup{}
	)

	glog.V(5).Infof("fetch data in page %d", 1)
	firstPageResult, response, err := c.Client.Projects.ListProjects(
		&gitlab.ListProjectsOptions{
			Owned:      gitlab.Bool(true),
			Membership: gitlab.Bool(true),
			ListOptions: gitlab.ListOptions{
				Page:    1,
				PerPage: PerPage,
			},
		})
	if err != nil {
		glog.Errorf("error list project in gitlab: %v", err)
		return
	}

	if firstPageResult != nil {
		repositories = append(repositories, firstPageResult...)
	}

	if response == nil || response.TotalPages < 2 {
		glog.V(5).Infof("No next page, return current result")
		return
	}

	for page := 2; page < response.TotalPages+1; page++ {
		glog.V(5).Infof("fetch data in page %d", page)
		wg.Add(1)
		go func(page, perPage int) {
			defer wg.Done()

			pagedResult, _, err := c.Client.Projects.ListProjects(
				&gitlab.ListProjectsOptions{
					Owned:      gitlab.Bool(true),
					Membership: gitlab.Bool(true),
					ListOptions: gitlab.ListOptions{
						Page:    page,
						PerPage: PerPage,
					},
				})
			if err != nil {
				return
			}
			if pagedResult != nil {
				repositories = append(repositories, pagedResult...)
			}
		}(page, PerPage)
	}
	wg.Wait()

	return
}

func (c *GitlabClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	result = &devops.CodeRepoBindingRepositories{
		Type:   devops.CodeRepoServiceTypeGitlab,
		Owners: []devops.CodeRepositoryOwner{},
	}

	repositories, err := c.getAllRepos()
	if err != nil || repositories == nil {
		return
	}

	dictOwners := make(map[string]*devops.CodeRepositoryOwner)
	for _, project := range repositories {
		repositoryOwnerKey := project.Namespace.FullPath

		if owner, ok := dictOwners[repositoryOwnerKey]; !ok {
			log.Printf("Append owner (kind: %s, name: %s, id: %d) to result",
				project.Namespace.Kind, project.Namespace.FullPath, project.Namespace.ID)
			owner = &devops.CodeRepositoryOwner{
				Type:         devops.GetOwnerType(project.Namespace.Kind),
				ID:           strconv.Itoa(project.Namespace.ID),
				Name:         project.Namespace.FullPath,
				Email:        "",
				HTMLURL:      c.getOwnerWebURL(project),
				AvatarURL:    c.getOwnerAvatarURL(project),
				DiskUsage:    0,
				Repositories: make([]devops.OriginCodeRepository, 0, 10),
			}
			dictOwners[repositoryOwnerKey] = owner
		}
		log.Printf("Append repo (name: %s, id: %d) to owner %s", project.Name, project.ID, dictOwners[repositoryOwnerKey].Name)

		codeRepo := c.ConvertRemoteRepoToBindingRepo(project)
		dictOwners[repositoryOwnerKey].Repositories = append(dictOwners[repositoryOwnerKey].Repositories, codeRepo)
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return
}

func (c *GitlabClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if gitlabRepo, ok := remoteRepo.(*gitlab.Project); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeGitlab,
			ID:                  strconv.Itoa(gitlabRepo.ID),
			Name:                gitlabRepo.Name,
			FullName:            gitlabRepo.PathWithNamespace,
			Description:         gitlabRepo.Description,
			HTMLURL:             gitlabRepo.WebURL,
			CloneURL:            gitlabRepo.HTTPURLToRepo,
			SSHURL:              gitlabRepo.SSHURLToRepo,
			Language:            "",
			Owner: devops.OwnerInRepository{
				ID:   strconv.Itoa(gitlabRepo.Namespace.ID),
				Name: gitlabRepo.Namespace.FullPath,
				Type: devops.GetOwnerType(gitlabRepo.Namespace.Kind),
			},

			CreatedAt: ConvertToMetaTime(gitlabRepo.CreatedAt),
			PushedAt:  nil,
			UpdatedAt: ConvertToMetaTime(gitlabRepo.LastActivityAt),

			Private:      c.isPrivate(gitlabRepo.Visibility),
			Size:         0,
			SizeHumanize: "0",
		}
	}

	return
}

func (c *GitlabClient) getRepoUrl(repoID string) string {
	return fmt.Sprintf("%sprojects/%s", c.getBaseUrl(), repoID)
}

func (c *GitlabClient) getHeader(lastModifyAt *metav1.Time) (header map[string]string) {
	header = c.ServiceClient.getHeader(lastModifyAt)
	if c.isBasicAuth() {
		header[GitlabPrivateTokenKey] = c.getPassword()
	}
	return
}

func (c *GitlabClient) getOwnerAvatarURL(project *gitlab.Project) (ownerAvatarURL string) {
	if project == nil || project.Owner == nil {
		return
	}

	return project.Owner.AvatarURL
}

func (c *GitlabClient) getOwnerWebURL(project *gitlab.Project) (ownerAvatarURL string) {
	if project == nil || project.Owner == nil {
		return
	}

	return project.Owner.WebsiteURL
}

func (c *GitlabClient) isPrivate(value gitlab.VisibilityValue) bool {
	return value != gitlab.PublicVisibility
}

func (c *GitlabClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.getHtml(), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GitlabClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/oauth/token", c.getHtml())
}

func (c *GitlabClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGitlab{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}
func (c *GitlabClient) getBaseUrl() string {
	return c.Client.BaseURL().String()
}

// endregion

// region Gitee
type GiteeClient struct {
	*ServiceClient
	Client *gitee.Client
}

func NewGiteeClient(serviceClient *ServiceClient) (*GiteeClient, error) {
	var (
		client = &gitee.Client{}
	)

	if serviceClient.isBasicAuth() {
		client = gitee.NewClient(nil, serviceClient.getPassword())
	} else {
		serviceClient.OAuth2Info.AccessTokenKey = k8s.GiteeAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.GiteeAccessTokenScope

		client = gitee.NewClient(nil, serviceClient.OAuth2Info.AccessToken)
	}
	err := client.SetBaseURL(serviceClient.Service.GetHost())
	if err != nil {
		return nil, err
	}
	return &GiteeClient{ServiceClient: serviceClient, Client: client}, nil
}

func (c *GiteeClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *GiteeClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser?%s=%s", c.getBaseUrl(), gitee.AccessToken, c.getAccessToken())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *GiteeClient) CheckRepositoryAvailable(repoID, repoName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getRepoUrl(repoName), c.getHeader(lastModifyAt))
}

func (c *GiteeClient) getAllRepos() (repositories []*gitee.Project, err error) {
	var (
		wg      = sync.WaitGroup{}
		perPage = 5000
	)

	glog.V(5).Infof("fetch data in page %d", 1)
	firstPageResult, response, err := c.Client.Projects.List("",
		&gitee.ListProjectsOptions{
			ListOptions: gitee.ListOptions{
				Page:    1,
				PerPage: perPage,
			},
		})
	if err != nil {
		glog.Errorf("error list project in gitee: %v", err)
		return
	}

	if firstPageResult != nil {
		repositories = append(repositories, firstPageResult...)
	}

	if response == nil || response.TotalPages < 2 {
		glog.V(5).Infof("No next page, return current result")
		return
	}

	for page := 2; page < response.TotalPages+1; page++ {
		glog.V(5).Infof("fetch data in page %d", page)
		wg.Add(1)
		go func(page, perPage int) {
			defer wg.Done()

			pagedResult, _, err := c.Client.Projects.List("",
				&gitee.ListProjectsOptions{
					ListOptions: gitee.ListOptions{
						Page:    page,
						PerPage: PerPage,
					},
				})
			if err != nil {
				return
			}
			if pagedResult != nil {
				repositories = append(repositories, pagedResult...)
			}
		}(page, PerPage)
	}
	wg.Wait()

	return
}

func (c *GiteeClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	result = &devops.CodeRepoBindingRepositories{
		Type:   devops.CodeRepoServiceTypeGitee,
		Owners: []devops.CodeRepositoryOwner{},
	}

	repositories, err := c.getAllRepos()
	if err != nil || repositories == nil {
		return
	}

	// List all organizations
	orgs, _, err := c.Client.Organizations.List("", nil)
	if err != nil {
		glog.Errorf("error list orgs in gitee: %v", err)
		return
	}
	dictOwners := make(map[string]*devops.CodeRepositoryOwner)
	if orgs != nil {
		for _, org := range orgs {
			// add the org owner to the dictOwners
			log.Printf("Append owner (kind: %s, name: %s, id: %d) to result",
				devops.OriginCodeRepoRoleTypeOrg, org.Login, org.ID)
			dictOwners[org.Login] = &devops.CodeRepositoryOwner{
				Type:         devops.OriginCodeRepoRoleTypeOrg,
				ID:           strconv.Itoa(org.ID),
				Name:         org.Login,
				Email:        "",
				HTMLURL:      org.URL,
				AvatarURL:    org.AvatarURL,
				DiskUsage:    0,
				Repositories: make([]devops.OriginCodeRepository, 0, 10),
			}
		}
	}

	for _, project := range repositories {
		repositoryOwnerKey := project.GetOwnerLogin()

		// add the personal owner to the dictOwners
		if owner, ok := dictOwners[repositoryOwnerKey]; !ok {
			log.Printf("Append owner (kind: %s, name: %s, id: %d) to result",
				project.Owner.Type, project.Owner.Login, project.Owner.ID)
			owner = &devops.CodeRepositoryOwner{
				Type:         devops.GetOwnerType(project.Owner.Type),
				ID:           strconv.Itoa(project.Owner.ID),
				Name:         project.Owner.Login,
				Email:        "",
				HTMLURL:      project.GetOwnerWebURL(),
				AvatarURL:    project.GetOwnerAvatarURL(),
				DiskUsage:    0,
				Repositories: make([]devops.OriginCodeRepository, 0, 10),
			}
			dictOwners[repositoryOwnerKey] = owner
		}

		// add the project to owner
		log.Printf("Append repo (name: %s, id: %d) to owner %s", project.Name, project.ID, dictOwners[repositoryOwnerKey].Name)
		codeRepo := c.ConvertRemoteRepoToBindingRepo(project)
		codeRepo.Owner = devops.OwnerInRepository{
			ID:   dictOwners[repositoryOwnerKey].ID,
			Name: dictOwners[repositoryOwnerKey].Name,
			Type: dictOwners[repositoryOwnerKey].Type,
		}
		dictOwners[repositoryOwnerKey].Repositories = append(dictOwners[repositoryOwnerKey].Repositories, codeRepo)
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return
}

func (c *GiteeClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if giteeRepo, ok := remoteRepo.(*gitee.Project); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeGitee,
			ID:                  strconv.Itoa(giteeRepo.ID),
			Name:                giteeRepo.Name,
			FullName:            giteeRepo.FullName,
			Description:         giteeRepo.Description,
			HTMLURL:             giteeRepo.HtmlURL,
			CloneURL:            giteeRepo.GetCloneURL(),
			SSHURL:              giteeRepo.GetSSHURL(),
			Language:            giteeRepo.Language,
			CreatedAt:           ConvertToMetaTime(giteeRepo.CreatedAt),
			PushedAt:            ConvertToMetaTime(giteeRepo.PushedAt),
			UpdatedAt:           ConvertToMetaTime(giteeRepo.UpdatedAt),

			Private:      giteeRepo.Private,
			Size:         0,
			SizeHumanize: "0",
		}
	}

	return
}

func (c *GiteeClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.getHtml(), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GiteeClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGitee{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *GiteeClient) getAccessToken() string {
	if c.isBasicAuth() {
		return c.getPassword()
	} else {
		return c.ServiceClient.getAccessToken()
	}
}

func (c *GiteeClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/oauth/token", c.getHtml())
}

func (c *GiteeClient) getRepoUrl(repoName string) string {
	return fmt.Sprintf("%srepos/%s?%s=%s", c.getBaseUrl(), repoName, gitee.AccessToken, c.getAccessToken())
}

func (c *GiteeClient) getBaseUrl() string {
	return c.Client.BaseURL().String()
}

// endregion

// region Bitbucket
type BitbucketClient struct {
	*ServiceClient
	Client *bitbucket.Client
}

func NewBitbucketClient(serviceClient *ServiceClient) (*BitbucketClient, error) {
	var (
		err    error
		client = &bitbucket.Client{}
	)

	if serviceClient.isBasicAuth() {
		client, err = bitbucket.NewBasicAuthClient(nil, "", serviceClient.getUsername(), serviceClient.getPassword())
	} else {
		serviceClient.OAuth2Info.AccessTokenKey = k8s.BitbucketAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.BitbucketAccessTokenScope

		client = bitbucket.NewClient(nil, serviceClient.OAuth2Info.AccessToken)
	}
	if err != nil {
		return nil, err
	}
	return &BitbucketClient{ServiceClient: serviceClient, Client: client}, nil
}

func (c *BitbucketClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *BitbucketClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser", c.getBaseUrl())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *BitbucketClient) CheckRepositoryAvailable(repoID, repoName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getRepoUrl(repoName), c.getHeader(lastModifyAt))
}

func (c *BitbucketClient) getAllRepos() (repositories []*bitbucket.Repository, err error) {
	var (
		perPage = 5000
		page    = 0
	)

	for {
		page = page + 1

		glog.V(5).Infof("fetch data in page %d", page)
		firstPageResult, _, err1 := c.Client.Repositories.List("",
			&bitbucket.ListRepositoriesOptions{Role: bitbucket.Role(bitbucket.RoleValueMember),
				ListOptions: bitbucket.ListOptions{
					Page:    page,
					PageLen: perPage,
				},
			})
		if err1 != nil {
			err = err1
			glog.Errorf("error list project in bitbucket: %v", err)
			return
		}

		if firstPageResult != nil && firstPageResult.Values != nil {
			repositories = append(repositories, firstPageResult.Values...)
		}

		if firstPageResult == nil || firstPageResult.Next == "" {
			glog.V(5).Infof("No next page, return current result")
			return
		}
	}

	return
}

func (c *BitbucketClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	result = &devops.CodeRepoBindingRepositories{
		Type:   devops.CodeRepoServiceTypeBitbucket,
		Owners: []devops.CodeRepositoryOwner{},
	}

	repositories, err := c.getAllRepos()
	if err != nil || repositories == nil {
		return
	}

	dictOwners := make(map[string]*devops.CodeRepositoryOwner)
	for _, repository := range repositories {
		repositoryOwnerKey := repository.GetOwnerLogin()

		if owner, ok := dictOwners[repositoryOwnerKey]; !ok {
			log.Printf("Append owner (kind: %s, name: %s, id: %s) to result",
				repository.Owner.Type, repository.GetOwnerLogin(), repository.Owner.UUID)
			owner = &devops.CodeRepositoryOwner{
				Type:         devops.GetOwnerType(repository.Owner.Type),
				ID:           repository.Owner.UUID,
				Name:         repository.GetOwnerLogin(),
				Email:        "",
				HTMLURL:      repository.GetOwnerWebURL(),
				AvatarURL:    repository.GetOwnerAvatarURL(),
				DiskUsage:    0,
				Repositories: make([]devops.OriginCodeRepository, 0, 10),
			}
			dictOwners[repositoryOwnerKey] = owner
		}
		log.Printf("Append repo (name: %s, id: %s) to owner %s", repository.Name, repository.UUID, dictOwners[repositoryOwnerKey].Name)

		codeRepo := c.ConvertRemoteRepoToBindingRepo(repository)
		dictOwners[repositoryOwnerKey].Repositories = append(dictOwners[repositoryOwnerKey].Repositories, codeRepo)
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return
}

func (c *BitbucketClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if bitbucketRepo, ok := remoteRepo.(*bitbucket.Repository); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeBitbucket,
			ID:                  bitbucketRepo.UUID,
			Name:                bitbucketRepo.Name,
			FullName:            bitbucketRepo.FullName,
			Description:         bitbucketRepo.Description,
			HTMLURL:             bitbucketRepo.Links.HTML.Href,
			CloneURL:            bitbucketRepo.GetCloneURL(),
			SSHURL:              bitbucketRepo.GetSSHURL(),
			Language:            bitbucketRepo.Language,
			Owner: devops.OwnerInRepository{
				ID:   bitbucketRepo.Owner.UUID,
				Name: bitbucketRepo.Owner.Username,
				Type: devops.GetOwnerType(bitbucketRepo.Owner.Type),
			},

			CreatedAt: ConvertToMetaTime(bitbucketRepo.CreatedOn),
			PushedAt:  nil,
			UpdatedAt: ConvertToMetaTime(bitbucketRepo.UpdatedOn),

			Private:      bitbucketRepo.IsPrivate,
			Size:         bitbucketRepo.GetSize() * 1000,
			SizeHumanize: humanize.Bytes(uint64(bitbucketRepo.GetSize())),
		}
	}

	return
}

func (c *BitbucketClient) getRepoUrl(repoName string) string {
	return fmt.Sprintf("%srepositories/%s", c.getBaseUrl(), repoName)
}

func (c *BitbucketClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/site/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.getHtml(), c.getClientID(), redirectUrl, c.getScope())
}

func (c *BitbucketClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/site/oauth2/access_token", c.getHtml())
}

func (c *BitbucketClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseBitbucket{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *BitbucketClient) getBaseUrl() string {
	return c.Client.BaseURL().String()
}

// endregion

func ConvertToMetaTime(metaTime interface{}) *metav1.Time {
	glog.V(7).Infof("metatime: %v; type: %s", metaTime, reflect.TypeOf(metaTime))
	if metaTime == nil {
		return nil
	}

	switch x := metaTime.(type) {
	case github.Timestamp:
		return &metav1.Time{Time: x.Time}
	case *time.Time:
		if x == nil {
			return nil
		}
		return &metav1.Time{Time: *x}
	}
	return nil
}

type AccessTokenResponse struct {
	TokenType   string `json:"token_type"` // is always 'bearer'
	AccessToken string `json:"access_token"`
}

func (a *AccessTokenResponse) GetAccessToken() string {
	return a.AccessToken
}

type AccessTokenResponseGithub struct {
	AccessTokenResponse
	Scope string `json:"scope"`
}

func (a *AccessTokenResponseGithub) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken: a.AccessToken,
		Scope:       a.Scope,
		CreatedAt:   generic.ConvertTimeToStr(time.Now()),
	}
}

type AccessTokenResponseGitlab struct {
	AccessTokenResponse
	Scope string `json:"scope"`

	RefreshToken string `json:"refresh_token"`
	CreatedAt    int64  `json:"created_at"`
}

func (a *AccessTokenResponseGitlab) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scope,
		RefreshToken: a.RefreshToken,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
		ExpiresIn:    "7200",
	}
}

type AccessTokenResponseGitee struct {
	AccessTokenResponse
	Scope string `json:"scope"`

	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"` // default value is 86400
	CreatedAt    int64  `json:"created_at"`

	IDToken string `json:"id_token,omitempty"` // 'gitee-private' has this field
}

func (a *AccessTokenResponseGitee) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scope,
		RefreshToken: a.RefreshToken,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
		ExpiresIn:    string(a.ExpiresIn),
	}
}

type AccessTokenResponseBitbucket struct {
	AccessTokenResponse
	Scopes string `json:"scopes"`

	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"` // default value is 7200
}

func (a *AccessTokenResponseBitbucket) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scopes,
		RefreshToken: a.RefreshToken,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
		ExpiresIn:    string(a.ExpiresIn),
	}
}
