package externalversions

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	"io/ioutil"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/url"
	"strings"
)

type authType int

const (
	basicAuth authType = iota
	oAuthToken
	privateToken
)

type ServiceClient struct {
	AuthType      authType
	Service       *devops.CodeRepoService
	Secret        *corev1.Secret
	BasicAuthInfo *k8s.SecretDataBasicAuth
	OAuth2Info    *k8s.SecretDataOAuth2
}

func NewServiceClient(service *devops.CodeRepoService, secret *corev1.Secret) *ServiceClient {
	client := ServiceClient{
		Service: service,
		Secret:  secret,
	}

	client.AuthType = basicAuth
	client.BasicAuthInfo = k8s.GetDataBasicAuthFromSecret(secret)
	if secret != nil && secret.Type == devops.SecretTypeOAuth2 {
		client.AuthType = oAuthToken
		client.OAuth2Info = k8s.GetDataOAuth2FromSecret(secret)
	}
	return &client
}

func (s *ServiceClient) isReadyToAccessToken() error {
	if s.Secret == nil {
		return fmt.Errorf("Secret is missed, can not access token")
	}

	if s.isBasicAuth() {
		return fmt.Errorf("Secret '%s' is baisc auth, can not access token", s.Secret.Name)
	}

	if s.getCode() == "" || s.getClientID() == "" || s.getClientSecret() == "" {
		return fmt.Errorf(
			"Secret '%s' can access token only if 'code' and 'clientID' and 'clientSecret' is not empty",
			s.Secret.Name)
	}

	return nil
}

func (s *ServiceClient) isBasicAuth() bool {
	return s.AuthType == basicAuth
}

func (s ServiceClient) getServiceType() devops.CodeRepoServiceType {
	return s.Service.Spec.Type
}

func (s *ServiceClient) getHost() string {
	return s.Service.GetHost()
}

func (s *ServiceClient) getHtml() string {
	return s.Service.GetHtml()
}

func (s *ServiceClient) getUsername() string {
	if s.BasicAuthInfo == nil {
		return ""
	}
	return s.BasicAuthInfo.Username
}

func (s *ServiceClient) getPassword() string {
	if s.BasicAuthInfo == nil {
		return ""
	}
	return s.BasicAuthInfo.Password
}

func (s *ServiceClient) getClientID() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.ClientID
}

func (s *ServiceClient) getClientSecret() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.ClientSecret
}

func (s *ServiceClient) getCode() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.Code
}

func (s *ServiceClient) getRefreshToken() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.RefreshToken
}

func (s *ServiceClient) getExpiresIn() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.ExpiresIn
}

func (s *ServiceClient) getAccessTokenKey() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.AccessTokenKey
}

func (s *ServiceClient) getAccessToken() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.AccessToken
}

func (s *ServiceClient) getScope() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.Scope
}

func (c *ServiceClient) getAuthorizePayload(redirectUrl, scope string) map[string]interface{} {
	return map[string]interface{}{
		"client_id":     c.getClientID(),
		"redirect_uri":  redirectUrl,
		"response_type": "code",
		"scope":         scope,
	}
}

func (c *ServiceClient) getAccessTokenPayload(redirectUrl string) map[string]interface{} {
	return map[string]interface{}{
		"grant_type":    "authorization_code",
		"code":          c.getCode(),
		"client_id":     c.getClientID(),
		"client_secret": c.getClientSecret(),
		"redirect_uri":  redirectUrl,
	}
}

func (c *ServiceClient) getRefreshTokenPayload() map[string]interface{} {
	return map[string]interface{}{
		"grant_type":    "refresh_token",
		"refresh_token": c.getRefreshToken(),
		"client_id":     c.getClientID(),
		"client_secret": c.getClientSecret(),
	}
}

func (c *ServiceClient) getJsonHeader() map[string]string {
	return map[string]string{
		generic.HeaderKeyAccept:      generic.HeaderValueContentTypeJson,
		generic.HeaderKeyContentType: generic.HeaderValueContentTypeJson,
	}
}

func (c *ServiceClient) getFormHeader() map[string]string {
	return map[string]string{generic.HeaderKeyContentType: generic.HeaderValueContentTypeForm}
}

func (c *ServiceClient) accessToken(response AccessTokenResponseInterface, redirectUrl, accessTokenUrl string) (*corev1.Secret, error) {
	var (
		secretCopy = c.Secret.DeepCopy()
		err        error
		payload    map[string]interface{}
	)

	err = c.isReadyToAccessToken()
	if err != nil {
		return nil, err
	}

	if c.OAuth2Info.HasAccessToken() {
		if c.OAuth2Info.IsExpired() {
			glog.V(5).Infof("The access_token will be expired in %s, refresh it", c.getExpiresIn())

			payload = c.getRefreshTokenPayload()
			err = c.getAccessTokenResponse(response, accessTokenUrl, payload)
		} else {
			glog.V(5).Infof("The access_token is not expired, continue to use it")
			return nil, nil
		}
	} else {
		glog.V(5).Infof("For the first time to access token")

		payload = c.getAccessTokenPayload(redirectUrl)
		err = c.getAccessTokenResponse(response, accessTokenUrl, payload)
	}

	if err != nil {
		return nil, err
	}

	if response != nil {
		c.OAuth2Info.SecretDataOAuth2Details = response.ConvertToSecretDataOAuth2Details()
		secretCopy.StringData = c.OAuth2Info.GetData()
	}

	return secretCopy, nil
}

func (c *ServiceClient) getHeader(lastModifyAt *metav1.Time) (header map[string]string) {
	header = make(map[string]string, 0)
	if lastModifyAt != nil {
		header[GithubIfModifiedSince] = lastModifyAt.String()
	}

	if c.isBasicAuth() {
		header["Authorization"] = "Basic " + generic.BasicAuth(c.getUsername(), c.getPassword())
	} else {
		header["Authorization"] = "Bearer " + c.getAccessToken()
	}
	return
}

func (c *ServiceClient) getAccessTokenResponse(response AccessTokenResponseInterface, accessTokenUrl string, payload map[string]interface{}) (err error) {
	var (
		req    *http.Request
		header map[string]string
	)

	if c.getServiceType() == devops.CodeRepoServiceTypeBitbucket {
		header = c.getFormHeader()
		if payload != nil {
			values := url.Values{}
			for key, value := range payload {
				if value == nil {
					continue
				}
				values.Add(key, value.(string))
			}
			req, err = http.NewRequest("POST", accessTokenUrl, strings.NewReader(values.Encode()))
		}

	} else {
		header = c.getJsonHeader()
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(payload)
		req, err = http.NewRequest("POST", accessTokenUrl, b)
	}

	glog.V(5).Infof("accessTokenUrl: %s; header: %s; payload: %s", accessTokenUrl, header, payload)
	if err != nil {
		glog.Errorf("error when new request")
		return
	}

	if header != nil {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	res, err := http.DefaultClient.Do(req)
	defer res.Body.Close()
	if err != nil {
		return
	}

	if res.StatusCode >= 400 {
		err = fmt.Errorf("Response status is %s when accessing token", res.Status)
		return
	}

	body, _ := ioutil.ReadAll(res.Body)
	err = json.Unmarshal(body, response)
	if err != nil {
		return
	}

	if response.GetAccessToken() == "" {
		err = fmt.Errorf("%s", body)
	}

	return
}
