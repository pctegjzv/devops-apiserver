package externalversions

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"

	"github.com/golang/glog"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"net/http"
	"time"
)

func CheckService(url string, header map[string]string) (status *devops.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = v1.NewTime(start)
	)
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	http.DefaultClient.Timeout = time.Second * 30
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	if header != nil && len(header) > 0 {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	glog.V(3).Infof("Check Service url: %s; req.Header: %v", url, req.Header)
	resp, err = http.DefaultClient.Do(req)
	if resp != nil {
		status.StatusCode = int(resp.StatusCode)
		data, err1 := ioutil.ReadAll(resp.Body)
		if data != nil {
			status.Response = string(data)
		}
		if err1 != nil {
			err = err1
			runtime.HandleError(err)
		}
	} else if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	}

	duration = time.Since(start)
	status.Delay = &duration
	return
}
