//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package gitee

import (
	"fmt"
)

// ProjectsService handles communication with the repositories related methods
// of the Gitee API.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type OrganizationsService struct {
	client *Client
}

// Project represents a Gitee project.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type Organization struct {
	ID          int    `json:"id"`
	Login       string `json:"login"`
	URL         string `json:"url"`
	AvatarURL   string `json:"avatar_url"`
	ReposURL    string `json:"repos_url"`
	EventsURL   string `json:"events_url"`
	MembersURL  string `json:"members_url"`
	Description string `json:"description"`
}

func (s Organization) String() string {
	return Stringify(s)
}

// ListProjectsOptions represents the available List() options.
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5UserOrgs
type ListOrganizationsOptions struct {
	ListOptions
}

// List gets a list of projects accessible by the authenticated user.
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5UserOrgs
func (s *OrganizationsService) List(user string, opt *ListProjectsOptions, options ...OptionFunc) ([]*Organization, *Response, error) {
	var u string
	if user != "" {
		u = fmt.Sprintf("users/%s/orgs", user)
	} else {
		u = "user/orgs"
	}
	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var p []*Organization
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}
