package main

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-gitee"
	"log"
)

func main() {
	OrganizationExample()
}

func ProjectExample() {
	git := gitee.NewClient(nil, "242dcc10e395470ae7c08a74cf7355c9")

	projects, _, err := git.Projects.List("", nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	for _, project := range projects {
		log.Printf("Found project: %s; owner: %s", project.FullName, project.Owner.Login)
	}

	projects, _, err = git.Projects.List("sniperyen1", nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	for _, project := range projects {
		log.Printf("Found user project: %s; owner: %s", project.FullName, project.Owner.Login)
	}

}

func OrganizationExample() {
	git := gitee.NewClient(nil, "242dcc10e395470ae7c08a74cf7355c9")

	orgs, _, err := git.Organizations.List("sniperyen1", nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	for _, org := range orgs {
		log.Printf("Found user org: %s; avatar_url: %s", org.Login, org.AvatarURL)
	}
}
