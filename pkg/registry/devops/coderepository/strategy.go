/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package coderepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	"fmt"
	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
	"time"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	codeRepository, ok := obj.(*devops.CodeRepository)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a CodeRepository")
	}
	return labels.Set(codeRepository.ObjectMeta.Labels), toSelectableFields(codeRepository), codeRepository.Initializers != nil, nil
}

// MatchCodeRepository is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchCodeRepository(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.CodeRepository) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy strategy for CodeRepository
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx genericapirequest.Context, obj runtime.Object) {
	newObj, ok := obj.(*devops.CodeRepository)
	if ok {
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate currently does not do anything specific
func (Strategy) PrepareForUpdate(ctx genericapirequest.Context, obj, old runtime.Object) {
	var (
		newObj, oldObj *devops.CodeRepository
	)
	newObj, _ = obj.(*devops.CodeRepository)
	if old != nil {
		oldObj, _ = old.(*devops.CodeRepository)
	}
	if isChanged(oldObj, newObj) {
		glog.Infof("codeRepository %s/%s was changed", newObj.GetNamespace(), newObj.GetName())
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
		newObj.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

func isChanged(old, new *devops.CodeRepository) bool {
	if old == nil {
		glog.Info("old is nil")
		return true
	}

	oldRepo, newRepo := old.Spec.Repository, new.Spec.Repository

	changed := old.Spec.CodeRepoBinding.Name != new.Spec.CodeRepoBinding.Name || !oldRepo.UpdatedAt.Equal(newRepo.UpdatedAt)
	if changed {
		glog.Infof("repo changed: %v; oldBindingName: %s; newBindingName: %s; oldUpdatedAt: %v; newUpdatedAt",
			changed, old.Spec.CodeRepoBinding.Name, new.Spec.CodeRepoBinding.Name, oldRepo.UpdatedAt, newRepo.UpdatedAt)
	}

	return changed
}

// Validate validate a request object
func (Strategy) Validate(ctx genericapirequest.Context, obj runtime.Object) (errs field.ErrorList) {
	codeRepository := obj.(*devops.CodeRepository)
	return validation.ValidateCodeRepository(codeRepository)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx genericapirequest.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = validation.ValidateCodeRepository(obj.(*devops.CodeRepository))
	return append(errs, validation.ValidateCodeRepositoryUpdate(obj.(*devops.CodeRepository), old.(*devops.CodeRepository))...)
}

// ResourceGetter is an interface for retrieving resources by ResourceLocation.
type ResourceGetter interface {
	Get(genericapirequest.Context, string, *metav1.GetOptions) (runtime.Object, error)
}
