/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package coderepobinding

import (
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"fmt"
	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	registrygeneric "k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
	"net/url"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"k8s.io/apiserver/pkg/endpoints/request"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.CodeRepoBinding)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a CodeRepoBinding")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// MatchCodeRepoBinding is the filter used by the registrygeneric etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchCodeRepoBinding(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.CodeRepoBinding) fields.Set {
	return registrygeneric.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy coderepobinding strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx request.Context, obj runtime.Object) {
	glog.V(5).Infoln("PrepareForCreate...")

	newObj, ok := obj.(*devops.CodeRepoBinding)
	if ok {
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate Verify if any of the following parts were changed and
// cleanup status if any change happened
// to make sure the controller will check the data again
func (Strategy) PrepareForUpdate(ctx request.Context, obj, old runtime.Object) {
	glog.V(5).Infoln("PrepareForUpdate...")

	var (
		newObj, oldObj *devops.CodeRepoBinding
	)
	newObj, _ = obj.(*devops.CodeRepoBinding)
	if old != nil {
		oldObj, _ = old.(*devops.CodeRepoBinding)
	}
	if isChanged(oldObj, newObj) {
		glog.Infof("codeRepoBinding %s/%s was changed", newObj.GetNamespace(), newObj.GetName())
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
		newObj.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

func isChanged(old, new *devops.CodeRepoBinding) bool {
	if old == nil {
		glog.Info("old is nil")
		return true
	}

	var changed, accountChange, ownerChanged bool
	accountChange = old.Spec.Account.Secret.Name != new.Spec.Account.Secret.Name ||
		old.Spec.Account.Secret.APITokenKey != new.Spec.Account.Secret.APITokenKey ||
		old.Spec.Account.Secret.UsernameKey != new.Spec.Account.Secret.UsernameKey

	oldOwners := old.Spec.Account.Owners
	newOwners := new.Spec.Account.Owners
	if len(oldOwners) != len(newOwners) {
		glog.Infof("the count of owners changed: len(oldOwners)=%d; len(newOwners)=%d", len(oldOwners), len(newOwners))
		ownerChanged = true
	} else {
		for _, oldOwner := range oldOwners {
			var foundOwner *devops.CodeRepositoryOwnerSync
			for _, newOwner := range newOwners {
				if oldOwner.Name == newOwner.Name && oldOwner.Type == newOwner.Type {
					foundOwner = &newOwner
					break
				}
			}

			if foundOwner == nil {
				glog.Infof("old owner (kind: %s, name: %s) was lost in new binding", oldOwner.Type, oldOwner.Name)
				ownerChanged = true
				break
			}

			if foundOwner.All != oldOwner.All || len(foundOwner.Repositories) != len(oldOwner.Repositories) {
				glog.Infof("old owner (kind: %s, name: %s) was changed in new binding", oldOwner.Type, oldOwner.Name)
				ownerChanged = true
				break
			}
		}
	}

	changed = accountChange || ownerChanged
	if changed {
		glog.Infof("accountChange: %v; ownerChanged: %v", accountChange, ownerChanged)
	}
	return changed
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx request.Context, obj, old runtime.Object) (errs field.ErrorList) {
	glog.V(5).Infoln("ValidateUpdate...")

	newObj, ok := obj.(*devops.CodeRepoBinding)
	if ok {
		return validation.ValidateCodeRepoBinding(newObj)
	}
	return
}

// Validate validates a request object
func (Strategy) Validate(ctx request.Context, obj runtime.Object) (errs field.ErrorList) {
	glog.V(5).Infoln("ValidateCreate...")

	newObj, ok := obj.(*devops.CodeRepoBinding)
	if ok {
		return validation.ValidateCodeRepoBinding(newObj)
	}
	return
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

func GetCodeRepoBinding(getter devopsgeneric.ResourceGetter, ctx request.Context, name string) (codeRepoBinding *devops.CodeRepoBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	codeRepoBinding = obj.(*devops.CodeRepoBinding)
	if codeRepoBinding == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}

func GetCodeRepoService(getter devopsgeneric.ResourceGetter, ctx request.Context, name string) (codeRepoService *devops.CodeRepoService, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	codeRepoService = obj.(*devops.CodeRepoService)
	if codeRepoService == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}

func BuildLogURL(service *devops.CodeRepoService, options *devops.CodeRepoBindingRepositoryOptions) (urlLoc *url.URL, err error) {
	scheme, host := generic.GetSchemaAndHost(service.Spec.HTTP.Host)
	params := url.Values{}

	urlLoc = &url.URL{
		Scheme:   scheme,
		Host:     host,
		Path:     "/",
		RawQuery: params.Encode(),
	}
	return
}
