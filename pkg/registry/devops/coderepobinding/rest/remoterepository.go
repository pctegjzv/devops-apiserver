package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	bindingregistry "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apiserver/pkg/registry/rest"

	corev1listers "k8s.io/client-go/listers/core/v1"
)

var (
	sessionStore map[string]*CodeRepoServiceSession
)

func GetSessionStore() map[string]*CodeRepoServiceSession {
	if sessionStore == nil {
		sessionStore = map[string]*CodeRepoServiceSession{}
	}
	return sessionStore
}

type CodeRepoServiceSession struct {
	LastModifiedAt              time.Time                           `json:"lastModifiedAt"`
	CodeRepoBindingRepositories *devops.CodeRepoBindingRepositories `json:"codeRepoBindingRepositories"`
}

func (s *CodeRepoServiceSession) IsExpired() bool {
	return s.LastModifiedAt.Add(devops.TTLSession).Before(time.Now())
}

// RepositoryREST implements the log endpoint for a Pod
type RepositoryREST struct {
	CodeRepoServiceStore *devopsregistry.REST
	CodeRepoBindingStore *devopsregistry.REST
	SecretLister         corev1listers.SecretLister
}

// NewRepositoryREST starts a new log store
func NewRepositoryREST(
	codeRepoServiceStore rest.StandardStorage,
	codeRepoBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
) rest.Storage {
	return &RepositoryREST{
		CodeRepoServiceStore: codeRepoServiceStore.(*devopsregistry.REST),
		CodeRepoBindingStore: codeRepoBindingStore.(*devopsregistry.REST),
		SecretLister:         secretLister,
	}
}

// RepositoryREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&RepositoryREST{})

// New creates a new Pod log options object
func (r *RepositoryREST) New() runtime.Object {
	return &devops.CodeRepoBindingRepositories{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoBindingRepositories as the generating object
func (r *RepositoryREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoBindingRepositories{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *RepositoryREST) Get(ctx genericapirequest.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		secretRef devops.SecretKeySetRef
		err       error
		namespace = genericapirequest.NamespaceValue(ctx)
	)

	binding, err := bindingregistry.GetCodeRepoBinding(r.CodeRepoBindingStore, ctx, name)
	if err != nil {
		return nil, fmt.Errorf("error get binding %s, err: %v", name, err)
	}

	service, err := bindingregistry.GetCodeRepoService(r.CodeRepoServiceStore, ctx, binding.Spec.CodeRepoService.Name)
	if err != nil {
		return nil, fmt.Errorf("error get service from binding %s, err: %v", name, err)
	}

	secretRef = binding.Spec.Account.Secret
	secret, err := r.SecretLister.Secrets(namespace).Get(secretRef.Name)
	if err != nil {
		return nil, fmt.Errorf("error get secret %s, err: %v", secretRef.Name, err)
	}

	var sessionKey string
	if secret.Type == v1alpha1.SecretTypeOAuth2 {
		sessionKey = fmt.Sprintf("%s-%s",
			service.Name,
			k8s.GetValueInSecret(secret, v1alpha1.OAuth2AccessTokenKey))
	} else {
		sessionKey = fmt.Sprintf("%s-%s-%s",
			service.Name,
			k8s.GetValueInSecret(secret, corev1.BasicAuthUsernameKey),
			k8s.GetValueInSecret(secret, corev1.BasicAuthPasswordKey))
	}
	ss := GetSessionStore()
	if session, ok := ss[sessionKey]; ok && !session.IsExpired() {
		glog.Infof("Get info from session(sessionKey: %s)", sessionKey)
		return session.CodeRepoBindingRepositories, nil
	}

	factory := internalthirdparty.NewCodeRepoServiceClientFactory()
	serviceClient, err := factory.GetCodeRepoServiceClient(service, secret)
	if err != nil {
		return nil, err
	}

	OriginCodeRepo, err := serviceClient.GetRemoteRepos()
	if err != nil {
		return OriginCodeRepo, err
	}

	ss[sessionKey] = &CodeRepoServiceSession{
		CodeRepoBindingRepositories: OriginCodeRepo,
		LastModifiedAt:              time.Now(),
	}
	return OriginCodeRepo, nil
}

// NewGetOptions creates a new options object
func (r *RepositoryREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoBindingRepositoryOptions{}, false, ""
}
