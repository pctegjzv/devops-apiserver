package pipelinetasktemplate_test

import (
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelinetasktemplate"
	genericregistry "k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage/storagebackend"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewRest(t *testing.T) {
	registry, err := pipelinetasktemplate.NewREST(apiserver.Scheme, genericregistry.RESTOptions{
		StorageConfig: &storagebackend.Config{
			ServerList: []string{"http://localhost:30002"},
		},
		Decorator:      genericregistry.UndecoratedStorage,
		ResourcePrefix: defaultEtcdPathPrefix,
	})
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"all", "devops", "templates"}
	if !reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"pipetaskt"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if cpt, ok := obj.(*devops.PipelineTaskTemplate); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.PipelineTaskTemplate", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cpt, ok := objList.(*devops.PipelineTaskTemplateList); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.PipelineTaskTemplateList", reflect.TypeOf(objList))
	}
	_, err = pipelinetasktemplate.NewREST(apiserver.Scheme, genericregistry.RESTOptions{})
	if err == nil {
		t.Errorf("Empty rest options did not return error")
	}
}
