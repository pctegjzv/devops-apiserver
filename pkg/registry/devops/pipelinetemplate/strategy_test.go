package pipelinetemplate_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplate"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStrategy(t *testing.T) {
	Schema := runtime.NewScheme()
	strategy := pipelinetemplate.NewStrategy(Schema)

	if !strategy.NamespaceScoped() {
		t.Errorf("PipelineTemplate strategy should be namespace scoped")
	}

	if strategy.AllowCreateOnUpdate() {
		t.Errorf("PipelineTemplate strategy should not allow create on update")
	}

	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("PipelineTemplate strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.PipelineTemplate
		old      *devopsv1alpha1.PipelineTemplate
		expected *devopsv1alpha1.PipelineTemplate
		method   func(*Table)
	}

	table := []Table{
		{
			name: "simple",
			new: &devopsv1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.PipelineTemplateSpec{
					Parameters: []devopsv1alpha1.PipelineParameter{
						devopsv1alpha1.PipelineParameter{
							Name: " ",
						},
					},
				},
			},
			old: nil,
			expected: &devopsv1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineTemplateSpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tb.name, err,
					)
				}
			},
		},
	}

	for _, tb := range table {
		tb.method(&tb)
	}
}
