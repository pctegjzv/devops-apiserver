package rest

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

// PreviewREST implements the preview endpoint
type PreviewREST struct {
	PipelineTemplateStore     rest.StandardStorage
	PipelineTaskTemplateStore rest.StandardStorage

	Context genericapirequest.Context
}

// NewPreviewREST create rest for preview pai
func NewPreviewREST(
	pipelineTemplateStore rest.StandardStorage,
	pipelineTaskTemplateStore rest.StandardStorage,
) rest.Storage {
	return &PreviewREST{
		PipelineTemplateStore:     pipelineTemplateStore,
		PipelineTaskTemplateStore: pipelineTaskTemplateStore,
	}
}

var _ = rest.NamedCreater(&PreviewREST{})

// New create a new JenkinsfilePreview option object
func (p *PreviewREST) New() runtime.Object {
	return &devops.JenkinsfilePreviewOptions{}
}

// Create genereate jenkinsfile from PipelineTemplate
func (p *PreviewREST) Create(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
	previewOpt, ok := obj.(*devops.JenkinsfilePreviewOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", previewOpt)
	}

	// kind := previewOpt.Kind
	p.Context = ctx

	// TODO here maybe is other kind in the future
	options := metav1.GetOptions{}
	options.Kind = "PipelineTemplate"
	template, err := p.PipelineTemplateStore.Get(ctx, name, &options)
	if err != nil {
		return nil, fmt.Errorf("can't find PipelineTemplate by name %s, error: %v", name, err)
	}

	pipelineTemplate, ok := template.(*devops.PipelineTemplate)
	if !ok {
		return nil, fmt.Errorf("invalid type, %v", template)
	}

	values := previewOpt.Values
	jenkinsfile, err := util.JenkinsfileRenderFromTemplate(pipelineTemplate, previewOpt.Source, values, p)
	if err != nil {
		return nil, fmt.Errorf("error happened in the process of genereate jenkinsfile, %v", err)
	}

	return &devops.JenkinsfilePreview{
		Jenkinsfile: jenkinsfile,
	}, err
}

// GetPipelineTaskTemplate get pipelineTaskTemplate according name
func (p *PreviewREST) GetPipelineTaskTemplate(name string) (taskTemplate *devops.PipelineTaskTemplate, err error) {
	obj, err := p.PipelineTaskTemplateStore.Get(p.Context, name, &metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	taskTemplate, ok := obj.(*devops.PipelineTaskTemplate)
	if !ok {
		err = fmt.Errorf("invalid type %#v, should be PipelineTaskTemplate", obj)
	}

	return
}
