package rest

import (
	"strings"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestPreview(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	pipelineTemplateStore := mockregistry.NewMockStandardStorage(ctrl)
	pipelineTaskTemplateStore := mockregistry.NewMockStandardStorage(ctrl)

	previewREST := &PreviewREST{
		PipelineTemplateStore:     pipelineTemplateStore,
		PipelineTaskTemplateStore: pipelineTaskTemplateStore,
	}

	type TestCase struct {
		Name                 string
		PipelineTemplate     func() (*devops.PipelineTemplate, error)
		PipelineTaskTemplate func() (*devops.PipelineTaskTemplate, error)
		Prepare              func() (ctx genericapirequest.Context, name string, opts runtime.Object)
		Run                  func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Expect               string
		Err                  error
	}

	table := []TestCase{
		{
			Name: "case 0: all ok simplest jenkinsfile",
			PipelineTemplate: func() (tempalte *devops.PipelineTemplate, err error) {
				tempalte = &devops.PipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Type: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
					},
				}
				return
			},
			PipelineTaskTemplate: func() (taskTemplate *devops.PipelineTaskTemplate, err error) {
				taskTemplate = &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '1'",
					},
				}
				taskTemplate.Name = "test"
				return
			},
			Prepare: func() (ctx genericapirequest.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, includeUninitialized)
			},
			Err:    nil,
			Expect: "echo '1'",
		},
		{
			Name: "case 1: test values",
			PipelineTemplate: func() (tempalte *devops.PipelineTemplate, err error) {
				tempalte = &devops.PipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Type: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
						Arguments: []devops.PipelineTemplateArgumentGroup{
							{
								DisplayName: devops.I18nName{
									Zh: "param",
									En: "param",
								},
								Items: []devops.PipelineTemplateArgumentValue{
									{
										PipelineTemplateArgument: devops.PipelineTemplateArgument{
											Name: "Param",
											Schema: devops.PipelineTaskArgumentSchema{
												Type: "string",
											},
											Display: devops.PipelineTaskArgumentDisplay{
												Type: "string",
												Name: devops.I18nName{
													Zh: "param",
													En: "param",
												},
											},
											Binding: []string{"test.args.Param"},
										},
									},
								},
							},
						},
					},
				}
				return
			},
			PipelineTaskTemplate: func() (taskTemplate *devops.PipelineTaskTemplate, err error) {
				taskTemplate = &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '{{.Param}}'",
						Arguments: []devops.PipelineTaskArgument{
							{
								Name: "Param",
								Schema: devops.PipelineTaskArgumentSchema{
									Type: "string",
								},
							},
						},
					},
				}
				taskTemplate.Name = "test"
				return
			},
			Prepare: func() (ctx genericapirequest.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{
					Values: map[string]string{
						"Param": "param",
					},
				}
				return
			},
			Run: func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, includeUninitialized)
			},
			Err:    nil,
			Expect: "echo 'param'",
		},
	}
	for i, test := range table {
		ctx, name, opts := test.Prepare()

		options := metav1.GetOptions{}
		options.Kind = "PipelineTemplate"
		pipelineTemplateStore.EXPECT().
			Get(ctx, name, &options).
			Return(test.PipelineTemplate())

		pipelineTaskTemplateStore.EXPECT().
			Get(ctx, name, &metav1.GetOptions{}).
			Return(test.PipelineTaskTemplate())

		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		preview, ok := result.(*devops.JenkinsfilePreview)
		if !ok || !strings.Contains(preview.Jenkinsfile, test.Expect) {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsfilePreview", i, test.Name)
		}
	}
}
