/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pipelinetemplate

import (
	"fmt"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	pipelineTemplate, ok := obj.(*devops.PipelineTemplate)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a PipelineTemplate")
	}
	return labels.Set(pipelineTemplate.ObjectMeta.Labels), toSelectableFields(pipelineTemplate), pipelineTemplate.Initializers != nil, nil
}

// MatchPipeline is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchPipeline(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.PipelineTemplate) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy strategy for PipelineTemplate
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx genericapirequest.Context, obj runtime.Object) {
	pipelineTemplate := obj.(*devops.PipelineTemplate)
	// pipeline.Status = devops.PipelineStatus{Phase: devops.PipelinePhasePending}
	prepareName(pipelineTemplate)
}

// PrepareForUpdate currently does not do anything specific
// all the updates come from the Jenkins Sync Controller
// TODO: Add Stop part
func (Strategy) PrepareForUpdate(ctx genericapirequest.Context, obj, old runtime.Object) {
}

// Validate valites a request object
func (Strategy) Validate(ctx genericapirequest.Context, obj runtime.Object) (errs field.ErrorList) {
	pipelineTemplate := obj.(*devops.PipelineTemplate)
	return validation.ValidatePipelineTemplate(pipelineTemplate)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx genericapirequest.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = validation.ValidatePipelineTemplate(obj.(*devops.PipelineTemplate))
	return append(errs, validation.ValidatePipelineTemplateUpdate(obj.(*devops.PipelineTemplate), old.(*devops.PipelineTemplate))...)
}

func prepareName(pipelineTemplate *devops.PipelineTemplate) {
	// if val, ok := pipelineTemplate.Annotations[devops.AnnotationsKeyPipelineNumber]; ok {
	// pipelineTemplate.Name = fmt.Sprintf("%s-%s", pipelineTemplate.Spec.pipelineTemplate.Name, val)
	// }
}

// ResourceGetter is an interface for retrieving resources by ResourceLocation.
// type ResourceGetter interface {
// 	Get(genericapirequest.Context, string, *metav1.GetOptions) (runtime.Object, error)
// }
