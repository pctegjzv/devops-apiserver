package project

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestProjectPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	project := &devops.Project{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "project-1",
			CreationTimestamp: now,
		},
		Spec: devops.ProjectSpec{
			Namespaces: []devops.ProjectNamespace{
				devops.ProjectNamespace{
					Name: "project-namespace",
					Type: "Owned",
				},
				devops.ProjectNamespace{
					Name: "project-1",
					Type: "",
				},
			},
		},
	}
	list := &devops.ProjectList{
		Items: []devops.Project{*project},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: project},
			Cells: []interface{}{
				"project-1", "project-namespace, project-1", registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printProjectList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing project: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
