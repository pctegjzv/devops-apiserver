package project

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1alpha1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: registry.ResourceNamespaces, Type: registry.TableTypeString, Description: "Namespaces"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printProjectList)
	h.TableHandler(columnDefinitions, printProject)

	registry.AddDefaultHandlers(h)

}

// PrintProjectList prints a project list
func printProjectList(projectList *devops.ProjectList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(projectList.Items))
	for i := range projectList.Items {
		r, err := printProject(&projectList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// PrintProject prints a project
func printProject(project *devops.Project, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: project},
	}

	// getting display name
	namespaces := "-"
	if len(project.Spec.Namespaces) > 0 {
		for i, n := range project.Spec.Namespaces {
			if i == 0 {
				namespaces = n.Name
			} else {
				namespaces = fmt.Sprintf("%s, %s", namespaces, n.Name)
			}

		}
	}

	// basic rows
	row.Cells = append(row.Cells, project.Name, namespaces, registry.TranslateTimestamp(project.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
