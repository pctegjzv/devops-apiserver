package imageregistrybinding

import (
	"fmt"
	"net/url"
	"time"

	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	k8sregistrygeneric "k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	devopsregistrygeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	devopsgeneric "alauda.io/devops-apiserver/pkg/util/generic"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.ImageRegistryBinding)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a ImageRepoBinding")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.ImageRegistryBinding) fields.Set {
	return k8sregistrygeneric.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// MatchImageRepoBinding is the filter used by the k8sregistrygeneric etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchImageRegistryBinding(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// Strategy imagerepobinding strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx genericapirequest.Context, obj runtime.Object) {
	imageRegistryBinding, ok := obj.(*devops.ImageRegistryBinding)
	if ok {
		imageRegistryBinding.Status.Phase = devops.ServiceStatusPhaseCreating
		imageRegistryBinding.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate Verify if any of the following parts were changed and
// cleanup status if any change happened
// to make sure the controller will check the data again
func (Strategy) PrepareForUpdate(ctx genericapirequest.Context, obj, old runtime.Object) {
	var (
		newImageRegistryBinding, oldImageRegistryBinding *devops.ImageRegistryBinding
	)
	newImageRegistryBinding, _ = obj.(*devops.ImageRegistryBinding)
	if old != nil {
		oldImageRegistryBinding, _ = old.(*devops.ImageRegistryBinding)
	}
	if oldImageRegistryBinding == nil ||
		(oldImageRegistryBinding != nil && checkBindingChange(oldImageRegistryBinding, newImageRegistryBinding)) {
		newImageRegistryBinding.Status.Phase = devops.ServiceStatusPhaseCreating
		glog.Infof("ImageRepoBinding %s/%s was changed", newImageRegistryBinding.GetNamespace(), newImageRegistryBinding.GetName())
		newImageRegistryBinding.Status.HTTPStatus = nil
		newImageRegistryBinding.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

func checkBindingChange(oldImageRegistryBinding, newImageRegistryBinding *devops.ImageRegistryBinding) bool {
	changed := oldImageRegistryBinding.Spec.Secret.Name != newImageRegistryBinding.Spec.Secret.Name ||
		oldImageRegistryBinding.Spec.Secret.APITokenKey != newImageRegistryBinding.Spec.Secret.APITokenKey ||
		oldImageRegistryBinding.Spec.Secret.UsernameKey != newImageRegistryBinding.Spec.Secret.UsernameKey

	var bindingChanged bool
	oldRepos := oldImageRegistryBinding.Spec.RepoInfo.Repositories
	newRepos := newImageRegistryBinding.Spec.RepoInfo.Repositories
	if len(oldRepos) != len(newRepos) {
		bindingChanged = true
	} else {
		// compare repos is same or not
		repoMap := make(map[string]int)
		for i, v := range oldRepos {
			repoMap[v] = 1
			repoMap[newRepos[i]] = 1
		}
		if len(repoMap) != len(oldRepos) {
			bindingChanged = true
		}
	}
	return changed || bindingChanged
}

// Validate validates a request object
func (Strategy) Validate(ctx genericapirequest.Context, obj runtime.Object) (errs field.ErrorList) {
	imageRegistryBinding := obj.(*devops.ImageRegistryBinding)
	return validation.ValidateImageRegistryBinding(imageRegistryBinding)
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx genericapirequest.Context, obj, old runtime.Object) (errs field.ErrorList) {
	imageRegistryBinding := obj.(*devops.ImageRegistryBinding)
	return validation.ValidateImageRegistryBinding(imageRegistryBinding)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

func GetImageRegistryBinding(getter devopsregistrygeneric.ResourceGetter, ctx genericapirequest.Context, name string) (imageRegistryBinding *devops.ImageRegistryBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	imageRegistryBinding = obj.(*devops.ImageRegistryBinding)
	if imageRegistryBinding == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}

func GetImageRegistry(getter devopsregistrygeneric.ResourceGetter, ctx genericapirequest.Context, name string) (imageRegistry *devops.ImageRegistry, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	imageRegistry = obj.(*devops.ImageRegistry)
	if imageRegistry == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}

func BuildLogURL(service *devops.ImageRegistry, options *devops.ImageRegistryBindingRepositoryOptions) (urlLoc *url.URL, err error) {
	// todo different service return different url
	scheme, host := devopsgeneric.GetSchemaAndHost(service.Spec.HTTP.Host)
	params := url.Values{}

	urlLoc = &url.URL{
		Scheme:   scheme,
		Host:     host,
		Path:     "/",
		RawQuery: params.Encode(),
	}
	return
}
