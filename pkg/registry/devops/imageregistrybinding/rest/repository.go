package rest

import (
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	corev1listers "k8s.io/client-go/listers/core/v1"
	"github.com/golang/glog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	bindingregistry "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/util/k8s"
)

const TTLSession = 3 * time.Minute

var (
	sessionStore map[string]*ImageRegistrySession
)

func GetSessionStore() map[string]*ImageRegistrySession {
	if sessionStore == nil {
		sessionStore = map[string]*ImageRegistrySession{}
	}
	return sessionStore
}

type ImageRegistrySession struct {
	LastModifiedAt       time.Time  `json:"lastModifiedAt"`
	ImageRegistryBindingRepositories *devops.ImageRegistryBindingRepositories `json:"imageRegistryBindingRepositories"`
}

func (s *ImageRegistrySession) IsExpired() bool {
	return s.LastModifiedAt.Add(TTLSession).Before(time.Now())
}

// RepositoryREST implements the binding repository endpoint for a pod
type RepositoryREST struct {
	ImageRegistryStore        *devopsregistry.REST
	ImageRegistryBindingStore *devopsregistry.REST
	SecretLister              corev1listers.SecretLister
}

// NewRepositoryREST starts a new repository store
func NewRepositoryREST(
	imageRegistryStore rest.StandardStorage,
	imageRegistryBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
) rest.Storage {
	return &RepositoryREST{
		ImageRegistryStore: imageRegistryStore.(*devopsregistry.REST),
		ImageRegistryBindingStore: imageRegistryBindingStore.(*devopsregistry.REST),
		SecretLister: secretLister,
	}
}

// New creates a new pod imageregistrybinding repository options object
func (r *RepositoryREST) New() runtime.Object {
	return &devops.ImageRegistryBindingRepositories{}
}

// ProducesObject binding RepositoryREST implements StorageMetadata, return string as the generating object
func (r *RepositoryREST) ProducesObject(verb string) interface {} {
	return ""
}

// Get retrieves a runtime.Object that will stream the contents of ImageRegistry
func (r *RepositoryREST) Get(ctx genericapirequest.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		secretRef                                devops.SecretKeySetRef
		serviceType                             devops.ImageRegistryType
		serviceName, endpoint, username, credential        string
		err error
		namespace                                = genericapirequest.NamespaceValue(ctx)
	)
	bindingOpts, ok := opts.(*devops.ImageRegistryBindingRepositoryOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	binding, err := bindingregistry.GetImageRegistryBinding(r.ImageRegistryBindingStore, ctx, name)
	if err != nil {
		return nil, fmt.Errorf("error get binding %s, err: %v", name, err)
	}
	secretRef = binding.Spec.Secret

	service, err := bindingregistry.GetImageRegistry(r.ImageRegistryStore, ctx, binding.Spec.ImageRegistry.Name)
	if err != nil {
		return nil, fmt.Errorf("error get service from binding %s, err: %v", name, err)
	}
	serviceName = service.Name
	serviceType = service.Spec.Type
	serviceUrl, err := bindingregistry.BuildLogURL(service, bindingOpts)
	if err != nil {
		return nil, fmt.Errorf("error get service url from service %s, err: %v", serviceName, err)
	}
	endpoint = serviceUrl.String()
	username, credential, err = k8s.GetUsernameAndPasswordFromSecret(r.SecretLister, secretRef.Name, secretRef.UsernameKey, secretRef.APITokenKey, namespace)
	if  err != nil {
		return nil, fmt.Errorf("error get user login info from secretRef: %s, err: %v", secretRef.Name, err)
	}

	// reduce frequently request
	sessionKey := fmt.Sprintf("%s-%s-%s", serviceName, serviceType, username)
	ss := GetSessionStore()
	if session, ok := ss[sessionKey]; ok && !session.IsExpired() {
		glog.V(3).Infof("Get info from session, key: %s", sessionKey)
		return session.ImageRegistryBindingRepositories, nil
	}

	factory := internalthirdparty.NewImageRegistryClientFactory()
	serviceClient := factory.GetImageRegistryClient(serviceType, endpoint, username, credential)
	repos, err := serviceClient.GetImageRepos()
	if err != nil {
		return repos, err
	}
	glog.Infof("Get remote repo list from registry: %s, result is: %s", service.Name, repos.Items)
	ss[sessionKey] = &ImageRegistrySession{
		ImageRegistryBindingRepositories: repos,
		LastModifiedAt:                   time.Now(),
	}
	return repos, nil
}

func (r *RepositoryREST) NewGetOptions() (runtime.Object, bool, string)  {
	return &devops.ImageRegistryBindingRepositoryOptions{}, false, ""
}