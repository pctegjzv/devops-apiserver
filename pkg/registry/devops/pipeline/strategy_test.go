package pipeline_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStategy(t *testing.T) {
	Scheme := runtime.NewScheme()
	strategy := pipeline.NewStrategy(Scheme)
	// metaTime := metav1.NewTime(time.Now())

	if !strategy.NamespaceScoped() {
		t.Errorf("Pipeline strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("Pipeline strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("Pipeline strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.Pipeline
		old      *devopsv1alpha1.Pipeline
		expected *devopsv1alpha1.Pipeline
		method   func(*Table)
	}

	table := []Table{
		{
			name: "create: settings status",
			new: &devopsv1alpha1.Pipeline{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineSpec{},
				// content is not important, it should be kept the same
				Status: devopsv1alpha1.PipelineStatus{},
			},
			old: nil,
			expected: &devopsv1alpha1.Pipeline{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineSpec{},
				// content is not important, it should be kept the same
				Status: devopsv1alpha1.PipelineStatus{
					Phase: devopsv1alpha1.PipelinePhasePending,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				err = strategy.ValidateUpdate(nil, tst.new, tst.old)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - ValidateUpdate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				_, _, _, getErr := pipeline.GetAttrs(tst.new)
				if getErr != nil {
					t.Errorf(
						"Test: \"%s\" - GetAttr did return err for pipeline object: %v",
						tst.name, getErr,
					)
				}
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}
