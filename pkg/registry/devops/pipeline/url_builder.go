package pipeline

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// URLBuilder URL Builder interface
type URLBuilder interface {
	GetLogURL(
		pipe *devops.Pipeline,
		jenkinsBinding *devops.JenkinsBinding,
		jenkins *devops.Jenkins,
		opts *devops.PipelineLogOptions,
	) (urlLoc *url.URL, err error)
	GetPipelineDependencies(
		getter devopsgeneric.ResourceGetter,
		jenkinsGetter devopsgeneric.ResourceGetter,
		jenkinsBindingGetter devopsgeneric.ResourceGetter,
		ctx genericapirequest.Context,
		name string,
	) (pipe *devops.Pipeline, jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, err error)
	GetTaskURL(
		pipe *devops.Pipeline,
		jenkinsBinding *devops.JenkinsBinding,
		jenkins *devops.Jenkins,
		opts *devops.PipelineTaskOptions,
	) (urlLoc *url.URL, err error)
	GetSecret(
		secretLister corev1listers.SecretLister,
		account devops.UserAccount,
		namespace string,
	) (*corev1.Secret, error)
}

// make sure the builder implements interface
var _ URLBuilder = URLBuilderImp{}

// URLBuilderImp builder implementation for URL builder
type URLBuilderImp struct {
}

// GetPipelineDependencies get pipeline dependencies/related resources
func (URLBuilderImp) GetPipelineDependencies(
	getter devopsgeneric.ResourceGetter,
	jenkinsGetter devopsgeneric.ResourceGetter,
	jenkinsBindingGetter devopsgeneric.ResourceGetter,
	ctx genericapirequest.Context,
	name string,
) (pipe *devops.Pipeline, jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, err error) {
	// fetching basic resources for this query
	pipe, err = getPipeline(getter, ctx, name)
	if err != nil {
		return
	}
	jenkinsBinding, err = getJenkinsBinding(jenkinsBindingGetter, ctx, pipe.Spec.JenkinsBinding.Name)
	if err != nil {
		return
	}
	jenkins, err = getJenkins(jenkinsGetter, ctx, jenkinsBinding.Spec.Jenkins.Name)
	return
}

// GetLogURL get location for pipeline logs
// will initiate all the necessary data to perform a log request
func (URLBuilderImp) GetLogURL(
	pipe *devops.Pipeline,
	jenkinsBinding *devops.JenkinsBinding,
	jenkins *devops.Jenkins,
	opts *devops.PipelineLogOptions) (urlLoc *url.URL, err error) {
	// build URL Location
	urlLoc, err = buildLogURL(pipe, jenkins, opts)
	return
}

// GetTaskURL return task URL
func (URLBuilderImp) GetTaskURL(
	pipe *devops.Pipeline,
	jenkinsBinding *devops.JenkinsBinding,
	jenkins *devops.Jenkins,
	opts *devops.PipelineTaskOptions,
) (urlLoc *url.URL, err error) {
	// build URL Location
	// urlLoc, err = buildLogURL(pipe, jenkins, opts)
	var path string
	namespace := pipe.GetNamespace()
	scheme, host := getJenkinsHostAndScheme(jenkins)
	if opts.Stage > 0 {
		path = fmt.Sprintf(
			// namespace - namespace - pipelineConfigName - build - stage
			"/blue/rest/organizations/jenkins/pipelines/%s/pipelines/%s-%s/runs/%s/nodes/%d/steps/",
			namespace,
			namespace, pipe.Spec.PipelineConfig.Name,
			pipe.Status.Jenkins.Build,
			opts.Stage,
		)
	} else {
		path = fmt.Sprintf(
			// namespace - namespace - pipelineConfigName - build - stage
			"/blue/rest/organizations/jenkins/pipelines/%s/pipelines/%s-%s/runs/%s/nodes/",
			namespace,
			namespace, pipe.Spec.PipelineConfig.Name,
			pipe.Status.Jenkins.Build,
		)
	}
	urlLoc = &url.URL{
		Host:     host,
		Path:     path,
		Scheme:   scheme,
		RawQuery: url.Values{}.Encode(),
	}
	return
}

// GetSecret fetches a secret from a lister
func (URLBuilderImp) GetSecret(
	secretLister corev1listers.SecretLister,
	account devops.UserAccount,
	namespace string,
) (secret *corev1.Secret, err error) {
	if account.Secret.Name == "" || account.Secret.UsernameKey == "" || account.Secret.APITokenKey == "" {
		return
	}
	return secretLister.Secrets(namespace).Get(account.Secret.Name)
}

func getPipeline(getter devopsgeneric.ResourceGetter, ctx genericapirequest.Context, name string) (pipe *devops.Pipeline, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	pipe = obj.(*devops.Pipeline)
	if pipe == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkinsBinding(getter devopsgeneric.ResourceGetter, ctx genericapirequest.Context, name string) (jenkBinding *devops.JenkinsBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	jenkBinding = obj.(*devops.JenkinsBinding)
	if jenkBinding == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkins(getter devopsgeneric.ResourceGetter, ctx genericapirequest.Context, name string) (jenkins *devops.Jenkins, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	jenkins = obj.(*devops.Jenkins)
	if jenkins == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func buildLogURL(
	pipeline *devops.Pipeline,
	jenkins *devops.Jenkins,
	opts *devops.PipelineLogOptions,
) (urlLoc *url.URL, err error) {
	// job/default-1/job/default-1-example/17/logText/progressiveText
	// job/default-1/job/default-1-example/17/
	var buildURI string
	scheme, jenkinsURI := getJenkinsHostAndScheme(jenkins)

	// removed fetching log address from annotations

	// will build url according to the parameters

	if buildURI == "" && pipeline.Status.Jenkins != nil {
		switch {
		case IsStageLog(opts) && IsStepLog(opts):
			// blue/rest/organizations/jenkins/pipelines
			// /namespace/pipelines/namespace - pipeline config name/runs/build number
			// /nodes/stage number/steps/step number/log
			buildURI = fmt.Sprintf(
				"/blue/rest/organizations/jenkins/pipelines"+
					"/%s/pipelines/%s-%s/runs/%s"+
					"/nodes/%d/steps/%d/log/",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
				opts.Stage,
				opts.Step,
			)
		case IsStageLog(opts):
			// blue/rest/organizations/jenkins/pipelines
			// /namespace/pipelines/namespace - pipeline config name/runs/build number
			// /nodes/stage number/log
			buildURI = fmt.Sprintf(
				"/blue/rest/organizations/jenkins/pipelines"+
					"/%s/pipelines/%s-%s/runs/%s"+
					"/nodes/%d/log/",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
				opts.Stage,
			)
		case IsStepLog(opts):
			// blue/rest/organizations/jenkins/pipelines
			// /namespace/pipelines/namespace - pipeline config name/runs/build number
			// /steps/step number/log
			buildURI = fmt.Sprintf(
				"/blue/rest/organizations/jenkins/pipelines"+
					"/%s/pipelines/%s-%s/runs/%s"+
					"/steps/%d/log/",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
				opts.Step,
			)
		default:
			// fallback
			// namespace - namespace - pipeline config name - build number
			buildURI = fmt.Sprintf(
				"/job/%s/job/%s-%s/%s/logText/progressiveText",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
			)
		}

	}
	// last check
	if buildURI == "" {
		err = fmt.Errorf("Log URL Not found for %#v", pipeline)
		return
	}

	// dealing with parameters
	params := url.Values{}
	params.Add("start", strconv.FormatInt(opts.Start, 10))

	urlLoc = &url.URL{
		Scheme:   scheme,
		Host:     jenkinsURI,
		Path:     buildURI,
		RawQuery: params.Encode(),
	}
	return
}

func getJenkinsHostAndScheme(jenkins *devops.Jenkins) (scheme, host string) {
	host = strings.TrimSuffix(jenkins.Spec.HTTP.Host, "/")

	schemeSplit := strings.Split(host, "//")
	if len(schemeSplit) > 0 {
		scheme = schemeSplit[0]
		scheme = strings.Replace(scheme, ":", "", -1)
	}
	if len(schemeSplit) > 1 {
		host = schemeSplit[1]
	}
	return
}

// IsStageLog returns true if the log options is for a stage
func IsStageLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Stage > 0
}

// IsStepLog returns true if the log options is for a step
func IsStepLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Step > 0
}
