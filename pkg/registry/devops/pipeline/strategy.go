/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pipeline

import (
	"fmt"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	pipeline, ok := obj.(*devops.Pipeline)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a Pipeline")
	}
	return labels.Set(pipeline.ObjectMeta.Labels), toSelectableFields(pipeline), pipeline.Initializers != nil, nil
}

// MatchPipeline is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchPipeline(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.Pipeline) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy strategy for Pipeline
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx genericapirequest.Context, obj runtime.Object) {
	pipeline := obj.(*devops.Pipeline)
	pipeline.Status = devops.PipelineStatus{Phase: devops.PipelinePhasePending}
	prepareName(pipeline)
}

// PrepareForUpdate currently does not do anything specific
// all the updates come from the Jenkins Sync Controller
// TODO: Add Stop part
func (Strategy) PrepareForUpdate(ctx genericapirequest.Context, obj, old runtime.Object) {
}

// Validate valites a request object
func (Strategy) Validate(ctx genericapirequest.Context, obj runtime.Object) (errs field.ErrorList) {
	pipeline := obj.(*devops.Pipeline)
	return validation.ValidatePipeline(pipeline)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx genericapirequest.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = validation.ValidatePipeline(obj.(*devops.Pipeline))
	return append(errs, validation.ValidatePipelineUpdate(obj.(*devops.Pipeline), old.(*devops.Pipeline))...)
}

func prepareName(pipeline *devops.Pipeline) {
	if val, ok := pipeline.Annotations[devops.AnnotationsKeyPipelineNumber]; ok {
		pipeline.Name = fmt.Sprintf("%s-%s", pipeline.Spec.PipelineConfig.Name, val)
	}
}

/*

// GetPipelineDependencies get pipeline dependencies/related resources
func GetPipelineDependencies(
	getter devopsgeneric.ResourceGetter,
	jenkinsGetter devopsgeneric.ResourceGetter,
	jenkinsBindingGetter devopsgeneric.ResourceGetter,
	ctx genericapirequest.Context,
	name string,
) (pipe *devops.Pipeline, jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, err error) {
	// fetching basic resources for this query
	pipe, err = getPipeline(getter, ctx, name)
	if err != nil {
		return
	}
	jenkinsBinding, err = getJenkinsBinding(jenkinsBindingGetter, ctx, pipe.Spec.JenkinsBinding.Name)
	if err != nil {
		return
	}
	jenkins, err = getJenkins(jenkinsGetter, ctx, jenkinsBinding.Spec.Jenkins.Name)
	return
}

// LogLocation get location for pipeline logs
// will initiate all the necessary data to perform a log request
func LogLocation(
	pipe *devops.Pipeline,
	jenkinsBinding *devops.JenkinsBinding,
	jenkins *devops.Jenkins,
	opts *devops.PipelineLogOptions) (urlLoc *url.URL, userAccount devops.UserAccount, err error) {
	userAccount = jenkinsBinding.Spec.Account
	// build URL Location
	urlLoc, err = buildLogURL(pipe, jenkins, opts)
	if err != nil {
		return
	}
	glog.V(5).Infof("urlLoc %v", urlLoc)

	return
}

func getPipeline(getter devopsgeneric.ResourceGetter, ctx genericapirequest.Context, name string) (pipe *devops.Pipeline, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	pipe = obj.(*devops.Pipeline)
	if pipe == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkinsBinding(getter devopsgeneric.ResourceGetter, ctx genericapirequest.Context, name string) (jenkBinding *devops.JenkinsBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	jenkBinding = obj.(*devops.JenkinsBinding)
	if jenkBinding == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkins(getter devopsgeneric.ResourceGetter, ctx genericapirequest.Context, name string) (jenkins *devops.Jenkins, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return
	}
	jenkins = obj.(*devops.Jenkins)
	if jenkins == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

var (
	httpRegex = regexp.MustCompile("^(http(s)?:\\/\\/|null)")
)

func buildLogURL(
	pipeline *devops.Pipeline,
	jenkins *devops.Jenkins,
	opts *devops.PipelineLogOptions,
) (urlLoc *url.URL, err error) {
	// job/default-1/job/default-1-example/17/logText/progressiveText
	// job/default-1/job/default-1-example/17/
	var (
		buildURI, scheme string
		jenkinsURI       = strings.TrimSuffix(jenkins.Spec.HTTP.Host, "/")
	)

	schemeSplit := strings.Split(jenkinsURI, "//")
	if len(schemeSplit) > 0 {
		scheme = schemeSplit[0]
		scheme = strings.Replace(scheme, ":", "", -1)
	}
	if len(schemeSplit) > 1 {
		jenkinsURI = schemeSplit[1]
	}

	// removed fetching log address from annotations

	// will build url according to the parameters

	if buildURI == "" && pipeline.Status.Jenkins != nil {
		switch {
		case IsStageLog(opts) && IsStepLog(opts):
			// blue/rest/organizations/jenkins/pipelines
			// /namespace/pipelines/namespace - pipeline config name/runs/build number
			// /nodes/stage number/steps/step number/log
			buildURI = fmt.Sprintf(
				"/blue/rest/organizations/jenkins/pipelines"+
					"/%s/pipelines/%s-%s/runs/%s"+
					"/nodes/%d/steps/%d/log/",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
				opts.Stage,
				opts.Step,
			)
		case IsStageLog(opts):
			// blue/rest/organizations/jenkins/pipelines
			// /namespace/pipelines/namespace - pipeline config name/runs/build number
			// /nodes/stage number/log
			buildURI = fmt.Sprintf(
				"/blue/rest/organizations/jenkins/pipelines"+
					"/%s/pipelines/%s-%s/runs/%s"+
					"/nodes/%d/log/",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
				opts.Stage,
			)
		case IsStepLog(opts):
			// blue/rest/organizations/jenkins/pipelines
			// /namespace/pipelines/namespace - pipeline config name/runs/build number
			// /steps/step number/log
			buildURI = fmt.Sprintf(
				"/blue/rest/organizations/jenkins/pipelines"+
					"/%s/pipelines/%s-%s/runs/%s"+
					"/steps/%d/log/",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
				opts.Step,
			)
		default:
			// fallback
			// namespace - namespace - pipeline config name - build number
			buildURI = fmt.Sprintf(
				"/job/%s/job/%s-%s/%s/logText/progressiveText",
				pipeline.GetNamespace(),
				pipeline.GetNamespace(),
				pipeline.Spec.PipelineConfig.Name,
				pipeline.Status.Jenkins.Build,
			)
		}

	}
	// last check
	if buildURI == "" {
		err = fmt.Errorf("Log URL Not found for %#v", pipeline)
		return
	}

	// dealing with parameters
	params := url.Values{}
	params.Add("start", strconv.FormatInt(opts.Start, 10))

	urlLoc = &url.URL{
		Scheme:   scheme,
		Host:     jenkinsURI,
		Path:     buildURI,
		RawQuery: params.Encode(),
	}
	return
}

// IsStageLog returns true if the log options is for a stage
func IsStageLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Stage > 0
}

// IsStepLog returns true if the log options is for a step
func IsStepLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Step > 0
}
*/
