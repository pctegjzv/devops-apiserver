package rest

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/golang/glog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	pipelineregistry "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"

	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// LogREST implements the log endpoint for a Pod
type LogREST struct {
	Store               rest.StandardStorage
	JenkinsStore        rest.StandardStorage
	JenkinsBindingStore rest.StandardStorage
	SecretLister        corev1listers.SecretLister
	RoundTripper        http.RoundTripper
	Builder             pipelineregistry.URLBuilder
}

// NewLogREST starts a new log store
func NewLogREST(
	pipelineStore rest.StandardStorage,
	jenkinsStore rest.StandardStorage,
	jenkinsBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
	builder pipelineregistry.URLBuilder,
) rest.Storage {
	return &LogREST{
		Store:               pipelineStore,
		JenkinsStore:        jenkinsStore,
		JenkinsBindingStore: jenkinsBindingStore,
		SecretLister:        secretLister,
		RoundTripper:        roundTripper,
		Builder:             builder,
	}
}

// LogREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&LogREST{})

// New creates a new PipelineLogOptions object
func (r *LogREST) New() runtime.Object {
	return &devops.Pipeline{}
}

// ProducesMIMETypes LogREST implements StorageMetadata
func (r *LogREST) ProducesMIMETypes(verb string) []string {
	// the "produces" section for pipelines/{name}/log
	return []string{
		"application/json",
	}
}

// ProducesObject LogREST implements StorageMetadata, return PipelineLog as the generating object
func (r *LogREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.PipelineLog{}
}

// Get retrieves a runtime.Object that will stream the contents of the pod log
func (r *LogREST) Get(ctx genericapirequest.Context, name string, opts runtime.Object) (runtime.Object, error) {
	interOpts, ok := opts.(*devops.PipelineLogOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}
	newOffsetInt := int64(0)
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(3).Infof(
		"got log options: start: %d stage: %d step: %d namespace: %v",
		interOpts.Start, interOpts.Stage, interOpts.Step, namespace,
	)

	// fetch pipeline dependencies
	pipe, jenkinsBinding, jenkins, err := r.Builder.GetPipelineDependencies(
		r.Store,
		r.JenkinsStore,
		r.JenkinsBindingStore,
		ctx,
		name,
	)
	if err != nil {
		glog.Errorf("Error fetching pipeline dependencies %s/%s: %v", namespace, name, err)
		return nil, err
	}

	// build pipeline log URL
	urlLoc, err := r.Builder.GetLogURL(
		pipe, jenkinsBinding, jenkins, interOpts,
	)
	glog.V(3).Infof("got pipeline log url: %v", urlLoc)
	if err != nil || urlLoc == nil {
		if err != nil {
			glog.Errorf("Error fetching pipeline log url for %s/%s: %v", namespace, name, err)
		}
		glog.V(2).Infof("Log url is not present for this pipeline: %s/%s. Maybe it didn't start yet?. Returning empty PipelineLog", namespace, name)
		return &v1alpha1.PipelineLog{
			HasMore:   true,
			NextStart: &newOffsetInt,
		}, nil
	}

	// fetch secret now
	var (
		userAccount              = jenkinsBinding.Spec.Account
		username, apiToken, text string
		request                  *http.Request
		response                 *http.Response
	)
	request, err = http.NewRequest(http.MethodGet, urlLoc.RequestURI(), nil)
	if err != nil {
		glog.Errorf("error generating request: %v", err)
		return nil, err
	}
	request.URL = urlLoc
	secret, err := r.Builder.GetSecret(r.SecretLister, userAccount, namespace)
	if err != nil {
		glog.Errorf("error fetching secret: %v", err)
		return nil, err
	}
	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[userAccount.Secret.UsernameKey]))
		apiToken = strings.TrimSpace(string(secret.Data[userAccount.Secret.APITokenKey]))
		request.SetBasicAuth(username, apiToken)
	}
	// blue ocean API needs this header
	// otherwise it will return a 500 error
	request.Header.Add("Accept", "*/*")
	response, err = r.RoundTripper.RoundTrip(request)
	if err != nil {
		glog.Errorf("error getting response: %v", err)
		return nil, err
	}
	glog.V(5).Infof("Got response: %v err %v", response, err)
	more := "false"
	newOffset := "0"

	if response.Header != nil {
		more = response.Header.Get("X-More-Data")
		newOffset = response.Header.Get("X-Text-Size")
		newOffsetInt, _ = strconv.ParseInt(newOffset, 10, 64)
	}
	textByte, err := ioutil.ReadAll(response.Body)
	if err != nil {
		glog.Errorf("Error reading reponse body: %v", err)
	}
	if textByte != nil {
		text = string(textByte)
	}
	switch response.StatusCode {
	case http.StatusOK, http.StatusAccepted, http.StatusNoContent:
		pipelineLog := &devops.PipelineLog{
			HasMore:   strings.ToLower(more) == "true",
			Text:      text,
			NextStart: &newOffsetInt,
		}
		return pipelineLog, nil
	}
	return nil, fmt.Errorf("Unexpected response: %s", text)

}

// NewGetOptions creates a new options object
func (r *LogREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.PipelineLogOptions{}, false, ""
}
