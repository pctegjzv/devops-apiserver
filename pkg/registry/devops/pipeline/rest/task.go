package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	pipelineregistry "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/runtime"

	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"

	corev1listers "k8s.io/client-go/listers/core/v1"
)

// TaskREST implements the log endpoint for a Pod
type TaskREST struct {
	Store               rest.StandardStorage
	JenkinsStore        rest.StandardStorage
	JenkinsBindingStore rest.StandardStorage
	SecretLister        corev1listers.SecretLister
	RoundTripper        http.RoundTripper
	Builder             pipelineregistry.URLBuilder
}

// NewTaskREST starts a new log store
func NewTaskREST(
	pipelineStore rest.StandardStorage,
	jenkinsStore rest.StandardStorage,
	jenkinsBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
	builder pipelineregistry.URLBuilder,
) rest.Storage {
	return &TaskREST{
		Store:               pipelineStore,
		JenkinsStore:        jenkinsStore,
		JenkinsBindingStore: jenkinsBindingStore,
		SecretLister:        secretLister,
		RoundTripper:        roundTripper,
		Builder:             builder,
	}
}

// TaskREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&TaskREST{})

// New creates a new PipelineLogOptions object
func (r *TaskREST) New() runtime.Object {
	return &devops.Pipeline{}
}

// ProducesMIMETypes LogREST implements StorageMetadata
func (r *TaskREST) ProducesMIMETypes(verb string) []string {
	// the "produces" section for pipelines/{name}/log
	return []string{
		"application/json",
	}
}

// ProducesObject implements StorageMetadata, return PipelineLog as the generating object
func (r *TaskREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.PipelineTask{}
}

// Get retrieves a runtime.Object that will stream the contents of the pod log
func (r *TaskREST) Get(ctx genericapirequest.Context, name string, opts runtime.Object) (runtime.Object, error) {
	pipelineTaskOpts, ok := opts.(*devops.PipelineTaskOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(3).Infof(
		"got pipeline task options: stage: %d namespace: %v",
		pipelineTaskOpts.Stage,
		namespace,
	)
	// fetch pipeline dependencies
	pipe, jenkinsBinding, jenkins, err := r.Builder.GetPipelineDependencies(
		r.Store,
		r.JenkinsStore,
		r.JenkinsBindingStore,
		ctx,
		name,
	)
	if err != nil {
		glog.Errorf("Error fetching pipeline dependencies %s/%s: %v", namespace, name, err)
		return nil, err
	}
	if pipe.Status.Jenkins == nil || pipe.Status.Jenkins.Build == "" {
		glog.V(3).Infof("Pipeline has no build ID %s/%s: %v", namespace, name, pipe.Status)
		return &devops.PipelineTask{
			Tasks: []devops.PipelineBlueOceanTask{},
		}, nil
	}

	// building url
	url, err := r.Builder.GetTaskURL(pipe, jenkinsBinding, jenkins, pipelineTaskOpts)
	if err != nil {
		glog.Errorf("Error fetching pipeline task url for %s/%s: %v", namespace, name, err)
		return nil, err
	}
	glog.V(3).Infof("Got url for pipeline stages %s/%s: %s", namespace, name, url.RequestURI())

	// generating request
	request, err := http.NewRequest(http.MethodGet, url.RequestURI(), nil)
	if err != nil {
		glog.Errorf("error generating request %s/%s: %v", namespace, name, err)
		return nil, err
	}
	request.URL = url

	userAccount := jenkinsBinding.Spec.Account
	secret, err := r.Builder.GetSecret(r.SecretLister, userAccount, namespace)
	if err != nil {
		glog.Errorf("error fetching secret %s/%s: %v", namespace, name, err)
		return nil, err
	}
	if secret != nil && secret.Data != nil {
		username := strings.TrimSpace(string(secret.Data[userAccount.Secret.UsernameKey]))
		apiToken := strings.TrimSpace(string(secret.Data[userAccount.Secret.APITokenKey]))
		request.SetBasicAuth(username, apiToken)
	}

	// executing request
	response, err := r.RoundTripper.RoundTrip(request)
	if err != nil {
		glog.Errorf("error executing request %s/%s: %v", namespace, name, err)
		return nil, err
	}
	textByte, err := ioutil.ReadAll(response.Body)
	if err != nil {
		glog.Errorf("Error reading reponse body %s/%s: %v", namespace, name, err)
		return nil, err
	}
	var pipelineTask *devops.PipelineTask
	switch response.StatusCode {
	case http.StatusOK, http.StatusAccepted:

		tasks := []devops.PipelineBlueOceanTask{}
		err = json.Unmarshal(textByte, &tasks)
		if err != nil {
			glog.Errorf("Error Unmarshal jenkins response err %s/%s: err: %v response: %s",
				namespace, name, err, textByte,
			)
			return nil, err
		}
		pipelineTask = &devops.PipelineTask{
			Tasks: tasks,
		}
		err = nil
	default:
		err = fmt.Errorf("Unexpected response: %s", textByte)
		pipelineTask = nil
	}
	return pipelineTask, err

}

// NewGetOptions creates a new options object
func (r *TaskREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.PipelineTaskOptions{}, false, ""
}
