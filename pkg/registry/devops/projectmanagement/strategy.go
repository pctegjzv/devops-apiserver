/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package projectmanagement

import (
	"fmt"
	"time"

	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	"k8s.io/apiserver/pkg/endpoints/request"
)

// NewStrategy creates a new Strategy instance for projectManage
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// Strategy projectManage strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns false
func (Strategy) NamespaceScoped() bool {
	return false
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// PrepareForCreate prepares an object for create request
func (Strategy) PrepareForCreate(ctx request.Context, new runtime.Object) {
	newObj, ok := new.(*devops.ProjectManagement)
	if ok {
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx request.Context, new, old runtime.Object) {
	var (
		newObj, oldObj *devops.ProjectManagement
	)
	newObj, _ = new.(*devops.ProjectManagement)
	if old != nil {
		oldObj, _ = old.(*devops.ProjectManagement)
	}

	if isChanged(oldObj, newObj) {
		glog.Infof("projectManage %s was changed", newObj.GetName())
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
		newObj.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

// Validate validates a request object
func (Strategy) Validate(ctx request.Context, obj runtime.Object) (errs field.ErrorList) {
	newObj, ok := obj.(*devops.ProjectManagement)
	if ok {
		return validation.ValidateProjectManage(newObj)
	}
	return
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx request.Context, new, old runtime.Object) (errs field.ErrorList) {
	newObj, ok := new.(*devops.ProjectManagement)
	oldObj, ok1 := old.(*devops.ProjectManagement)
	if ok && ok1 {
		return validation.ValidateProjectManageUpdate(newObj, oldObj)
	}
	return
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
	// Often implemented as a type check or empty method
}

func isChanged(old, new *devops.ProjectManagement) bool {
	if old == nil {
		glog.Info("old is nil")
		return true
	}

	changed := old.Spec.HTTP.Host != new.Spec.HTTP.Host
	return changed
}

// MatchProjectManage is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchProjectManage(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	projectManage, ok := obj.(*devops.ProjectManagement)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a ProjectManagement")
	}
	return labels.Set(projectManage.ObjectMeta.Labels), toSelectableFields(projectManage), projectManage.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.ProjectManagement) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, false)
}
