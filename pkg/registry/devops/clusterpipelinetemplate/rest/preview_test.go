package rest

import (
	"strings"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestPreview(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	pipelineTemplateStore := mockregistry.NewMockStandardStorage(ctrl)
	pipelineTaskTemplateStore := mockregistry.NewMockStandardStorage(ctrl)

	previewREST := &PreviewREST{
		ClusterPipelineTemplateStore:     pipelineTemplateStore,
		ClusterPipelineTaskTemplateStore: pipelineTaskTemplateStore,
	}

	type TestCase struct {
		Name                        string
		ClusterPipelineTemplate     func() (*devops.ClusterPipelineTemplate, error)
		ClusterPipelineTaskTemplate func() (*devops.ClusterPipelineTaskTemplate, error)
		Prepare                     func() (ctx genericapirequest.Context, name string, opts runtime.Object)
		Run                         func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Expect                      string
		Err                         error
	}

	table := []TestCase{
		{
			Name: "case 0: all ok simplest jenkinsfile",
			ClusterPipelineTemplate: func() (tempalte *devops.ClusterPipelineTemplate, err error) {
				tempalte = &devops.ClusterPipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Type: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
					},
				}
				return
			},
			ClusterPipelineTaskTemplate: func() (taskTemplate *devops.ClusterPipelineTaskTemplate, err error) {
				taskTemplate = &devops.ClusterPipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '1'",
					},
				}
				taskTemplate.Name = "test"
				return
			},
			Prepare: func() (ctx genericapirequest.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, includeUninitialized)
			},
			Err:    nil,
			Expect: "echo '1'",
		},
	}
	for i, test := range table {
		ctx, name, opts := test.Prepare()

		options := metav1.GetOptions{}
		options.Kind = "ClusterPipelineTemplate"
		pipelineTemplateStore.EXPECT().
			Get(ctx, name, &options).
			Return(test.ClusterPipelineTemplate())

		pipelineTaskTemplateStore.EXPECT().
			Get(ctx, name, &metav1.GetOptions{}).
			Return(test.ClusterPipelineTaskTemplate())

		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		preview, ok := result.(*devops.JenkinsfilePreview)
		if ok {
			strings.Contains(preview.Jenkinsfile, test.Expect)
		} else {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsfilePreview", i, test.Name)
		}
	}
}
