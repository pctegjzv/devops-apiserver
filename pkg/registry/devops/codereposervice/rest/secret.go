package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	bindingregistry "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"fmt"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	"k8s.io/apiserver/pkg/registry/rest"

	corev1 "k8s.io/api/core/v1"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	CodeRepoServiceStore *devopsregistry.REST
	CodeRepoBindingStore *devopsregistry.REST
	SecretLister         corev1listers.SecretLister
	ConfigMapLister      corev1listers.ConfigMapLister
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	codeRepoServiceStore rest.StandardStorage,
	codeRepoBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
) rest.Storage {
	return &SecretREST{
		CodeRepoServiceStore: codeRepoServiceStore.(*devopsregistry.REST),
		CodeRepoBindingStore: codeRepoBindingStore.(*devopsregistry.REST),
		SecretLister:         secretLister,
		ConfigMapLister:      configMapLister,
	}
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeResponse{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *SecretREST) Get(ctx genericapirequest.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		validateOptions  = &devops.CodeRepoServiceAuthorizeOptions{}
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.CodeRepoService
		status           *devops.HostPortStatus
		err              error
	)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	service, err = bindingregistry.GetCodeRepoService(r.CodeRepoServiceStore, ctx, name)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("codeRepoService '%s' is not found in k8s", validateOptions.SecretName)
		}
		return nil, fmt.Errorf("error getting service '%s', err: %v", name, err)
	}

	secret, err = r.SecretLister.Secrets(validateOptions.Namespace).Get(validateOptions.SecretName)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("secret '%s' is not found in k8s", validateOptions.SecretName)
		}
		return nil, fmt.Errorf("error getting secret '%s', err: %v", validateOptions.SecretName, err)
	}

	// secret is not allowed to be used by two service
	if secret.Type == devops.SecretTypeOAuth2 {
		glog.V(3).Infof("check whether secret %s is used by another service", secret.GetName())
		serviceName, refered := r.IsSecretUsedByAnotherService(secret, name)
		if refered && len(secret.GetOwnerReferences()) > 1 {
			return nil, fmt.Errorf("secret has been used by service '%s', please select another secret", serviceName)
		}
	}

	factory := internalthirdparty.NewCodeRepoServiceClientFactory()
	serviceClient, err := factory.GetCodeRepoServiceClient(service, secret)
	if err != nil {
		return nil, err
	}

	status, err = serviceClient.CheckAuthentication(nil)
	glog.V(3).Infof("status code: %d; secret type: %s", status.StatusCode, secret.Type)
	if status.StatusCode >= 400 {
		// return authorize url if secret's type is oauth2
		if secret.Type == devops.SecretTypeOAuth2 {
			redirectUrl := k8s.GetOAuthRedirectUrl(r.ConfigMapLister)
			authorizeUrl := serviceClient.GetAuthorizeUrl(redirectUrl)
			glog.V(3).Infof("authorizeUrl is %s", authorizeUrl)
			validateResponse.AuthorizeUrl = authorizeUrl
		} else {
			err = fmt.Errorf("username or password in secret '%s' is incorrect", secret.GetName())
		}
	}
	return validateResponse, err
}

func (r *SecretREST) IsSecretUsedByAnotherService(secret *corev1.Secret, name string) (string, bool) {
	serviceOwnerReferences := k8s.GetOwnerReferences(secret.ObjectMeta, devops.TypeCodeRepoService)
	if len(serviceOwnerReferences) > 0 {
		for _, ref := range serviceOwnerReferences {
			if ref.Name != name {
				return ref.Name, true
			}
		}
	}
	return "", false
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
