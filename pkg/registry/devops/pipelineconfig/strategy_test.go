package pipelineconfig_test

import (
	"testing"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStategyPrepare(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := pipelineconfig.NewStrategy(Scheme)
	metaTime := metav1.NewTime(time.Now())

	if !strategy.NamespaceScoped() {
		t.Errorf("Pipeline strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("Pipeline strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("Pipeline strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.PipelineConfig
		old      *devopsv1alpha1.PipelineConfig
		expected *devopsv1alpha1.PipelineConfig
		method   func(*Table)
	}

	table := []Table{
		{
			name: "create: settings status",
			new: &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status:     devopsv1alpha1.PipelineConfigStatus{
				// content is not important, it should be kept the same
				},
			},
			old: nil,
			expected: &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					// content is not important, it should be kept the same
					Phase: devopsv1alpha1.PipelineConfigPhaseCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				_, _, _, getErr := pipelineconfig.GetAttrs(tst.new)
				if getErr != nil {
					t.Errorf(
						"Test: \"%s\" - GetAttr did return err for pipelineconfig object: %v",
						tst.name, getErr,
					)
				}
			},
		},
		{
			name: "update: setting status for syncing",
			new: &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase:      devopsv1alpha1.PipelineConfigPhaseReady,
					Message:    "sss",
					Reason:     "xxx",
					LastUpdate: &metaTime,
					Conditions: []devopsv1alpha1.Condition{
						devopsv1alpha1.Condition{
							Type:   "jenkins",
							Status: string(devopsv1alpha1.PipelineConfigPhaseReady),
						},
					},
				},
			},
			old: nil,
			expected: &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase:      devopsv1alpha1.PipelineConfigPhaseReady,
					Message:    "sss",
					Reason:     "xxx",
					LastUpdate: &metaTime,
					Conditions: []devopsv1alpha1.Condition{
						devopsv1alpha1.Condition{
							Type:   "jenkins",
							Status: string(devopsv1alpha1.PipelineConfigPhaseReady),
						},
					},
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForUpdate(nil, tst.new, tst.old)

				err := strategy.ValidateUpdate(nil, tst.new, tst.old)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - ValidateUpdate invalid input did not generate error: %v",
						tst.name, err,
					)
				}
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
		if tst.new.Status.Message != tst.expected.Status.Message {
			t.Errorf(
				"Test %d: \"%v\" - status.message are different: %v != %v",
				i, tst.name,
				tst.new.Status.Message, tst.expected.Status.Message,
			)
		}
		if tst.new.Status.Reason != tst.expected.Status.Reason {
			t.Errorf(
				"Test %d: \"%v\" - status.message are different: %v != %v",
				i, tst.name,
				tst.new.Status.Message, tst.expected.Status.Message,
			)
		}
		if len(tst.new.Status.Conditions) != len(tst.expected.Status.Conditions) {
			t.Errorf(
				"Test %d: \"%v\" - conditions len are different: %v != %v",
				i, tst.name,
				tst.new.Status.Conditions, tst.expected.Status.Conditions,
			)
		}
	}

}
