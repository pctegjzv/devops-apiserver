package rest

import (
	"strings"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestPreview(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	pipelineConfigStore := mockregistry.NewMockStandardStorage(ctrl)

	previewREST := &PreviewREST{
		PipelineConfigStore: pipelineConfigStore,
	}

	type TestCase struct {
		Name           string
		PipelineConfig func() (*devops.PipelineConfig, error)
		Prepare        func() (ctx genericapirequest.Context, name string, opts runtime.Object)
		Run            func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Expect         string
		Err            error
	}

	table := []TestCase{
		{
			Name: "case 0, all ok simplest jenkinsfile",
			PipelineConfig: func() (pipelineConfig *devops.PipelineConfig, err error) {
				task := devops.PipelineTemplateTaskInstance{
					Spec: devops.PipelineTemplateTaskInstanceSpec{
						Type: "PipelineTask",
						Body: "echo '1'",
					},
				}
				task.Name = "test"

				pipelineConfig = &devops.PipelineConfig{
					Spec: devops.PipelineConfigSpec{
						Strategy: devops.PipelineStrategy{
							Template: &devops.PipelineConfigTemplate{
								Spec: devops.PipelineConfigTemplateSpec{
									Stages: []devops.PipelineStageInstance{
										{
											Name:  "test",
											Tasks: []devops.PipelineTemplateTaskInstance{task},
										},
									},
								},
							},
						},
					},
				}
				return
			},
			Prepare: func() (ctx genericapirequest.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, includeUninitialized)
			},
			Expect: "echo '1'",
			Err:    nil,
		},
	}

	for i, test := range table {
		ctx, name, opts := test.Prepare()

		pipelineConfigStore.EXPECT().
			Get(ctx, name, &metav1.GetOptions{}).
			Return(test.PipelineConfig())

		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		preview, ok := result.(*devops.JenkinsfilePreview)
		if ok {
			strings.Contains(preview.Jenkinsfile, test.Expect)
		} else {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsfilePreview", i, test.Name)
		}
	}
}
