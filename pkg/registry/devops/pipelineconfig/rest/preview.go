package rest

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

// PreviewREST implements the preview endpoint
type PreviewREST struct {
	PipelineConfigStore rest.StandardStorage
}

// NewPreviewREST create rest for preview api
func NewPreviewREST(pipelineConfigStore rest.StandardStorage) rest.Storage {
	return &PreviewREST{
		PipelineConfigStore: pipelineConfigStore,
	}
}

// PreviewREST implements NamedCreater
var _ = rest.NamedCreater(&PreviewREST{})

// New create a new JenkinsfilePreview option object
func (r *PreviewREST) New() runtime.Object {
	return &devops.JenkinsfilePreviewOptions{}
}

// Create genereate jenkinsfile from PipelineTemplate
func (r *PreviewREST) Create(ctx genericapirequest.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
	previewOptions, ok := obj.(*devops.JenkinsfilePreviewOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", previewOptions)
	}

	obj, err := r.PipelineConfigStore.Get(ctx, name, &metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("can't find PipelineConfig name[%s], error[%v]", name, err)
	}

	pipelineConfig, ok := obj.(*devops.PipelineConfig)
	if !ok {
		return nil, fmt.Errorf("unknow type [%#v]", obj)
	}

	jenkinsfile, err := util.JenkinsfileRender(&pipelineConfig.Spec)
	return &devops.JenkinsfilePreview{
		Jenkinsfile: jenkinsfile,
	}, err
}
