package jenkinsbinding_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/jenkinsbinding"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStategyValidate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := jenkinsbinding.NewStrategy(Scheme)

	type Table struct {
		name     string
		input    *devopsv1alpha1.JenkinsBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	table := []Table{
		{
			name: "create: empty jenkins and account data",
			input: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.JenkinsBindingSpec{},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("jenkins").Child("name"),
					"",
					"please provide a jenkins instance name",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("name"),
					"",
					"please provide a secret information with Jenkins' username and API Token",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("usernameKey"),
					"",
					"please provide the secret's key for Jenkins' username",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("apiTokenKey"),
					"",
					"please provide the secret's key for Jenkins' API Token",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "create: normal jenkins",
			input: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "secret",
							APITokenKey: "apikey",
							UsernameKey: "usernameKey",
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "update: empty jenkins and account data",
			input: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.JenkinsBindingSpec{},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("jenkins").Child("name"),
					"",
					"please provide a jenkins instance name",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("name"),
					"",
					"please provide a secret information with Jenkins' username and API Token",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("usernameKey"),
					"",
					"please provide the secret's key for Jenkins' username",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("apiTokenKey"),
					"",
					"please provide the secret's key for Jenkins' API Token",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.ValidateUpdate(nil, obj, nil)
			},
		},
		{
			name: "update: empty jenkins and account data",
			input: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "secret",
							APITokenKey: "apikey",
							UsernameKey: "usernameKey",
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.ValidateUpdate(nil, obj, nil)
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: %v != %v",
				i, tst.name,
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						tst.expected, res,
					)
				}
			}
		}
	}

}

func TestStategyPrepareForUpdate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := jenkinsbinding.NewStrategy(Scheme)

	type Table struct {
		name     string
		new      *devopsv1alpha1.JenkinsBinding
		old      *devopsv1alpha1.JenkinsBinding
		expected *devopsv1alpha1.JenkinsBinding
	}

	table := []Table{
		{
			name: "no old jenkinsbinding",
			new: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status: devopsv1alpha1.StatusReady,
					HTTPStatus: &devopsv1alpha1.HostPortStatus{
						StatusCode: 100,
					},
				},
			},
			old: nil,
			expected: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status:     devopsv1alpha1.StatusCreating,
					HTTPStatus: nil,
				},
			},
		},
		{
			name: "no change on the data",
			new: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "abc",
							UsernameKey: "user",
							APITokenKey: "api",
						},
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status: devopsv1alpha1.StatusReady,
					HTTPStatus: &devopsv1alpha1.HostPortStatus{
						StatusCode: 100,
					},
				},
			},
			old: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "abc",
							UsernameKey: "user",
							APITokenKey: "api",
						},
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status: devopsv1alpha1.StatusReady,
					HTTPStatus: &devopsv1alpha1.HostPortStatus{
						StatusCode: 100,
					},
				},
			},
			expected: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "abc",
							UsernameKey: "user",
							APITokenKey: "api",
						},
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status: devopsv1alpha1.StatusReady,
					HTTPStatus: &devopsv1alpha1.HostPortStatus{
						StatusCode: 100,
					},
				},
			},
		},
		{
			name: "changed jenkins instance",
			new: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "new-jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "abc",
							UsernameKey: "user",
							APITokenKey: "api",
						},
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status: devopsv1alpha1.StatusReady,
					HTTPStatus: &devopsv1alpha1.HostPortStatus{
						StatusCode: 100,
					},
					Conditions: []devopsv1alpha1.JenkinsBindingCondition{
						// add one to make sure it will be cleared out
						devopsv1alpha1.JenkinsBindingCondition{},
					},
				},
			},
			old: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "abc",
							UsernameKey: "user",
							APITokenKey: "api",
						},
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status: devopsv1alpha1.StatusReady,
					HTTPStatus: &devopsv1alpha1.HostPortStatus{
						StatusCode: 100,
					},
				},
			},
			expected: &devopsv1alpha1.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.JenkinsBindingSpec{
					Jenkins: devopsv1alpha1.JenkinsInstance{
						Name: "new-jenkins",
					},
					Account: devopsv1alpha1.UserAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							Name:        "abc",
							UsernameKey: "user",
							APITokenKey: "api",
						},
					},
				},
				Status: devopsv1alpha1.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					Status:     devopsv1alpha1.StatusCreating,
					HTTPStatus: nil,
					Conditions: nil,
				},
			},
		},
	}

	for i, tst := range table {
		strategy.PrepareForUpdate(nil, tst.new, tst.old)

		if tst.new.Status.Status != tst.expected.Status.Status {
			t.Errorf(
				"Test %d: \"%v\" - status.status are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
		if (tst.new.Status.HTTPStatus == nil && tst.expected.Status.HTTPStatus != nil) ||
			(tst.new.Status.HTTPStatus != nil && tst.expected.Status.HTTPStatus == nil) {
			t.Errorf(
				"Test %d: \"%v\" - http status are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
		if len(tst.new.Status.Conditions) != len(tst.expected.Status.Conditions) {
			t.Errorf(
				"Test %d: \"%v\" - conditions len are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}
