/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package jenkinsbinding

import (
	"fmt"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.JenkinsBinding)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a JenkinsBinding")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// MatchJenkinsBinding is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchJenkinsBinding(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.JenkinsBinding) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy jenkinsbinfing strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx genericapirequest.Context, obj runtime.Object) {
	jenkins, ok := obj.(*devops.JenkinsBinding)
	if ok {
		jenkins.Status.Status = devops.StatusCreating
		jenkins.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate Verify if any of the following parts were changed and
// cleanup status if any change happened
// to make sure the controller will check the data again
func (Strategy) PrepareForUpdate(ctx genericapirequest.Context, obj, old runtime.Object) {
	var (
		newJenkins, oldJenkins *devops.JenkinsBinding
	)
	newJenkins, _ = obj.(*devops.JenkinsBinding)
	if old != nil {
		oldJenkins, _ = old.(*devops.JenkinsBinding)
	}
	if oldJenkins == nil ||
		(oldJenkins != nil &&
			(oldJenkins.Spec.Jenkins.Name != newJenkins.Spec.Jenkins.Name ||
				oldJenkins.Spec.Account.Secret.Name != newJenkins.Spec.Account.Secret.Name ||
				oldJenkins.Spec.Account.Secret.APITokenKey != newJenkins.Spec.Account.Secret.APITokenKey ||
				oldJenkins.Spec.Account.Secret.UsernameKey != newJenkins.Spec.Account.Secret.UsernameKey)) {
		newJenkins.Status.Conditions = nil
		newJenkins.Status.Status = devops.StatusCreating
		newJenkins.Status.HTTPStatus = nil
	}
}

// Validate validates a request object
func (Strategy) Validate(ctx genericapirequest.Context, obj runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	bind, ok := obj.(*devops.JenkinsBinding)
	if ok {
		errs = append(errs, validateJenkinsBinding(bind)...)
	}
	return
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx genericapirequest.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	bind, ok := obj.(*devops.JenkinsBinding)
	if ok {
		errs = append(errs, validateJenkinsBinding(bind)...)
	}
	return
}

func validateJenkinsBinding(jenkins *devops.JenkinsBinding) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if jenkins.Spec.Jenkins.Name == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("jenkins").Child("name"),
			jenkins.Spec.Jenkins.Name,
			"please provide a jenkins instance name",
		))
	}
	if jenkins.Spec.Account.Secret.Name == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("account").Child("secret").Child("name"),
			jenkins.Spec.Account.Secret.Name,
			"please provide a secret information with Jenkins' username and API Token",
		))
	}
	if jenkins.Spec.Account.Secret.UsernameKey == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("account").Child("secret").Child("usernameKey"),
			jenkins.Spec.Account.Secret.UsernameKey,
			"please provide the secret's key for Jenkins' username",
		))
	}
	if jenkins.Spec.Account.Secret.APITokenKey == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("account").Child("secret").Child("apiTokenKey"),
			jenkins.Spec.Account.Secret.APITokenKey,
			"please provide the secret's key for Jenkins' API Token",
		))
	}
	return
}
