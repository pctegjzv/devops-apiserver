package cmd

import (
	"flag"
	"io"
	"time"

	"github.com/golang/glog"
	"github.com/spf13/cobra"

	clientsetversioned "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	externalinformer "alauda.io/devops-apiserver/pkg/client/informers/externalversions"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"

	"alauda.io/devops-apiserver/pkg/controller"
	coderepoctrl "alauda.io/devops-apiserver/pkg/controller/devops/coderepo"
	imagectrl "alauda.io/devops-apiserver/pkg/controller/devops/image"
	jenkinsctrl "alauda.io/devops-apiserver/pkg/controller/devops/jenkins"
	pipelinectrl "alauda.io/devops-apiserver/pkg/controller/devops/pipeline"
	pipelinetemplatesyncctrl "alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplatesync"
	projectctrl "alauda.io/devops-apiserver/pkg/controller/devops/project"
	rolesctrl "alauda.io/devops-apiserver/pkg/controller/devops/roles"
	secretctrl "alauda.io/devops-apiserver/pkg/controller/devops/secret"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func init() {
	// Add root as parent
	command.RootCmd.AddCommand(ctrlCmd)

	o = NewControllerOptions(command.StdOut, command.StdErr)
	ctrlCmd.Flags().AddGoFlagSet(flag.CommandLine)
	flags := ctrlCmd.Flags()
	flags.StringVarP(&o.MasterURL, "master", "", "", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	flags.StringVarP(&o.Kubeconfig, "kubeconfig", "", "", "Path to a kubeconfig. Only required if out-of-cluster.")
	flags.DurationVarP(&o.Timeout, "timeout", "t", time.Second*30, "Timeout duration while waiting for listers to fetch cache")
}

// ControllerOptions options for the controller
type ControllerOptions struct {
	Kubeconfig string
	MasterURL  string
	Timeout    time.Duration

	StdOut io.Writer
	StdErr io.Writer
}

var (
	o       *ControllerOptions
	ctrlCmd = &cobra.Command{
		Use:   "controller",
		Short: "Launch a devops controller",
		Long:  "Launch a devops controller",
		RunE: func(c *cobra.Command, args []string) error {
			glog.V(7).Infof("%s", c.Short)

			if err := o.Validate(args); err != nil {
				return err
			}
			return o.Run(2, command.StopCh)
		},
	}
)

// NewControllerOptions constructor function for the controller options
func NewControllerOptions(out, errOut io.Writer) *ControllerOptions {
	o := &ControllerOptions{
		StdOut: out,
		StdErr: errOut,
	}

	return o
}

// Validate will validate arguments and configuration
func (o ControllerOptions) Validate(args []string) error {
	errors := []error{}
	return utilerrors.NewAggregate(errors)
}

// Run will start the controller
func (o ControllerOptions) Run(threadiness int, stopCh <-chan struct{}) (err error) {
	var (
		clientConfig *restclient.Config
		k8sClient    *kubernetes.Clientset
		devopsClient *clientsetversioned.Clientset

		kubeInformers   kubeinformers.SharedInformerFactory
		devopsInformers externalinformer.SharedInformerFactory
	)

	if clientConfig, err = clientcmd.BuildConfigFromFlags(o.MasterURL, o.Kubeconfig); err != nil {
		glog.Fatalf("Error building config: %s", err.Error())
		return
	}
	if k8sClient, err = kubernetes.NewForConfig(clientConfig); err != nil {
		glog.Fatalf("Error building kubernetes client: %s", err.Error())
		return
	}
	if devopsClient, err = clientsetversioned.NewForConfig(clientConfig); err != nil {
		glog.Fatalf("Error building devops client: %s", err.Error())
		return
	}
	kubeInformers = kubeinformers.NewSharedInformerFactory(k8sClient, time.Second*30)
	devopsInformers = externalinformer.NewSharedInformerFactory(devopsClient, time.Second*30)

	codeRepoServiceClientFactory := externalthirdparty.NewCodeRepoServiceClientFactory()
	imageRegistryClientFactory := externalthirdparty.NewImageRegistryClientFactory()
	thirdParty := externalthirdparty.NewThirdParty(
		codeRepoServiceClientFactory,
		imageRegistryClientFactory,
	)

	// TODO: Change this
	ctrl := controller.NewController(
		k8sClient, devopsClient, kubeInformers, devopsInformers, thirdParty, o.Timeout,
	)

	// subcontrollers
	ctrl.AddSubcontroller(projectctrl.New())
	ctrl.AddSubcontroller(jenkinsctrl.New())
	ctrl.AddSubcontroller(pipelinectrl.New())
	ctrl.AddSubcontroller(pipelinetemplatesyncctrl.New())
	ctrl.AddSubcontroller(coderepoctrl.New())
	ctrl.AddSubcontroller(imagectrl.New())
	ctrl.AddSubcontroller(secretctrl.New())
	ctrl.AddSubcontroller(rolesctrl.New())

	return ctrl.Run(threadiness, stopCh)
}
