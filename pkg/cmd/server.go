/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"flag"
	"fmt"
	"io"
	"net"

	"github.com/golang/glog"
	"github.com/spf13/cobra"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/admission/plugin/banproject"
	"alauda.io/devops-apiserver/pkg/admission/plugin/verifydependency"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/apiserver"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
	genericapiserver "k8s.io/apiserver/pkg/server"
	genericoptions "k8s.io/apiserver/pkg/server/options"
)

func init() {
	// Add root as parent
	command.RootCmd.AddCommand(serverCmd)

	// needs to do this on init because otherwise cobra will
	// fail saying that the added flags are not valid
	serverOpts = NewDevopsServerOptions(command.StdOut, command.StdErr)
	serverCmd.Flags().AddGoFlagSet(flag.CommandLine)
	flags := serverCmd.Flags()
	serverOpts.RecommendedOptions.AddFlags(flags)
	serverOpts.Admission.AddFlags(flags)
}

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

// DevopsServerOptions options for the Server
type DevopsServerOptions struct {
	RecommendedOptions *genericoptions.RecommendedOptions
	Admission          *genericoptions.AdmissionOptions

	SharedInformerFactory informers.SharedInformerFactory
	StdOut                io.Writer
	StdErr                io.Writer
}

// NewDevopsServerOptions constructor for the Server
func NewDevopsServerOptions(out, errOut io.Writer) *DevopsServerOptions {
	o := &DevopsServerOptions{
		RecommendedOptions: genericoptions.NewRecommendedOptions(defaultEtcdPathPrefix, apiserver.Codecs.LegacyCodec(v1alpha1.SchemeGroupVersion)),
		Admission:          genericoptions.NewAdmissionOptions(),

		StdOut: out,
		StdErr: errOut,
	}
	//
	o.Admission.RecommendedPluginOrder = []string{
		banproject.PluginName, verifydependency.PluginName,
	}

	return o
}

var (
	serverOpts *DevopsServerOptions
	serverCmd  = &cobra.Command{
		Use:   "server",
		Short: "Launch a devops apiserver",
		Long:  "Launch a devops apiserver",
		RunE: func(c *cobra.Command, args []string) error {
			glog.V(7).Infof("%s", c.Short)

			if err := serverOpts.Complete(); err != nil {
				return err
			}
			if err := serverOpts.Validate(args); err != nil {
				return err
			}
			return serverOpts.RunDevopsServer(command.StopCh)
		},
	}
)

// Validate runs basic validation on arguments
func (o DevopsServerOptions) Validate(args []string) error {
	errors := []error{}
	errors = append(errors, o.RecommendedOptions.Validate()...)
	errors = append(errors, o.Admission.Validate()...)
	return utilerrors.NewAggregate(errors)
}

// Complete complete any required arguments
func (o *DevopsServerOptions) Complete() error {
	return nil
}

//Config run full drill configuration and return apiserver.Config pointer
func (o *DevopsServerOptions) Config() (*apiserver.Config, error) {
	// register admission plugins
	banproject.Register(o.Admission.Plugins)
	verifydependency.Register(o.Admission.Plugins)

	// TODO have a "real" external address
	if err := o.RecommendedOptions.SecureServing.MaybeDefaultWithSelfSignedCerts("localhost", nil, []net.IP{net.ParseIP("127.0.0.1")}); err != nil {
		return nil, fmt.Errorf("error creating self-signed certificates: %v", err)
	}

	serverConfig := genericapiserver.NewRecommendedConfig(apiserver.Codecs)
	if err := o.RecommendedOptions.ApplyTo(serverConfig); err != nil {
		return nil, err
	}

	client, err := clientset.NewForConfig(serverConfig.LoopbackClientConfig)
	if err != nil {
		return nil, err
	}
	informerFactory := informers.NewSharedInformerFactory(client, serverConfig.LoopbackClientConfig.Timeout)
	admissionInitializer := devopsinitializer.New(informerFactory, serverConfig.SharedInformerFactory, client)
	o.SharedInformerFactory = informerFactory

	if err := o.Admission.ApplyTo(&serverConfig.Config, serverConfig.SharedInformerFactory, serverConfig.ClientConfig, apiserver.Scheme, admissionInitializer); err != nil {
		return nil, err
	}

	config := &apiserver.Config{
		GenericConfig: serverConfig,
		ExtraConfig:   apiserver.ExtraConfig{},
	}
	return config, nil
}

// RunDevopsServer run the devops apiserver
func (o *DevopsServerOptions) RunDevopsServer(stopCh <-chan struct{}) error {
	config, err := o.Config()
	if err != nil {
		return err
	}

	server, err := config.Complete().New()
	if err != nil {
		return err
	}

	err = server.GenericAPIServer.AddPostStartHook("start-devops-apiserver-informers", func(context genericapiserver.PostStartHookContext) error {
		// by default it does not start the
		// internal SharedInformerFactory so we
		// need to add this goroutine here to start it
		go func() {
			glog.Infof("starting internal SharedInformerFactory")
			o.SharedInformerFactory.Start(context.StopCh)
		}()
		glog.Infof("starting config.GenericConfig.SharedInformerFactory")
		config.GenericConfig.SharedInformerFactory.Start(context.StopCh)
		return nil
	})
	if err != nil {
		return err
	}

	return server.GenericAPIServer.PrepareRun().Run(stopCh)
}
