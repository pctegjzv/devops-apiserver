package plugin

import (
	"github.com/golang/glog"
	"io"
	"io/ioutil"
	"reflect"
)

func ReadConfigInPlugin(config io.Reader, pluginName string) (configBytes []byte, err error) {
	if config == nil || reflect.ValueOf(config).IsNil() {
		glog.V(5).Infof("Config in plugin %s is nil", pluginName)
	} else {
		configBytes, err := ioutil.ReadAll(config)
		if err != nil {
			glog.V(5).Infof("Read config in plugin %s, err: %v", err)
			return nil, err
		}
		glog.V(5).Infof("Config in plugin %s is %s", configBytes)
	}
	return
}
