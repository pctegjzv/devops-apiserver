package banproject

import (
	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/admission/plugin"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"fmt"
	"github.com/golang/glog"
	"io"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"
)

// PluginName plugin name
// must be unique in the API server
const PluginName = "BanProject"

// Register registers a plugin
func Register(plugins *admission.Plugins) {
	plugins.Register(PluginName, func(config io.Reader) (admission.Interface, error) {
		plugin.ReadConfigInPlugin(config, PluginName)

		return New([]string{"kube-system", "default", "alauda-system"})
	})
}

// DisallowProject type to control validation logic
type DisallowProject struct {
	*admission.Handler
	blacklist []string
}

var _ = devopsinitializer.WantsInternalDevopsInformerFactory(&DisallowProject{})

// Admit ensures that the object in-flight is of kind Project.
// In addition checks that the Name is not on the banned list.
func (d *DisallowProject) Admit(a admission.Attributes) error {
	// we are only interested in projects
	if a.GetKind().GroupKind() != devops.Kind("Project") {
		return nil
	}
	metaAccessor, err := meta.Accessor(a.GetObject())
	if err != nil {
		return err
	}
	projectName := metaAccessor.GetName()
	glog.Infof("projectName: %v", projectName)
	if d.IsBlacklisted(projectName) {
		return errors.NewForbidden(
			a.GetResource().GroupResource(),
			a.GetName(),
			fmt.Errorf("this name may not be used, please change the project name"),
		)
	}

	project := a.GetObject().(*devops.Project)
	glog.Infof("project: %v", project)
	if len(project.Spec.Namespaces) == 0 {
		project.Spec.Namespaces = []devops.ProjectNamespace{
			devops.ProjectNamespace{
				Name: project.GetName(),
				Type: devops.ProjectNamespaceTypeOwned,
			},
		}
	} else if len(project.Spec.Namespaces) > 1 {
		return errors.NewForbidden(
			a.GetResource().GroupResource(),
			a.GetName(),
			fmt.Errorf("a project can only have one namespace"),
		)
	} else {
		for _, n := range project.Spec.Namespaces {
			if n.Name != project.GetName() {
				return errors.NewForbidden(
					a.GetResource().GroupResource(),
					a.GetName(),
					fmt.Errorf("a project cannot own a namespace of different name"),
				)
			}
			if n.Type != devops.ProjectNamespaceTypeOwned {
				return errors.NewForbidden(
					a.GetResource().GroupResource(),
					a.GetName(),
					fmt.Errorf("project-namespace relationship type is not allowed, use \"%s\" instead", devops.ProjectNamespaceTypeOwned),
				)
			}
		}
	}
	return nil
}

// IsBlacklisted checks if a name is in the blacklist
func (d *DisallowProject) IsBlacklisted(name string) bool {
	if len(d.blacklist) == 0 {
		return false
	}
	for _, n := range d.blacklist {
		if n == name {
			return true
		}
	}
	return false
}

// SetInternalDevopsInformerFactory gets Lister from SharedInformerFactory.
func (d *DisallowProject) SetInternalDevopsInformerFactory(f informers.SharedInformerFactory) {
	glog.V(3).Infof("got internal informer factory: %v", f)
}

// SetKubernetesInformerFactory set kubernetes informer factory.
func (d *DisallowProject) SetKubernetesInformerFactory(f k8sinformers.SharedInformerFactory) {
	glog.V(3).Infof("got kubernetes informer factory: %v", f)
}

func (d *DisallowProject) SetDevopsClient(c devopsclient.Interface) {
	glog.V(3).Infof("got devops client: %v", c)
}

// ValidateInitialization checks whether the plugin was correctly initialized.
func (d *DisallowProject) ValidateInitialization() error {
	return nil
}

// New creates a new ban flunder admission plugin
func New(blacklist []string) (*DisallowProject, error) {
	return &DisallowProject{
		Handler:   admission.NewHandler(admission.Create),
		blacklist: blacklist,
	}, nil
}
