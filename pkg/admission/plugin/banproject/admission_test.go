package banproject_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/admission/plugin/banproject"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/admission"
)

// TestBanProjectAdmissionPlugin tests various test cases against
// ban project admission plugin
func TestBanProjectAdmissionPlugin(t *testing.T) {
	var scenarios = []struct {
		blacklist              []string
		admissionInput         devops.Project
		admissionInputKind     schema.GroupVersionKind
		admissionInputResource schema.GroupVersionResource
		admissionMustFail      bool
	}{
		// scenario 1:
		// a project with a name that appears on the blacklist must be banned
		{
			blacklist: []string{"badname"},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "badname",
					Namespace: "",
				},
			},
			admissionInputKind:     devops.Kind("Project").WithVersion("version"),
			admissionInputResource: devops.Resource("projects").WithVersion("version"),
			admissionMustFail:      true,
		},
		// scenario 2:
		// a flunder with a name that does not appear on the blacklist must be admitted
		{
			blacklist: []string{"badname"},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "goodname",
					Namespace: "",
				},
			},
			admissionInputKind:     devops.Kind("Project").WithVersion("version"),
			admissionInputResource: devops.Resource("projects").WithVersion("version"),
			admissionMustFail:      false,
		},
		// scenario 3:
		// a project with a name that appears on the blacklist would be banned
		// but the kind passed in is not a project thus the whole request is accepted
		{
			blacklist: []string{"badname"},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "badname",
					Namespace: "",
				},
			},
			admissionInputKind:     devops.Kind("NotProject").WithVersion("version"),
			admissionInputResource: devops.Resource("notprojects").WithVersion("version"),
			admissionMustFail:      false,
		},
		// scenario 4:
		// blacklist is empty
		{
			blacklist: []string{},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "badname",
					Namespace: "",
				},
			},
			admissionInputKind:     devops.Kind("Project").WithVersion("version"),
			admissionInputResource: devops.Resource("projects").WithVersion("version"),
			admissionMustFail:      false,
		},
		// scenario 5:
		// multiple namespaces while creating
		{
			blacklist: []string{"alauda-system"},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "some-name",
					Namespace: "",
				},
				Spec: devops.ProjectSpec{
					Namespaces: []devops.ProjectNamespace{
						devops.ProjectNamespace{},
						devops.ProjectNamespace{},
					},
				},
			},
			admissionInputKind:     devops.Kind("Project").WithVersion("version"),
			admissionInputResource: devops.Resource("projects").WithVersion("version"),
			admissionMustFail:      true,
		},
		// scenario 6:
		// namespace name is different from project name
		{
			blacklist: []string{"alauda-system"},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "some-name",
					Namespace: "",
				},
				Spec: devops.ProjectSpec{
					Namespaces: []devops.ProjectNamespace{
						devops.ProjectNamespace{
							Type: devops.ProjectNamespaceTypeOwned,
							Name: "another-name",
						},
					},
				},
			},
			admissionInputKind:     devops.Kind("Project").WithVersion("version"),
			admissionInputResource: devops.Resource("projects").WithVersion("version"),
			admissionMustFail:      true,
		},
		// scenario 7:
		// namespace-project relationship is not valid
		{
			blacklist: []string{"alauda-system"},
			admissionInput: devops.Project{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "some-name",
					Namespace: "",
				},
				Spec: devops.ProjectSpec{
					Namespaces: []devops.ProjectNamespace{
						devops.ProjectNamespace{
							Type: "something",
							Name: "some-name",
						},
					},
				},
			},
			admissionInputKind:     devops.Kind("Project").WithVersion("version"),
			admissionInputResource: devops.Resource("projects").WithVersion("version"),
			admissionMustFail:      true,
		},
	}

	for index, scenario := range scenarios {
		func() {
			// prepare
			target, err := banproject.New(scenario.blacklist)
			// target.blacklist = scenario.blacklist
			if err != nil {
				t.Fatalf("scenario %d: failed to create banproject admission plugin due to = %v", index, err)
			}

			err = admission.ValidateInitialization(target)
			if err != nil {
				t.Fatalf("scenario %d: failed to initialize banproject admission plugin due to =%v", index, err)
			}

			// act
			err = target.Admit(admission.NewAttributesRecord(
				&scenario.admissionInput,
				nil,
				scenario.admissionInputKind,
				scenario.admissionInput.ObjectMeta.Namespace,
				"",
				scenario.admissionInputResource,
				"",
				admission.Create,
				nil),
			)

			// validate
			if scenario.admissionMustFail && err == nil {
				t.Errorf("scenario %d: expected an error but got nothing", index)
			}

			if !scenario.admissionMustFail && err != nil {
				t.Errorf("scenario %d: banflunder admission plugin returned unexpected error = %v", index, err)
			}
		}()
	}
}
