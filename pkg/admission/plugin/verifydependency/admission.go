package verifydependency

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"fmt"
	"io"
	// "sort"
	"strconv"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/internalversion"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// PluginName plugin name
// must be unique in the API server

const (
	PluginName     = "VerifyDependency"
	currentVersion = "v1alpha1"
)

// Register registers a plugin
func Register(plugins *admission.Plugins) {
	plugins.Register(PluginName, func(config io.Reader) (admission.Interface, error) {
		plugin.ReadConfigInPlugin(config, PluginName)

		return New()
	})
}

// VerifyDependency type to control validation logic
// this plugin main purpose is to validate entry on create and update
// and avoid creating JenkinsBinding for non-existing Jenkins instances
type VerifyDependency struct {
	*admission.Handler
	jenkinsLister                  listers.JenkinsLister
	jenkinsBindingLister           listers.JenkinsBindingLister
	pipelineConfigLister           listers.PipelineConfigLister
	pipelineLister                 listers.PipelineLister
	codeRepoServiceLister          listers.CodeRepoServiceLister
	codeRepoBindingLister          listers.CodeRepoBindingLister
	codeRepositoryLister           listers.CodeRepositoryLister
	namespaceLister                corev1listers.NamespaceLister
	secretLister                   corev1listers.SecretLister
	imageRegistryLister            listers.ImageRegistryLister
	imageRegistryBindingLister     listers.ImageRegistryBindingLister
	imageRepositoryLister          listers.ImageRepositoryLister
	projectManagementLister        listers.ProjectManagementLister
	projectManagementBindingLister listers.ProjectManagementBindingLister
	testToolLister                 listers.TestToolLister
	testToolBindingLister          listers.TestToolBindingLister

	// devops client
	devopsClient devopsclient.Interface
}

var _ = devopsinitializer.WantsInternalDevopsInformerFactory(&VerifyDependency{})

// Admit ensures that the object in-flight is of kind JenkinsBinding.
// In addition checks that the Jenkins instance used does exist
func (d *VerifyDependency) Admit(a admission.Attributes) error {

	_, err := meta.Accessor(a.GetObject())
	if err != nil {
		if devops.IgnoredKinds.Has(a.GetKind().Kind) {
			return nil
		} else {
			glog.Errorf("Admit Accessor err: %v", err)
		}
		return err
	}

	glog.Infof("Admit  kind %v", a.GetKind().GroupKind())

	// we split the logic based on the kind
	// and skip the ones we are not interested in
	switch a.GetKind().GroupKind() {
	case devops.Kind("JenkinsBinding"):
		jenkinsBinding := a.GetObject().(*devops.JenkinsBinding)
		return d.verifyJenkinsBindingDependency(jenkinsBinding)
	case devops.Kind("PipelineConfig"):
		pipelineConfig := a.GetObject().(*devops.PipelineConfig)
		return d.verifyPipelineConfigDependency(pipelineConfig)
	case devops.Kind("Pipeline"):
		pipeline := a.GetObject().(*devops.Pipeline)
		return d.verifyPipelineDependency(pipeline, a.GetResource())
	case devops.Kind("CodeRepoService"):
		service := a.GetObject().(*devops.CodeRepoService)
		return d.verifyCodeRepoServiceDependency(service, a.GetResource())
	case devops.Kind("CodeRepoBinding"):
		binding := a.GetObject().(*devops.CodeRepoBinding)
		return d.verifyCodeRepoBindingDependency(binding, a.GetResource())
	case devops.Kind("CodeRepository"):
		repository := a.GetObject().(*devops.CodeRepository)
		return d.verifyCodeRepositoryDependency(repository, a.GetResource())
	case devops.Kind("PipelineTemplate"):
		// TODO handle depdencies (tasks)
		return nil
	case devops.Kind("ClusterPipelineTemplate"):
		// TODO handle depdencies (tasks)
		return nil
	case devops.Kind("ImageRegistryBinding"):
		registryBinding := a.GetObject().(*devops.ImageRegistryBinding)
		return d.verifyImageRegistryBindingDependency(registryBinding, a.GetResource())
	case devops.Kind("ImageRepository"):
		registryRepository := a.GetObject().(*devops.ImageRepository)
		return d.verifyImageRepositoryDependency(registryRepository, a.GetResource())
	case devops.Kind("ProjectManagement"):
		service := a.GetObject().(*devops.ProjectManagement)
		return d.verifyProjectManagementDependency(service, a.GetResource())
	//case devops.Kind("ProjectManagementBinding"):
	//	binding := a.GetObject().(*devops.ProjectManagementBinding)
	//	return d.verifyProjectManageBindingDependency(binding, a.GetResource())
	case devops.Kind("TestTool"):
		service := a.GetObject().(*devops.TestTool)
		return d.verifyTestToolDependency(service, a.GetResource())
	//case devops.Kind("TestToolBinding"):
	//	binding := a.GetObject().(*devops.TestToolBinding)
	//	return d.verifyTestToolBindingDependency(binding, a.GetResource())
	default:
		return nil
	}
}

func (d *VerifyDependency) verifyJenkinsBindingDependency(jenkinsBinding *devops.JenkinsBinding) error {
	// If the Jenkins service is provided, we should verify its existance
	if jenkinsBinding.Spec.Jenkins.Name != "" {
		glog.V(7).Infof("Got jenkins service name: %s", jenkinsBinding.Spec.Jenkins.Name)
		_, err := d.jenkinsLister.Get(jenkinsBinding.Spec.Jenkins.Name)
		glog.V(7).Infof("Got jenkins instance err: %v", err)
		if err != nil {
			return err
		}
	}
	// Verify if the given namespace exists
	if jenkinsBinding.ObjectMeta.Namespace != "" {
		glog.V(7).Infof("Got namespace: %s", jenkinsBinding.ObjectMeta.Namespace)
		_, err := d.namespaceLister.Get(jenkinsBinding.ObjectMeta.Namespace)
		glog.V(7).Infof("Got namespace instance err: %v", err)
		if err != nil {
			return err
		}
	}
	// Verify that the provided secret name exists
	// in the smae namespace
	if jenkinsBinding.Spec.Account.Secret.Name != "" {
		glog.V(7).Infof("Got secret: %s", jenkinsBinding.Spec.Account.Secret.Name)
		_, err := d.secretLister.Secrets(jenkinsBinding.ObjectMeta.Namespace).Get(jenkinsBinding.Spec.Account.Secret.Name)
		glog.V(7).Infof("Got secret instance err: %v", err)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *VerifyDependency) verifyPipelineConfigDependency(pipelineConfig *devops.PipelineConfig) (err error) {
	if err = d.verifyJenkinsBindingExists(pipelineConfig.GetNamespace(), pipelineConfig.Spec.JenkinsBinding); err != nil {
		return
	}
	if _, err = d.verifySecretExists(pipelineConfig.GetNamespace(), pipelineConfig.Spec.Source.Secret); err != nil {
		return
	}
	return
}

func (d *VerifyDependency) verifyProjectManagementDependency(service *devops.ProjectManagement, resource schema.GroupVersionResource) (err error) {
	if service.Annotations == nil {
		service.Annotations = map[string]string{}
	}

	return
}

func (d *VerifyDependency) verifyProjectManagementExists(configRef *devops.LocalObjectReference) (config *devops.ProjectManagement, err error) {
	if configRef == nil || config.Name == "" {
		err = fmt.Errorf("projectManagement in spec is missing")
		return
	}

	config, err = d.projectManagementLister.Get(configRef.Name)
	glog.V(7).Infof("Got ProjectManagement %s, err: %v", configRef.Name, err)
	return
}

func (d *VerifyDependency) verifyProjectManageBindingDependency(binding *devops.ProjectManagementBinding, resource schema.GroupVersionResource) (err error) {
	var (
		projectManagement *devops.ProjectManagement
	)
	if projectManagement, err = d.verifyProjectManagementExists(&binding.Spec.ProjectManagement); err != nil {
		return
	}

	binding.OwnerReferences = []metav1.OwnerReference{
		*metav1.NewControllerRef(projectManagement, schema.GroupVersionKind{
			Group:   devops.GroupName,
			Version: currentVersion,
			Kind:    devops.TypeProjectManagement,
		}),
	}
	return
}

func (d *VerifyDependency) verifyTestToolDependency(service *devops.TestTool, resource schema.GroupVersionResource) (err error) {
	if service.Annotations == nil {
		service.Annotations = map[string]string{}
	}

	return
}

func (d *VerifyDependency) verifyTestToolExists(configRef *devops.LocalObjectReference) (config *devops.TestTool, err error) {
	if configRef == nil || config.Name == "" {
		err = fmt.Errorf("testTool in spec is missing")
		return
	}

	config, err = d.testToolLister.Get(configRef.Name)
	glog.V(7).Infof("Got TestTool %s, err: %v", configRef.Name, err)
	return
}

func (d *VerifyDependency) verifyTestToolBindingDependency(binding *devops.TestToolBinding, resource schema.GroupVersionResource) (err error) {
	var (
		projectManage *devops.TestTool
	)
	if projectManage, err = d.verifyTestToolExists(&binding.Spec.TestTool); err != nil {
		return
	}

	binding.OwnerReferences = []metav1.OwnerReference{
		*metav1.NewControllerRef(projectManage, schema.GroupVersionKind{
			Group:   devops.GroupName,
			Version: currentVersion,
			Kind:    devops.TypeTestTool,
		}),
	}
	return
}

func (d *VerifyDependency) verifyCodeRepoServiceDependency(service *devops.CodeRepoService, resource schema.GroupVersionResource) (err error) {
	if service.Annotations == nil {
		service.Annotations = map[string]string{}
	}

	service.Annotations[devops.AnnotationsCreateAppUrl] = service.GetHtml()
	return
}

func (d *VerifyDependency) verifyCodeRepoBindingDependency(binding *devops.CodeRepoBinding, resource schema.GroupVersionResource) (err error) {
	var (
		codeRepoService *devops.CodeRepoService
		secret          *corev1.Secret
	)
	if codeRepoService, err = d.verifyCodeRepoServiceExists(&binding.Spec.CodeRepoService); err != nil {
		return
	}

	if secret, err = d.verifySecretExists(binding.GetNamespace(), &devops.LocalObjectReference{
		Name: binding.Spec.Account.Secret.Name,
	}); err != nil {
		return
	}

	if binding.Labels == nil {
		binding.Labels = map[string]string{}
	}

	binding.Labels[devops.LabelCodeRepoServiceType] = codeRepoService.Spec.Type.String()
	binding.Labels[devops.LabelCodeRepoService] = codeRepoService.GetName()
	binding.Labels[devops.LabelCodeRepoServicePublic] = strconv.FormatBool(codeRepoService.IsPublic())
	binding.OwnerReferences = []metav1.OwnerReference{
		*metav1.NewControllerRef(codeRepoService, schema.GroupVersionKind{
			Group:   devops.GroupName,
			Version: currentVersion,
			Kind:    devops.TypeCodeRepoService,
		}),
	}

	if binding.Annotations == nil {
		binding.Annotations = map[string]string{}
	}

	binding.Annotations[devops.AnnotationsSecretType] = string(secret.Type)

	return
}

func (d *VerifyDependency) verifyCodeRepoServiceExists(configRef *devops.LocalObjectReference) (config *devops.CodeRepoService, err error) {
	if configRef != nil && configRef.Name != "" {
		config, err = d.codeRepoServiceLister.Get(configRef.Name)
		glog.V(7).Infof("Got CodeRepoService %s, err: %v", configRef.Name, err)
	}
	return
}

func (d *VerifyDependency) verifyCodeRepositoryDependency(repository *devops.CodeRepository, resource schema.GroupVersionResource) (err error) {
	binding, _ := d.verifyCodeRepoBindingExists(repository.GetNamespace(), &repository.Spec.CodeRepoBinding)

	if repository.Labels == nil {
		repository.Labels = map[string]string{}
	}

	if binding != nil {
		repository.Labels[devops.LabelCodeRepoBinding] = binding.GetName()
		repository.Labels[devops.LabelCodeRepoService] = binding.Spec.CodeRepoService.Name
		ownerReference := metav1.NewControllerRef(binding, schema.GroupVersionKind{
			Group:   devops.GroupName,
			Version: currentVersion,
			Kind:    devops.TypeCodeRepoBinding,
		})
		ownerReference.BlockOwnerDeletion = generic.Bool(false)
		repository.OwnerReferences = []metav1.OwnerReference{*ownerReference}
	}
	return
}

func (d *VerifyDependency) verifyCodeRepoBindingExists(namespace string, configRef *devops.LocalObjectReference) (config *devops.CodeRepoBinding, err error) {
	if configRef != nil && configRef.Name != "" {
		config, err = d.codeRepoBindingLister.CodeRepoBindings(namespace).Get(configRef.Name)
	}
	return
}

func (d *VerifyDependency) verifyPipelineDependency(pipeline *devops.Pipeline, resource schema.GroupVersionResource) (err error) {
	var config *devops.PipelineConfig
	if config, err = d.verifyPipelineConfigExists(pipeline.GetNamespace(), &pipeline.Spec.PipelineConfig); err != nil {
		return
	}
	if err = d.verifyJenkinsBindingExists(pipeline.GetNamespace(), pipeline.Spec.JenkinsBinding); err != nil {
		return
	}
	if _, err = d.verifySecretExists(pipeline.GetNamespace(), pipeline.Spec.Source.Secret); err != nil {
		return
	}
	if err = d.verifySerialPipeline(pipeline.GetNamespace(), pipeline, config, resource); err != nil {
		return
	}
	if err = d.verifyPipelineParams(pipeline, config, &resource); err != nil {
		return
	}

	// TODO: Move to another place
	if err = d.mutatePipeline(pipeline.GetNamespace(), pipeline, config); err != nil {
		return
	}

	return
}

func (d *VerifyDependency) verifyJenkinsBindingExists(namespace string, jenkinsBindingRef devops.LocalObjectReference) (err error) {
	if jenkinsBindingRef.Name != "" {
		glog.V(7).Infof("Got jenkinsBinding: %s/%s", namespace, jenkinsBindingRef.Name)
		_, err = d.jenkinsBindingLister.JenkinsBindings(namespace).Get(jenkinsBindingRef.Name)
		glog.V(7).Infof("Got jenkinsBinding %s/%s, err: %v", namespace, jenkinsBindingRef.Name, err)
	}
	return
}

func (d *VerifyDependency) verifySecretExists(namespace string, secretRef *devops.LocalObjectReference) (secret *corev1.Secret, err error) {
	if secretRef != nil && secretRef.Name != "" {
		glog.V(7).Infof("Got secret: %s/%s", namespace, secretRef.Name)
		secret, err = d.secretLister.Secrets(namespace).Get(secretRef.Name)
		glog.V(7).Infof("Got secret %s/%s, err: %v", namespace, secretRef.Name, err)
	}
	return
}

func (d *VerifyDependency) verifyPipelineConfigExists(namespace string, pipelineConfigRef *devops.LocalObjectReference) (config *devops.PipelineConfig, err error) {
	if pipelineConfigRef != nil && pipelineConfigRef.Name != "" {
		glog.V(7).Infof("Got pipelineConfig: %s/%s", namespace, pipelineConfigRef.Name)
		config, err = d.pipelineConfigLister.PipelineConfigs(namespace).Get(pipelineConfigRef.Name)
		glog.V(7).Infof("Got pipelineConfig %s/%s, err: %v", namespace, pipelineConfigRef.Name, err)
	}
	return
}

func (d *VerifyDependency) verifySerialPipeline(namespace string, pipeline *devops.Pipeline, config *devops.PipelineConfig, resource schema.GroupVersionResource) (err error) {
	// not serial should ignore
	if config.Spec.RunPolicy != devops.PipelinePolicySerial {
		return
	}
	// if is an update
	if pipeline.Annotations[devops.AnnotationsKeyPipelineNumber] != "" {
		return
	}
	var list []*devops.Pipeline

	list, err = d.pipelineLister.Pipelines(namespace).List(d.getPipelineConfigLabelSelector(pipeline.Spec.PipelineConfig))
	if err != nil {
		return
	}
	if len(list) == 0 {
		return
	}
	// filter
	list = func(original []*devops.Pipeline) []*devops.Pipeline {
		newList := make([]*devops.Pipeline, 0, len(original))
		for _, i := range original {
			if i.Spec.PipelineConfig.Name == pipeline.Spec.PipelineConfig.Name && pipeline.GetName() != i.GetName() {
				switch i.Status.Phase {
				case devops.PipelinePhasePending, devops.PipelinePhaseQueued:
					newList = append(newList, i)
				}
			}
		}
		return newList
	}(list)
	if len(list) >= 1 {
		err = errors.NewConflict(
			resource.GroupResource(),
			pipeline.GetName(),
			fmt.Errorf("PipelineConfig \"%s\" Serial policy is already executing. Wait until other pipelines finish executing and try again", pipeline.Spec.PipelineConfig.Name),
		)
	}
	return
}

func (d *VerifyDependency) mutatePipeline(namespace string, pipeline *devops.Pipeline, config *devops.PipelineConfig) (err error) {
	if pipeline.Labels == nil {
		pipeline.Labels = map[string]string{}
	}
	if pipeline.Annotations == nil {
		pipeline.Annotations = map[string]string{}
	}

	// already have a number.. quit
	if pipeline.Annotations[devops.AnnotationsKeyPipelineNumber] != "" {
		return
	}

	configCopy := config.DeepCopy()
	nextNumber := d.getPipelineNextNumber(namespace, pipeline, config)
	glog.V(5).Infof("Got next number: %v", nextNumber)
	pipeline.Labels[devops.LabelPipelineConfig] = pipeline.Spec.PipelineConfig.Name
	pipeline.Annotations[devops.AnnotationsKeyPipelineConfig] = pipeline.Spec.PipelineConfig.Name
	numberStr := strconv.Itoa(nextNumber)
	pipeline.Annotations[devops.AnnotationsKeyPipelineNumber] = numberStr
	if configCopy.Annotations == nil {
		configCopy.Annotations = map[string]string{}
	}
	configCopy.Annotations[devops.AnnotationsKeyPipelineLastNumber] = numberStr

	configCopy, err = d.devopsClient.Devops().PipelineConfigs(namespace).Update(configCopy)

	if config != nil {
		// Adding owner reference to delete all pipelines
		// when the PipelineConfig is deleted
		pipeline.OwnerReferences = []metav1.OwnerReference{
			*metav1.NewControllerRef(config, schema.GroupVersionKind{
				Group:   devops.GroupName,
				Version: currentVersion,
				Kind:    devops.TypePipelineConfig,
			}),
		}
	}
	return
}

func (d *VerifyDependency) verifyPipelineParams(pipeline *devops.Pipeline, config *devops.PipelineConfig, resource *schema.GroupVersionResource) (err error) {
	err = d.pipelineParamsProcess(pipeline, config, resource)

	return
}

func (d *VerifyDependency) pipelineParamsProcess(pipeline *devops.Pipeline, config *devops.PipelineConfig, resource *schema.GroupVersionResource) (err error) {
	cfgParams := config.Spec.Parameters
	pipParams := pipeline.Spec.Parameters
	var lackParams []devops.PipelineParameter

	for _, param := range cfgParams {
		var fond bool

		for _, pipeParam := range pipParams {
			if param.Name == pipeParam.Name && param.Type == pipeParam.Type {
				fond = true
				break
			}
		}

		if !fond {
			lackParams = append(lackParams, param)
		}
	}

	if len(lackParams) > 0 {
		pipelineName := pipeline.GetName()
		err = errors.NewForbidden(
			resource.GroupResource(),
			pipelineName,
			fmt.Errorf("Can not found params %v in PipelineConfig \"%s\"", lackParams, config.Name),
		)
	}

	return
}

func (d *VerifyDependency) getPipelineNextNumber(namespace string, pipeline *devops.Pipeline, config *devops.PipelineConfig) (value int) {

	// look for pipelines
	// fetch again to get the latest data
	config, _ = d.pipelineConfigLister.PipelineConfigs(namespace).Get(pipeline.Spec.PipelineConfig.Name)
	if config != nil && len(config.Annotations) > 0 && config.Annotations[devops.AnnotationsKeyPipelineLastNumber] != "" {
		glog.V(5).Infof("PipelineConfig: %s/%s - last number: %s", config.GetNamespace(), config.GetName(), config.Annotations[devops.AnnotationsKeyPipelineLastNumber])
		value, _ = strconv.Atoi(config.Annotations[devops.AnnotationsKeyPipelineLastNumber])
	}
	value++
	return
}

func (d *VerifyDependency) getPipelineConfigLabelSelector(pipelineConfig devops.LocalObjectReference) (selector labels.Selector) {
	selector = labels.NewSelector()
	req, err := labels.NewRequirement(devops.LabelPipelineConfig, selection.Equals, []string{pipelineConfig.Name})
	if err != nil {
		glog.Errorf("error generating pipelineconfig label selecto: %vr", err)
	}
	if req != nil {
		selector.Add(*req)
	}
	return selector
}

func (d *VerifyDependency) verifyImageRegistryBindingDependency(binding *devops.ImageRegistryBinding, resource schema.GroupVersionResource) (err error) {
	var (
		imageRegistry *devops.ImageRegistry
		secret        *corev1.Secret
	)

	if imageRegistry, err = d.verifyImageRegistryExists(&binding.Spec.ImageRegistry); err != nil {
		return
	}

	if secret, err = d.verifySecretExists(binding.GetNamespace(), &devops.LocalObjectReference{
		Name: binding.Spec.Secret.Name,
	}); err != nil {
		return
	}

	if binding.Labels == nil {
		binding.Labels = map[string]string{}
	}

	binding.Labels[devops.LabelImageRegistryType] = imageRegistry.Spec.Type.String()
	binding.Labels[devops.LabelImageRegistry] = imageRegistry.GetName()
	binding.OwnerReferences = []metav1.OwnerReference{
		*metav1.NewControllerRef(imageRegistry, schema.GroupVersionKind{
			Group:   devops.GroupName,
			Version: currentVersion,
			Kind:    devops.TypeImageRegistry,
		}),
	}
	if binding.Annotations == nil {
		binding.Annotations = map[string]string{}
	}
	if secret != nil {
		binding.Annotations[devops.AnnotationsSecretType] = string(secret.Type)
	}
	return
}

func (d *VerifyDependency) verifyImageRegistryExists(configRef *devops.LocalObjectReference) (config *devops.ImageRegistry, err error) {
	if configRef != nil && configRef.Name != "" {
		config, err = d.imageRegistryLister.Get(configRef.Name)
		glog.V(7).Infof("Got ImageRegistry %s, err: %v", configRef.Name, err)
	}
	return
}

func (d *VerifyDependency) verifyImageRepositoryDependency(repository *devops.ImageRepository, resource schema.GroupVersionResource) (err error) {
	binding, _ := d.verifyImageRegistryBindingExists(repository.GetNamespace(), &repository.Spec.ImageRegistryBinding)

	if repository.Labels == nil {
		repository.Labels = map[string]string{}
	}

	if binding != nil {
		repository.Labels[devops.LabelImageRegistryBinding] = binding.GetName()
		repository.Labels[devops.LabelImageRegistry] = binding.Spec.ImageRegistry.Name
		ownerReference := metav1.NewControllerRef(binding, schema.GroupVersionKind{
			Group:   devops.GroupName,
			Version: currentVersion,
			Kind:    devops.TypeImageRegistryBinding,
		})
		ownerReference.BlockOwnerDeletion = generic.Bool(false)
		repository.OwnerReferences = []metav1.OwnerReference{*ownerReference}
	}
	return
}

func (d *VerifyDependency) verifyImageRegistryBindingExists(namespace string, configRef *devops.LocalObjectReference) (config *devops.ImageRegistryBinding, err error) {
	if configRef != nil && configRef.Name != "" {
		config, err = d.imageRegistryBindingLister.ImageRegistryBindings(namespace).Get(configRef.Name)
	}
	return
}

// SetInternalDevopsInformerFactory gets Lister from SharedInformerFactory.
func (d *VerifyDependency) SetInternalDevopsInformerFactory(f informers.SharedInformerFactory) {
	glog.V(3).Infof("got internal informer factory: %v", f)
	d.jenkinsLister = f.Devops().InternalVersion().Jenkinses().Lister()
	d.jenkinsBindingLister = f.Devops().InternalVersion().JenkinsBindings().Lister()
	d.pipelineConfigLister = f.Devops().InternalVersion().PipelineConfigs().Lister()
	d.pipelineLister = f.Devops().InternalVersion().Pipelines().Lister()
	d.codeRepoServiceLister = f.Devops().InternalVersion().CodeRepoServices().Lister()
	d.codeRepoBindingLister = f.Devops().InternalVersion().CodeRepoBindings().Lister()
	d.codeRepositoryLister = f.Devops().InternalVersion().CodeRepositories().Lister()
	d.imageRegistryLister = f.Devops().InternalVersion().ImageRegistries().Lister()
	d.imageRegistryBindingLister = f.Devops().InternalVersion().ImageRegistryBindings().Lister()
	d.imageRepositoryLister = f.Devops().InternalVersion().ImageRepositories().Lister()
	d.projectManagementLister = f.Devops().InternalVersion().ProjectManagements().Lister()
	//d.projectManagementBindingLister = f.Devops().InternalVersion().ProjectManagementBindings().Lister()
	d.testToolLister = f.Devops().InternalVersion().TestTools().Lister()
	//d.testToolBindingLister = f.Devops().InternalVersion().TestToolBindings().Lister()
}

// SetKubernetesInformerFactory set kubernetes informer factory
func (d *VerifyDependency) SetKubernetesInformerFactory(f k8sinformers.SharedInformerFactory) {
	glog.V(3).Infof("got kubernetes informer factory: %v", f)
	d.namespaceLister = f.Core().V1().Namespaces().Lister()
	d.secretLister = f.Core().V1().Secrets().Lister()
}

// SetDevopsClient set client for devops
func (d *VerifyDependency) SetDevopsClient(c devopsclient.Interface) {
	glog.V(3).Infof("got devops client: %v", c)
	d.devopsClient = c
}

// ValidateInitialization checks whether the plugin was correctly initialized.
func (d *VerifyDependency) ValidateInitialization() error {
	if d.jenkinsLister == nil {
		return fmt.Errorf("missing jenkins lister")
	}
	if d.jenkinsBindingLister == nil {
		return fmt.Errorf("missing jenkinsbinding lister")
	}
	if d.pipelineConfigLister == nil {
		return fmt.Errorf("missing pipelineconfig lister")
	}
	if d.pipelineLister == nil {
		return fmt.Errorf("missing pipeline lister")
	}
	if d.secretLister == nil {
		return fmt.Errorf("missing secret lister")
	}
	if d.namespaceLister == nil {
		return fmt.Errorf("missing namespace lister")
	}
	if d.devopsClient == nil {
		return fmt.Errorf("missing devops client")
	}
	if d.codeRepoServiceLister == nil {
		return fmt.Errorf("missing codeRepoService lister")
	}
	if d.codeRepoBindingLister == nil {
		return fmt.Errorf("missing codeRepoBinding lister")
	}
	if d.codeRepositoryLister == nil {
		return fmt.Errorf("missing codeRepository lister")
	}
	if d.imageRegistryLister == nil {
		return fmt.Errorf("missing imageRegistry lister")
	}
	if d.imageRegistryBindingLister == nil {
		return fmt.Errorf("missing imageRegistryBinding lister")
	}
	if d.imageRepositoryLister == nil {
		return fmt.Errorf("missing imageRepository lister")
	}
	if d.projectManagementLister == nil {
		return fmt.Errorf("missing projectManagement lister")
	}
	//if d.projectManagementBindingLister == nil {
	//	return fmt.Errorf("missing projectManagementBinding lister")
	//}
	if d.testToolLister == nil {
		return fmt.Errorf("missing testTool lister")
	}
	//if d.testToolBindingLister == nil {
	//	return fmt.Errorf("missing testToolBinding lister")
	//}

	return nil
}

// New creates a new jenkins verify admission plugin
func New() (*VerifyDependency, error) {
	return &VerifyDependency{
		Handler: admission.NewHandler(admission.Create, admission.Update),
	}, nil
}
