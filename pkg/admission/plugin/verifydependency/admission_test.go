package verifydependency_test

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/admission/plugin/verifydependency"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/fake"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/admission"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	clienttesting "k8s.io/client-go/testing"
)

// TestVerifyDependencyAdmissionPlugin tests various test cases against
// jenkinsverify admission plugin
func TestVerifyDependencyAdmissionPlugin(t *testing.T) {
	var scenarios = []struct {
		jenkinsListOutput         *devops.JenkinsList
		jenkinsBindingListOutput  *devops.JenkinsBindingList
		pipelineConfigListOutput  *devops.PipelineConfigList
		codeRepoServiceListOutput *devops.CodeRepoServiceList
		codeRepoBindingListOutput *devops.CodeRepoBindingList
		imageRegistryListOutput   *devops.ImageRegistryList
		imageRegistryBindingListOutput *devops.ImageRegistryBinding
		namespaceOutput           *corev1.NamespaceList
		secretOutput              *corev1.SecretList
		admissionInput            runtime.Object
		inputNamespace            string
		admissionInputKind        schema.GroupVersionKind
		admissionInputResource    schema.GroupVersionResource
		admissionOperation        admission.Operation
		admissionMustFail         bool
	}{
		// scenario 0:
		// a lister returns empty list
		{
			jenkinsListOutput: &devops.JenkinsList{
				Items: []devops.Jenkins{},
			},
			namespaceOutput: &corev1.NamespaceList{
				Items: []corev1.Namespace{},
			},
			secretOutput: &corev1.SecretList{
				Items: []corev1.Secret{},
			},
			admissionInput: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "jenkins-binding",
					Namespace: "default",
				},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins-service",
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("JenkinsBinding").WithVersion("version"),
			admissionInputResource: devops.Resource("JenkinsBinding").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 1:
		// a lister returns item and is the same
		// namespace not found
		{
			jenkinsListOutput: &devops.JenkinsList{
				Items: []devops.Jenkins{
					devops.Jenkins{
						ObjectMeta: metav1.ObjectMeta{
							Name: "jenkins-service",
						},
					},
				},
			},
			namespaceOutput: &corev1.NamespaceList{
				Items: []corev1.Namespace{},
			},
			secretOutput: &corev1.SecretList{
				Items: []corev1.Secret{},
			},
			admissionInput: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "jenkins-binding",
					Namespace: "default",
				},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins-service",
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("JenkinsBinding").WithVersion("version"),
			admissionInputResource: devops.Resource("JenkinsBinding").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 2:
		// a lister returns item and is the not-same
		{
			jenkinsListOutput: &devops.JenkinsList{
				Items: []devops.Jenkins{
					devops.Jenkins{
						ObjectMeta: metav1.ObjectMeta{
							Name: "jenkins-service1",
						},
					},
				},
			},
			namespaceOutput: &corev1.NamespaceList{
				Items: []corev1.Namespace{
					corev1.Namespace{
						ObjectMeta: metav1.ObjectMeta{
							Name: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				Items: []corev1.Secret{},
			},
			admissionInput: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "jenkins-binding",
					Namespace: "default",
				},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins-service",
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("JenkinsBinding").WithVersion("version"),
			admissionInputResource: devops.Resource("JenkinsBinding").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 3:
		// a lister returns item and is the same
		// namespace found, secret not found
		{
			jenkinsListOutput: &devops.JenkinsList{
				Items: []devops.Jenkins{
					devops.Jenkins{
						ObjectMeta: metav1.ObjectMeta{
							Name: "jenkins-service",
						},
					},
				},
			},
			namespaceOutput: &corev1.NamespaceList{
				Items: []corev1.Namespace{
					corev1.Namespace{
						ObjectMeta: metav1.ObjectMeta{
							Name: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				Items: []corev1.Secret{},
			},
			admissionInput: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "jenkins-binding",
					Namespace: "default",
				},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins-service",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("JenkinsBinding").WithVersion("version"),
			admissionInputResource: devops.Resource("JenkinsBinding").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 4:
		// a lister returns item and is the same
		// namespace found, secret found
		{
			jenkinsListOutput: &devops.JenkinsList{
				Items: []devops.Jenkins{
					devops.Jenkins{
						ObjectMeta: metav1.ObjectMeta{
							Name: "jenkins-service",
						},
					},
				},
			},
			namespaceOutput: &corev1.NamespaceList{
				Items: []corev1.Namespace{
					corev1.Namespace{
						ObjectMeta: metav1.ObjectMeta{
							Name: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "jenkins-binding",
					Namespace: "default",
				},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins-service",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("JenkinsBinding").WithVersion("version"),
			admissionInputResource: devops.Resource("JenkinsBinding").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      false,
		},
		// scenario 5:
		// no jenkins binding for pipelineconfig
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
				Spec: devops.PipelineConfigSpec{
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding-not-found",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("PipelineConfig").WithVersion("version"),
			admissionInputResource: devops.Resource("PipelineConfig").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 6:
		// no secret for pipelineconfig
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret1",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
				Spec: devops.PipelineConfigSpec{
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("PipelineConfig").WithVersion("version"),
			admissionInputResource: devops.Resource("PipelineConfig").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 7:
		// no secret for pipelineconfig
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
				Spec: devops.PipelineConfigSpec{
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("PipelineConfig").WithVersion("version"),
			admissionInputResource: devops.Resource("PipelineConfig").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      false,
		},
		// scenario 8:
		// no  pipelineconfig for pipeline
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			pipelineConfigListOutput: &devops.PipelineConfigList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.PipelineConfig{
					devops.PipelineConfig{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "pipelineconfig",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.Pipeline{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipeline",
					Namespace: "default",
				},
				Spec: devops.PipelineSpec{
					PipelineConfig: devops.LocalObjectReference{
						Name: "pipelineconfig-not-found",
					},
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("Pipeline").WithVersion("version"),
			admissionInputResource: devops.Resource("Pipeline").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 9:
		// no  jenkinsbinding for pipeline
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			pipelineConfigListOutput: &devops.PipelineConfigList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.PipelineConfig{
					devops.PipelineConfig{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "pipelineconfig",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.Pipeline{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipeline",
					Namespace: "default",
				},
				Spec: devops.PipelineSpec{
					PipelineConfig: devops.LocalObjectReference{
						Name: "pipelineconfig",
					},
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding-not-found",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("Pipeline").WithVersion("version"),
			admissionInputResource: devops.Resource("Pipeline").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 10:
		// no secret for pipeline
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			pipelineConfigListOutput: &devops.PipelineConfigList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.PipelineConfig{
					devops.PipelineConfig{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "pipelineconfig",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.Pipeline{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipeline",
					Namespace: "default",
				},
				Spec: devops.PipelineSpec{
					PipelineConfig: devops.LocalObjectReference{
						Name: "pipelineconfig",
					},
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret-not-found",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("Pipeline").WithVersion("version"),
			admissionInputResource: devops.Resource("Pipeline").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 11:
		// all ok for pipeline
		{
			jenkinsBindingListOutput: &devops.JenkinsBindingList{
				Items: []devops.JenkinsBinding{
					devops.JenkinsBinding{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "jenkinsbinding",
							Namespace: "default",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			pipelineConfigListOutput: &devops.PipelineConfigList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.PipelineConfig{
					devops.PipelineConfig{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "pipelineconfig",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.Pipeline{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipeline",
					Namespace: "default",
				},
				Spec: devops.PipelineSpec{
					PipelineConfig: devops.LocalObjectReference{
						Name: "pipelineconfig",
					},
					JenkinsBinding: devops.LocalObjectReference{
						Name: "jenkinsbinding",
					},
					Source: devops.PipelineSource{
						Secret: &devops.LocalObjectReference{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("Pipeline").WithVersion("version"),
			admissionInputResource: devops.Resource("Pipeline").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      false,
		},
		// scenario 12:
		// no service for coderepobinding
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "coderepobinding",
					Namespace: "default",
				},
				Spec: devops.CodeRepoBindingSpec{
					CodeRepoService: devops.LocalObjectReference{
						Name: "codereposervice1",
					},
					Account: devops.CodeRepoBindingAccount{
						Secret: devops.SecretKeySetRef{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind(devops.TypeCodeRepoBinding).WithVersion("version"),
			admissionInputResource: devops.Resource(devops.TypeCodeRepoBinding).WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 13:
		// no secret for coderepobinding
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "coderepobinding",
					Namespace: "default",
				},
				Spec: devops.CodeRepoBindingSpec{
					CodeRepoService: devops.LocalObjectReference{
						Name: "codereposervice",
					},
					Account: devops.CodeRepoBindingAccount{
						Secret: devops.SecretKeySetRef{
							Name: "secret1",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind(devops.TypeCodeRepoBinding).WithVersion("version"),
			admissionInputResource: devops.Resource(devops.TypeCodeRepoBinding).WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 14:
		// all ok for coderepobinding
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "coderepobinding",
					Namespace: "default",
				},
				Spec: devops.CodeRepoBindingSpec{
					CodeRepoService: devops.LocalObjectReference{
						Name: "codereposervice",
					},
					Account: devops.CodeRepoBindingAccount{
						Secret: devops.SecretKeySetRef{
							Name: "secret",
						},
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind(devops.TypeCodeRepoBinding).WithVersion("version"),
			admissionInputResource: devops.Resource(devops.TypeCodeRepoBinding).WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      false,
		},
		// scenario 15:
		// no registry for imageregistrybinding
		{
			imageRegistryListOutput:&devops.ImageRegistryList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.ImageRegistry{
					devops.ImageRegistry{
						ObjectMeta: metav1.ObjectMeta{
							Name: "imageregistry",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.ImageRegistryBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "imageregistrybinding",
					Namespace: "default",
				},
				Spec: devops.ImageRegistryBindingSpec{
					ImageRegistry: devops.LocalObjectReference{
						Name: "imageregistry1",
					},
					Secret: devops.SecretKeySetRef{
						Name: "secret",
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind(devops.TypeImageRegistryBinding).WithVersion("version"),
			admissionInputResource: devops.Resource(devops.TypeImageRegistryBinding).WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 16:
		// no secret for imageregistrybinding
		{
			imageRegistryListOutput:&devops.ImageRegistryList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.ImageRegistry{
					devops.ImageRegistry{
						ObjectMeta: metav1.ObjectMeta{
							Name: "imageregistry",
						},
					},
				},
			},
			admissionInput: &devops.ImageRegistryBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "imageregistrybinding",
					Namespace: "default",
				},
				Spec: devops.ImageRegistryBindingSpec{
					ImageRegistry: devops.LocalObjectReference{
						Name: "imageregistry",
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind(devops.TypeImageRegistryBinding).WithVersion("version"),
			admissionInputResource: devops.Resource(devops.TypeImageRegistryBinding).WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      false,
		},
		// scenario 17:
		// error secret for imageregistrybinding
		{
			imageRegistryListOutput:&devops.ImageRegistryList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.ImageRegistry{
					devops.ImageRegistry{
						ObjectMeta: metav1.ObjectMeta{
							Name: "imageregistry",
						},
					},
				},
			},
			secretOutput: &corev1.SecretList{
				ListMeta: metav1.ListMeta{},
				Items: []corev1.Secret{
					corev1.Secret{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "secret",
							Namespace: "default",
						},
					},
				},
			},
			admissionInput: &devops.ImageRegistryBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "imageregistrybinding",
					Namespace: "default",
				},
				Spec: devops.ImageRegistryBindingSpec{
					ImageRegistry: devops.LocalObjectReference{
						Name: "imageregistry",
					},
					Secret: devops.SecretKeySetRef{
						Name: "secret1",
					},
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind(devops.TypeImageRegistryBinding).WithVersion("version"),
			admissionInputResource: devops.Resource(devops.TypeImageRegistryBinding).WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
	}

	for index, scenario := range scenarios {
		func() {
			// prepare
			cs := &fake.Clientset{}
			if scenario.jenkinsListOutput != nil {
				cs.AddReactor("list", "jenkinses", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.jenkinsListOutput, nil
				})
			}
			if scenario.jenkinsBindingListOutput != nil {
				cs.AddReactor("list", "jenkinsbindings", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.jenkinsBindingListOutput, nil
				})
			}
			if scenario.pipelineConfigListOutput != nil {
				cs.AddReactor("list", "pipelineconfigs", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.pipelineConfigListOutput, nil
				})
			}
			if scenario.codeRepoServiceListOutput != nil {
				cs.AddReactor("list", "codereposervices", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.codeRepoServiceListOutput, nil
				})
			}
			if scenario.codeRepoBindingListOutput != nil {
				cs.AddReactor("list", "coderepobindings", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.codeRepoBindingListOutput, nil
				})
			}
			if scenario.imageRegistryListOutput != nil {
				cs.AddReactor("list", "imageregistries", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.imageRegistryListOutput, nil
				})
			}
			if scenario.imageRegistryBindingListOutput != nil {
				cs.AddReactor("list", "imageregistrybindings", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.imageRegistryBindingListOutput, nil
				})
			}
			informersFactory := informers.NewSharedInformerFactory(cs, 5*time.Minute)

			k8sclient := &k8sfake.Clientset{}
			if scenario.namespaceOutput != nil {
				k8sclient.AddReactor("list", "namespaces", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.namespaceOutput, nil
				})
			}
			if scenario.secretOutput != nil {
				k8sclient.AddReactor("list", "secrets", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.secretOutput, nil
				})
			}
			k8sInformerFactory := k8sinformer.NewSharedInformerFactory(k8sclient, 5*time.Minute)

			target, err := verifydependency.New()
			if err != nil {
				t.Fatalf("scenario %d: failed to create banflunder admission plugin due to = %v", index, err)
			}

			targetInitializer := devopsinitializer.New(informersFactory, k8sInformerFactory, cs)
			targetInitializer.Initialize(target)

			err = admission.ValidateInitialization(target)
			if err != nil {
				t.Fatalf("scenario %d: failed to initialize banflunder admission plugin due to =%v", index, err)
			}

			stop := make(chan struct{})
			defer close(stop)
			informersFactory.Start(stop)
			informersFactory.WaitForCacheSync(stop)
			k8sInformerFactory.Start(stop)
			k8sInformerFactory.WaitForCacheSync(stop)

			// act
			err = target.Admit(admission.NewAttributesRecord(
				scenario.admissionInput,
				nil,
				scenario.admissionInputKind,
				scenario.inputNamespace,
				"",
				scenario.admissionInputResource,
				"",
				scenario.admissionOperation,
				nil),
			)

			// validate
			if scenario.admissionMustFail && err == nil {
				t.Errorf("scenario %d: expected an error but got nothing", index)
			}
			// t.Logf("index: %d  scenario: %v", index, scenario)
			if !scenario.admissionMustFail && err != nil {
				t.Errorf("scenario %d: verify dependency admission plugin returned unexpected error = %v", index, err)
			}
		}()
	}
}
