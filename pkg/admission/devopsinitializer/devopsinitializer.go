/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package devopsinitializer

import (
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"
)

type pluginInitializer struct {
	informers    informers.SharedInformerFactory
	k8sinformers k8sinformers.SharedInformerFactory
	devopsClient devopsclient.Interface
}

var _ admission.PluginInitializer = pluginInitializer{}

// New creates an instance of devops admission plugins initializer.
func New(informers informers.SharedInformerFactory, k8sinformers k8sinformers.SharedInformerFactory, client devopsclient.Interface) pluginInitializer {
	return pluginInitializer{
		informers:    informers,
		k8sinformers: k8sinformers,
		devopsClient: client,
	}
}

// Initialize checks the initialization interfaces implemented by a plugin
// and provide the appropriate initialization data
func (i pluginInitializer) Initialize(plugin admission.Interface) {
	if wants, ok := plugin.(WantsInternalDevopsInformerFactory); ok {
		wants.SetInternalDevopsInformerFactory(i.informers)
		wants.SetKubernetesInformerFactory(i.k8sinformers)
		wants.SetDevopsClient(i.devopsClient)
	}
}
