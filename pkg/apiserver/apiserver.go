/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package apiserver

import (
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/apimachinery/announced"
	"k8s.io/apimachinery/pkg/apimachinery/registered"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/apiserver/pkg/registry/rest"
	genericapiserver "k8s.io/apiserver/pkg/server"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/install"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	clusterpipelinetasktemplate "alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetasktemplate"
	clusterpipelinetemplate "alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplate"
	clusterpipelinetemplaterest "alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplate/rest"
	coderepobindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding"
	coderepobindingstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding/rest"
	codereposervicestorage "alauda.io/devops-apiserver/pkg/registry/devops/codereposervice"
	codereposervicestoragerest "alauda.io/devops-apiserver/pkg/registry/devops/codereposervice/rest"
	coderepositorystorage "alauda.io/devops-apiserver/pkg/registry/devops/coderepository"
	imageregistrystorage "alauda.io/devops-apiserver/pkg/registry/devops/imageregistry"
	imageregistrybindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding"
	imageregistrybindingstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding/rest"
	imagerepositorystorage "alauda.io/devops-apiserver/pkg/registry/devops/imagerepository"
	jenkinsstorage "alauda.io/devops-apiserver/pkg/registry/devops/jenkins"
	jenkinsbindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/jenkinsbinding"
	pipelinestorage "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	pipelinestoragerest "alauda.io/devops-apiserver/pkg/registry/devops/pipeline/rest"
	pipelineconfigstorage "alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig"
	pipelineconfigstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig/rest"
	pipelinetasktemplate "alauda.io/devops-apiserver/pkg/registry/devops/pipelinetasktemplate"
	pipelinetemplate "alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplate"
	pipelinetemplaterest "alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplate/rest"
	pipelinetemplatesync "alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplatesync"
	projectstorage "alauda.io/devops-apiserver/pkg/registry/devops/project"
	projectmanagementstorage "alauda.io/devops-apiserver/pkg/registry/devops/projectmanagement"
	testtoolstorage "alauda.io/devops-apiserver/pkg/registry/devops/testtool"
)

var (
	groupFactoryRegistry = make(announced.APIGroupFactoryRegistry)
	registry             = registered.NewOrDie("")
	// Scheme object holding server scheme
	Scheme = runtime.NewScheme()
	// Codecs codec factory for the server
	Codecs = serializer.NewCodecFactory(Scheme)
)

func init() {
	install.Install(groupFactoryRegistry, registry, Scheme)

	// we need to add the options to empty v1
	// TODO fix the server code to avoid this
	metav1.AddToGroupVersion(Scheme, schema.GroupVersion{Version: "v1"})

	// TODO: keep the generic API server from wanting this
	unversioned := schema.GroupVersion{Group: "", Version: "v1"}
	Scheme.AddUnversionedTypes(unversioned,
		&metav1.Status{},
		&metav1.APIVersions{},
		&metav1.APIGroupList{},
		&metav1.APIGroup{},
		&metav1.APIResourceList{},
		// adding parameters types
		// we will use them as ParametersCodec
		&metav1.ListOptions{},
		&metav1.ExportOptions{},
		&metav1.GetOptions{},
		&metav1.DeleteOptions{},
	)
	// register defaults for metav1
	metav1.RegisterDefaults(Scheme)
}

// ExtraConfig extra configuration for the server
type ExtraConfig struct {
	// Place you custom config here.
}

// Config this is the general configuration for the server
type Config struct {
	GenericConfig *genericapiserver.RecommendedConfig
	ExtraConfig   ExtraConfig
}

// DevopsServer contains state for a Kubernetes cluster master/api server.
type DevopsServer struct {
	GenericAPIServer *genericapiserver.GenericAPIServer
}

type completedConfig struct {
	GenericConfig genericapiserver.CompletedConfig
	ExtraConfig   *ExtraConfig
}

// CompletedConfig complete configuration for server
type CompletedConfig struct {
	// Embed a private pointer that cannot be instantiated outside of this package.
	*completedConfig
}

func getDefaultEnv(envKey, defaultVal string) (val string) {
	val = os.Getenv(envKey)
	if val == "" {
		val = defaultVal
	}
	return
}

// Complete fills in any fields not set that are required to have valid data. It's mutating the receiver.
func (cfg *Config) Complete() CompletedConfig {
	appVersion := getDefaultEnv("APP_VERSION", "v0.3")
	split := strings.Split(
		strings.TrimPrefix(
			appVersion,
			"v",
		),
		".",
	)
	major, minor := "0", "3"
	if len(split) > 0 {
		major = split[0]
		if len(split) > 1 {
			minor = split[1]
		}
	}

	// Adding swagger
	cfg.GenericConfig.SwaggerConfig = genericapiserver.DefaultSwaggerConfig()
	// Adding OpenAPI
	cfg.GenericConfig.OpenAPIConfig = genericapiserver.DefaultOpenAPIConfig(v1alpha1.GetOpenAPIDefinitions, Scheme)
	cfg.GenericConfig.OpenAPIConfig.Info.Title = "DevOps"
	cfg.GenericConfig.OpenAPIConfig.Info.Version = appVersion

	cfg.GenericConfig.Version = &version.Info{
		Major:     major,
		Minor:     minor,
		GitCommit: getDefaultEnv("COMMIT_ID", "v0.3"),
		BuildDate: getDefaultEnv("BUILD_DATE", ""),
	}

	c := completedConfig{
		cfg.GenericConfig.Complete(),
		&cfg.ExtraConfig,
	}

	return CompletedConfig{&c}
}

// New returns a new instance of DevopsServer from the given config.
func (c completedConfig) New() (*DevopsServer, error) {
	genericServer, err := c.GenericConfig.New("devops-apiserver", genericapiserver.EmptyDelegate)
	if err != nil {
		return nil, err
	}

	s := &DevopsServer{
		GenericAPIServer: genericServer,
	}
	secretLister := c.GenericConfig.SharedInformerFactory.Core().V1().Secrets().Lister()
	configMapLister := c.GenericConfig.SharedInformerFactory.Core().V1().ConfigMaps().Lister()

	// round tripper
	roundTripper := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}

	// bootstraping API Sever
	apiGroupInfo := genericapiserver.NewDefaultAPIGroupInfo(
		devops.GroupName, // API GroupName fixed as devops.alauda.io
		registry,         // basic API registry
		Scheme,           // API Scheme
		runtime.NewParameterCodec(Scheme), // ParameterCodec will regulate all parameter types
		Codecs, // Basic codecs for all types
	)
	apiGroupInfo.GroupMeta.GroupVersion = v1alpha1.SchemeGroupVersion
	v1alpha1storage := map[string]rest.Storage{}
	// Adding Project REST API
	v1alpha1storage["projects"] = devopsregistry.RESTInPeace(projectstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	// Adding Jenkins REST API
	jenkinsStore := devopsregistry.RESTInPeace(jenkinsstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["jenkinses"] = jenkinsStore
	// adding jenkinsbinding REST API
	jenkinsBindingStore := devopsregistry.RESTInPeace(jenkinsbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["jenkinsbindings"] = jenkinsBindingStore
	// adding PipelineConfig REST API
	pipelineconfigsStore := devopsregistry.RESTInPeace(pipelineconfigstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["pipelineconfigs"] = pipelineconfigsStore
	// adding Jenkinsfile preview REST API
	v1alpha1storage["pipelineconfigs/preview"] = pipelineconfigstoragerest.NewPreviewREST(pipelineconfigsStore)
	// adding Pipeline REST API
	pipelineStore := devopsregistry.RESTInPeace(pipelinestorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["pipelines"] = pipelineStore
	v1alpha1storage["pipelines/logs"] = pipelinestoragerest.NewLogREST(pipelineStore, jenkinsStore, jenkinsBindingStore, secretLister, roundTripper, pipelinestorage.URLBuilderImp{})
	v1alpha1storage["pipelines/tasks"] = pipelinestoragerest.NewTaskREST(pipelineStore, jenkinsStore, jenkinsBindingStore, secretLister, roundTripper, pipelinestorage.URLBuilderImp{})

	// adding RepoService REST API
	codeRepoServiceStore := devopsregistry.RESTInPeace(codereposervicestorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	codeRepoBindingStore := devopsregistry.RESTInPeace(coderepobindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["codereposervices"] = codeRepoServiceStore
	v1alpha1storage["codereposervices/authorize"] = codereposervicestoragerest.NewSecretREST(codeRepoServiceStore, codeRepoBindingStore, secretLister, configMapLister)
	v1alpha1storage["coderepobindings"] = codeRepoBindingStore
	v1alpha1storage["coderepobindings/remoterepositories"] = coderepobindingstoragerest.NewRepositoryREST(codeRepoServiceStore, codeRepoBindingStore, secretLister)
	v1alpha1storage["coderepositories"] = devopsregistry.RESTInPeace(coderepositorystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))

	// adding PipelineTaskTemplate REST API
	pipelinetasktemplate := devopsregistry.RESTInPeace(pipelinetasktemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["pipelinetasktemplates"] = pipelinetasktemplate
	pipelinetemplate := devopsregistry.RESTInPeace(pipelinetemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["pipelinetemplates"] = pipelinetemplate
	v1alpha1storage["pipelinetemplates/preview"] = pipelinetemplaterest.NewPreviewREST(pipelinetemplate, pipelinetasktemplate)
	// adding PipelineTemplateSync REST API
	v1alpha1storage["pipelinetemplatesyncs"] = devopsregistry.RESTInPeace(pipelinetemplatesync.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	// cluster template
	clusterPipelinetemplate := devopsregistry.RESTInPeace(clusterpipelinetemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	clusterPipelineTasktemplate := devopsregistry.RESTInPeace(clusterpipelinetasktemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["clusterpipelinetemplates"] = clusterPipelinetemplate
	v1alpha1storage["clusterpipelinetasktemplates"] = clusterPipelineTasktemplate
	v1alpha1storage["clusterpipelinetemplates/preview"] = clusterpipelinetemplaterest.NewPreviewREST(clusterPipelinetemplate, clusterPipelineTasktemplate)
	// adding ImageRegistry REST API
	imageRegistryStore := devopsregistry.RESTInPeace(imageregistrystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["imageregistries"] = imageRegistryStore
	// adding ImageRegistryBinding REST API
	imageRegistryBindingStore := devopsregistry.RESTInPeace(imageregistrybindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["imageregistrybindings"] = imageRegistryBindingStore
	// adding ImageRegistryBindingRepository REST API
	v1alpha1storage["imageregistrybindings/repositories"] = imageregistrybindingstoragerest.NewRepositoryREST(imageRegistryStore, imageRegistryBindingStore, secretLister)
	// adding ImageRepository REST API
	imageRepositoryStore := devopsregistry.RESTInPeace(imagerepositorystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["imagerepositories"] = imageRepositoryStore

	// projectManagement
	projectManagementStore := devopsregistry.RESTInPeace(projectmanagementstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	// projectManagementBindingStore := devopsregistry.RESTInPeace(projectmanagementbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["projectmanagements"] = projectManagementStore
	// v1alpha1storage["projectmanagementbindings"] = projectManagementBindingStore

	// testTool
	testToolStore := devopsregistry.RESTInPeace(testtoolstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	// testToolBindingStore := devopsregistry.RESTInPeace(testtoolbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
	v1alpha1storage["testtools"] = testToolStore
	// v1alpha1storage["testtoolbindings"] = testToolBindingStore

	glog.V(5).Infof("Health checks %v", s.GenericAPIServer.HealthzChecks())
	apiGroupInfo.VersionedResourcesStorageMap["v1alpha1"] = v1alpha1storage

	if err := s.GenericAPIServer.InstallAPIGroup(&apiGroupInfo); err != nil {
		return nil, err
	}

	return s, nil
}
