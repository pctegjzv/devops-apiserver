package controller

import (
	"fmt"
	"time"

	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	devopsscheme "alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
	informers "alauda.io/devops-apiserver/pkg/client/informers/externalversions"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"

	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
)

// SubController a subcontroller interface to
// work with different resources individually
type SubController interface {
	// Will provide all the necessary data to the subcontroller for initialization
	Initialize(Interface)
	GetType() string
	Start()

	// should inform if the cache is already synced
	HasSynced() []cache.InformerSynced
	Shutdown()
}

// Interface controller base interface for subcontrollers
type Interface interface {
	GetClients() (kubernetes.Interface, clientset.Interface)
	KubeClient() kubernetes.Interface
	DevopsClient() clientset.Interface
	GetThirdParty() externalthirdparty.ThirdParty
	GetInformerFactory() (kubeinformers.SharedInformerFactory, informers.SharedInformerFactory)
	GetBroadcaster() record.EventBroadcaster
}

// Controller base controller struct that will handle the main tasks
type Controller struct {
	kubeclientset         kubernetes.Interface
	devopsclientset       clientset.Interface
	kubeInformerFactory   kubeinformers.SharedInformerFactory
	devopsInformerFactory informers.SharedInformerFactory
	thirdParty            externalthirdparty.ThirdParty

	eventBroadcaster record.EventBroadcaster

	// subcontrollers
	subcontrollers []SubController

	// timeout
	timeout time.Duration
}

// Make sure this controller satisfy Interface
var _ Interface = &Controller{}

// NewController controller constructor function
func NewController(
	kubeclientset kubernetes.Interface,
	devopsclientset clientset.Interface,
	kubernetesInformerFactory kubeinformers.SharedInformerFactory,
	devopsInformerFactor informers.SharedInformerFactory,
	thirdParty externalthirdparty.ThirdParty,
	timeout time.Duration,
) *Controller {
	// adding devops types to the events Scheme
	// so that Events can be logged for the devops types
	devopsscheme.AddToScheme(scheme.Scheme)
	// creating event broadcaster
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(glog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})

	ctrl := &Controller{

		kubeclientset:   kubeclientset,
		devopsclientset: devopsclientset,

		kubeInformerFactory:   kubernetesInformerFactory,
		devopsInformerFactory: devopsInformerFactor,
		thirdParty:            thirdParty,

		eventBroadcaster: eventBroadcaster,
		subcontrollers:   []SubController{},
		timeout:          timeout,
	}
	return ctrl
}

// GetClients return basic clients for this controller
func (c *Controller) GetClients() (kubernetes.Interface, clientset.Interface) {
	return c.kubeclientset, c.devopsclientset
}

// KubeClient return basic clients for this controller
func (c *Controller) KubeClient() kubernetes.Interface {
	return c.kubeclientset
}

// DevopsClient return basic clients for this controller
func (c *Controller) DevopsClient() clientset.Interface {
	return c.devopsclientset
}

// GetThirdParty return thirdparty for this controller
func (c *Controller) GetThirdParty() externalthirdparty.ThirdParty {
	return c.thirdParty
}

// GetInformerFactory return basic shared informer factory for this controller
func (c *Controller) GetInformerFactory() (kubeinformers.SharedInformerFactory, informers.SharedInformerFactory) {
	return c.kubeInformerFactory, c.devopsInformerFactory
}

// GetBroadcaster return a general use event broadcaster
func (c *Controller) GetBroadcaster() record.EventBroadcaster {
	return c.eventBroadcaster
}

func (c *Controller) shutdown() {
	for _, c := range c.subcontrollers {
		c.Shutdown()
	}
}

// Run will start running the controller
// this will block execution until a stop signal comes through the channel
func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) (err error) {
	defer runtime.HandleCrash()
	// defer c.workqueue.ShutDown()
	defer c.shutdown()

	// Start the informer factories to begin populating the informer caches
	glog.Info("Starting main controller and subcontrollers")
	for i, sub := range c.subcontrollers {
		glog.Infof("%d. Initializing controller: %s ...", i, sub.GetType())
		sub.Initialize(c)
	}

	// we should start the factories only after initializing all the controllers
	// this is because the factories will store all the registered listers and informers
	// and will use that information to start fetching data
	// if this is done before the subcontroller, it will quit immediately
	glog.Info("Starting factories")
	go c.kubeInformerFactory.Start(stopCh)
	go c.devopsInformerFactory.Start(stopCh)

	glog.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.getSyncs()...); !ok {
		err = fmt.Errorf("failed to wait for caches to sync")
	}

	glog.Info("Starting workers")
	for i, sub := range c.subcontrollers {
		glog.Infof("%d. Starting worker: %s ...", i, sub.GetType())
		go wait.Until(sub.Start, time.Second, stopCh)
	}

	glog.Info("Started workers")
	<-stopCh
	glog.Info("Shutting down workers")

	return
}

// will fetch all sync functions from all subcontrollers
func (c *Controller) getSyncs() (sync []cache.InformerSynced) {
	sync = make([]cache.InformerSynced, 0, len(c.subcontrollers)*2)
	for _, c := range c.subcontrollers {
		sync = append(sync, c.HasSynced()...)
	}
	return
}

// AddSubcontroller adding a subcontroller
func (c *Controller) AddSubcontroller(sub SubController) {
	if c.subcontrollers == nil {
		c.subcontrollers = []SubController{}
	}
	c.subcontrollers = append(c.subcontrollers, sub)
}
