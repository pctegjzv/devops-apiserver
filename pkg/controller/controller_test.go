package controller_test

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	controller "alauda.io/devops-apiserver/pkg/controller"
	"alauda.io/devops-apiserver/pkg/mock/client-go/informers"
	controllerMock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	devopsinformer "alauda.io/devops-apiserver/pkg/mock/devops/informers"

	"github.com/golang/mock/gomock"
	kubernetes "k8s.io/client-go/kubernetes/fake"
	"k8s.io/client-go/tools/cache"
)

func TestMainController(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	k8sclient := &kubernetes.Clientset{}
	devopsclient := &fake.Clientset{}
	thirdparty := externalthirdparty.ThirdParty{}

	// factories
	k8sInformers := informers.NewMockSharedInformerFactory(ctrl)
	devopsInformers := devopsinformer.NewMockSharedInformerFactory(ctrl)

	mainCtrl := controller.NewController(k8sclient, devopsclient, k8sInformers, devopsInformers, thirdparty, time.Second)

	stopChan := make(chan struct{})
	// fake subcontroller
	subCtrl := controllerMock.NewMockSubController(ctrl)
	mainCtrl.AddSubcontroller(subCtrl)

	// should call this method once
	subCtrl.EXPECT().Initialize(mainCtrl).Times(1)
	subCtrl.EXPECT().GetType().AnyTimes()

	// needs to use this function to limit the channel type
	func(stop <-chan struct{}) {
		// should initialize informers
		k8sInformers.EXPECT().Start(stop)
		devopsInformers.EXPECT().Start(stop)
	}(stopChan)

	// should call start for subcontroller
	gotStart := make(chan struct{})
	subCtrl.EXPECT().Start().Do(func() {
		close(gotStart)
		// fake a busy task here
		// otherwise worker will keep restarting
		<-time.NewTimer(time.Second * 3).C
	}).Times(1)
	// should get syncs from subcontroller
	subCtrl.EXPECT().HasSynced().Return([]cache.InformerSynced{func() bool { return true }}).AnyTimes()

	// should call shutdown for subcontroller
	gotShutdown := make(chan struct{})
	subCtrl.EXPECT().Shutdown().Do(func() {
		close(gotShutdown)
	})

	var err error
	go func() {
		err = mainCtrl.Run(1, stopChan)
	}()
	// wait the start signal or timeout
	timer := time.NewTimer(time.Second * 3)
	select {
	case <-gotStart:
	case <-timer.C:
		t.Errorf("Worker not started...")
	}

	// stop for the worker
	close(stopChan)

	// wait workers get shutdown signal or timeout
	timer = time.NewTimer(time.Second * 3)
	select {
	case <-gotShutdown:
	case <-timer.C:
		t.Errorf("Worker was not shut down...")
	}

	if err != nil {
		t.Errorf("should not fail: %v", err)
	}

}
