package image

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1" // k8s native resource, eg: Service/Secret...
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apimachinery/pkg/util/runtime"
	kubeClientset "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	kubeListers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsClientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	devopsListers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller"
	"alauda.io/devops-apiserver/pkg/util/k8s"
)

const controllerAgentName = "devops-image-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing = "ErrSyncing"

	MessageImageRegistrySynced        = "ImageRegistry synced successfully"
	MessageImageRegistryBindingSynced = "ImageRegistryBinding synced successfully"
	MessageImageRepositorySynced      = "ImageRepository synced successfully"

	TypeImageRegistry        = "ImageRegistry"
	TypeImageRegistryBinding = "ImageRegistryBinding"
	TypeImageRepository      = "ImageRepository"

	TTLServiceCheck    = time.Minute * 5
	TTLBindingCheck    = time.Minute * 5
	TTLRepositoryCheck = time.Minute * 5
)

type Controller struct {
	// kubernetes API client
	kubeclientset kubeClientset.Interface
	// Devops API client
	devopsclientset devopsClientset.Interface
	// ThirdParty
	thirdParty externalthirdparty.ThirdParty

	// Data sync for ImageRegistry
	imageRegistryLister devopsListers.ImageRegistryLister
	imageRegistrySynced cache.InformerSynced

	// Data sync for ImageRegistryBinding
	imageRegistryBindingLister devopsListers.ImageRegistryBindingLister
	imageRegistryBindingSynced cache.InformerSynced

	// Data sync for ImageRepository
	imageRepositoryLister devopsListers.ImageRepositoryLister
	imageRepositorySynced cache.InformerSynced

	// Data sync for secrets
	secretLister kubeListers.SecretLister
	secretSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

func New() *Controller {
	return &Controller{}
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()
	c.thirdParty = base.GetThirdParty()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	k8sFactory, devopsFactory := base.GetInformerFactory()

	// devops ImageRegistries
	imageRegistryInformer := devopsFactory.Devops().V1alpha1().ImageRegistries()
	c.imageRegistryLister = imageRegistryInformer.Lister()
	c.imageRegistrySynced = imageRegistryInformer.Informer().HasSynced

	// devops ImageRegistryBinding
	imageRegistryBindingInformer := devopsFactory.Devops().V1alpha1().ImageRegistryBindings()
	c.imageRegistryBindingLister = imageRegistryBindingInformer.Lister()
	c.imageRegistryBindingSynced = imageRegistryBindingInformer.Informer().HasSynced

	// devops ImageRepositories
	imageRepositoryInformer := devopsFactory.Devops().V1alpha1().ImageRepositories()
	c.imageRepositoryLister = imageRepositoryInformer.Lister()
	c.imageRepositorySynced = imageRepositoryInformer.Informer().HasSynced

	// k8s secret
	secretInformer := k8sFactory.Core().V1().Secrets()
	c.secretLister = secretInformer.Lister()
	c.secretSynced = secretInformer.Informer().HasSynced

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "image")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting events
	// imageRegistry
	imageRegistryInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueImageRegistry,
		UpdateFunc: func(old, new interface{}) {
			c.enqueueImageRegistry(new)
		},
		DeleteFunc: c.enqueueImageRegistry,
	})

	// imageRegistryBinding
	imageRegistryBindingInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueImageRegistryBinding,
		UpdateFunc: func(old, new interface{}) {
			c.enqueueImageRegistryBinding(new)
		},
		DeleteFunc: c.enqueueImageRegistryBinding,
	})

	// imageRepository
	imageRepositoryInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueImageRepository,
		UpdateFunc: func(old, new interface{}) {
			c.enqueueImageRepository(new)
		},
		DeleteFunc: c.enqueueImageRepository,
	})
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "Image"
}

// Start should start the controller
func (c *Controller) Start() {
	//go c.CheckConfigMap()

	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the devopsListers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.imageRegistrySynced, c.imageRegistryBindingSynced, c.imageRepositorySynced, c.secretSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()
	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		//We call Done here so the workqueue knows we have finished
		//processing this item. We also must remember to call Forget if we
		//do not want this work item being re-queued. For example, we do
		//not call Forget if a transient error occurs, instead the item is
		//put back on the workqueue and attempted again after a back-off
		//period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		var err error
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %v", obj))
			return nil
		}
		glog.Infof("will handle this key: %s", key)
		spl := strings.Split(key, "|")
		objType := TypeImageRegistry
		if len(spl) == 2 {
			objType = spl[0]
			key = spl[1]
		}

		switch objType {
		case TypeImageRegistry:
			// Run the syncImageRegistryHandler, passing it the name string of the
			// ImageRegistry resource to be synced.
			err = c.syncImageRegistryHandler(key)
		case TypeImageRegistryBinding:
			// Run the syncImageRegistryBindingHandler, passing it the name string of the
			// ImageRegistry resource to be synced.
			err = c.syncImageRegistryBindingHandler(key)
		case TypeImageRepository:
			// Run the syncImageRepositoryHandler, passing it the name string of the
			// ImageRegistry resource to be synced.
			err = c.syncImageRepositoryHandler(key)
		}

		if err != nil {
			return fmt.Errorf("error sync '%s'|'%s': %s", objType, key, err.Error())
		}

		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced Image '%s'", key)
		return nil
	}(obj)
	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

// region 1. enqueue
// enqueueImageRegistry takes a ImageRegistry resource and converts it into a type|namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than ImageRegistry.
func (c *Controller) enqueueImageRegistry(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", TypeImageRegistry, key)
	c.workqueue.AddRateLimited(key)
}

// enqueueImageRegistryBinding takes a ImageRegistryBinding resource and converts it into a type|namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than ImageRegistryBinding.
func (c *Controller) enqueueImageRegistryBinding(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", TypeImageRegistryBinding, key)
	c.workqueue.AddRateLimited(key)
}

// enqueueImageRegistry takes a ImageRepository resource and converts it into a type|namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than ImageRepository.
func (c *Controller) enqueueImageRepository(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", TypeImageRepository, key)
	c.workqueue.AddRateLimited(key)
}

// endregion

// region 2. sync
func (c *Controller) syncImageRegistryHandler(key string) error {
	_, name, err := cache.SplitMetaNamespaceKey(key)
	glog.Infof("syncing ImageRegistry %v, err: %v", name, err)

	registry, err := c.imageRegistryLister.Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("service '%s' in work queue no longer exist", key))
			return nil
		}
		return err
	}
	if registry.Status.HTTPStatus != nil &&
		registry.Status.HTTPStatus.LastAttempt != nil &&
		registry.Status.HTTPStatus.LastAttempt.Add(TTLServiceCheck).After(time.Now()) {
		glog.Infof("ImageRegistry %s was checked in the last %v", registry.GetName())
		return err
	}

	registryCopy := registry.DeepCopy()
	registryClient := c.thirdParty.ImageRegistryClientFactory.GetImageRegistryClient(registryCopy.GetType(), registryCopy.GetEndpoint(), "", "")
	registryCopy.Status.HTTPStatus, err = registryClient.CheckRegistryAvailable(getLastAttempt(registryCopy.Status.HTTPStatus))
	glog.Infof("ImageRegistry %s check returned error: %v", registry.GetName(), err)
	if err != nil || registryCopy.Status.HTTPStatus.StatusCode >= 500 {
		registryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseError
		registryCopy.Status.Message = registryCopy.Status.HTTPStatus.Response
		runtime.HandleError(err)
	} else {
		registryCopy.Status.Message = MessageImageRegistrySynced
		registryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseReady
	}
	err = c.updateImageRegistry(registryCopy)
	return err
}

func (c *Controller) syncImageRegistryBindingHandler(key string) (err error) {
	var (
		namespace, name                string
		secretErr, serviceErr, repoErr bool

		secretRef            devopsv1alpha1.SecretKeySetRef
		binding, bindingCopy *devopsv1alpha1.ImageRegistryBinding
		oldRepoConditions    []devopsv1alpha1.BindingCondition
		imageRegistry        *devopsv1alpha1.ImageRegistry
		result               *devopsv1alpha1.ImageRegistryBindingRepositories

		now       = time.Now()
		metaTime  = metav1.NewTime(now)
		condition devopsv1alpha1.BindingCondition
	)

	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err = cache.SplitMetaNamespaceKey(key)
	glog.Infof("Syncing ImageRegistryBinding %v, namespace: %v, err: %v", name, namespace, err)

	binding, err = c.imageRegistryBindingLister.ImageRegistryBindings(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("imageRegistryBinding %s in work queue no longer exists", key))
			err = nil
		}
		return
	}
	if isCheckedInLastAttempt(binding.Status.HTTPStatus, TTLBindingCheck) {
		glog.Infof("ImageRegistryBinding %s was checked in the last %v", binding.GetName(), TTLBindingCheck)
		return
	}

	// backup the repo conditions, in case of some error happen
	if len(binding.Status.Conditions) > 0 {
		for _, condition := range binding.Status.Conditions {
			if condition.Type == devopsv1alpha1.JenkinsBindingStatusTypeImageRepository {
				oldRepoConditions = append(oldRepoConditions, condition)
			}
		}
	}

	bindingCopy = binding.DeepCopy()
	bindingCopy.Status.Conditions = []devopsv1alpha1.BindingCondition{}

	// check secretRef
	secretRef = bindingCopy.Spec.Secret
	glog.Infof("Check the secretRef %s.", secretRef.Name)
	if secretRef.Name != "" {
		secret, err := c.secretLister.Secrets(bindingCopy.GetNamespace()).Get(secretRef.Name)
		condition = devopsv1alpha1.BindingCondition{
			Name:        secretRef.Name,
			Type:        devopsv1alpha1.JenkinsBindingStatusTypeSecret,
			LastAttempt: &metaTime,
		}

		if err != nil {
			secretErr = true
			condition.Message = err.Error()
			if errors.IsNotFound(err) {
				condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
				condition.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			} else {
				condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotValid
				condition.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotValid
			}
		} else if secret != nil {
			condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusReady
			condition.Reason = ""
			condition.Message = ""
		}
		bindingCopy.Status.Conditions = append(bindingCopy.Status.Conditions, condition)
	}
	// get credential from secret
	username, password, err := k8s.GetUsernameAndPasswordFromSecret(c.secretLister, secretRef.Name, secretRef.UsernameKey, secretRef.APITokenKey, bindingCopy.GetNamespace())
	if err != nil {
		glog.Errorf("Get username and password from secret: %v failed: %v", secretRef.Name, err)
		secretErr = true
		bindingCopy.Status.Message = err.Error()
	} else {
		glog.Infof("Check the service %s in binding %s", bindingCopy.Spec.ImageRegistry.Name, bindingCopy.GetName())
		imageRegistry, err = c.imageRegistryLister.Get(bindingCopy.Spec.ImageRegistry.Name)
		if err != nil {
			glog.Errorf("Get image registry object: %s is failed: %v", bindingCopy.Spec.ImageRegistry.Name, err)
			serviceErr = true
			bindingCopy.Status.Message = err.Error()
		} else {
			// check registry status
			glog.Infof("Check binding registry status")
			registryClient := c.thirdParty.ImageRegistryClientFactory.GetImageRegistryClient(imageRegistry.GetType(), imageRegistry.GetEndpoint(), "", "")
			if registryClient != nil {
				bindingCopy.Status.HTTPStatus, err = registryClient.CheckRegistryAvailable(getLastAttempt(bindingCopy.Status.HTTPStatus))
			}
			// get registryClient
			glog.Infof("RegistryClient: %s username is: %s, password is: %s", bindingCopy.Spec.ImageRegistry.Name, username, password)
			result, err = c.devopsclientset.DevopsV1alpha1().ImageRegistryBindings(namespace).GetImageRepos(bindingCopy.GetName())
			if err != nil {
				glog.Errorf("Get repository list failed from: %s, errors is: %v", bindingCopy.Spec.ImageRegistry.Name, err)
				repoErr = true
				bindingCopy.Status.Message = err.Error()
			}
		}
	}
	if secretErr || serviceErr || repoErr {
		bindingCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseError
		glog.Errorf("Some error happened, restore the ImageRepository conditions count %d", len(oldRepoConditions))
		if len(oldRepoConditions) > 0 {
			bindingCopy.Status.Conditions = append(bindingCopy.Status.Conditions, oldRepoConditions...)
		}
	} else {
		bindingCopy.Status.Message = MessageImageRegistryBindingSynced
		bindingCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseReady

		// fetch all repos in the registry
		selector := getRegistrySelector(imageRegistry.GetName())
		localRepos, _ := c.imageRepositoryLister.ImageRepositories(namespace).List(selector)
		glog.Infof("List image repository from namespace: %s, result is: %s", namespace, localRepos)
		repoPaths := bindingCopy.Spec.RepoInfo.Repositories
		// get all repo from user choose path
		repos := getRepoFromPath(result.Items, repoPaths)
		for _, repo := range repos {
			newRepo := generateRepo(repo, bindingCopy)
			// add repo condition to status
			condition = devopsv1alpha1.BindingCondition{
				Type:        devopsv1alpha1.JenkinsBindingStatusTypeImageRepository,
				Name:        newRepo.Name,
				LastAttempt: &metaTime,
				Message:     "",
				Reason:      "",
				Status:      devopsv1alpha1.JenkinsBindingStatusConditionStatusReady,
			}
			bindingCopy.Status.Conditions = append(bindingCopy.Status.Conditions, condition)

			// sync repo to k8s
			var foundRepo *devopsv1alpha1.ImageRepository
			for _, localRepo := range localRepos {
				if newRepo.GetName() == localRepo.GetName() {
					foundRepo = localRepo
					break
				}
			}
			if foundRepo == nil {
				glog.Infof("Image repository if not found ,create: %s", newRepo.GetName())
				_, err := c.devopsclientset.DevopsV1alpha1().ImageRepositories(namespace).Create(&newRepo)
				if err != nil {
					glog.Errorf("error when creating ImageRepository: %v", err)
				}
			}
		}
	}

	err = c.updateImageRegistryBinding(bindingCopy)
	if err != nil {
		runtime.HandleError(err)
	} else {
		c.recorder.Event(bindingCopy, corev1.EventTypeNormal, SuccessSynced, MessageImageRegistryBindingSynced)
	}
	return
}

func (c *Controller) syncImageRepositoryHandler(key string) (err error) {
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	glog.Infof("Syncing ImageRepository %s, namespace: %s, err: %v", name, namespace, err)

	repository, err := c.imageRepositoryLister.ImageRepositories(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("repository '%s' in work queue no longer exists", key))
			return nil
		}
		return
	}
	if isCheckedInLastAttempt(repository.Status.HTTPStatus, TTLRepositoryCheck) {
		glog.Infof("ImageRepository %s was checked in the last %v", repository.GetName(), TTLRepositoryCheck)
		return
	}

	repositoryCopy := repository.DeepCopy()
	selector := getRegistrySelector(repository.GetRegistryName())
	bindings, err := c.imageRegistryBindingLister.ImageRegistryBindings(repository.Namespace).List(selector)
	if err != nil {
		glog.Errorf("Error when get bindings refer to repository %s, err: %v", repository.GetName(), err)
		return
	}

	var foundBinding *devopsv1alpha1.ImageRegistryBinding
	for _, binding := range bindings {
		if binding.Status.Conditions != nil {
			for _, condition := range binding.Status.Conditions {
				if condition.Name == repository.GetName() {
					foundBinding = binding
					break
				}
			}
		}
	}

	if foundBinding == nil {
		glog.Infof("Can not find binding contains the repo %s, delete it", repository.GetName())
		// if repository has reference, status is WaitingToDelete
		return c.devopsclientset.DevopsV1alpha1().ImageRepositories(namespace).Delete(repository.GetName(), &metav1.DeleteOptions{})
	} else {
		registry, err := c.imageRegistryLister.Get(foundBinding.Spec.ImageRegistry.Name)
		if err != nil {
			return err
		}
		secretRef := foundBinding.Spec.Secret
		username, password, err := k8s.GetUsernameAndPasswordFromSecret(c.secretLister, secretRef.Name, secretRef.UsernameKey, secretRef.APITokenKey, namespace)
		if err != nil {
			glog.Errorf("Error when get username and password form secret: %s; err: %v", foundBinding.Spec.Secret, err)
			runtime.HandleError(err)
		}
		registryClient := c.thirdParty.ImageRegistryClientFactory.GetImageRegistryClient(registry.GetType(), registry.GetEndpoint(), username, password)
		repositoryCopy.Status.HTTPStatus, err = registryClient.CheckRepositoryAvailable(repositoryCopy.Spec.Image, getLastAttempt(repository.Status.HTTPStatus))
		if err != nil {
			glog.Errorf("Check repository available failed, error is: %s", err)
			runtime.HandleError(err)
		}
		glog.Infof("Repository %s check returned error: %v", repositoryCopy.Name, err)
		statusCode := repositoryCopy.Status.HTTPStatus.StatusCode
		if err != nil || statusCode >= 400 {
			repositoryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseError
			repositoryCopy.Status.Message = repositoryCopy.Status.HTTPStatus.Response
			runtime.HandleError(err)
		} else {
			tags, err := registryClient.GetImageTags(repositoryCopy.Spec.Image)
			if err != nil {
				repositoryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseListTagError
				repositoryCopy.Status.Message = err.Error()
				runtime.HandleError(err)
			} else {
				repositoryCopy.Status.Message = MessageImageRepositorySynced
				repositoryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseReady
				sort.Slice(tags, func(i, j int) bool {
					return tags[i].CreatedAt.After(tags[j].CreatedAt.Time)
				})
				repositoryCopy.Status.Tags = tags
			}
		}
	}
	err = c.updateImageRepository(repositoryCopy)
	return err
}

// endregion

// region 3. update
func (c *Controller) updateImageRegistry(registry *devopsv1alpha1.ImageRegistry) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().ImageRegistries().Update(registry)
	c.recorder.Event(registry, corev1.EventTypeNormal, SuccessSynced, MessageImageRegistrySynced)
	return
}

func (c *Controller) updateImageRegistryBinding(binding *devopsv1alpha1.ImageRegistryBinding) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().ImageRegistryBindings(binding.GetNamespace()).Update(binding)
	c.recorder.Event(binding, corev1.EventTypeNormal, SuccessSynced, MessageImageRegistryBindingSynced)
	return
}

func (c *Controller) updateImageRepository(repository *devopsv1alpha1.ImageRepository) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().ImageRepositories(repository.Namespace).Update(repository)
	c.recorder.Event(repository, corev1.EventTypeNormal, SuccessSynced, MessageImageRepositorySynced)
	return
}

// endregion

func generateRepo(repo string, binding *devopsv1alpha1.ImageRegistryBinding) (newRepo devopsv1alpha1.ImageRepository) {
	registryName := binding.GetLabels()[devopsv1alpha1.LabelImageRegistry]
	name := registryName + "/" + repo
	names := strings.Split(name, "/")
	repoName := strings.Join(names, "-")

	newRepo = devopsv1alpha1.ImageRepository{
		TypeMeta: metav1.TypeMeta{
			Kind:       TypeImageRepository,
			APIVersion: "devops.alauda.io/v1alpha1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      repoName,
			Namespace: binding.GetNamespace(),
		},
		Spec: devopsv1alpha1.ImageRepositorySpec{
			ImageRegistry:        devopsv1alpha1.LocalObjectReference{Name: registryName},
			ImageRegistryBinding: devopsv1alpha1.LocalObjectReference{Name: binding.GetName()},
			Image:                repo,
		},
	}
	return
}

func getRepoFromPath(remoteRepos, repoPaths []string) (repos []string) {
	glog.Infof("getRepoFromPath remoteRepos: %v, repoPaths: %v", remoteRepos, repoPaths)
	repoMap := make(map[string]int)
	for _, repoPath := range repoPaths {
		if repoPath == "/" {
			repos = remoteRepos
			return
		} else {
			for _, remoteRepo := range remoteRepos {
				repoTrim := strings.TrimPrefix(remoteRepo, repoPath)
				if repoTrim == "" || strings.HasPrefix(repoTrim, "/") {
					repoMap[remoteRepo] = 1
				}
			}
		}
	}
	for key := range repoMap {
		repos = append(repos, key)
	}
	return
}

func getSimpleSelector(key, value string) (selector labels.Selector) {
	selector = labels.NewSelector()
	req, _ := labels.NewRequirement(key, selection.Equals, []string{value})
	selector.Add(*req)
	return
}

func getRegistrySelector(registryName string) (selector labels.Selector) {
	return getSimpleSelector(devopsv1alpha1.LabelImageRegistry, registryName)
}

func isCheckedInLastAttempt(status *devopsv1alpha1.HostPortStatus, ttlCheck time.Duration) bool {
	return status != nil && status.LastAttempt != nil && status.LastAttempt.Add(ttlCheck).After(time.Now())
}

func getLastAttempt(status *devopsv1alpha1.HostPortStatus) *metav1.Time {
	if status == nil || status.LastAttempt == nil {
		return nil
	}
	return status.LastAttempt
}
