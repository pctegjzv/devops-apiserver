package project

import (
	"fmt"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	kubeListers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerAgentName = "devops-secret-controller"

// Controller is a subcontroller to manage project resources
// syncing and life-cycle
type Controller struct {
	controller.Interface
	// kubernetes API client
	kubeclientset kubernetes.Interface
	// Devops API client
	devopsclientset clientset.Interface
	// ThirdParty
	thirdParty externalthirdparty.ThirdParty

	// Data sync for projects
	secretLister kubeListers.SecretLister
	secretSynced cache.InformerSynced

	configMapLister kubeListers.ConfigMapLister

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// New constructor for Project Controller
func New() *Controller {
	return &Controller{}
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

// Initialize to start basic initialization for projects
// controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	k8sFactory, _ := base.GetInformerFactory()
	c.thirdParty = base.GetThirdParty()

	// **** kubernetes resources
	// secret
	secretInformer := k8sFactory.Core().V1().Secrets()
	c.secretLister = secretInformer.Lister()
	c.secretSynced = secretInformer.Informer().HasSynced

	// configmap
	configMapInformer := k8sFactory.Core().V1().ConfigMaps()
	c.configMapLister = configMapInformer.Lister()

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Secrets")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// secrets
	secretInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(old, new interface{}) {
			oldSecret := old.(*corev1.Secret)
			newSecret := new.(*corev1.Secret)

			if newSecret.Type != devopsv1alpha1.SecretTypeOAuth2 {
				return
			}
			glog.V(6).Infof("old version: %s; new version: %s", oldSecret.GetResourceVersion(), newSecret.GetResourceVersion())

			c.enqueueSecret(new)
		},
	})
}

func (c *Controller) enqueueSecret(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}
	c.workqueue.AddRateLimited(key)
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "Secret"
}

// Start should start the controller
func (c *Controller) Start() {
	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the listers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.secretSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// / processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		glog.Infof("will handle this key: %s", key)
		// Run the syncHandler, passing it the namespace/name string of the
		// Project resource to be synced.
		if err := c.syncHandler(key); err != nil {
			return fmt.Errorf("error syncing '%s': %s", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced project '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

func (c *Controller) syncHandler(key string) error {
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	glog.V(3).Infof("syncing secret: namespace: %v, name: %v, err: %v", namespace, name, err)

	secret, err := c.secretLister.Secrets(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("secret '%s' in work queue no longer exists", key))
			return nil
		}
		return err
	}

	// OwnerReferences will become empty when service is deleted, reset the secret when it was happened.
	ownerReferences := secret.GetOwnerReferences()
	if ownerReferences == nil || len(ownerReferences) == 0 {
		glog.V(3).Infof("no ownerReferences in secret '%s', reset it", secret.GetName())
		return c.resetSecret(secret)
	}

	var codeRepoService *devopsv1alpha1.CodeRepoService
	for _, ownerRef := range ownerReferences {
		if ownerRef.Kind == devopsv1alpha1.TypeCodeRepoService {
			codeRepoService, err = c.devopsclientset.DevopsV1alpha1().CodeRepoServices().Get(ownerRef.Name, metav1.GetOptions{})
			if err != nil {
				glog.Errorf("getting codereposervice '%s', err: %v", ownerRef.Name, err)
				return err
			}
		}
	}
	// Stop the access token process followed only if codeRepoService is not found in ownerReferences,
	if codeRepoService == nil {
		glog.V(3).Infof("will not access token because codereposervice is not found in ownerReferences")
		return nil
	}

	// Access token and update the new info to secret
	serviceClient, err := c.thirdParty.CodeRepoServiceClientFactory.GetCodeRepoServiceClient(codeRepoService, secret)
	if err != nil {
		return err
	}
	redirectUrl := k8s.GetOAuthRedirectUrl(c.configMapLister)
	secretCopy, err := serviceClient.AccessToken(redirectUrl)
	if err != nil {
		return fmt.Errorf("error happens when accessing token in secret '%s', err: %v", secret.Name, err)
	}

	if secretCopy == nil {
		glog.V(3).Infoln("Secret has no change, skip...")
		return nil
	}

	c.updateSecret(secretCopy)

	return err
}

func (c *Controller) updateSecret(secret *corev1.Secret) (err error) {
	glog.V(5).Infoln("update secret in controller")
	_, err = c.kubeclientset.CoreV1().Secrets(secret.GetNamespace()).Update(secret)
	if err != nil {
		glog.Errorf("error updating secret '%s'", secret.Name)
	}
	return
}

func (c *Controller) resetSecret(secret *corev1.Secret) (err error) {
	secretCopy := secret.DeepCopy()
	secretCopy.ObjectMeta.OwnerReferences = nil
	secretCopy.Data = make(map[string][]byte, 0)
	secretCopy.Data[devopsv1alpha1.OAuth2ClientIDKey] = secret.Data[devopsv1alpha1.OAuth2ClientIDKey]
	secretCopy.Data[devopsv1alpha1.OAuth2ClientSecretKey] = secret.Data[devopsv1alpha1.OAuth2ClientSecretKey]
	return c.updateSecret(secretCopy)
}
