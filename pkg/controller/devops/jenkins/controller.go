package jenkins

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	controller "alauda.io/devops-apiserver/pkg/controller"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	coreListers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerAgentName = "devops-jenkins-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing = "ErrSyncing"
	// ErrResourceExists is used as part of the Event 'reason' when a Project fails
	// to sync due to a Namespace of the same name already existing.
	ErrResourceExists = "ErrResourceExists"
	// ErrProjectSpecInvalid error when controller finds an invalid project spec
	ErrProjectSpecInvalid = "ErrProjectSpecInvalid"

	// MessageResourceSynced is the message used for an Event fired when a Project
	// is synced successfully
	MessageResourceSynced = "Jenkins synced successfully"
	// MessageJenkinsBindingSynced is the message used for an Event fired when a Project
	// is synced successfully
	MessageJenkinsBindingSynced = "JenkinsBinding synced successfully"

	// MessageProjectUpdated event mesage for when a full project update was performed
	MessageProjectUpdated = "Project updated successfully"
	// MessageProjectSyncErr message for syncing issue
	MessageProjectSyncErr = "Project sync error: %s"
)

// Controller is a subcontroller to manage project resources
// syncing and life-cycle
type Controller struct {
	// kubernetes API client
	kubeclientset kubernetes.Interface
	// Devops API client
	devopsclientset clientset.Interface

	// Data sync for jenkins
	jenkinsLister listers.JenkinsLister
	jenkinsSynced cache.InformerSynced

	// Data sync for jenkinsbinding
	jenkinsBindingLister listers.JenkinsBindingLister
	jenkinsBindingSynced cache.InformerSynced

	// Data sync for secrets
	secretLister coreListers.SecretLister
	secretSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

// Initialize to start basic initialization for projects
// controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	k8sFactory, devopsFactory := base.GetInformerFactory()

	// devops jenkins
	jenkinsInformer := devopsFactory.Devops().V1alpha1().Jenkinses()
	c.jenkinsLister = jenkinsInformer.Lister()
	c.jenkinsSynced = jenkinsInformer.Informer().HasSynced

	// devops jenkinsbinding
	jenkinsBindingInformer := devopsFactory.Devops().V1alpha1().JenkinsBindings()
	c.jenkinsBindingLister = jenkinsBindingInformer.Lister()
	c.jenkinsBindingSynced = jenkinsBindingInformer.Informer().HasSynced

	// k8s secret
	secretInformer := k8sFactory.Core().V1().Secrets()
	c.secretLister = secretInformer.Lister()
	c.secretSynced = secretInformer.Informer().HasSynced

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Jenkins")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting events
	// jenkins
	jenkinsInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueJenkins,
		UpdateFunc: func(old, new interface{}) {
			c.enqueueJenkins(new)
		},
		DeleteFunc: c.enqueueJenkins,
	})

	// jenkinsbinding
	jenkinsBindingInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueJenkinsBinding,
		UpdateFunc: func(old, new interface{}) {
			c.enqueueJenkinsBinding(new)
		},
		DeleteFunc: c.enqueueJenkinsBinding,
	})
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "Jenkins"
}

// Start should start the controller
func (c *Controller) Start() {
	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the listers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.jenkinsSynced, c.jenkinsBindingSynced, c.secretSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		var err error
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		glog.Infof("will handle this key: %s", key)
		spl := strings.Split(key, "|")
		objType := devopsv1alpha1.TypeJenkins
		if len(spl) == 2 {
			objType = spl[0]
			key = spl[1]
		}

		switch objType {
		case devopsv1alpha1.TypeJenkins:
			// Run the syncJenkinsHandler, passing it the name string of the
			// Jenkins resource to be synced.
			err = c.syncJenkinsHandler(key)
		case devopsv1alpha1.TypeJenkinsBinding:
			// Run the syncJenkinsBindingHandler, passing it the name string of the
			// Jenkinsbinding resource to be synced.
			err = c.syncJenkinsBindingHandler(key)
		}
		if err != nil {
			return fmt.Errorf("error syncing '%s'|'%s': %s", objType, key, err.Error())
		}

		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced jenkins '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

// syncJenkinsHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the Jenkins resource
// with the current status of the resource.
func (c *Controller) syncJenkinsHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	_, name, err := cache.SplitMetaNamespaceKey(key)
	glog.V(4).Infof("syncing jenkins: name: %v, err: %v", name, err)

	jenkins, err := c.jenkinsLister.Get(name)
	if err != nil {
		// The Project resource may no longer exist, in which case we stop
		// processing.
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("jenkins '%s' in work queue no longer exists", key))
			return nil
		}
		return err
	}
	glog.V(4).Infof("got jenkins instance: %v", jenkins)
	if jenkins.Status.HTTPStatus != nil &&
		jenkins.Status.HTTPStatus.LastAttempt != nil &&
		jenkins.Status.HTTPStatus.LastAttempt.Add(time.Minute*5).After(time.Now()) {
		glog.V(4).Infof("jenkins was checked in the last 5 minutes...: %v", jenkins)
		return err
	}
	jenkinsCopy := jenkins.DeepCopy()
	jenkinsCopy.Status.HTTPStatus, err = c.checkJenkins(jenkinsCopy.Spec.HTTP.Host, "", "")
	if err != nil {
		runtime.HandleError(err)
		glog.V(4).Infof("Jenkins host returned error: %v", err)
		jenkinsCopy.Status.Status = devopsv1alpha1.StatusError
	} else {
		jenkinsCopy.Status.Status = devopsv1alpha1.StatusReady
	}

	err = c.updateJenkins(jenkinsCopy)
	return err
}

func (c *Controller) updateJenkins(jenk *devopsv1alpha1.Jenkins) (err error) {
	// proj must be a copy of the original and modified
	_, err = c.devopsclientset.DevopsV1alpha1().Jenkinses().Update(jenk)
	c.recorder.Event(jenk, corev1.EventTypeNormal, SuccessSynced, MessageResourceSynced)
	return
}

// syncJenkinsBindingHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the JenkinsBinding resource
// with the current status of the resource.
func (c *Controller) syncJenkinsBindingHandler(key string) (err error) {
	var (
		namespace, name                      string
		hasErr, shouldSync                   bool
		conditionShouldSync, conditionHasErr bool

		jenkinsBinding, jenkinsBindingCopy *devopsv1alpha1.JenkinsBinding
		jenkins                            *devopsv1alpha1.Jenkins
		secret                             *corev1.Secret
		syncPeriod                         = time.Minute * 5
		now                                = time.Now()
		metaTime                           = metav1.NewTime(now)
		resource                           metav1.Object
		condition                          devopsv1alpha1.JenkinsBindingCondition
		index                              int
	)
	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err = cache.SplitMetaNamespaceKey(key)
	glog.V(4).Infof("JenkinsBinding[%s] syncing jenkinsbinding: namespace: %v, name: %v, err: %v", key, namespace, name, err)

	jenkinsBinding, err = c.jenkinsBindingLister.JenkinsBindings(namespace).Get(name)
	if err != nil {
		// The Project resource may no longer exist, in which case we stop
		// processing.
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("jenkinsbinding '%s' in work queue no longer exists", key))
			err = nil
		}
		return
	}
	glog.V(4).Infof("JenkinsBinding[%s] got jenkinsbinding instance: %v", key, jenkinsBinding)
	jenkinsBindingCopy = jenkinsBinding.DeepCopy()
	if jenkinsBindingCopy.Status.Conditions == nil {
		jenkinsBindingCopy.Status.Conditions = []devopsv1alpha1.JenkinsBindingCondition{}
	}

	// Verifying Jenkins condition
	// this will get the information for the resource
	// and fill the condition information accordinly
	condition, index, resource, conditionShouldSync, conditionHasErr = checkResourceCondition(
		jenkinsBindingCopy.Status.Conditions,
		devopsv1alpha1.JenkinsBindingStatusTypeJenkins,
		jenkinsBindingCopy.Spec.Jenkins.Name,
		func() (metav1.Object, error) {
			return c.jenkinsLister.Get(jenkinsBindingCopy.Spec.Jenkins.Name)
		},
		syncPeriod,
	)
	glog.V(7).Infof(
		"JenkinsBinding[%s] condition result: condition: %v index: %d, resource: %v, shouldSync: %v, hasErr: %v",
		key, condition, index, resource, conditionShouldSync, conditionHasErr,
	)
	if resource != nil {
		jenkins = resource.(*devopsv1alpha1.Jenkins)
	}
	shouldSync = shouldSync || conditionShouldSync
	hasErr = hasErr || conditionHasErr
	if conditionShouldSync {
		condition.LastAttempt = &metaTime
		if index < 0 {
			jenkinsBindingCopy.Status.Conditions = append(jenkinsBindingCopy.Status.Conditions, condition)
		} else {
			jenkinsBindingCopy.Status.Conditions[index] = condition
		}
	}

	// Verifying Secret condition
	// this will get the information for the resource
	// and fill the condition information accordinly
	condition, index, resource, conditionShouldSync, conditionHasErr = checkResourceCondition(
		jenkinsBindingCopy.Status.Conditions,
		devopsv1alpha1.JenkinsBindingStatusTypeSecret,
		jenkinsBindingCopy.Spec.Account.Secret.Name,
		func() (metav1.Object, error) {
			return c.secretLister.
				Secrets(jenkinsBindingCopy.GetNamespace()).
				Get(jenkinsBindingCopy.Spec.Account.Secret.Name)
		},
		syncPeriod,
	)
	glog.V(7).Infof(
		"JenkinsBinding[%s] condition result: condition: %v index: %d, resource: %v, shouldSync: %v, hasErr: %v",
		key, condition, index, resource, conditionShouldSync, conditionHasErr,
	)
	if resource != nil {
		secret = resource.(*corev1.Secret)
	}
	shouldSync = shouldSync || conditionShouldSync
	hasErr = hasErr || conditionHasErr
	if conditionShouldSync {
		condition.LastAttempt = &metaTime
		if index < 0 {
			jenkinsBindingCopy.Status.Conditions = append(jenkinsBindingCopy.Status.Conditions, condition)
		} else {
			jenkinsBindingCopy.Status.Conditions[index] = condition
		}
	}

	// if both are not nil we can use the credentials
	// to send a request to the jenkins server
	if jenkins != nil &&
		secret != nil &&
		(jenkinsBindingCopy.Status.HTTPStatus == nil ||
			isExpired(jenkinsBindingCopy.Status.HTTPStatus.LastAttempt, now, syncPeriod)) {
		if jenkinsBindingCopy.Spec.Account.Secret.UsernameKey == "" ||
			jenkinsBindingCopy.Spec.Account.Secret.APITokenKey == "" {
			hasErr = true
		}

		url := jenkins.Spec.HTTP.Host
		username := secret.Data[jenkinsBindingCopy.Spec.Account.Secret.UsernameKey]
		password := secret.Data[jenkinsBindingCopy.Spec.Account.Secret.APITokenKey]
		glog.V(7).Infof("JenkinsBinding[%s] checking: %s - %s - %s", url, username, password)

		// using the secret data to request
		// jenkins and fetch some data to check health
		jenkinsBindingCopy.Status.HTTPStatus, err = c.checkJenkins(url, string(username), string(password))
		if err != nil {
			hasErr = true
		}

	}
	if hasErr {
		jenkinsBindingCopy.Status.Status = devopsv1alpha1.StatusError
	} else {
		jenkinsBindingCopy.Status.Status = devopsv1alpha1.StatusReady
	}
	if hasErr || shouldSync {
		_, err = c.devopsclientset.DevopsV1alpha1().JenkinsBindings(jenkinsBindingCopy.GetNamespace()).Update(jenkinsBindingCopy)
		if err != nil {
			runtime.HandleError(err)
		} else {
			c.recorder.Event(jenkinsBindingCopy, corev1.EventTypeNormal, SuccessSynced, MessageJenkinsBindingSynced)
		}
	}
	return
}

// TODO: move client logic to some other place
func (c *Controller) checkJenkins(url, username, password string) (status *devopsv1alpha1.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	// prepare
	status = &devopsv1alpha1.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	http.DefaultClient.Timeout = time.Second * 30
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}
	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
	}

	resp, err = http.DefaultClient.Do(req)
	if resp != nil {
		status.StatusCode = int(resp.StatusCode)
		data, err := ioutil.ReadAll(resp.Body)
		if data != nil {
			status.Response = string(data)
		} else {
			status.Response = ""
		}
		if err != nil {
			runtime.HandleError(err)
		}
		status.Version = resp.Header.Get("X-Jenkins")
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

// enqueueJenkins takes a Jenkins resource and converts it into a type|namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than Jenkins.
func (c *Controller) enqueueJenkins(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypeJenkins, key)
	c.workqueue.AddRateLimited(key)
}

// enqueueJenkinsBinding takes a Project resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than JenkinsBinding.
func (c *Controller) enqueueJenkinsBinding(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypeJenkinsBinding, key)
	c.workqueue.AddRateLimited(key)
}

func getJenkinsCondition(conds []devopsv1alpha1.JenkinsBindingCondition, conditionType string) (cond devopsv1alpha1.JenkinsBindingCondition, index int) {
	index = -1
	cond = devopsv1alpha1.JenkinsBindingCondition{Type: conditionType}
	if len(conds) == 0 {
		return
	}
	for i, x := range conds {
		if x.Type == conditionType {
			cond = x
			index = i
			return
		}
	}
	return
}

func checkResourceCondition(conditions []devopsv1alpha1.JenkinsBindingCondition, conditionType string, resourceName string, getResource func() (metav1.Object, error), syncPeriod time.Duration) (condition devopsv1alpha1.JenkinsBindingCondition, index int, obj metav1.Object, shouldSync, hasErr bool) {
	now := time.Now()
	var err error
	condition, index = getJenkinsCondition(conditions, conditionType)
	if index < 0 || isExpired(condition.LastAttempt, now, syncPeriod) {
		shouldSync = true
		obj, err = getResource()
		glog.V(7).Infof("JenkinsBinding[%s] get resource result: %v err %v", resourceName, obj, err)
		if resourceName == "" {
			hasErr = true
			condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotValid
			condition.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotValid
			condition.Message = "Resource name not provided"
		} else if errors.IsNotFound(err) {
			hasErr = true
			condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			condition.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			condition.Message = err.Error()
		} else if obj != nil {
			condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusReady
			condition.Reason = ""
			condition.Message = ""
		}
	}
	return
}

func isExpired(last *metav1.Time, now time.Time, period time.Duration) bool {
	return last == nil || last.Add(period).Before(now)
}

// New constructor for Project Controller
func New() *Controller {
	return &Controller{}
}
