package coderepo

import (
	"alauda.io/devops-apiserver/pkg/util/generic"
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	"github.com/mitchellh/mapstructure"
	"strings"
	"time"

	// resources defined in devops or native k8s
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1" // devops resource, eg: CodeRepoService/PipelineConfig...
	corev1 "k8s.io/api/core/v1"                                          // k8s native resource, eg: Service/Secret...
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"                        // k8s meta info, eg: ObjectMeta/DeleteOptions...

	// used to operate resources in devops or native k8s
	devopsClientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	kubeClientset "k8s.io/client-go/kubernetes"

	// used to get the resources in devops or native k8s
	devopsListers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	kubeListers "k8s.io/client-go/listers/core/v1"

	"alauda.io/devops-apiserver/pkg/controller"
	"alauda.io/devops-apiserver/pkg/util/k8s"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerAgentName = "devops-coderepo-controller"

const (
	SuccessSynced = "Synced"

	MessageCodeRepoServiceSynced = "CodeRepoService synced successfully"
	MessageCodeRepoBindingSynced = "CodeRepoBinding synced successfully"
	MessageCodeRepositorySynced  = "CodeRepository synced successfully"
)

type Controller struct {
	// kubernetes API client
	kubeclientset kubeClientset.Interface
	// Devops API client
	devopsclientset devopsClientset.Interface
	// ThirdParty
	thirdParty externalthirdparty.ThirdParty

	// Data sync for codeRepoService
	codeRepoServiceLister devopsListers.CodeRepoServiceLister
	codeRepoServiceSynced cache.InformerSynced

	// Data sync for codeRepoBinding
	codeRepoBindingLister devopsListers.CodeRepoBindingLister
	codeRepoBindingSynced cache.InformerSynced

	// Data sync for codeRepository
	codeRepositoryLister devopsListers.CodeRepositoryLister
	codeRepositorySynced cache.InformerSynced

	// Data sync for secrets
	secretLister kubeListers.SecretLister
	secretSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

func New() *Controller {
	return &Controller{}
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

// Initialize to start basic initialization for codeRepo controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()
	c.thirdParty = base.GetThirdParty()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	k8sFactory, devopsFactory := base.GetInformerFactory()

	// devops CodeRepoServices
	codeRepoServiceInformer := devopsFactory.Devops().V1alpha1().CodeRepoServices()
	c.codeRepoServiceLister = codeRepoServiceInformer.Lister()
	c.codeRepoServiceSynced = codeRepoServiceInformer.Informer().HasSynced

	// devops CodeRepoBindings
	codeRepoBindingInformer := devopsFactory.Devops().V1alpha1().CodeRepoBindings()
	c.codeRepoBindingLister = codeRepoBindingInformer.Lister()
	c.codeRepoBindingSynced = codeRepoBindingInformer.Informer().HasSynced

	// devops CodeRepositories
	codeRepositoryInformer := devopsFactory.Devops().V1alpha1().CodeRepositories()
	c.codeRepositoryLister = codeRepositoryInformer.Lister()
	c.codeRepositorySynced = codeRepositoryInformer.Informer().HasSynced

	// k8s secret
	secretInformer := k8sFactory.Core().V1().Secrets()
	c.secretLister = secretInformer.Lister()
	c.secretSynced = secretInformer.Informer().HasSynced

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "CodeRepo")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting events
	// codeRepoService
	codeRepoServiceInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueCodeRepoService,
		UpdateFunc: func(old, new interface{}) {
			glog.V(7).Infof("old codeRepoService: %; new codeRepoService: %s", old, new)
			c.enqueueCodeRepoService(new)
		},
		DeleteFunc: c.enqueueCodeRepoService,
	})

	// codeRepoBinding
	codeRepoBindingInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(new interface{}) {
			var (
				oldConfig *devopsv1alpha1.CodeRepoBinding
				newConfig *devopsv1alpha1.CodeRepoBinding
				ok        bool
			)

			if newConfig, ok = new.(*devopsv1alpha1.CodeRepoBinding); !ok {
				glog.Errorf("The format of new codeRepoBinding is invalid. value: %v", newConfig)
				return
			}
			go c.updateOwnerReferenceInSecret(oldConfig, newConfig)
			c.enqueueCodeRepoBinding(new)
		},
		UpdateFunc: func(old, new interface{}) {
			var (
				oldConfig *devopsv1alpha1.CodeRepoBinding
				newConfig *devopsv1alpha1.CodeRepoBinding
				ok        bool
			)

			if oldConfig, ok = old.(*devopsv1alpha1.CodeRepoBinding); !ok {
				glog.Errorf("The format of old codeRepoBinding is invalid. value: %v", newConfig)
				return
			}
			if newConfig, ok = new.(*devopsv1alpha1.CodeRepoBinding); !ok {
				glog.Errorf("The format of new codeRepoBinding is invalid. value: %v", newConfig)
				return
			}
			go c.updateOwnerReferenceInSecret(oldConfig, newConfig)
			c.enqueueCodeRepoBinding(new)
		},
		DeleteFunc: c.enqueueCodeRepoBinding,
	})

	// codeRepository
	codeRepositoryInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueCodeRepository,
		UpdateFunc: func(old, new interface{}) {
			glog.V(7).Infof("old codeRepository: %; new codeRepository: %s", old, new)

			c.enqueueCodeRepository(new)
		},
		DeleteFunc: c.enqueueCodeRepository,
	})
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "CodeRepo"
}

// Start should start the controller
func (c *Controller) Start() {
	go c.initConfigmap()

	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the devopsListers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.codeRepoServiceSynced, c.codeRepoBindingSynced, c.codeRepositorySynced, c.secretSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		var err error
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %v", obj))
			return nil
		}
		glog.Infof("will handle this key: %s", key)
		spl := strings.Split(key, "|")
		objType := devopsv1alpha1.TypeCodeRepoService
		if len(spl) == 2 {
			objType = spl[0]
			key = spl[1]
		}

		switch objType {
		case devopsv1alpha1.TypeCodeRepoService:
			// Run the syncCodeRepoServiceHandler, passing it the name string of the
			// CodeRepoService resource to be synced.
			err = c.syncCodeRepoServiceHandler(key)
		case devopsv1alpha1.TypeCodeRepoBinding:
			// Run the syncCodeRepoBindingHandler, passing it the name string of the
			// CodeRepoBinding resource to be synced.
			err = c.syncCodeRepoBindingHandler(key)
		case devopsv1alpha1.TypeCodeRepository:
			// Run the syncCodeRepositoryHandler, passing it the name string of the
			// CodeRepository resource to be synced.
			err = c.syncCodeRepositoryHandler(key)
		}

		if err != nil {
			return fmt.Errorf("error syncing '%s'|'%s': %s", objType, key, err.Error())
		}

		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced codeRepo '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

// region init
func (c *Controller) initGithubServiceInConfigmap(domainMap map[string]interface{}) error {
	if githubCreated, _ := domainMap[devopsv1alpha1.SettingsKeyGithubCreated]; githubCreated != "true" {
		serviceList, err := c.codeRepoServiceLister.List(labels.Everything())
		if err != nil {
			glog.Errorf("get service list, err: %v", err)
			return err
		}

		glog.Infof("check whether github service exist")
		var githubCreated bool
		for _, service := range serviceList {
			if service.Spec.Type == devopsv1alpha1.CodeRepoServiceTypeGithub {
				githubCreated = true
				break
			}
		}
		glog.Infof("github service exist: %v", githubCreated)

		if !githubCreated {
			githubService := devopsv1alpha1.GetDefaultGithubService()
			_, err := c.devopsclientset.DevopsV1alpha1().CodeRepoServices().Create(githubService)
			if err != nil {
				glog.Errorf("create default github service, err: %v", err)
			}
		}
		domainMap[devopsv1alpha1.SettingsKeyGithubCreated] = true
	}
	return nil
}

func (c *Controller) initToolChainsInConfigmap(domainMap map[string]interface{}) error {
	var (
		defaultToolChains = devopsv1alpha1.GetDefaultToolChains()
		storedToolChains  = make([]*devopsv1alpha1.ToolChainElement, 0)
	)

	if toolChains, ok := domainMap[devopsv1alpha1.SettingsKeyToolChains]; ok && toolChains != nil {
		err := mapstructure.Decode(toolChains, &storedToolChains)
		if err != nil {
			glog.Errorf("decode the tool chain, err: %v", err)
			return err
		}
	}

	mergedChain := c.mergeToolChainElements(defaultToolChains, storedToolChains)
	domainMap[devopsv1alpha1.SettingsKeyToolChains] = mergedChain

	return nil
}

func (c *Controller) mergeToolChainElements(
	defaultToolChain []*devopsv1alpha1.ToolChainElement,
	storedToolChain []*devopsv1alpha1.ToolChainElement,
) []*devopsv1alpha1.ToolChainElement {
	var (
		mergedToolChain = make([]*devopsv1alpha1.ToolChainElement, 0)
	)

	if len(storedToolChain) == 0 {
		return defaultToolChain
	}

	for _, defaultToolElement := range defaultToolChain {
		var foundToolElement *devopsv1alpha1.ToolChainElement
		for _, storedTool := range storedToolChain {
			if defaultToolElement.Name == storedTool.Name {
				foundToolElement = storedTool
				break
			}
		}

		// append new Element
		if foundToolElement == nil {
			glog.V(5).Infof("-- append new element: '%s'", defaultToolElement.Name)
			mergedToolChain = append(mergedToolChain, defaultToolElement)
			break
		}

		glog.V(5).Infof("-- update old element: '%s'", defaultToolElement.Name)
		tmpItems := c.mergeToolChainItems(defaultToolElement, foundToolElement)
		foundToolElement.Items = tmpItems
		mergedToolChain = append(mergedToolChain, foundToolElement)
	}
	return mergedToolChain
}

func (c *Controller) mergeToolChainItems(
	defaultToolElement *devopsv1alpha1.ToolChainElement,
	foundToolElement *devopsv1alpha1.ToolChainElement,
) []*devopsv1alpha1.ToolChainElementItem {
	if defaultToolElement.Items == nil {
		defaultToolElement.Items = make([]*devopsv1alpha1.ToolChainElementItem, 0)
	}

	if foundToolElement.Items == nil {
		foundToolElement.Items = make([]*devopsv1alpha1.ToolChainElementItem, 0)
	}

	// append old Element which was updated
	tmpItems := make([]*devopsv1alpha1.ToolChainElementItem, 0)
	for _, defaultToolItem := range defaultToolElement.Items {
		var tmpItem *devopsv1alpha1.ToolChainElementItem
		for _, foundToolItem := range foundToolElement.Items {
			if defaultToolItem.Name == foundToolItem.Name {
				tmpItem = foundToolItem
				break
			}
		}

		// add new item to Element
		if tmpItem == nil {
			glog.V(5).Infof("---- append new item '%s' to element '%s'", defaultToolItem.Name, foundToolElement.Name)
			tmpItems = append(tmpItems, defaultToolItem)
			break
		}

		glog.V(5).Infof("---- append old item '%s' to element '%s'", defaultToolItem.Name, foundToolElement.Name)
		tmpItems = append(tmpItems, tmpItem)
	}
	glog.V(5).Infof("------ total: %d items in '%s'", len(tmpItems), foundToolElement.Name)
	return tmpItems
}

func (c *Controller) initConfigmap() (err error) {
	var (
		domainMap           = make(map[string]interface{})
		devopsConfigMap     = k8s.GetDevopsConfigmap(c.kubeclientset)
		devopsConfigMapCopy = devopsConfigMap.DeepCopy()
	)

	if domain, ok := devopsConfigMapCopy.Data[devopsv1alpha1.SettingsKeyDomain]; ok && domain != "" {
		domainByte := []byte(domain)
		json.Unmarshal(domainByte, &domainMap)
	}

	// init some default configs to the configmap
	c.initGithubServiceInConfigmap(domainMap)
	c.initToolChainsInConfigmap(domainMap)

	b, err := json.Marshal(domainMap)
	if err != nil {
		glog.Errorf("marshal the domain, err: %v", err)
		return err
	}
	devopsConfigMapCopy.Data[devopsv1alpha1.SettingsKeyDomain] = string(b)

	_, err = c.kubeclientset.CoreV1().ConfigMaps(devopsv1alpha1.SettingsConfigMapNamespace).Update(devopsConfigMapCopy)
	if err != nil {
		glog.Errorf("Update configmap, err: %v", err)
	}
	return err
}

// endregion

// region update secret info when secret changed in binding
func (c *Controller) updateOwnerReferenceInSecret(old, new *devopsv1alpha1.CodeRepoBinding) error {
	if old == nil {
		return c.updateOwnerReferenceInSecretWhenBindingCreated(new)
	}
	return c.updateOwnerReferenceInSecretWhenBindingUpdated(old, new)
}

func (c *Controller) updateOwnerReferenceInSecretWhenBindingCreated(new *devopsv1alpha1.CodeRepoBinding) error {
	newSecret, err := c.secretLister.Secrets(new.Namespace).Get(new.Spec.Account.Secret.Name)
	if err != nil {
		glog.Errorf("error when get new secret %s, err: %v", new.Spec.Account.Secret.Name, err)
		return err
	}
	if newSecret.Type == devopsv1alpha1.SecretTypeOAuth2 {
		return c.addBindingOwnerReferenceInSecret(newSecret, new)
	}
	return nil
}

func (c *Controller) updateOwnerReferenceInSecretWhenBindingUpdated(old, new *devopsv1alpha1.CodeRepoBinding) error {
	newSecret, err := c.secretLister.Secrets(new.Namespace).Get(new.Spec.Account.Secret.Name)
	if err != nil {
		glog.Errorf("error when get new secret %s, err: %v", new.Spec.Account.Secret.Name, err)
		return err
	}

	oldSecret, err := c.secretLister.Secrets(new.Namespace).Get(old.Spec.Account.Secret.Name)
	if err != nil {
		glog.Errorf("error when get old secret %s, err: %v", new.Spec.Account.Secret.Name, err)
		return err
	}

	if oldSecret.Type == newSecret.Type && oldSecret.Name == newSecret.Name {
		glog.V(5).Infof("secret has no change, skip...")
		return nil
	}

	glog.V(5).Infoln("secret has been changed")
	var err1, err2 error
	if oldSecret.Type == devopsv1alpha1.SecretTypeOAuth2 {
		err1 = c.removeBindingOwnerReferenceInSecret(oldSecret, old)
	}

	if newSecret.Type == devopsv1alpha1.SecretTypeOAuth2 {
		err2 = c.addBindingOwnerReferenceInSecret(newSecret, new)
	}

	if err1 != nil || err2 != nil {
		glog.Errorf("err1: %v; err2: %v", err1, err2)
		return fmt.Errorf("Error when update ownerReferences in secret. err1: %v; err2: %v", err1, err2)
	}
	return nil
}

func (c *Controller) removeBindingOwnerReferenceInSecret(secret *corev1.Secret, binding *devopsv1alpha1.CodeRepoBinding) error {
	glog.V(5).Infoln("remove binding refer %s in secret %s", binding.GetName(), secret.GetName())

	newSecretCopy := secret.DeepCopy()
	if len(newSecretCopy.OwnerReferences) == 0 {
		return nil
	}

	newSecretCopy.OwnerReferences = make([]metav1.OwnerReference, 0)
	for _, value := range secret.OwnerReferences {
		if value.Kind == devopsv1alpha1.TypeCodeRepoBinding && value.Name == binding.GetName() {
			continue
		}
		newSecretCopy.OwnerReferences = append(newSecretCopy.OwnerReferences, value)
	}
	return c.updateSecret(newSecretCopy)
}

func (c *Controller) addBindingOwnerReferenceInSecret(secret *corev1.Secret, binding *devopsv1alpha1.CodeRepoBinding) error {
	glog.V(5).Infoln("add binding refer %s in secret %s", binding.GetName(), secret.GetName())

	newSecretCopy := secret.DeepCopy()
	if len(newSecretCopy.OwnerReferences) == 0 {
		newSecretCopy.OwnerReferences = make([]metav1.OwnerReference, 0)
	}

	newSecretCopy.OwnerReferences = append(newSecretCopy.OwnerReferences, metav1.OwnerReference{
		APIVersion:         devopsv1alpha1.APIVersionV1Alpha1,
		Kind:               devopsv1alpha1.TypeCodeRepoBinding,
		UID:                binding.GetUID(),
		Name:               binding.GetName(),
		Controller:         generic.Bool(false),
		BlockOwnerDeletion: generic.Bool(false),
	})
	return c.updateSecret(newSecretCopy)
}

func (c *Controller) updateSecret(secret *corev1.Secret) error {
	_, err := c.kubeclientset.CoreV1().Secrets(secret.Namespace).Update(secret)
	if err != nil {
		glog.Errorf("error when update secret, err: %v", err)
		return err
	}
	return nil
}

// endregion

// region CodeRepoService
// enqueueCodeRepoService takes a CodeRepoService resource and converts it into a type|namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than CodeRepoService.
func (c *Controller) enqueueCodeRepoService(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypeCodeRepoService, key)
	c.workqueue.AddRateLimited(key)
}

func (c *Controller) syncCodeRepoServiceHandler(key string) (err error) {
	_, name, err := cache.SplitMetaNamespaceKey(key)
	glog.Infof("syncing CodeRepoService %v, err: %v", name, err)

	service, err := c.codeRepoServiceLister.Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("service '%s' in work queue no longer exists", key))
			return nil
		}
		return err
	}

	if IsCheckedInLastAttempt(service.Status.HTTPStatus, devopsv1alpha1.TTLCheckCodeRepoService) {
		glog.Infof("CodeRepoBinding %s was checked in the last %v minutes", service.GetName(), devopsv1alpha1.TTLCheckCodeRepoService)
		return
	}

	serviceCopy := service.DeepCopy()
	serviceClient, err := c.thirdParty.CodeRepoServiceClientFactory.GetCodeRepoServiceClient(service, nil)
	if err != nil {
		return err
	}
	serviceCopy.Status.HTTPStatus, err = serviceClient.CheckAvailable(GetLastAttempt(serviceCopy.Status.HTTPStatus))
	glog.Infof("CodeRepoService %s check returned error: %v", service.GetName(), err)
	if err != nil || serviceCopy.Status.HTTPStatus.StatusCode >= 500 {
		serviceCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseError
		serviceCopy.Status.Message = serviceCopy.Status.HTTPStatus.Response
		runtime.HandleError(err)
	} else {
		serviceCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseReady
	}
	err = c.updateCodeRepoService(serviceCopy)
	return err
}

func (c *Controller) updateCodeRepoService(service *devopsv1alpha1.CodeRepoService) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().CodeRepoServices().Update(service)
	c.recorder.Event(service, corev1.EventTypeNormal, SuccessSynced, MessageCodeRepoServiceSynced)
	return
}

// endregion

// region CodeRepoBinding
// enqueueCodeRepoBinding takes a Project resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than CodeRepoBinding.
func (c *Controller) enqueueCodeRepoBinding(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypeCodeRepoBinding, key)
	c.workqueue.AddRateLimited(key)
}

func (c *Controller) syncCodeRepoBindingHandler(key string) (err error) {
	var (
		namespace, name                string
		secretErr, serviceErr, repoErr bool

		binding, bindingCopy *devopsv1alpha1.CodeRepoBinding
		oldRepoConditions    []devopsv1alpha1.BindingCondition
		service              *devopsv1alpha1.CodeRepoService
		secret               *corev1.Secret

		now = metav1.NewTime(time.Now())
	)

	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err = cache.SplitMetaNamespaceKey(key)
	glog.Infof("syncing CodeRepoBinding %v, namespace: %v, err: %v", name, namespace, err)

	binding, err = c.codeRepoBindingLister.CodeRepoBindings(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("coderepobinding '%s' in work queue no longer exists", key))
			err = nil
		}
		return
	}

	if IsCheckedInLastAttempt(binding.Status.HTTPStatus, devopsv1alpha1.TTLCheckCodeRepoBinding) {
		glog.Infof("CodeRepoBinding %s was checked in the last %v minutes", binding.GetName(), devopsv1alpha1.TTLCheckCodeRepoBinding)
		return
	}
	oldRepoConditions = c.backupRepoConditions(binding, oldRepoConditions)

	bindingCopy = binding.DeepCopy()
	bindingCopy.Status.Conditions = []devopsv1alpha1.BindingCondition{}

	// check secret
	glog.Infof("check the secret %s in binding %s", bindingCopy.Spec.Account.Secret.Name, bindingCopy.GetName())
	secret, secretErr = c.checkSecret(bindingCopy, now)

	// check service
	glog.Infof("check the service %s in binding %s", bindingCopy.Spec.CodeRepoService.Name, bindingCopy.GetName())
	service, secretErr = c.checkService(bindingCopy, secret)

	// get repos in binding
	glog.Infof("get sync repos in binding %s", bindingCopy.GetName())
	syncRepos, repoErr := c.getSyncRepos(bindingCopy)

	if secretErr || serviceErr || repoErr {
		c.restoreConditionsWhenErrorHappened(bindingCopy, oldRepoConditions)
	} else {
		c.syncReposToK8s(bindingCopy, service, syncRepos, now)
	}

	err = c.updateCodeRepoBinding(bindingCopy)
	if err != nil {
		runtime.HandleError(err)
	} else {
		c.recorder.Event(bindingCopy, corev1.EventTypeNormal, SuccessSynced, MessageCodeRepoBindingSynced)
	}

	return
}

func (c *Controller) restoreConditionsWhenErrorHappened(bindingCopy *devopsv1alpha1.CodeRepoBinding, oldRepoConditions []devopsv1alpha1.BindingCondition) {
	glog.Error("Some error happened, restore the conditions")
	bindingCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseError

	if len(oldRepoConditions) > 0 {
		bindingCopy.Status.Conditions = append(bindingCopy.Status.Conditions, oldRepoConditions...)
	}
}

func (c *Controller) syncReposToK8s(bindingCopy *devopsv1alpha1.CodeRepoBinding, service *devopsv1alpha1.CodeRepoService, syncRepos []devopsv1alpha1.OriginCodeRepository, metaTime metav1.Time) (err error) {
	glog.V(5).Info("sync repos defined in binding to k8s")
	bindingCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseReady

	// fetch all repos in the service
	selector := devopsv1alpha1.GetServiceSelector(service.GetName())
	localRepos, _ := c.codeRepositoryLister.CodeRepositories(bindingCopy.Namespace).List(selector)
	for _, syncRepo := range syncRepos {
		newLocalRepo := c.generateLocalRepo(syncRepo, bindingCopy)

		// add repo condition to status
		condition := devopsv1alpha1.BindingCondition{
			Type:        devopsv1alpha1.JenkinsBindingStatusTypeRepository,
			Name:        newLocalRepo.Name,
			LastAttempt: &metaTime,
			Message:     "",
			Reason:      "",
			Status:      devopsv1alpha1.JenkinsBindingStatusConditionStatusReady,
		}
		bindingCopy.Status.Conditions = append(bindingCopy.Status.Conditions, condition)

		// sync repo to k8s
		var foundRepo *devopsv1alpha1.CodeRepository
		for _, localRepo := range localRepos {
			if newLocalRepo.GetName() == localRepo.GetName() {
				foundRepo = localRepo
				break
			}
		}

		if foundRepo == nil {
			glog.Infof("creating CodeRepository %s", newLocalRepo.GetName())
			_, err = c.devopsclientset.DevopsV1alpha1().CodeRepositories(bindingCopy.Namespace).Create(&newLocalRepo)
			if err != nil {
				glog.Errorf("error when creating CodeRepository: %v", err)
			}
		} else {
			glog.Infof("updating CodeRepository %s", newLocalRepo.GetName())
			foundRepoCopy := foundRepo.DeepCopy()
			foundRepoCopy.Spec.Repository = newLocalRepo.Spec.Repository
			_, err = c.devopsclientset.DevopsV1alpha1().CodeRepositories(bindingCopy.Namespace).Update(foundRepoCopy)
			if err != nil {
				glog.Errorf("error when updating CodeRepository: %v", err)
			}
		}
	}
	return err
}

// getSyncRepos get the repos included in binding which will be sync.
func (c *Controller) getSyncRepos(bindingCopy *devopsv1alpha1.CodeRepoBinding) (syncRepos []devopsv1alpha1.OriginCodeRepository, repoErr bool) {
	syncRepos = []devopsv1alpha1.OriginCodeRepository{}
	opts := &devopsv1alpha1.CodeRepoBindingRepositoryOptions{}
	remoteRepositories, err := c.devopsclientset.DevopsV1alpha1().CodeRepoBindings(bindingCopy.Namespace).GetRemoteRepositories(bindingCopy.Name, opts)
	if err != nil {
		glog.Errorf("Error when fetch remote repositories, err: %v", err)
		repoErr = true
		bindingCopy.Status.Message = err.Error()
		return
	}

	for _, bindingOwner := range bindingCopy.Spec.Account.Owners {
		for _, remoteOwner := range remoteRepositories.Owners {
			if bindingOwner.Type == remoteOwner.Type && bindingOwner.Name == remoteOwner.Name {
				if bindingOwner.All {
					syncRepos = append(syncRepos, remoteOwner.Repositories...)
					continue
				}

				for _, bindingRepo := range bindingOwner.Repositories {
					for _, remoteRepo := range remoteOwner.Repositories {
						if bindingRepo == remoteRepo.Name {
							syncRepos = append(syncRepos, remoteRepo)
						}
					}
				}
			}
		}
	}

	return
}

// checkService check service defined in binding
func (c *Controller) checkService(bindingCopy *devopsv1alpha1.CodeRepoBinding, secret *corev1.Secret) (service *devopsv1alpha1.CodeRepoService, serviceErr bool) {
	service, err := c.codeRepoServiceLister.Get(bindingCopy.Spec.CodeRepoService.Name)
	if err != nil {
		glog.Errorf("Error when get codeRepoService %s, err: %v", bindingCopy.Spec.CodeRepoService.Name, err)
		serviceErr = true
		return
	}

	serviceClient, err := c.thirdParty.CodeRepoServiceClientFactory.GetCodeRepoServiceClient(service, secret)
	if err != nil {
		glog.Errorf("Error when get codeRepoServiceClient, err: %v", err)
		serviceErr = true
		return
	}

	bindingCopy.Status.HTTPStatus, err = serviceClient.CheckAuthentication(GetLastAttempt(bindingCopy.Status.HTTPStatus))
	if err != nil || bindingCopy.Status.HTTPStatus.StatusCode >= 300 {
		glog.Errorf("Error when checkAuthentication, err: %v", err)
		serviceErr = true
	}
	return
}

// generateLocalRepo generate local repo by sync repo defined
func (c *Controller) generateLocalRepo(syncRepo devopsv1alpha1.OriginCodeRepository, binding *devopsv1alpha1.CodeRepoBinding) (localRepo devopsv1alpha1.CodeRepository) {
	serviceName := binding.GetLabels()[devopsv1alpha1.LabelCodeRepoService]
	snippets := []string{serviceName}
	names := strings.Split(syncRepo.FullName, "/")
	for _, name := range names {
		snippets = append(snippets, name)
	}
	repoName := strings.Join(snippets, "-")

	localRepo = devopsv1alpha1.CodeRepository{
		TypeMeta: metav1.TypeMeta{
			Kind:       devopsv1alpha1.TypeCodeRepository,
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      repoName,
			Namespace: binding.GetNamespace(),
		},
		Spec: devopsv1alpha1.CodeRepositorySpec{
			CodeRepoBinding: devopsv1alpha1.LocalObjectReference{
				Name: binding.GetName(),
			},
			Repository: syncRepo,
		},
	}
	return
}

func (c *Controller) checkSecret(bindingCopy *devopsv1alpha1.CodeRepoBinding, metaTime metav1.Time) (*corev1.Secret, bool) {
	var (
		secretErr bool
		secretRef = bindingCopy.Spec.Account.Secret
	)
	glog.Infof("check the secret %s in binding %s", secretRef.Name, bindingCopy.GetName())
	secret, err := c.secretLister.Secrets(bindingCopy.GetNamespace()).Get(secretRef.Name)
	condition := devopsv1alpha1.BindingCondition{
		Name:        secretRef.Name,
		Type:        devopsv1alpha1.JenkinsBindingStatusTypeSecret,
		LastAttempt: &metaTime,
	}
	if err != nil {
		secretErr = true
		condition.Message = err.Error()
		if errors.IsNotFound(err) {
			condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			condition.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
		} else {
			condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotValid
			condition.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotValid
		}
	} else if secret != nil {
		condition.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusReady
		condition.Reason = ""
		condition.Message = ""
	}
	bindingCopy.Status.Conditions = append(bindingCopy.Status.Conditions, condition)
	return secret, secretErr
}

func (c *Controller) backupRepoConditions(binding *devopsv1alpha1.CodeRepoBinding, oldRepoConditions []devopsv1alpha1.BindingCondition) []devopsv1alpha1.BindingCondition {
	// backup the repo conditions, in case of some error happen
	if len(binding.Status.Conditions) > 0 {
		for _, condition := range binding.Status.Conditions {
			if condition.Type == devopsv1alpha1.JenkinsBindingStatusTypeRepository {
				oldRepoConditions = append(oldRepoConditions, condition)
			}
		}
	}
	return oldRepoConditions
}

func (c *Controller) updateCodeRepoBinding(binding *devopsv1alpha1.CodeRepoBinding) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().CodeRepoBindings(binding.GetNamespace()).Update(binding)
	c.recorder.Event(binding, corev1.EventTypeNormal, SuccessSynced, MessageCodeRepoBindingSynced)
	return
}

// endregion

// region CodeRepository

// enqueueCodeRepository takes a CodeRepository resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than CodeRepository.
func (c *Controller) enqueueCodeRepository(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypeCodeRepository, key)
	c.workqueue.AddRateLimited(key)
}

func (c *Controller) syncCodeRepositoryHandler(key string) (err error) {
	var (
		namespace, name            string
		foundBinding               *devopsv1alpha1.CodeRepoBinding
		repository, repositoryCopy *devopsv1alpha1.CodeRepository
	)

	namespace, name, err = cache.SplitMetaNamespaceKey(key)
	glog.Infof("syncing CodeRepository %s, namespace: %s, err: %v", name, namespace, err)

	repository, err = c.codeRepositoryLister.CodeRepositories(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("repository '%s' in work queue no longer exists", key))
			go c.disablePipelineConfigs(namespace, name)
			return nil
		}
		return
	}

	if IsCheckedInLastAttempt(repository.Status.HTTPStatus, devopsv1alpha1.TTLCheckCodeRepository) {
		glog.Infof("CodeRepository %s was checked in the last %v minutes", repository.GetName(), devopsv1alpha1.TTLCheckCodeRepository)
		return
	}

	repositoryCopy = repository.DeepCopy()
	foundBinding = c.findBindingReferToRepo(repositoryCopy)

	if foundBinding == nil {
		err = c.doSomethingIfNoBindingReferToRepo(repositoryCopy)
	} else {
		err = c.doSomethingIfAnyBindingReferToRepo(repositoryCopy, foundBinding)
	}
	if err != nil {
		return err
	}

	err = c.updateCodeRepository(repositoryCopy)
	return err
}

func (c Controller) doSomethingIfAnyBindingReferToRepo(repositoryCopy *devopsv1alpha1.CodeRepository, foundBinding *devopsv1alpha1.CodeRepoBinding) error {
	glog.Infof("Found binding %s contains the repo %s", foundBinding.GetName(), repositoryCopy.GetName())
	service, err := c.codeRepoServiceLister.Get(foundBinding.Spec.CodeRepoService.Name)
	if err != nil {
		return err
	}

	secretRef := foundBinding.Spec.Account.Secret
	secret, err := c.secretLister.Secrets(foundBinding.GetNamespace()).Get(secretRef.Name)
	if err != nil {
		return err
	}

	serviceClient, err := c.thirdParty.CodeRepoServiceClientFactory.GetCodeRepoServiceClient(service, secret)
	if err != nil {
		return err
	}
	repositoryCopy.Status.HTTPStatus, err = serviceClient.CheckRepositoryAvailable(
		repositoryCopy.GetRepoID(),
		repositoryCopy.GetRepoFullName(),
		GetLastAttempt(repositoryCopy.Status.HTTPStatus),
	)
	glog.Infof("Repository %s check returned error: %v", repositoryCopy.Name, err)
	statusCode := repositoryCopy.Status.HTTPStatus.StatusCode
	if err != nil || statusCode >= 400 {
		repositoryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseError
		repositoryCopy.Status.Message = repositoryCopy.Status.HTTPStatus.Response
		runtime.HandleError(err)
	} else {
		repositoryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseReady
	}
	return nil
}

func (c Controller) doSomethingIfNoBindingReferToRepo(repositoryCopy *devopsv1alpha1.CodeRepository) error {
	glog.Infof("Can not find binding contains the repo %s", repositoryCopy.GetName())
	pipelineConfigList, errPipe := c.getPipelineConfigs(repositoryCopy.GetNamespace(), repositoryCopy.GetName())
	if errPipe != nil || pipelineConfigList.Items == nil || len(pipelineConfigList.Items) == 0 {
		// Neither coderepobinding nor pielineconfig referenced to the repository, delete it
		glog.Infof("No pipeline refer to the repo, delete it")
		return c.devopsclientset.DevopsV1alpha1().CodeRepositories(repositoryCopy.GetNamespace()).Delete(repositoryCopy.GetName(), &metav1.DeleteOptions{})
	} else {
		// No coderepobinding but has pielineconfig, mark it as WaitingToDelete
		glog.Infof("but %v pipelines refer to the repo, change it's status to WaitToDelete", len(pipelineConfigList.Items))
		repositoryCopy.Status.Phase = devopsv1alpha1.ServiceStatusPhaseWaitingToDelete
	}
	return nil
}

func (c *Controller) findBindingReferToRepo(repositoryCopy *devopsv1alpha1.CodeRepository) (foundBinding *devopsv1alpha1.CodeRepoBinding) {
	selector := devopsv1alpha1.GetServiceSelector(repositoryCopy.GetServiceName())
	bindings, err := c.codeRepoBindingLister.CodeRepoBindings(repositoryCopy.Namespace).List(selector)
	if err != nil {
		glog.Errorf("Error when find bindings refer to repository %s, err: %v", repositoryCopy.GetName(), err)
		return
	}

	for _, binding := range bindings {
		if binding.Status.Conditions != nil {
			for _, condition := range binding.Status.Conditions {
				if condition.Name == repositoryCopy.GetName() {
					foundBinding = binding
					break
				}
			}
		}
	}
	return
}

func (c *Controller) updateCodeRepository(repository *devopsv1alpha1.CodeRepository) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().CodeRepositories(repository.Namespace).Update(repository)
	c.recorder.Event(repository, corev1.EventTypeNormal, SuccessSynced, MessageCodeRepositorySynced)
	return
}

// endregion

// getPipelineConfigs get all pipelineconfigs refered to the codeRepo
func (c *Controller) getPipelineConfigs(namespace, codeRepoName string) (*devopsv1alpha1.PipelineConfigList, error) {
	pipelineConfigList, err := c.devopsclientset.DevopsV1alpha1().PipelineConfigs(namespace).List(
		metav1.ListOptions{
			LabelSelector: fmt.Sprintf("%s=%s", devopsv1alpha1.LabelCodeRepository, codeRepoName),
		})
	if err != nil {
		return nil, err
	}
	return pipelineConfigList, nil
}

// disablePipelineConfigs disable the pipelineconfigs
func (c *Controller) disablePipelineConfigs(namespace, codeRepoName string) (err error) {
	//pipelineConfigList, err := c.getPipelineConfigs(namespace, codeRepoName)
	//if err != nil {
	//	return
	//}
	//var wg sync.WaitGroup
	//for _, pipelineConfig := range pipelineConfigList.Items {
	//	wg.Add(1)
	//	go func() {
	//		pipelineConfigCopy := pipelineConfig.DeepCopy()
	//		pipelineConfigCopy.Status.Phase = devopsv1alpha1.PipelineConfigPhaseDisabled
	//		c.devopsclientset.DevopsV1alpha1().PipelineConfigs(namespace).Update(pipelineConfigCopy)
	//		wg.Done()
	//	}()
	//}
	//wg.Wait()
	return
}

// IsCheckedInLastAttempt return true if checked in the last attempt
func IsCheckedInLastAttempt(status *devopsv1alpha1.HostPortStatus, ttlCheck time.Duration) bool {
	return status != nil && status.LastAttempt != nil && status.LastAttempt.Add(ttlCheck).After(time.Now())
}

// GetLastAttempt get the last attempt
func GetLastAttempt(status *devopsv1alpha1.HostPortStatus) *metav1.Time {
	if status == nil || status.LastAttempt == nil {
		return nil
	}
	return status.LastAttempt
}
