package coderepo_test

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/coderepo"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
	time "time"
)

func TestController(t *testing.T) {
	subController := coderepo.New()
	assert.Equal(t, subController.GetType(), "CodeRepo")
}

func TestIsCheckedInLastAttempt(t *testing.T) {
	type Table struct {
		name          string
		inputStatus   *devopsv1alpha1.HostPortStatus
		inputTTlCheck time.Duration
		expected      bool
	}
	tests := []Table{
		{
			name:          "status is nil",
			inputStatus:   nil,
			inputTTlCheck: 60 * time.Second,
			expected:      false,
		},
		{
			name: "LastAttempt is nil",
			inputStatus: &devopsv1alpha1.HostPortStatus{
				StatusCode:  200,
				LastAttempt: nil,
			},
			inputTTlCheck: 60 * time.Second,
			expected:      false,
		},
		{
			name: "checked in last attempt",
			inputStatus: &devopsv1alpha1.HostPortStatus{
				StatusCode:  200,
				LastAttempt: &metav1.Time{Time: time.Now()},
			},
			inputTTlCheck: 60 * time.Second,
			expected:      true,
		},
		{
			name: "not checked in last attempt",
			inputStatus: &devopsv1alpha1.HostPortStatus{
				StatusCode:  200,
				LastAttempt: &metav1.Time{Time: time.Now().Add(-100 * time.Second)},
			},
			inputTTlCheck: 60 * time.Second,
			expected:      false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expected, coderepo.IsCheckedInLastAttempt(test.inputStatus, test.inputTTlCheck))
		})
	}
}

func TestGetLastAttempt(t *testing.T) {
	now := &metav1.Time{Time: time.Now()}

	type Table struct {
		name     string
		input    *devopsv1alpha1.HostPortStatus
		expected *metav1.Time
	}
	tests := []Table{
		{
			name:     "status is nil",
			input:    nil,
			expected: nil,
		},
		{
			name: "LastAttempt is nil",
			input: &devopsv1alpha1.HostPortStatus{
				LastAttempt: nil,
			},
			expected: nil,
		},
		{
			name: "LastAttempt is not nil",
			input: &devopsv1alpha1.HostPortStatus{
				LastAttempt: now,
			},
			expected: now,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expected, coderepo.GetLastAttempt(test.input))
		})
	}
}
