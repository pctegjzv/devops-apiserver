package pipeline

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller"
	util "alauda.io/devops-apiserver/pkg/util"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerAgentName = "devops-pipeline-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing = "ErrSyncing"
	// ErrResourceExists is used as part of the Event 'reason' when a Project fails
	// to sync due to a Namespace of the same name already existing.
	ErrResourceExists = "ErrResourceExists"
	// ErrProjectSpecInvalid error when controller finds an invalid project spec
	ErrProjectSpecInvalid = "ErrProjectSpecInvalid"

	// ErrRenderJenkinsfile error rendering jenkinsfile
	ErrRenderJenkinsfile = "ErrRenderJenkinsfile"

	// MessageResourceSynced is the message used for an Event fired when a PipelineConfig
	// is synced successfully
	MessageResourceSynced = "PipelineConfig synced successfully"
	// MessageJenkinsBindingSynced is the message used for an Event fired when a Pipeline
	// is synced successfully
	MessageJenkinsBindingSynced = "Pipeline synced successfully"

	// MessageProjectUpdated event mesage for when a full project update was performed
	MessageProjectUpdated = "Project updated successfully"
	// MessageProjectSyncErr message for syncing issue
	MessageProjectSyncErr = "Project sync error: %s"
)

// Controller is a subcontroller to manage project resources
// syncing and life-cycle
type Controller struct {
	// kubernetes API client
	kubeclientset kubernetes.Interface
	// Devops API client
	devopsclientset clientset.Interface

	// Data sync for pipelineconfig
	piplineConfigLister  listers.PipelineConfigLister
	pipelineConfigSynced cache.InformerSynced

	// Data sync for pipeline
	pipelineLister listers.PipelineLister
	pipelineSynced cache.InformerSynced

	codeRepositoryLister  listers.CodeRepositoryLister
	codeRepoBindingLister listers.CodeRepoBindingLister

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

func typeof(v interface{}) string {
	return reflect.TypeOf(v).String()
}

// Initialize to start basic initialization for projects
// controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	_, devopsFactory := base.GetInformerFactory()

	// devops pipelineconfig
	pipelineConfigInformer := devopsFactory.Devops().V1alpha1().PipelineConfigs()
	c.piplineConfigLister = pipelineConfigInformer.Lister()
	c.pipelineConfigSynced = pipelineConfigInformer.Informer().HasSynced

	// devops pipeline
	pipelineInformer := devopsFactory.Devops().V1alpha1().Pipelines()
	c.pipelineLister = pipelineInformer.Lister()
	c.pipelineSynced = pipelineInformer.Informer().HasSynced

	codeRepositoryInformer := devopsFactory.Devops().V1alpha1().CodeRepositories()
	c.codeRepositoryLister = codeRepositoryInformer.Lister()

	codeRepoBindingInformer := devopsFactory.Devops().V1alpha1().CodeRepoBindings()
	c.codeRepoBindingLister = codeRepoBindingInformer.Lister()

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Pipeline")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting events for pipelineconfig
	pipelineConfigInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			var (
				oldConfig *devopsv1alpha1.PipelineConfig
				newConfig *devopsv1alpha1.PipelineConfig
				ok        bool
			)
			if newConfig, ok = obj.(*devopsv1alpha1.PipelineConfig); !ok {
				glog.Errorf("The format of new pipelineConfig is invalid. value: %v", newConfig)
				return
			}

			for _, hook := range newConfig.Spec.Hooks {
				glog.V(7).Infof("Events defined: %v", hook.Events)
				hasEvent := hook.HasPipelineEvent(devopsv1alpha1.PipelineEventConfigCreated)
				glog.V(7).Infof("Is there an add event: %v", hasEvent)
				if hasEvent {
					payload := devopsv1alpha1.PipelineConfigPayload{
						Event: devopsv1alpha1.PipelineEventConfigCreated,
						Data:  devopsv1alpha1.PipelineConfigData{Old: oldConfig, New: newConfig},
					}
					// async hook requests
					go c.triggerPipelineConfigHook(hook, payload, newConfig)
				}
			}
			c.enqueuePipelineConfig(obj)
		},
		UpdateFunc: func(old, new interface{}) {
			var (
				oldConfig *devopsv1alpha1.PipelineConfig
				newConfig *devopsv1alpha1.PipelineConfig
				ok        bool
			)

			if oldConfig, ok = old.(*devopsv1alpha1.PipelineConfig); !ok {
				glog.Errorf("The format of old pipelineConfig is invalid. value: %v", oldConfig)
				return
			}

			if newConfig, ok = new.(*devopsv1alpha1.PipelineConfig); !ok {
				glog.Errorf("The format of new pipelineConfig is invalid. value: %v", oldConfig)
				return
			}

			if !c.isPipelineConfigChanged(oldConfig, newConfig) {
				return
			}

			c.handlePipelineConfigHooks(oldConfig, newConfig)

			c.enqueuePipelineConfig(new)
		},
		DeleteFunc: func(obj interface{}) {
			var (
				oldConfig *devopsv1alpha1.PipelineConfig
				newConfig *devopsv1alpha1.PipelineConfig
				ok        bool
			)

			if oldConfig, ok = obj.(*devopsv1alpha1.PipelineConfig); !ok {
				glog.Errorf("The format of deleted pipelineConfig is invalid. value: %v", oldConfig)
				return
			}

			for _, hook := range oldConfig.Spec.Hooks {
				glog.V(7).Infof("Events defined: %v", hook.Events)
				hasEvent := hook.HasPipelineEvent(devopsv1alpha1.PipelineEventConfigDeleted)
				glog.V(7).Infof("Is there an delete event: %v", hasEvent)
				if hasEvent {
					payload := devopsv1alpha1.PipelineConfigPayload{
						Event: devopsv1alpha1.PipelineEventConfigDeleted,
						Data:  devopsv1alpha1.PipelineConfigData{Old: oldConfig, New: newConfig},
					}
					go c.triggerPipelineConfigHook(hook, payload, oldConfig)
				}
			}
		},
	})

	// pipeline
	pipelineInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			var (
				oldConfig *devopsv1alpha1.Pipeline
				newConfig *devopsv1alpha1.Pipeline
				ok        bool
			)
			if newConfig, ok = obj.(*devopsv1alpha1.Pipeline); !ok {
				glog.Errorf("The format of new pipeline is invalid. value: %v", newConfig)
				return
			}

			for _, hook := range newConfig.Spec.Hooks {
				hasEvent := hook.HasPipelineEvent(devopsv1alpha1.PipelineEventPipelineStarted)
				glog.V(7).Infof("Is there an start event: %v", hasEvent)
				if hasEvent {
					payload := devopsv1alpha1.PipelinePayload{
						Event: devopsv1alpha1.PipelineEventPipelineStarted,
						Data:  devopsv1alpha1.PipelineData{Old: oldConfig, New: newConfig},
					}
					go c.triggerPipelineHook(hook, payload, newConfig)
				}
			}
			c.enqueuePipeline(obj)
		},
		UpdateFunc: func(old, new interface{}) {
			var (
				oldConfig *devopsv1alpha1.Pipeline
				newConfig *devopsv1alpha1.Pipeline
				ok        bool
			)

			if oldConfig, ok = old.(*devopsv1alpha1.Pipeline); !ok {
				glog.Errorf("The format of old pipeline is invalid. value: %v", oldConfig)
				return
			}

			if newConfig, ok = new.(*devopsv1alpha1.Pipeline); !ok {
				glog.Errorf("The format of new pipeline is invalid. value: %v", oldConfig)
				return
			}

			if !c.isPipelineChanged(oldConfig, newConfig) {
				return
			}

			c.handlePipelineHooks(oldConfig, newConfig)

			c.enqueuePipeline(new)
		},
		DeleteFunc: func(obj interface{}) {
			c.enqueuePipeline(obj)
		},
	})
}

func (c *Controller) handlePipelineConfigHooks(oldConfig *devopsv1alpha1.PipelineConfig, newConfig *devopsv1alpha1.PipelineConfig) {
	if oldConfig.Spec.Hooks == nil || len(oldConfig.Spec.Hooks) == 0 {
		return
	}

	for _, hook := range oldConfig.Spec.Hooks {
		glog.V(7).Infof("Events defined: %v", hook.Events)
		hasEvent := hook.HasPipelineEvent(devopsv1alpha1.PipelineEventConfigUpdated)
		glog.V(7).Infof("Is there an update event: %v", hasEvent)
		if hasEvent {
			payload := devopsv1alpha1.PipelineConfigPayload{
				Event: devopsv1alpha1.PipelineEventConfigUpdated,
				Data:  devopsv1alpha1.PipelineConfigData{Old: oldConfig, New: newConfig},
			}
			c.triggerPipelineConfigHook(hook, payload, newConfig)
		}
	}
}

func (c *Controller) handlePipelineHooks(oldConfig *devopsv1alpha1.Pipeline, newConfig *devopsv1alpha1.Pipeline) {
	if oldConfig.Spec.Hooks == nil || len(oldConfig.Spec.Hooks) == 0 {
		return
	}

	for _, hook := range newConfig.Spec.Hooks {
		var event devopsv1alpha1.PipelineEvent
		if newConfig.Status.Aborted {
			event = devopsv1alpha1.PipelineEventPipelineStopped
		} else {
			event = devopsv1alpha1.PipelineEventPipelineStarted
		}
		hasEvent := hook.HasPipelineEvent(event)
		glog.V(7).Infof("Is there an update event: %v", hasEvent)
		if hasEvent {
			payload := devopsv1alpha1.PipelinePayload{
				Event: event,
				Data:  devopsv1alpha1.PipelineData{Old: oldConfig, New: newConfig},
			}
			c.triggerPipelineHook(hook, payload, newConfig)
		}
	}
}

const SUCC_TRIGGER_FORMAT = "Successfully trigger the hook. status: %s; err: %v"

func (c *Controller) isPipelineChanged(oldConfig, newConfig *devopsv1alpha1.Pipeline) (isChanged bool) {
	oldVersion, newVersion := oldConfig.GetResourceVersion(), newConfig.GetResourceVersion()
	glog.V(7).Infof("old version: %s; new version: %s", oldVersion, newVersion)
	if oldVersion == newVersion {
		glog.V(7).Infof("The version %s has no change", oldVersion)
		return
	}

	if newConfig.Status.Aborted == oldConfig.Status.Aborted {
		glog.V(6).Info("The status of pipeline has no change. skip...")
		return
	}

	isChanged = true
	return
}

func (c *Controller) isPipelineConfigChanged(oldConfig, newConfig *devopsv1alpha1.PipelineConfig) (isChanged bool) {
	oldVersion, newVersion := oldConfig.GetResourceVersion(), newConfig.GetResourceVersion()
	glog.V(7).Infof("old version: %s; new version: %s", oldVersion, newVersion)
	if oldVersion == newVersion {
		glog.V(7).Infof("The version %s has no change", oldVersion)
		return
	}

	oldNumber, newNumber := oldConfig.GetLastNumber(), newConfig.GetLastNumber()
	glog.V(7).Infof("old Number: %s; new Number: %s", oldNumber, newNumber)
	if oldNumber != newNumber {
		glog.V(7).Infof("The change was caused by pipeline start. skip... old number: %s; new number: %s",
			oldNumber, newNumber)
		return
	}

	isChanged = true
	return
}

func (c *Controller) triggerPipelineHook(
	hook devopsv1alpha1.PipelineHook,
	payload devopsv1alpha1.PipelinePayload,
	config *devopsv1alpha1.Pipeline) (err error) {
	status, err := c.sendHttpRequest(hook.HTTPRequest, payload)
	c.recorder.Eventf(config, corev1.EventTypeNormal, string(payload.Event),
		SUCC_TRIGGER_FORMAT, status, err)
	return err
}

func (c *Controller) triggerPipelineConfigHook(
	hook devopsv1alpha1.PipelineHook,
	payload devopsv1alpha1.PipelineConfigPayload,
	config *devopsv1alpha1.PipelineConfig) (err error) {
	status, err := c.sendHttpRequest(hook.HTTPRequest, payload)
	c.recorder.Eventf(config, corev1.EventTypeNormal, string(payload.Event),
		SUCC_TRIGGER_FORMAT, status, err)
	return err
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "Pipeline"
}

// Start should start the controller
func (c *Controller) Start() {
	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the listers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.pipelineConfigSynced, c.pipelineSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		var err error
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		glog.V(7).Infof("will handle this key: %s", key)
		spl := strings.Split(key, "|")
		objType := devopsv1alpha1.TypePipelineConfig
		if len(spl) == 2 {
			objType = spl[0]
			key = spl[1]
		}
		glog.V(7).Infof("sync objType: %s; key: %s", objType, key)

		switch objType {
		case devopsv1alpha1.TypePipelineConfig:
			err = c.syncPipelineConfigHandler(key)
		case devopsv1alpha1.TypePipeline:
			err = c.syncPipelineHandler(key)
		}

		if err != nil {
			return fmt.Errorf("error syncing '%s'|'%s': %s", objType, key, err.Error())
		}

		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.V(7).Infof("Successfully synced %s '%s'", objType, key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

func (c *Controller) getPipelineConfigByKey(key string) (*devopsv1alpha1.PipelineConfig, error) {
	var (
		namespace, name string
		err             error
		pipelineConfig  *devopsv1alpha1.PipelineConfig
	)

	namespace, name, err = cache.SplitMetaNamespaceKey(key)
	glog.V(7).Infof("[%v] get pipelineConfig --> namespace: %v, name: %v, err: %v",
		key, namespace, name, err)
	if err != nil {
		return nil, err
	}

	pipelineConfig, err = c.piplineConfigLister.PipelineConfigs(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("pipelineConfig '%s' in work queue no longer exists", key))
		}
		return nil, err
	}
	return pipelineConfig, nil
}

func (c *Controller) getPipelineByKey(key string) (*devopsv1alpha1.Pipeline, error) {
	var (
		namespace, name string
		err             error
		pipelineConfig  *devopsv1alpha1.Pipeline
	)

	namespace, name, err = cache.SplitMetaNamespaceKey(key)
	glog.V(7).Infof("[%v] get pipeline --> namespace: %v, name: %v, err: %v",
		key, namespace, name, err)
	if err != nil {
		return nil, err
	}

	pipelineConfig, err = c.pipelineLister.Pipelines(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("pipeline '%s' in work queue no longer exists", key))
		}
		return nil, err
	}
	return pipelineConfig, nil
}

// syncPipelineConfigHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the PipelineConfig resource
// with the current status of the resource.
func (c *Controller) syncPipelineConfigHandler(key string) (err error) {
	var (
		pipelineConfig *devopsv1alpha1.PipelineConfig
	)

	pipelineConfig, err = c.getPipelineConfigByKey(key)
	if err != nil {
		return err
	}

	return c.templateHandle(pipelineConfig)
}

// templateHandle generate jenkinsFile from template
func (c *Controller) templateHandle(pipelineConfig *devopsv1alpha1.PipelineConfig) (err error) {
	namespace, name := pipelineConfig.GetNamespace(), pipelineConfig.GetName()
	key := fmt.Sprintf("%s/%s", namespace, name)
	glog.V(5).Infof("Will work on PipelineConfig %s/%s", namespace, name)
	shouldUpdate := false

	pipelineConfigCopy := pipelineConfig.DeepCopy()

	// we only take care of Creating phase
	if pipelineConfigCopy.Status.Phase != devopsv1alpha1.PipelineConfigPhaseCreating {
		glog.V(7).Infof("PipelineConfig %s/%s Status is invalid: %v. Leaving it alone...", namespace, name, pipelineConfigCopy.Status)
		return nil
	}

	defer func() {
		if shouldUpdate {
			now := metav1.NewTime(time.Now())
			pipelineConfigCopy.Status.LastUpdate = &now
			glog.V(5).Infof("Will update pipeline config %s/%s. Phase: %s", namespace, name, pipelineConfigCopy.Status.Phase)
			err = c.updatePipelineConfigPhase(namespace, pipelineConfigCopy)
			if err != nil {
				glog.Errorf("update pipelineconfig error: %#v", err)
			}
		}
	}()

	// handle multi-source git info
	err = util.PipelineSourceMerge(&pipelineConfigCopy.Spec.Source, c.codeRepositoryLister, c.codeRepoBindingLister, namespace)
	if err != nil {
		glog.Errorf("PipelineSourceMerge error %v", err)
		return err
	}

	shouldUpdate = true
	template := pipelineConfigCopy.Spec.Strategy.Template
	if template == nil {
		pipelineConfigCopy.Status.Phase = devopsv1alpha1.PipelineConfigPhaseSyncing
		glog.V(7).Infof("PipelineConfig '%s' has not template, it will change to Syncing.", key)
		return nil
	}

	previewOptions := &devopsv1alpha1.JenkinsfilePreviewOptions{
		Source: &pipelineConfigCopy.Spec.Source,
		Values: map[string]string{},
	}

	arguments := pipelineConfigCopy.Spec.Strategy.Template.Spec.Arguments
	for _, arg := range arguments {
		for _, item := range arg.Items {
			previewOptions.Values[item.Name] = item.Value
		}
	}

	preview, err := c.devopsclientset.DevopsV1alpha1().PipelineConfigs(namespace).Preview(name, previewOptions)
	if err != nil {
		glog.Errorf("generate jenkinsFile failured, '%s', error %v", key, err)

		pipelineConfigCopy.Status.Phase = devopsv1alpha1.PipelineConfigPhaseError
		pipelineConfigCopy.Status.Message = fmt.Sprintf("Error generating Jenkinsfile: %s", err.Error())
		pipelineConfigCopy.Status.Reason = ErrRenderJenkinsfile
		// TODO: Add conditions
		// pipelineConfigCopy.Status.Conditions
	} else {
		pipelineConfigCopy.Spec.Strategy.Jenkins.Jenkinsfile = preview.Jenkinsfile
		pipelineConfigCopy.Status.Phase = devopsv1alpha1.PipelineConfigPhaseSyncing

		glog.V(2).Infof("Generate jenkinsfile success, pipeline name is %s", pipelineConfigCopy.GetName())
	}
	return
}

// updatePipelineConfigPhase will update the phase of PipelineConfig
func (c *Controller) updatePipelineConfigPhase(namespace string, pipelineConfig *devopsv1alpha1.PipelineConfig) (err error) {
	pipelineConfigs := c.devopsclientset.DevopsV1alpha1().PipelineConfigs(namespace)

	if pipelineConfigs != nil {
		_, err = pipelineConfigs.Update(pipelineConfig)
	} else {
		err = fmt.Errorf("Can not found PipelineConfig by namespace %s", namespace)
	}

	return
}

func handleError(err error, msg string, args ...string) error {
	if errors.IsNotFound(err) {
		runtime.HandleError(fmt.Errorf(msg, args))
		return nil
	} else {
		return err
	}
}

func (c *Controller) updatePipelineConfig(pipelineConfig *devopsv1alpha1.PipelineConfig) (err error) {
	_, err = c.devopsclientset.DevopsV1alpha1().PipelineConfigs(pipelineConfig.GetNamespace()).Update(pipelineConfig)
	c.recorder.Event(pipelineConfig, corev1.EventTypeNormal, SuccessSynced, MessageResourceSynced)
	return
}

func (c *Controller) syncPipelineHandler(key string) (err error) {
	//var (
	//	pipeline     *devopsv1alpha1.Pipeline
	//	pipelineCopy *devopsv1alpha1.Pipeline
	//)
	//pipeline, err = c.getPipelineByKey(key)
	//if err != nil {
	//	return err
	//}
	//pipelineCopy = pipeline.DeepCopy()
	//if pipelineCopy.Status.HookConditions == nil {
	//	pipelineCopy.Status.HookConditions = []devopsv1alpha1.HookCondition{}
	//}
	return
}

func (c *Controller) sendHttpRequest(hookRequest *devopsv1alpha1.PipelineHookHTTPRequest, body interface{}) (status *devopsv1alpha1.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	// prepare
	status = &devopsv1alpha1.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	http.DefaultClient.Timeout = time.Second * 30
	byteBody, err := json.Marshal(body)
	if err != nil {
		runtime.HandleError(err)
	}

	req, err = http.NewRequest(hookRequest.Method, hookRequest.URI, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	if hookRequest.Headers != nil && len(hookRequest.Headers) > 0 {
		for k, v := range hookRequest.Headers {
			req.Header.Add(k, v)
		}
	}

	glog.V(7).Infof("url: %s; payload: %s; header: %s", hookRequest.URI, body, hookRequest.Headers)
	resp, err = http.DefaultClient.Do(req)
	glog.V(7).Infof("response: %v; err: %v", resp, err)
	if resp != nil {
		status.StatusCode = int(resp.StatusCode)
		data, err := ioutil.ReadAll(resp.Body)
		if data != nil {
			status.Response = string(data)
		} else {
			status.Response = ""
		}
		if err != nil {
			runtime.HandleError(err)
		}
	} else if err != nil {
		status.StatusCode = 500
		status.Response = fmt.Sprintf("Error hapens when send request. err: %v", err)
	}

	duration = time.Since(start)
	status.Delay = &duration
	return
}

// enqueuePipelineConfig takes a Jenkins resource and converts it into a type|namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than PipelineConfig .
func (c *Controller) enqueuePipelineConfig(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypePipelineConfig, key)
	c.workqueue.AddRateLimited(key)
}

// enqueuePipeline takes a Project resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than Pipeline.
func (c *Controller) enqueuePipeline(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	key = fmt.Sprintf("%s|%s", devopsv1alpha1.TypePipeline, key)
	c.workqueue.AddRateLimited(key)
}

// New constructor for Project Controller
func New() *Controller {
	return &Controller{}
}
