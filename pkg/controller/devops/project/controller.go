package project

import (
	"fmt"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	coreListers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerAgentName = "devops-project-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing = "ErrSyncing"
	// ErrResourceExists is used as part of the Event 'reason' when a Project fails
	// to sync due to a Namespace of the same name already existing.
	ErrResourceExists = "ErrResourceExists"
	// ErrProjectSpecInvalid error when controller finds an invalid project spec
	ErrProjectSpecInvalid = "ErrProjectSpecInvalid"

	// MessageResourceExists is the message used for Events when a resource
	// fails to sync due to a Deployment already existing
	MessageResourceExists = "Resource %q already exists and is not managed by Project"
	// MessageResourceSynced is the message used for an Event fired when a Project
	// is synced successfully
	MessageResourceSynced = "Project synced successfully"
	// MessageProjectSpecInvalid message content for the invalid project spec
	MessageProjectSpecInvalid = "Project's spec is invalid"
	// MessageProjectUpdated event mesage for when a full project update was performed
	MessageProjectUpdated = "Project updated successfully"
	// MessageNamespaceCreated message of success creating a namespace
	MessageNamespaceCreated = "Namespace %s created successfully"
	// MessageNamespaceErrCreating message for namespace creation err
	MessageNamespaceErrCreating = "Error creating namespace %s: %s"
	// MessageProjectSyncErr message for syncing issue
	MessageProjectSyncErr = "Project sync error: %s"
)

// Controller is a subcontroller to manage project resources
// syncing and life-cycle
type Controller struct {
	controller.Interface
	// kubernetes API client
	kubeclientset kubernetes.Interface
	// Devops API client
	devopsclientset clientset.Interface

	// Data sync for projects
	projectLister listers.ProjectLister
	projectSynced cache.InformerSynced

	// Data sync for namespace
	namespacesLister coreListers.NamespaceLister
	namespacesSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

// Initialize to start basic initialization for projects
// controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	k8sFactory, devopsFactory := base.GetInformerFactory()

	// **** kubernetes resources
	// namespace
	namespaceInformer := k8sFactory.Core().V1().Namespaces()
	c.namespacesLister = namespaceInformer.Lister()
	c.namespacesSynced = namespaceInformer.Informer().HasSynced

	// **** devops resources
	// devops project
	projectsInformer := devopsFactory.Devops().V1alpha1().Projects()
	c.projectLister = projectsInformer.Lister()
	c.projectSynced = projectsInformer.Informer().HasSynced

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Projects")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting events
	// projects
	projectsInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueueProject,
		UpdateFunc: func(old, new interface{}) {
			c.enqueueProject(new)
		},
		DeleteFunc: func(obj interface{}) {
			// It means the project has been deleted if come here.
			go func() {
				var (
					project *devopsv1alpha1.Project
					ok      bool
				)

				if project, ok = obj.(*devopsv1alpha1.Project); !ok {
					glog.Errorf("The format of deleted project is invalid. value: %v", project)
					return
				}

				c.DeleteProject(project)
			}()
		},
	})

	// namespaces
	// we only deal with namespaces that were created by projects
	// and ignore any non-updated namespaces
	namespaceInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.handleObject,
		UpdateFunc: func(old, new interface{}) {
			newNamespace := new.(*corev1.Namespace)
			oldNamespace := old.(*corev1.Namespace)
			if newNamespace.ResourceVersion == oldNamespace.ResourceVersion {
				// Periodic resync will send update events for all known Namespaces.
				// Two different versions of the same Namespace will always have different RVs.
				return
			}
			c.handleObject(new)
		},
		DeleteFunc: c.handleObject,
	})
}

func (c *Controller) DeleteProject(project *devopsv1alpha1.Project) (err error) {
	glog.Infof("Delete the project %v", project)
	if project.Spec.Namespaces != nil {
		for _, projectNamespace := range project.Spec.Namespaces {
			listOptions := metav1.ListOptions{}
			// Delete all resources in this namespace
			glog.V(5).Infof("Delete the resources in namespace %s", projectNamespace.Name)

			err = c.devopsclientset.DevopsV1alpha1().PipelineConfigs(projectNamespace.Name).DeleteCollection(nil, listOptions)
			glog.V(5).Infof("The result of pipelineconfigs delete: %v", err)

			err = c.devopsclientset.DevopsV1alpha1().Pipelines(projectNamespace.Name).DeleteCollection(nil, listOptions)
			glog.V(5).Infof("The result of pipelines delete: %v", err)

			err = c.devopsclientset.DevopsV1alpha1().JenkinsBindings(projectNamespace.Name).DeleteCollection(nil, listOptions)
			glog.V(5).Infof("The result of jenkinsbindings delete: %v", err)

			err = c.kubeclientset.CoreV1().Namespaces().Delete(projectNamespace.Name, &metav1.DeleteOptions{})
			glog.V(5).Infof("The result of namespace delete: %v", err)
		}
	}
	return
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "Project"
}

// Start should start the controller
func (c *Controller) Start() {
	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the listers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.projectSynced, c.namespacesSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// / processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		glog.Infof("will handle this key: %s", key)
		// Run the syncHandler, passing it the namespace/name string of the
		// Project resource to be synced.
		if err := c.syncHandler(key); err != nil {
			return fmt.Errorf("error syncing '%s': %s", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced project '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

// syncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the Project resource
// with the current status of the resource.
func (c *Controller) syncHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	_, name, err := cache.SplitMetaNamespaceKey(key)
	glog.V(4).Infof("syncing project: name: %v, err: %v", name, err)

	project, err := c.projectLister.Get(name)
	if err != nil {
		// The Project resource may no longer exist, in which case we stop
		// processing.
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("project '%s' in work queue no longer exists", key))
			return nil
		}

		return err
	}
	glog.V(4).Infof("got project instance")

	// NEVER modify objects from the store. It's a read-only, local cache.
	// You can use DeepCopy() to make a deep copy of original object and modify this copy
	// Or create a copy manually for better performance
	projCopy := project.DeepCopy()
	needsEvent := false

	// this is to enforce our current limitation of 1 project to 1 namespace
	// we need to make sure the data is correct and fix if necessary
	// TODO: change this rule once the project supports more than one namespace
	if len(project.Spec.Namespaces) != 1 {
		glog.V(4).Infof("project spec is invalid. Will fix it..")
		c.recorder.Event(project, corev1.EventTypeWarning, ErrProjectSpecInvalid, MessageProjectSpecInvalid)

		correct := devopsv1alpha1.ProjectNamespace{
			Name: project.GetName(),
			Type: devopsv1alpha1.ProjectNamespaceTypeOwned,
		}
		if len(project.Spec.Namespaces) == 0 {
			projCopy.Spec.Namespaces = []devopsv1alpha1.ProjectNamespace{correct}
		} else {
			projCopy.Spec.Namespaces = project.Spec.Namespaces[:1]
			projCopy.Spec.Namespaces[0] = correct
		}
		needsEvent = true
		// TODO: Once the status subresource is ready
		// we should update the project, return
		// and let the loop start again
	}

	if projCopy.Status.Namespaces == nil {
		projCopy.Status.Namespaces = []devopsv1alpha1.ProjectNamespaceStatus{}
	}

	var namespaceObj *corev1.Namespace

	// loop through project getting namespace status
	for _, n := range projCopy.Spec.Namespaces {
		found := -1
		for i, s := range projCopy.Status.Namespaces {
			if s.Name == n.Name {
				found = i
				break
			}
		}
		// not found, should add one
		if found == -1 {
			glog.V(4).Infof("project status does not contain namespace %v, will add now", n.Name)
			projCopy.Status.Namespaces = append(projCopy.Status.Namespaces, devopsv1alpha1.ProjectNamespaceStatus{
				Name:   n.Name,
				Status: devopsv1alpha1.ProjectNamespaceStatusUnknown,
			})
			found = len(projCopy.Status.Namespaces) - 1
		}
		// get the namespace object first
		namespaceObj, err = c.namespacesLister.Get(n.Name)
		if err != nil && errors.IsNotFound(err) {
			needsEvent = true
			glog.V(4).Infof("Namespace %v does not exist. Will create now", n.Name)
			namespaceObj, err = c.kubeclientset.CoreV1().Namespaces().Create(newNamespace(project))
			if namespaceObj != nil {
				// creating an event saying that our namespace was created
				msg := fmt.Sprintf(MessageNamespaceCreated, namespaceObj.Name)
				c.recorder.Event(project, corev1.EventTypeNormal, SuccessCreated, msg)
				projCopy.Status.Namespaces[found].Status = devopsv1alpha1.ProjectNamespaceStatusReady

			} else {
				glog.Errorf("Namespace %v create error: %v", n.Name, err)
				msg := fmt.Sprintf(MessageNamespaceErrCreating, n.Name, err.Error())
				c.recorder.Event(project, corev1.EventTypeWarning, ErrCreating, msg)
				projCopy.Status.Namespaces[found].Status = devopsv1alpha1.ProjectNamespaceStatusError
			}
		} else if err == nil && namespaceObj != nil {
			// namespace exists
			if projCopy.Status.Namespaces[found].Status != devopsv1alpha1.ProjectNamespaceStatusReady {
				needsEvent = true
				projCopy.Status.Namespaces[found].Status = devopsv1alpha1.ProjectNamespaceStatusReady
			}
		}
	}
	if needsEvent {
		eventErr := c.updateProjectSpec(projCopy)
		if err == nil {
			c.recorder.Event(project, corev1.EventTypeNormal, SuccessSynced, MessageResourceSynced)
		}
		if eventErr != nil {
			runtime.HandleError(eventErr)
		}
	}
	// // If an error occurs during Get/Create, we'll requeue the item so we can
	// // attempt processing again later. This could have been caused by a
	// // temporary network failure, or any other transient reason.
	// if err != nil {
	// 	return err
	// }
	return err
}

func (c *Controller) updateProjectSpec(proj *devopsv1alpha1.Project) (err error) {
	// proj must be a copy of the original and modified
	_, err = c.devopsclientset.DevopsV1alpha1().Projects().Update(proj)
	return
}

// enqueueProject takes a Project resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than Project.
func (c *Controller) enqueueProject(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}
	c.workqueue.AddRateLimited(key)
}

// handleObject will take any resource implementing metav1.Object and attempt
// to find the Project resource that 'owns' it. It does this by looking at the
// objects metadata.ownerReferences field for an appropriate OwnerReference.
// It then enqueues that Project resource to be processed. If the object does not
// have an appropriate OwnerReference, it will simply be skipped.
func (c *Controller) handleObject(obj interface{}) {
	var object metav1.Object
	var ok bool
	if object, ok = obj.(metav1.Object); !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			runtime.HandleError(fmt.Errorf("error decoding object, invalid type"))
			return
		}
		object, ok = tombstone.Obj.(metav1.Object)
		if !ok {
			runtime.HandleError(fmt.Errorf("error decoding object tombstone, invalid type"))
			return
		}
		glog.V(4).Infof("Recovered deleted object '%s' from tombstone", object.GetName())
	}
	glog.V(4).Infof("Processing object: %s", object.GetName())
	if ownerRef := metav1.GetControllerOf(object); ownerRef != nil {
		// If this object is not owned by a Project, we should not do anything more
		// with it.
		if ownerRef.Kind != "Project" {
			return
		}

		project, err := c.projectLister.Get(ownerRef.Name)
		if err != nil {
			glog.V(4).Infof("ignoring orphaned object '%s' of project '%s'", object.GetSelfLink(), ownerRef.Name)
			return
		}

		c.enqueueProject(project)
		return
	}
}

// newNamespace creates a new Namespace for a Project resource. It also sets
// the appropriate OwnerReferences on the resource so handleObject can discover
// the Project resource that 'owns' it.
func newNamespace(project *devopsv1alpha1.Project) *corev1.Namespace {
	labels := map[string]string{
		devopsv1alpha1.AnnotationsKeyProject: project.Name,
	}
	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   project.Name,
			Labels: labels,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(project, schema.GroupVersionKind{
					Group:   devopsv1alpha1.SchemeGroupVersion.Group,
					Version: devopsv1alpha1.SchemeGroupVersion.Version,
					Kind:    "Project",
				}),
			},
		},
	}
}

// New constructor for Project Controller
func New() *Controller {
	return &Controller{}
}
