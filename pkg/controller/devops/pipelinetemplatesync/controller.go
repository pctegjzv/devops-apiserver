package pipelinetemplatesync

import (
	"encoding/json"
	goerror "errors"
	"fmt"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller"
	devopsutil "alauda.io/devops-apiserver/pkg/util"
	util "alauda.io/devops-apiserver/pkg/util"
	domain "bitbucket.org/mathildetech/jenkinsfilext/domain"
	"github.com/blang/semver"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	coreListers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerType = "PipelineTemplateSync"
const controllerListType = "PipelineTemplateSyncs"
const controllerAgentName = "devops-pipelineTemplateSync-controller"

// Controller is a subcontroller to manage pipelineTemplateSync resources
// syncing and life-cycle
type Controller struct {
	// Kubernetes API client
	kubeclientset kubernetes.Interface
	// Devops API client
	devopsclientset clientset.Interface

	// Data sync for PipelineTemplateSync
	pipelineTemplateSyncLister listers.PipelineTemplateSyncLister
	pipelineTemplateSyncSynced cache.InformerSynced

	codeRepositoryLister  listers.CodeRepositoryLister
	codeRepoBindingLister listers.CodeRepoBindingLister

	// Data sync for namespace
	namespacesLister coreListers.NamespaceLister
	namespacesSynced cache.InformerSynced

	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

// Initialize to start basic initialization for pipelineTemplateSyncs
// controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()

	k8sFactory, devopsFactory := base.GetInformerFactory()

	// namespace
	namespaceInformer := k8sFactory.Core().V1().Namespaces()
	c.namespacesLister = namespaceInformer.Lister()
	c.namespacesSynced = namespaceInformer.Informer().HasSynced

	// devops pipelineTemplateSync
	pipelineTemplateSyncsInformer := devopsFactory.Devops().V1alpha1().PipelineTemplateSyncs()
	c.pipelineTemplateSyncLister = pipelineTemplateSyncsInformer.Lister()
	c.pipelineTemplateSyncSynced = pipelineTemplateSyncsInformer.Informer().HasSynced

	codeRepositoryInformer := devopsFactory.Devops().V1alpha1().CodeRepositories()
	c.codeRepositoryLister = codeRepositoryInformer.Lister()

	codeRepoBindingInformer := devopsFactory.Devops().V1alpha1().CodeRepoBindings()
	c.codeRepoBindingLister = codeRepoBindingInformer.Lister()

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerListType)
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting events
	// piplineTemplateSync
	pipelineTemplateSyncsInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: c.enqueuePipelineTemplateSync,
		UpdateFunc: func(old, new interface{}) {
			c.enqueuePipelineTemplateSync(new)
		},
	})
}

func (c *Controller) enqueuePipelineTemplateSync(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	glog.Infof("Enqueue PipelineTemplateSync %s", key)

	c.workqueue.AddRateLimited(key)
}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return controllerType
}

// Start should start the controller
func (c *Controller) Start() {
	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the listers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.pipelineTemplateSyncSynced, c.namespacesSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// processNextWorkITem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	err := func(obj interface{}) error {
		defer c.workqueue.Done(obj)
		var key string
		var ok bool

		if key, ok = obj.(string); !ok {
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}

		glog.Infof("will handle PipelineTemplateSync key: %s", key)

		if err := c.syncHandler(key); err != nil {
			return fmt.Errorf("error syncing '%s': %s", key, err)
		}

		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced PipelineTemplateSync '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
	}

	return true
}

func (c *Controller) syncHandler(key string) (err error) {
	namespace, name, _ := cache.SplitMetaNamespaceKey(key)

	doNothing := false
	pipTempSync, err := c.pipelineTemplateSyncLister.PipelineTemplateSyncs(namespace).Get(name)
	if err != nil {
		doNothing = true
		if errors.IsNotFound(err) {
			runtime.HandleError(fmt.Errorf("pipelineTemplateSync '%s' in work queue no logger exists", key))
			return nil
		}

		return err
	}

	// pipTempSync = nil
	pipTempSyncCopy := pipTempSync.DeepCopy()

	// mark status
	if (pipTempSyncCopy.Status == nil) || (pipTempSyncCopy.Status.Phase != devopsv1alpha1.PipelineTemplateSyncPhasePending) {
		glog.Infof("pipelineTemplateSync haven't triggered")
		doNothing = true
		return nil
	}

	pipTempSyncCopy.Status.StartTime = metav1.Now()
	defer func() {
		if pipTempSyncCopy == nil || doNothing {
			return
		}

		const success = "Success"
		const failed = "Failed"
		if err == nil {
			pipTempSyncCopy.Status.Message = success
			pipTempSyncCopy.Status.Phase = devopsv1alpha1.PipelineTemplateSyncPhaseReady
		} else {
			pipTempSyncCopy.Status.Message = failed
			pipTempSyncCopy.Status.Phase = devopsv1alpha1.PipelineTemplateSyncPhaseError
		}

		pipTempSyncCopy.Status.EndTime = metav1.Now()
		_, err = c.devopsclientset.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Update(pipTempSyncCopy)
	}()

	// handle multi-source into one
	err = c.handleGitSource(pipTempSyncCopy, namespace)
	if err != nil {
		c.handleError(namespace, pipTempSyncCopy, "handleGitSource error %v", err)
		return
	}

	// mark all template resources as stale in current namespaces
	err = c.markAsStale(namespace)
	if err != nil {
		glog.Errorf("got error %#v, when mark all template resources as stale", err)
	}

	// fetch template files
	err = c.handleTemplateFiles(pipTempSyncCopy)
	if err != nil {
		c.handleError(namespace, pipTempSyncCopy, "handleTemplateFiles error %v", err)
	}

	// delete all stale template resources
	c.handleStaleTemplates(namespace, pipTempSyncCopy)

	return
}

func (c *Controller) markAsStale(namespace string) (err error) {
	client := c.devopsclientset.DevopsV1alpha1()
	pipelineTemplates, err := client.PipelineTemplates(namespace).List(metav1.ListOptions{})
	if err != nil {
		return
	}

	pipelineTaskTemplates, err := client.PipelineTaskTemplates(namespace).List(metav1.ListOptions{})
	if err != nil {
		return
	}

	for _, template := range pipelineTemplates.Items {
		template.Annotations[AnnotationStale] = "true"
		client.PipelineTemplates(namespace).Update(&template)
	}

	for _, template := range pipelineTaskTemplates.Items {
		template.Annotations[AnnotationStale] = "true"
		client.PipelineTaskTemplates(namespace).Update(&template)
	}
	// runtime.Object

	return
}

func (c *Controller) handleStaleTemplates(namespace string, pipTempSync *devopsv1alpha1.PipelineTemplateSync) {
	client := c.devopsclientset.DevopsV1alpha1()
	pipelineTemplates, err := client.PipelineTemplates(namespace).List(metav1.ListOptions{})
	if err != nil {
		return
	}

	pipelineTaskTemplates, err := client.PipelineTaskTemplates(namespace).List(metav1.ListOptions{})
	if err != nil {
		return
	}

	for _, template := range pipelineTemplates.Items {
		if template.Annotations[AnnotationStale] != "true" {
			continue
		}

		c.conditionRecord(pipTempSync, "", "PipelineTemplate", template.ObjectMeta, "", DELETED)
		client.PipelineTemplates(namespace).Delete(template.Name, &metav1.DeleteOptions{})
	}

	for _, template := range pipelineTaskTemplates.Items {
		if template.Annotations[AnnotationStale] != "true" {
			continue
		}

		c.conditionRecord(pipTempSync, "", "PipelineTaskTemplate", template.ObjectMeta, "", DELETED)
		client.PipelineTaskTemplates(namespace).Delete(template.Name, &metav1.DeleteOptions{})
	}
}

func (c *Controller) handleError(namespace string, pipTempSync *devopsv1alpha1.PipelineTemplateSync, format string, args ...interface{}) {
	glog.Errorf(format, args)
	pipTempSync.Status.Phase = devopsv1alpha1.PipelineTemplateSyncPhaseError
}

// handleGitSource handle multi-source into one
func (c *Controller) handleGitSource(pipTempSync *devopsv1alpha1.PipelineTemplateSync, namespace string) error {
	return util.PipelineSourceMerge(&pipTempSync.Spec.Source, c.codeRepositoryLister, c.codeRepoBindingLister, namespace)
}

// handleTemplateFiles will sync templates from git
func (c *Controller) handleTemplateFiles(pipTempSync *devopsv1alpha1.PipelineTemplateSync) (err error) {
	gitSrc := pipTempSync.Spec.Source.Git
	if gitSrc == nil {
		return nil
	}

	url := gitSrc.URI
	ref := gitSrc.Ref

	namespace := pipTempSync.GetNamespace()

	user, passwd, err := c.getUserPasswd(pipTempSync)
	if err != nil {
		return err
	}

	glog.Infof("prepare to clone git %s", url)

	// clean the old conditions
	pipTempSync.Status.Conditions = make([]devopsv1alpha1.PipelineTemplateSyncCondition, 0)

	errs := devopsutil.GetObjectFrom(url, ref, user, passwd, func(data []byte, path string) (err error) {
		return c.handleTemplate(data, path, namespace, pipTempSync)
	})

	if len(errs) > 0 {
		err = fmt.Errorf("some error happend when sync from git, count %d", len(errs))
		glog.Errorf("%v", err)
		for _, e := range errs {
			glog.Errorf("%v", e)
		}
	}

	return
}

func IsNewer(oldVer string, newVer string) bool {
	v1, _ := semver.Make(oldVer)
	v2, _ := semver.Make(newVer)

	return v2.Compare(v1) > 0
}

// SKIPED skip handle templates
var SKIPED = goerror.New("skiped")

// DELETED handle template for delete
var DELETED = goerror.New("Deleted")

const (
	// MetaVersion object meta version const
	MetaVersion = "alauda.io/version"

	// AnnotationStale used for annotation
	AnnotationStale = "stale"
)

func isNewerMeta(oldMeta metav1.ObjectMeta, newMeta metav1.ObjectMeta) bool {
	return IsNewer(oldMeta.Annotations[MetaVersion], newMeta.Annotations[MetaVersion])
}

func (c *Controller) handlePipelineTemplate(data []byte, namespace string) (objectMeta metav1.ObjectMeta, previousVersion string, err error) {
	pipelineTemplate := &devopsv1alpha1.PipelineTemplate{}
	json.Unmarshal(data, pipelineTemplate)
	objectMeta = pipelineTemplate.ObjectMeta

	name := pipelineTemplate.Name
	client := c.devopsclientset.DevopsV1alpha1()
	var (
		old *devopsv1alpha1.PipelineTemplate
	)

	old, err = client.PipelineTemplates(namespace).Get(name, metav1.GetOptions{})
	oldCopy := old.DeepCopy()
	old = nil
	// could not fetch pipeline (new)
	if err != nil {
		pipelineTemplate.Namespace = namespace
		_, err = c.devopsclientset.DevopsV1alpha1().PipelineTemplates(namespace).Create(pipelineTemplate)
		return
	}

	if oldCopy.Annotations == nil {
		oldCopy.Annotations = make(map[string]string)
	}
	previousVersion = oldCopy.Annotations[MetaVersion]
	// this is a new version
	if isNewerMeta(oldCopy.ObjectMeta, pipelineTemplate.ObjectMeta) {
		pipelineTemplate.Namespace = namespace
		removeAnnotation(&pipelineTemplate.ObjectMeta, AnnotationStale)
		pipelineTemplate.UID = oldCopy.UID
		pipelineTemplate.SelfLink = oldCopy.SelfLink
		pipelineTemplate.ResourceVersion = oldCopy.ResourceVersion
		_, err = c.devopsclientset.DevopsV1alpha1().PipelineTemplates(namespace).Update(pipelineTemplate)
		if err != nil {
			removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
			c.devopsclientset.DevopsV1alpha1().PipelineTemplates(namespace).Update(oldCopy)
		}
		return
	}

	// version did not change
	removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
	_, err = c.devopsclientset.DevopsV1alpha1().PipelineTemplates(namespace).Update(oldCopy)
	if err == nil {
		err = SKIPED
		glog.Infof("already exists skip namespace[%s] name[%s]", namespace, name)
	}
	return
}

func removeAnnotation(meta *metav1.ObjectMeta, name string) {
	if meta.Annotations == nil {
		meta.Annotations = make(map[string]string)
	}
	delete(meta.Annotations, name)
}

func (c *Controller) handlePipelineTaskTemplate(data []byte, namespace string) (objectMeta metav1.ObjectMeta, previousVersion string, err error) {
	pipelineTaskTemplate := &devopsv1alpha1.PipelineTaskTemplate{}
	json.Unmarshal(data, pipelineTaskTemplate)

	name := pipelineTaskTemplate.Name
	objectMeta = pipelineTaskTemplate.ObjectMeta
	client := c.devopsclientset.DevopsV1alpha1()

	var (
		old *devopsv1alpha1.PipelineTaskTemplate
	)

	old, err = client.PipelineTaskTemplates(namespace).Get(name, metav1.GetOptions{})
	oldCopy := old.DeepCopy()
	old = nil
	// could not fetch pipeline (new)
	if err != nil {
		pipelineTaskTemplate.Namespace = namespace
		_, err = c.devopsclientset.DevopsV1alpha1().PipelineTaskTemplates(namespace).Create(pipelineTaskTemplate)
		return
	}
	previousVersion = oldCopy.ObjectMeta.Annotations[MetaVersion]
	// this is a new version
	if isNewerMeta(oldCopy.ObjectMeta, pipelineTaskTemplate.ObjectMeta) {
		pipelineTaskTemplate.Namespace = namespace
		removeAnnotation(&pipelineTaskTemplate.ObjectMeta, AnnotationStale)
		pipelineTaskTemplate.UID = oldCopy.UID
		pipelineTaskTemplate.SelfLink = oldCopy.SelfLink
		pipelineTaskTemplate.ResourceVersion = oldCopy.ResourceVersion
		_, err = c.devopsclientset.DevopsV1alpha1().PipelineTaskTemplates(namespace).Update(pipelineTaskTemplate)
		if err != nil {
			removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
			c.devopsclientset.DevopsV1alpha1().PipelineTaskTemplates(namespace).Update(oldCopy)
		}
		return
	}

	// version did not change
	removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
	_, err = c.devopsclientset.DevopsV1alpha1().PipelineTaskTemplates(namespace).Update(oldCopy)
	if err == nil {
		err = SKIPED
		glog.Infof("already exists skip namespace[%s] name[%s]", namespace, name)
	}
	return
}

func (c *Controller) handleClusterPipelineTemplate(data []byte) (objectMeta metav1.ObjectMeta, previousVersion string, err error) {
	clusterPipelineTemplate := &devopsv1alpha1.ClusterPipelineTemplate{}
	json.Unmarshal(data, clusterPipelineTemplate)

	name := clusterPipelineTemplate.Name
	client := c.devopsclientset.DevopsV1alpha1()
	objectMeta = clusterPipelineTemplate.ObjectMeta

	var (
		old *devopsv1alpha1.ClusterPipelineTemplate
	)

	old, err = client.ClusterPipelineTemplates().Get(name, metav1.GetOptions{})
	oldCopy := old.DeepCopy()
	old = nil
	// could not fetch pipeline (new)
	if err != nil {
		_, err = c.devopsclientset.DevopsV1alpha1().ClusterPipelineTemplates().Create(clusterPipelineTemplate)
		return
	}
	previousVersion = oldCopy.ObjectMeta.Annotations[MetaVersion]
	// this is a new version
	if isNewerMeta(oldCopy.ObjectMeta, clusterPipelineTemplate.ObjectMeta) {
		removeAnnotation(&clusterPipelineTemplate.ObjectMeta, AnnotationStale)
		clusterPipelineTemplate.UID = oldCopy.UID
		clusterPipelineTemplate.SelfLink = oldCopy.SelfLink
		clusterPipelineTemplate.ResourceVersion = oldCopy.ResourceVersion
		_, err = c.devopsclientset.DevopsV1alpha1().ClusterPipelineTemplates().Update(clusterPipelineTemplate)
		if err != nil {
			removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
			c.devopsclientset.DevopsV1alpha1().ClusterPipelineTemplates().Update(oldCopy)
		}
		return
	}

	// version did not change
	removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
	_, err = c.devopsclientset.DevopsV1alpha1().ClusterPipelineTemplates().Update(oldCopy)
	if err == nil {
		err = SKIPED
		glog.Infof("already exists skip name[%s]", name)
	}
	return
}

func (c *Controller) handleClusterPipelineTaskTemplate(data []byte) (objectMeta metav1.ObjectMeta, previousVersion string, err error) {
	clusterPipelineTaskTemplate := &devopsv1alpha1.ClusterPipelineTaskTemplate{}
	json.Unmarshal(data, clusterPipelineTaskTemplate)

	name := clusterPipelineTaskTemplate.Name
	client := c.devopsclientset.DevopsV1alpha1()
	objectMeta = clusterPipelineTaskTemplate.ObjectMeta

	var (
		old *devopsv1alpha1.ClusterPipelineTaskTemplate
	)

	old, err = client.ClusterPipelineTaskTemplates().Get(name, metav1.GetOptions{})
	oldCopy := old.DeepCopy()
	old = nil
	// could not fetch pipeline (new)
	if err != nil {
		_, err = c.devopsclientset.DevopsV1alpha1().ClusterPipelineTaskTemplates().Create(clusterPipelineTaskTemplate)
		return
	}
	previousVersion = oldCopy.ObjectMeta.Annotations[MetaVersion]
	// this is a new version
	if isNewerMeta(oldCopy.ObjectMeta, clusterPipelineTaskTemplate.ObjectMeta) {
		removeAnnotation(&clusterPipelineTaskTemplate.ObjectMeta, AnnotationStale)
		clusterPipelineTaskTemplate.UID = oldCopy.UID
		clusterPipelineTaskTemplate.SelfLink = oldCopy.SelfLink
		clusterPipelineTaskTemplate.ResourceVersion = oldCopy.ResourceVersion
		_, err = c.devopsclientset.DevopsV1alpha1().ClusterPipelineTaskTemplates().Update(clusterPipelineTaskTemplate)
		if err != nil {
			removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
			c.devopsclientset.DevopsV1alpha1().ClusterPipelineTaskTemplates().Update(oldCopy)
		}
		return
	}

	// version did not change
	removeAnnotation(&oldCopy.ObjectMeta, AnnotationStale)
	_, err = c.devopsclientset.DevopsV1alpha1().ClusterPipelineTaskTemplates().Update(oldCopy)
	if err == nil {
		err = SKIPED
		glog.Infof("already exists skip name[%s]", name)
	}
	return
}

func (c *Controller) handleTemplate(data []byte, path string, namespace string, pipTempSync *devopsv1alpha1.PipelineTemplateSync) (err error) {
	result := &metav1.TypeMeta{}
	err = json.Unmarshal(data, result)
	if err != nil {
		glog.Infof("can't Unmarshal data , path %s, data size %d, - %v", err, len(data), path)
		return
	}

	kind := result.Kind
	var objectMeta metav1.ObjectMeta
	var previousVersion string
	switch kind {
	case "PipelineTemplate":
		objectMeta, previousVersion, err = c.handlePipelineTemplate(data, namespace)
		if err != nil {
			glog.Errorf("create PipelineTemplate failed, name %s, %v", objectMeta.Name, err)
		}
		break
	case "PipelineTaskTemplate":
		objectMeta, previousVersion, err = c.handlePipelineTaskTemplate(data, namespace)
		if err != nil {
			glog.Errorf("create PipelineTaskTemplate failed, name: %s, %v", objectMeta.Name, err)
		}
		break
	case "ClusterPipelineTemplate":
		objectMeta, previousVersion, err = c.handleClusterPipelineTemplate(data)
		if err != nil {
			glog.Infof("create ClusterPipelineTemplate failed, name %s", objectMeta.Name)
		}
		break
	case "ClusterPipelineTaskTemplate":
		objectMeta, previousVersion, err = c.handleClusterPipelineTaskTemplate(data)
		if err != nil {
			glog.Infof("create ClusterPipelineTaskTemplate failed, name: %s", objectMeta.Name)
		}
		break
	default:
		glog.Infof("unknow data type: %s, path: %s", kind, path)
		break
	}

	c.conditionRecord(pipTempSync, path, kind, objectMeta, previousVersion, err)
	return
}

func (c *Controller) getUserPasswd(pipTempSync *devopsv1alpha1.PipelineTemplateSync) (user string, passwd string, err error) {
	if pipTempSync.Spec.Source.Secret == nil {
		return
	}

	namespace := pipTempSync.GetNamespace()
	secretRef := pipTempSync.Spec.Source.Secret.Name
	secret, err := c.kubeclientset.CoreV1().Secrets(namespace).Get(secretRef, metav1.GetOptions{})
	if err != nil {
		c.recorder.Eventf(pipTempSync, corev1.EventTypeWarning, string(devopsv1alpha1.PipelineTemplateSyncPhasePending),
			"can't found secret, namespace: %s, name: %s", namespace, secretRef)
		glog.Infof("can't found secret, namespace: %s, name: %s", namespace, secretRef)
		return
	}

	if secret.Type != corev1.SecretTypeBasicAuth {
		c.recorder.Eventf(pipTempSync, corev1.EventTypeWarning, string(devopsv1alpha1.PipelineTemplateSyncPhasePending),
			"not support secrete type '%s', supported type is '%s'", secret.Type, corev1.SecretTypeBasicAuth)
		err = fmt.Errorf("not support secrete type '%s', supported type is '%s'", secret.Type, corev1.SecretTypeBasicAuth)
		return
	}

	user = string(secret.Data[corev1.BasicAuthUsernameKey])
	passwd = string(secret.Data[corev1.BasicAuthPasswordKey])

	return
}

func (c *Controller) conditionRecord(sync *devopsv1alpha1.PipelineTemplateSync, path string, kind string,
	objectMeta metav1.ObjectMeta, previousVersion string, result error) {
	status := devopsv1alpha1.SyncStatusSuccess
	message := "Success"
	name, version := objectMeta.Name, objectMeta.Annotations[domain.AnnotationVersion]
	if result != nil {
		if result == SKIPED {
			status = devopsv1alpha1.SyncStatusSkip
			message = fmt.Sprintf("Skiped")
		} else if result == DELETED {
			status = devopsv1alpha1.SyncStatusDeleted
			message = fmt.Sprintf("Deleted")
		} else {
			status = devopsv1alpha1.SyncStatusFailure
			message = fmt.Sprintf("Failed: %v", result)
		}
	}

	condition := devopsv1alpha1.PipelineTemplateSyncCondition{
		LastTransitionTime: metav1.Now(),
		LastUpdateTime:     metav1.Now(),
		Target:             path,
		Name:               name,
		Type:               kind,
		PreviousVersion:    previousVersion,
		Version:            version,
		Status:             status,
		Message:            message,
	}

	if sync.Status == nil {
		sync.Status = &devopsv1alpha1.PipelineTemplateSyncStatus{
			Conditions: []devopsv1alpha1.PipelineTemplateSyncCondition{},
		}
	}

	sync.Status.Conditions = append(sync.Status.Conditions, condition)
}

// New constructor for Project Controller
func New() *Controller {
	return &Controller{}
}
