package pipelinetemplatesync_test

import (
	"testing"

	"github.com/golang/mock/gomock"

	pipelinetemplatesync "alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplatesync"
)

func TestIsNewer(t *testing.T) {
	if pipelinetemplatesync.IsNewer("2.0.0-beta", "2.0.0-beta") {
		t.Errorf("current version is equal")
	}

	if pipelinetemplatesync.IsNewer("2.0.0-beta", "1.0.0-beta") {
		t.Errorf("current version is older")
	}
}

func TestMainController(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
}
