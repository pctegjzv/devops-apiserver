package roles

import (
	"fmt"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	controller "alauda.io/devops-apiserver/pkg/controller"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	rbacv1listers "k8s.io/client-go/listers/rbac/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
)

const controllerAgentName = "devops-roles-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing = "ErrSyncing"
	// ErrResourceExists is used as part of the Event 'reason' when a Project fails
	// to sync due to a Namespace of the same name already existing.
	ErrResourceExists = "ErrResourceExists"
	// ErrProjectSpecInvalid error when controller finds an invalid project spec
	ErrProjectSpecInvalid = "ErrProjectSpecInvalid"

	// MessageRoleCreatedSuccessfully message for creation success
	MessageRoleCreatedSuccessfully = "Role %s created successfully"
	// MessageClusterRoleCreatedSuccessfully message for creation success
	MessageClusterRoleCreatedSuccessfully = "ClusterRole %s created successfully"
)

// Controller is a subcontroller to manage project resources
// syncing and life-cycle
type Controller struct {
	// kubernetes API client
	kubeclientset kubernetes.Interface
	// Devops API client
	devopsclientset clientset.Interface

	// Data sync for projects
	projectLister listers.ProjectLister
	projectSynced cache.InformerSynced

	// RoleBinding
	rolesBindingsLister rbacv1listers.RoleBindingLister
	rolesBindingsSynced cache.InformerSynced

	// ClusterRoleBinding
	clusterRolesBindingsLister rbacv1listers.ClusterRoleBindingLister
	clusterRolesBindingsSynced cache.InformerSynced

	// ClusterRole
	clusterRolesLister rbacv1listers.ClusterRoleLister
	clusterRolesSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder

	// RoleFactory
	roleFactory   devopsv1alpha1.RoleBuilder
	lastCheck     time.Time
	checkDuration time.Duration
	checkLock     bool
}

// make sure the controller conforms to the SubController interface
var _ controller.SubController = &Controller{}

// Initialize to start basic initialization for projects
// controller
func (c *Controller) Initialize(base controller.Interface) {
	c.kubeclientset, c.devopsclientset = base.GetClients()

	// these are basic informer factories
	// if there is a need for a more specialized, or a filtered informer factory
	// those should be constructed manually using the clients provided above
	k8sFactory, devopsFactory := base.GetInformerFactory()

	// **** kubernetes resources
	// rolebindings
	roleBindingInformer := k8sFactory.Rbac().V1().RoleBindings()
	c.rolesBindingsLister = roleBindingInformer.Lister()
	c.rolesBindingsSynced = roleBindingInformer.Informer().HasSynced

	// clusterrolebindings
	clusterRoleBindingInformer := k8sFactory.Rbac().V1().ClusterRoleBindings()
	c.clusterRolesBindingsLister = clusterRoleBindingInformer.Lister()
	c.clusterRolesBindingsSynced = clusterRoleBindingInformer.Informer().HasSynced

	// clusterrole
	clusterRoleInformer := k8sFactory.Rbac().V1().ClusterRoles()
	c.clusterRolesLister = clusterRoleInformer.Lister()
	c.clusterRolesSynced = clusterRoleInformer.Informer().HasSynced

	// **** devops resources
	// devops project
	projectsInformer := devopsFactory.Devops().V1alpha1().Projects()
	c.projectLister = projectsInformer.Lister()
	c.projectSynced = projectsInformer.Informer().HasSynced

	// start workqueue
	c.workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Roles")
	c.recorder = base.GetBroadcaster().NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	// setting up events
	// rolebinding
	c.initializeEvents(
		roleBindingInformer.Informer(),
		clusterRoleInformer.Informer(),
	)
	// roleBindingInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
	// 	AddFunc: func(obj interface{}) {
	// 		roleBinding, ok := obj.(*rbacv1.RoleBinding)
	// 		if !ok || (roleBinding != nil && !c.roleFactory.MatchesRole(roleBinding.RoleRef)) {
	// 			// glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", obj, ok)
	// 			glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", obj, ok)
	// 			return
	// 		}
	// 		glog.V(5).Infof("Got rolebinding create: %v", roleBinding)
	// 		c.enqueueRoleBinding(obj)

	// 	},
	// 	UpdateFunc: func(old, new interface{}) {
	// 		// filtering all non-devops rolebindings
	// 		roleBinding, ok := new.(*rbacv1.RoleBinding)
	// 		if !ok || (roleBinding != nil && !c.roleFactory.MatchesRole(roleBinding.RoleRef)) {
	// 			glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", new, ok)
	// 			return
	// 		}
	// 		glog.V(5).Infof("Got rolebinding update: %v", roleBinding)
	// 		c.enqueueRoleBinding(new)

	// 	},
	// 	DeleteFunc: func(obj interface{}) {
	// 		// filtering all non-devops rolebindings
	// 		roleBinding, ok := obj.(*rbacv1.RoleBinding)
	// 		if !ok || (roleBinding != nil && !c.roleFactory.MatchesRole(roleBinding.RoleRef)) {
	// 			glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", obj, ok)
	// 			return
	// 		}
	// 		glog.V(5).Infof("Got rolebinding delete: %v", roleBinding)
	// 		c.enqueueRoleBinding(obj)
	// 	},
	// })

	// // clusterroles
	// clusterRoleInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
	// 	DeleteFunc: func(obj interface{}) {
	// 		// Handling delete to check if our cluster role was deleted
	// 		// and recreate if necessary
	// 		clusterRole, ok := obj.(*rbacv1.ClusterRole)
	// 		if !ok || (clusterRole != nil && !c.isDevopsRole(clusterRole.ObjectMeta)) {
	// 			glog.V(7).Infof("ClusterRole ignored: %v \tok: %v", obj, ok)
	// 			return
	// 		}
	// 		c.clusterRoleDeleted(clusterRole)
	// 	},
	// })
}

func (c *Controller) initializeEvents(
	roleBindingInformer,
	clusterRoleInformer cache.SharedIndexInformer) {

	// rolebinding
	roleBindingInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			roleBinding, ok := obj.(*rbacv1.RoleBinding)
			if !ok || (roleBinding != nil && !c.roleFactory.MatchesRole(roleBinding.RoleRef)) {
				// glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", obj, ok)
				glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", obj, ok)
				return
			}
			glog.V(5).Infof("Got rolebinding create: %v", roleBinding)
			c.enqueueRoleBinding(obj)

		},
		UpdateFunc: func(old, new interface{}) {
			// filtering all non-devops rolebindings
			roleBinding, ok := new.(*rbacv1.RoleBinding)
			if !ok || (roleBinding != nil && !c.roleFactory.MatchesRole(roleBinding.RoleRef)) {
				glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", new, ok)
				return
			}
			glog.V(5).Infof("Got rolebinding update: %v", roleBinding)
			c.enqueueRoleBinding(new)

		},
		DeleteFunc: func(obj interface{}) {
			// filtering all non-devops rolebindings
			roleBinding, ok := obj.(*rbacv1.RoleBinding)
			if !ok || (roleBinding != nil && !c.roleFactory.MatchesRole(roleBinding.RoleRef)) {
				glog.V(7).Infof("RoleBinding ignored: %v \tok: %v", obj, ok)
				return
			}
			glog.V(5).Infof("Got rolebinding delete: %v", roleBinding)
			c.enqueueRoleBinding(obj)
		},
	})

	// clusterroles
	clusterRoleInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		DeleteFunc: func(obj interface{}) {
			// Handling delete to check if our cluster role was deleted
			// and recreate if necessary
			clusterRole, ok := obj.(*rbacv1.ClusterRole)
			if !ok || (clusterRole != nil && !c.isDevopsRole(clusterRole.ObjectMeta)) {
				glog.V(7).Infof("ClusterRole ignored: %v \tok: %v", obj, ok)
				return
			}
			err := c.clusterRoleDeleted(clusterRole)
			if err != nil {
				runtime.HandleError(err)
			}
		},
	})

}

// GetType returns the object type that this controller manages
func (c *Controller) GetType() string {
	return "Roles"
}

// Start should start the controller
func (c *Controller) Start() {
	for c.processNextWorkItem() {
	}
}

// HasSynced returns a function for checking synced status
// this is used by the main controller to make sure all the listers are ready
func (c *Controller) HasSynced() []cache.InformerSynced {
	return []cache.InformerSynced{c.projectSynced, c.clusterRolesSynced, c.rolesBindingsSynced, c.clusterRolesBindingsSynced}
}

// Shutdown triggers a shutdown of the workqueue
func (c *Controller) Shutdown() {
	c.workqueue.ShutDown()
}

// / processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	go c.checkDevopsRoles()
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// hook for posterior check

	// defer

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		glog.Infof("will handle this key: %s", key)
		// Run the syncHandler, passing it the namespace/name string of the
		// Project resource to be synced.
		if err := c.syncHandler(key); err != nil {
			return fmt.Errorf("error syncing '%s': %s", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced rolebinding '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}
	return true
}

// syncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the Project resource
// with the current status of the resource.
func (c *Controller) syncHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	glog.V(3).Infof("syncing rolebinding: namespace: %v, name: %v, err: %v", namespace, name, err)

	// we are only interested in namespaces that have a project
	if _, getErr := c.projectLister.Get(namespace); getErr != nil && errors.IsNotFound(getErr) {
		glog.V(3).Infof("RoleBinding[%s] Namespace %s does not have a project... leaving...", key, namespace)
		runtime.HandleError(getErr)
		return getErr
	}

	roleBinding, err := c.rolesBindingsLister.RoleBindings(namespace).Get(name)
	if err != nil && errors.IsNotFound(err) {
		glog.V(3).Infof("RoleBinding[%s] not found", key)
		runtime.HandleError(err)
		return err
	}
	glog.V(5).Infof("RoleBinding[%s] got roleBinding instance %v", key, roleBinding)
	binding := roleBinding.DeepCopy()
	// we only manage ClusterRole
	// not interested in other roles other than our platform's
	if !c.roleFactory.MatchesRole(binding.RoleRef) {
		glog.V(3).Infof("RoleBinding[%s] not exepected RoleRef: %v", key, binding.RoleRef)
		return nil
	}
	// we are not interested in other roles other than this two
	switch binding.RoleRef.Name {
	case devopsv1alpha1.RoleNameDeveloper, devopsv1alpha1.RoleNameProjectManager:
		// pass
	default:
		glog.V(3).Infof("RoleBinding[%s] not expected RoleName: %v", key, binding.RoleRef.Name)
		return nil
	}
	for _, subj := range binding.Subjects {
		switch subj.Kind {
		case rbacv1.UserKind, rbacv1.GroupKind:
			// we look for our general ClusterRole binding
			// if it does not exists we add it
			clusteRoleBindingName := c.roleFactory.NewClusterBindingName(devopsv1alpha1.RoleNameProjectUser, subj.Name)
			clusterBinding, listErr := c.clusterRolesBindingsLister.Get(clusteRoleBindingName)
			if listErr != nil && errors.IsNotFound(listErr) {
				// we should create this  cluster role binding
				clusterBinding = c.roleFactory.NewClusterRoleBinding(
					clusteRoleBindingName,
					c.roleFactory.GetRoleRef(devopsv1alpha1.RoleNameProjectUser),
					subj,
				)
				glog.V(3).Infof("RoleBinding[%s] will create ClusterRoleBinding: %v", key, clusterBinding)
				clusterBinding, err = c.kubeclientset.RbacV1().ClusterRoleBindings().Create(clusterBinding)
				if err != nil {
					runtime.HandleError(err)
				} else {
					glog.Infof("RoleBinding[%s] ClusterRoleBinding created successfuly: %v", key, clusterBinding)
				}
			} else if clusterBinding != nil {
				// this is the necessary cluster role binding
				// we should not do anything
			} else {
				runtime.HandleError(err)
			}
		default:
			glog.V(3).Infof("RoleBinding[%s] subject is not of kind interest: %v", key, subj.Kind)
		}
	}
	return err
}

// enqueueRoleBinding takes a RoleBinding resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than Project.
func (c *Controller) enqueueRoleBinding(obj interface{}) {
	var key string
	var err error

	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}

	c.workqueue.AddRateLimited(key)
}

func (c *Controller) checkDevopsRoles() {
	glog.V(5).Infof("Start checking Devops ClusterRoles...")
	if c.lastCheck.Add(c.checkDuration).After(time.Now()) || c.checkLock {
		glog.V(1).Infof("Not checking: lastCheck: %v \tlocked: %v", c.lastCheck, c.checkLock)
		return
	}
	c.checkLock = true
	var (
		err   error
		roles []*rbacv1.ClusterRole
	)
	defer func() {
		c.checkLock = false
		// if no error we should update the time
		if err == nil {
			c.lastCheck = time.Now()
		}
	}()
	roles, err = c.clusterRolesLister.List(c.roleFactory.GetRolesSelector())
	if err != nil {
		runtime.HandleError(err)
		return
	}
	matcher := c.roleFactory.NewRoleMatcher()
	for _, r := range roles {
		if c.isDevopsRole(r.ObjectMeta) {
			matcher.Add(r)
			glog.V(5).Infof("Adding matched ClusterRole: %v", r.ObjectMeta)
		}

	}
	missing := matcher.GetMissing()
	glog.V(3).Infof("Missing the following roles: %v", missing)
	for _, m := range missing {
		clusterRole := c.roleFactory.NewClusterRole(m)
		err = c.createClusterRole(clusterRole)
		if err != nil {
			runtime.HandleError(err)
			glog.Errorf("Error while generating ClusterRole: %v err: %v", clusterRole, err)
		}
	}

	roleNames := matcher.GetExpected()
	glog.V(5).Infof("Expected roles names: %v", roleNames)
	roleIdx := make(map[string]*rbacv1.ClusterRole)
	for _, r := range roleNames {
		roleIdx[r] = c.roleFactory.NewClusterRole(r)
	}
	glog.V(5).Infof("Expected roles index: %v", roleIdx)

	toUpdate := matcher.GetUpdates(missing, roleIdx)
	glog.V(5).Infof("Got Roles to be updated: %v", toUpdate)
	for _, u := range toUpdate {
		glog.V(5).Infof("Will update the following cluster role \"%s\" spec: %v", u.GetName(), *u)
		_, err := c.kubeclientset.RbacV1().ClusterRoles().Update(u)
		if err != nil {
			glog.Errorf("Update ClusterRole \"%s\" error: %v", u.GetName(), err)
		} else {
			glog.V(5).Infof("Update ClusterRole \"%s\" succeeded", u.GetName())
		}
	}

}

// clusterRoleDeleted will force create the same ClusterRole when deleted
// if the ClusterRole is one of the roles belonging to the DevOps Platform
func (c *Controller) clusterRoleDeleted(role *rbacv1.ClusterRole) (err error) {
	switch role.Name {
	case devopsv1alpha1.RoleNamePlatformAdmin, devopsv1alpha1.RoleNameDeveloper,
		devopsv1alpha1.RoleNameProjectManager, devopsv1alpha1.RoleNameProjectUser:
		roleCopy := role.DeepCopy()
		roleCopy.ObjectMeta = cleanMeta(roleCopy.ObjectMeta)
		err = c.createClusterRole(roleCopy)
	}
	return
}

func cleanMeta(objMeta metav1.ObjectMeta) metav1.ObjectMeta {
	objMeta.SetUID("")
	objMeta.SetSelfLink("")
	objMeta.SetResourceVersion("")
	objMeta.SetOwnerReferences(nil)
	objMeta.SetDeletionTimestamp(nil)
	objMeta.SetCreationTimestamp(metav1.NewTime(time.Now()))
	return objMeta
}

func (c *Controller) createClusterRole(role *rbacv1.ClusterRole) (err error) {
	if role, err = c.kubeclientset.RbacV1().ClusterRoles().Create(role); err != nil {
		runtime.HandleError(err)
		return
	}
	c.recorder.Event(role, corev1.EventTypeNormal, SuccessCreated, fmt.Sprintf(MessageClusterRoleCreatedSuccessfully, role.GetName()))
	return
}

func (c *Controller) isDevopsRole(objMeta metav1.ObjectMeta) bool {
	return devopsv1alpha1.IsDevOpsResource(objMeta)
}

// New constructor for Project Controller
func New() *Controller {
	return &Controller{
		roleFactory:   devopsv1alpha1.RoleFactory{},
		checkDuration: time.Minute * 5,
	}
}
