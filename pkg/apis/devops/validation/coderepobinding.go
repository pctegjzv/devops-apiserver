package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateCodeRepoBinding validates a codeRepoBinding config
func ValidateCodeRepoBinding(codeRepoBinding *devops.CodeRepoBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateCodeRepoBindingSpec(&codeRepoBinding.Spec, specField)
	return
}

// ValidateCodeRepoBindingSpec validates CodeRepoBindingSpec
func ValidateCodeRepoBindingSpec(spec *devops.CodeRepoBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// CodeRepoService
	errs = append(errs, validateSpecCodeRepoService(&spec.CodeRepoService, fldPath.Child("codeRepoService"))...)
	// Account
	errs = append(errs, validateSpecAccount(&spec.Account, fldPath.Child("account"))...)

	return
}

// validateSpecCodeRepoService validates SpecCodeRepoService
func validateSpecCodeRepoService(codeRepoService *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if codeRepoService == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			codeRepoService,
			"please provide a CodeRepoService",
		))

	} else if codeRepoService.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			codeRepoService.Name,
			"please provide a CodeRepoService name",
		))
	}
	return
}

// validateSpecAccount validates SpecAccount
func validateSpecAccount(bindingAccount *devops.CodeRepoBindingAccount, fldPath *field.Path) (errs field.ErrorList) {
	if bindingAccount == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			bindingAccount,
			"please provide a Account",
		))
	} else if bindingAccount.Secret.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("secret"),
			bindingAccount.Secret.Name,
			"please provide a secret",
		))
	}
	return
}
