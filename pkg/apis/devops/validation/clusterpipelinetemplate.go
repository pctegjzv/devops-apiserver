package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateClusterPipelineTemplate validation for ClusterPipelineTemplate create action
func ValidateClusterPipelineTemplate(clusterpipelinetemplate *devops.ClusterPipelineTemplate) (errs field.ErrorList) {
	errs = field.ErrorList{}

	errs = append(errs, ValidatePipelineTemplateSpec(&clusterpipelinetemplate.Spec, field.NewPath("spec"))...)

	return
}

// ValidateClusterPipelineTemplateUpdate validation for ClusterPipelineTemplate update actioin
func ValidateClusterPipelineTemplateUpdate(obj *devops.ClusterPipelineTemplate, old *devops.ClusterPipelineTemplate) (errs field.ErrorList) {
	glog.V(7).Infof("obj: %v; old: %v", obj, old)
	errs = field.ErrorList{}
	return
}
