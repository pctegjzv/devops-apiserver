package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateTestToolBinding(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.TestToolBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	invalidInput, invalidErrs := getInvalidTestToolBinding()
	validInput, validErrs := getValidTestToolBinding()

	table := []Table{
		{
			name:     "create: invalid TestToolBinding",
			input:    invalidInput,
			expected: invalidErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateTestToolBinding(obj.(*devops.TestToolBinding))
			},
		},
		{
			name:     "create: valid TestToolBinding",
			input:    validInput,
			expected: validErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateTestToolBinding(obj.(*devops.TestToolBinding))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidTestToolBinding() (*devops.TestToolBinding, field.ErrorList) {
	return &devops.TestToolBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.TestToolBindingSpec{
			TestTool: devops.LocalObjectReference{
				Name: devops.LabelTestTool,
			},
		},
	}, field.ErrorList{}
}

func getInvalidTestToolBinding() (pipe *devops.TestToolBinding, errs field.ErrorList) {
	pipe = &devops.TestToolBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.TestToolBindingSpec{
			TestTool: devops.LocalObjectReference{},
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child(devops.LabelTestTool).Child("name"),
			"",
			"please provide a testTool name",
		),
	}

	return
}
