package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateProjectManageBindingUpdate validates an update over the ProjectManagementBinding
func ValidateProjectManageBindingUpdate(new, old *devops.ProjectManagementBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateProjectManageBindingSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateProjectManageBindingSpec spec data from ProjectManagementBindingSpec
func ValidateProjectManageBindingSpecUpdate(newSpec, oldSpec *devops.ProjectManagementBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateProjectManageBindingSpec(newSpec, fldPath)...)
	return
}

// ValidateProjectManageBinding validates a ProjectManagementBinding config
func ValidateProjectManageBinding(new *devops.ProjectManagementBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateProjectManageBindingSpec(&new.Spec, specField)
	return
}

// ValidateProjectManageBindingSpec validates spec
func ValidateProjectManageBindingSpec(spec *devops.ProjectManagementBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// projectManage
	errs = append(errs, ValidateLocalObjectReference(devops.LabelProjectManagement, &spec.ProjectManagement, fldPath.Child(devops.LabelProjectManagement))...)
	return
}
