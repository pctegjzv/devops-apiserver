/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package devops

import (
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/sets"
)

// GroupName API group name
const GroupName = "devops.alauda.io"

// SchemeGroupVersion is group version used to register these objects
var SchemeGroupVersion = schema.GroupVersion{Group: GroupName, Version: runtime.APIVersionInternal}

// Kind takes an unqualified kind and returns back a Group qualified GroupKind
func Kind(kind string) schema.GroupKind {
	return SchemeGroupVersion.WithKind(kind).GroupKind()
}

// Resource takes an unqualified resource and returns back a Group qualified GroupResource
func Resource(resource string) schema.GroupResource {
	return SchemeGroupVersion.WithResource(resource).GroupResource()
}

var (
	// SchemeBuilder builder of scheme
	SchemeBuilder = runtime.NewSchemeBuilder(addKnownTypes)
	// AddToScheme function to add schemes from builder
	AddToScheme = SchemeBuilder.AddToScheme
	// IgnoredKinds for install method
	IgnoredKinds = sets.NewString(
		"PipelineLogOptions",
		"PipelineLog",
		"JenkinsfilePreviewOptions",
		"JenkinsfilePreview",
		"CodeRepoServiceAuthorizeResponse",
		"CodeRepoServiceAuthorizeOptions",
	)
)

// Adds the list of known types to the given scheme.
func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(SchemeGroupVersion,
		&Project{},
		&ProjectList{},
		&Jenkins{},
		&JenkinsList{},
		&JenkinsBinding{},
		&JenkinsBindingList{},
		&PipelineConfig{},
		&PipelineConfigList{},
		&JenkinsfilePreviewOptions{},
		&JenkinsfilePreview{},
		&Pipeline{},
		&PipelineList{},
		&PipelineLogOptions{},
		&PipelineLog{},
		&PipelineTaskOptions{},
		&PipelineTask{},
		&PipelineTaskTemplate{},
		&PipelineTaskTemplateList{},
		&PipelineTemplate{},
		&PipelineTemplateList{},
		&PipelineTemplateSync{},
		&PipelineTemplateSyncList{},
		&ClusterPipelineTemplate{},
		&ClusterPipelineTemplateList{},
		&ClusterPipelineTaskTemplate{},
		&ClusterPipelineTaskTemplateList{},
		&CodeRepoService{},
		&CodeRepoServiceList{},
		&CodeRepoBinding{},
		&CodeRepoBindingList{},
		&CodeRepository{},
		&CodeRepositoryList{},
		&CodeRepoBindingRepositories{},
		&CodeRepoBindingRepositoryOptions{},
		&ImageRegistry{},
		&ImageRegistryList{},
		&ImageRegistryBinding{},
		&ImageRegistryBindingList{},
		&ImageRepository{},
		&ImageRepositoryList{},
		&ImageRegistryBindingRepositories{},
		&ImageRegistryBindingRepositoryOptions{},
		&CodeRepoServiceAuthorizeResponse{},
		&CodeRepoServiceAuthorizeOptions{},
		&ProjectManagement{},
		&ProjectManagementList{},
		//&ProjectManagementBinding{},
		//&ProjectManagementBindingList{},
		&TestTool{},
		&TestToolList{},
		//&TestToolBinding{},
		//&TestToolBindingList{},
	)
	return nil
}
