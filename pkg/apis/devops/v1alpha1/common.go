package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
)

var requestedKeys = []string{AnnotationsKeyProduct, AnnotationsKeyProductVersion}

// IsDevOpsResource returns true for a given ObjectMeta if the resource was created
// by the DevOps Platform
// it will verify some requested keys inside annotations to make sure
func IsDevOpsResource(objMeta metav1.ObjectMeta) bool {
	if len(objMeta.Annotations) > 0 {
		for _, key := range requestedKeys {
			if _, ok := objMeta.Annotations[key]; !ok {
				return false
			}
		}
		return true
	}
	return false
}

func GetSimpleSelector(key, value string) (selector labels.Selector) {
	selector = labels.NewSelector()
	req, _ := labels.NewRequirement(key, selection.Equals, []string{value})
	selector.Add(*req)
	return
}

func GetRepoSelector(serviceName string) (selector labels.Selector) {
	return GetSimpleSelector(LabelCodeRepository, serviceName)
}

func GetServiceSelector(serviceName string) (selector labels.Selector) {
	return GetSimpleSelector(LabelCodeRepoService, serviceName)
}
