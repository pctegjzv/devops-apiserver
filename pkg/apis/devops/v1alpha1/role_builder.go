package v1alpha1

import (
	"fmt"
	"strings"

	"github.com/blang/semver"
	"github.com/golang/glog"

	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
)

var expectedRoles = []string{
	RoleNameDeveloper, RoleNamePlatformAdmin,
	RoleNameProjectManager, RoleNameProjectUser,
}

// RoleBuilder basic builder for Roles
type RoleBuilder interface {
	NewClusterRole(name string) *rbacv1.ClusterRole
	NewClusterBindingName(roleName, subjectName string) string
	NewClusterRoleBinding(bindingName string, roleRef rbacv1.RoleRef, subject rbacv1.Subject) *rbacv1.ClusterRoleBinding
	GetRoleRef(roleName string) (ref rbacv1.RoleRef)
	GetRolesSelector() labels.Selector
	NewRoleMatcher() RoleMatcher
	MatchesRole(roleRef rbacv1.RoleRef) bool
}

// RoleFactory role factory for the necessary roles
type RoleFactory struct{}

// Make sure that the struct meets the RoleBuilder interface
var _ RoleBuilder = RoleFactory{}

// NewClusterRole generates a Role based on its name and namespace
// returns nil if the role name does not match
// DevOps role names
func (RoleFactory) NewClusterRole(name string) *rbacv1.ClusterRole {
	return newClusterRole(name)
}

// NewClusterBindingName generates a ClusterRoleBinding name
func (RoleFactory) NewClusterBindingName(roleName, subjectName string) string {
	return fmt.Sprintf("%s:%s", roleName, subjectName)
}

// NewClusterRoleBinding creates a new cluster role binding
func (RoleFactory) NewClusterRoleBinding(bindingName string, roleRef rbacv1.RoleRef, subject rbacv1.Subject) *rbacv1.ClusterRoleBinding {
	return &rbacv1.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name: bindingName,
			Annotations: addRoleVersion(
				"v0.1",
				getDevopsAnnotations(),
			),
			Labels: map[string]string{
				LabelDevopsAlaudaIOKey: "true",
			},
		},
		RoleRef:  roleRef,
		Subjects: []rbacv1.Subject{subject},
	}
}

// GetRoleRef returns a RoleRef based on a specific role name
func (RoleFactory) GetRoleRef(roleName string) (ref rbacv1.RoleRef) {
	switch roleName {
	case RoleNameProjectUser, RoleNameDeveloper, RoleNamePlatformAdmin:
		ref = rbacv1.RoleRef{
			Kind:     "ClusterRole",
			Name:     roleName,
			APIGroup: rbacv1.GroupName,
		}
	}
	return
}

// GetRolesSelector returns a selector for DevOps roles
func (RoleFactory) GetRolesSelector() labels.Selector {
	selector := labels.NewSelector()
	req, _ := labels.NewRequirement(LabelDevopsAlaudaIOKey, selection.Equals, []string{"true"})
	selector.Add(*req)
	return selector
}

// NewRoleMatcher returns a devops role matcher
func (RoleFactory) NewRoleMatcher() RoleMatcher {
	return newDevOpsRoleMatcher()
}

// MatchesRole checks a RoleRef and returns true if it matchs DevOps platform roles
func (RoleFactory) MatchesRole(roleRef rbacv1.RoleRef) bool {
	if roleRef.Kind != "ClusterRole" {
		return false
	}
	switch roleRef.Name {
	case RoleNameDeveloper, RoleNamePlatformAdmin, RoleNameProjectManager, RoleNameProjectUser:
		return true
	}
	return false
}

// should return a new ClusterRole instance
// based on the name
func newClusterRole(roleName string) (role *rbacv1.ClusterRole) {
	version := "v0.2"
	role = &rbacv1.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{
			Name: roleName,
			Annotations: addAutoUpdate(
				addRoleVersion(
					version,
					getDevopsAnnotations(),
				),
			),
			Labels: getDevopsBasicLabels(),
		},
		Rules: []rbacv1.PolicyRule{},
	}
	switch roleName {
	case RoleNameDeveloper:
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayName] = RoleDisplayNameDeveloper
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayNameEn] = RoleDisplayNameDeveloperEn
		role.Rules = getDeveloperRules(version)
		role.Labels = addProjectLabel(role.Labels)

	case RoleNameProjectManager:
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayName] = RoleDisplayNameProjectManager
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayNameEn] = RoleDisplayNameProjectManagerEn
		role.Rules = getProjectManagerRules(version)
		role.Labels = addProjectLabel(role.Labels)

	case RoleNameProjectUser:
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayName] = RoleDisplayNameProjectUser
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayNameEn] = RoleDisplayNameProjectUserEn
		role.Rules = getListNamespaceProjects(version)

	case RoleNamePlatformAdmin:
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayName] = RoleDisplayNamePlatformAdmin
		role.ObjectMeta.Annotations[AnnotationsKeyDisplayNameEn] = RoleDisplayNamePlatformAdminEn
		role.Rules = getPlatformAdminRules(version)
	default:
		role = nil
	}
	return
}

func addProjectLabel(labels map[string]string) map[string]string {
	labels[LabelDevopsAlaudaIOProjectKey] = TrueString
	return labels
}

// returns a set of policies for the Developer role
func getDeveloperRules(version string) (policies []rbacv1.PolicyRule) {
	// version is not needed now, there is only one version
	// this parameter is added for future use
	policies = make([]rbacv1.PolicyRule, 0, 10)
	switch version {
	case "v0.2":
		policies = append(policies,
			// Reading jenkins data
			rbacv1.PolicyRule{
				APIGroups: []string{GroupName},
				Resources: []string{
					"jenkinsbindings",
					"jenkinses",
				},
				Verbs: []string{"get", "watch", "list"},
			},
			// Manage pipelines
			rbacv1.PolicyRule{
				APIGroups: []string{GroupName},
				Resources: []string{
					"pipelines",
					"pipelineconfigs",
				},
				Verbs: []string{"*"},
			},
			// Manage k8s resources
			rbacv1.PolicyRule{
				APIGroups: []string{
					"",     // core
					"apps", // deployments etc
					"extensions",
					"batch",
				},
				Resources: []string{
					"pods",
					"pods/logs",
					"deployments",
					"services",
					"secrets",
					"daemonsets",
					"statefulsets",
					"jobs",
					"configmaps",
					"ingresses",
				},
				Verbs: []string{"*"},
			},
		)
	case "v0.1":
		fallthrough
	default:
		policies = append(policies,
			// Reading jenkins data
			rbacv1.PolicyRule{
				APIGroups: []string{GroupName},
				Resources: []string{
					"jenkinsbindings",
					"jenkinses",
				},
				Verbs: []string{"get", "watch", "list"},
			},
			// Manage pipelines
			rbacv1.PolicyRule{
				APIGroups: []string{GroupName},
				Resources: []string{
					"pipelines",
					"pipelineruns",
				},
				Verbs: []string{"*"},
			},
			// Manage k8s resources
			rbacv1.PolicyRule{
				APIGroups: []string{
					"",     // core
					"apps", // deployments etc
					"extensions",
				},
				Resources: []string{
					"pods",
					"pods/logs",
					"deployments",
					"services",
					"secrets",
				},
				Verbs: []string{"*"},
			},
		)
	}
	return policies
}

// returns a set of policies for the Project Manager role
func getProjectManagerRules(version string) []rbacv1.PolicyRule {
	// version is not needed now, there is only one version
	// this parameter is added for future use
	glog.V(5).Infof("devops-project-manager version is %s", version)

	return []rbacv1.PolicyRule{
		// Reading jenkins data
		rbacv1.PolicyRule{
			APIGroups: []string{GroupName},
			Resources: []string{
				"*",
			},
			Verbs: []string{"get", "watch", "list"},
		},
		// Manage project users
		rbacv1.PolicyRule{
			APIGroups: []string{"rbac.authorization.k8s.io"},
			Resources: []string{
				"rolebindings",
			},
			Verbs: []string{"*"},
		},
	}
}

func getListNamespaceProjects(version string) []rbacv1.PolicyRule {
	// version is not needed now, there is only one version
	// this parameter is added for future use
	glog.V(5).Infof("devops-project-user version is %s", version)

	return []rbacv1.PolicyRule{
		// List namespaces
		rbacv1.PolicyRule{
			APIGroups: []string{
				"", // core
			},
			Resources: []string{
				"namespaces",
			},
			Verbs: []string{"list", "watch", "get"},
		},
		// List projects
		rbacv1.PolicyRule{
			APIGroups: []string{GroupName},
			Resources: []string{
				"projects",
			},
			Verbs: []string{"list", "watch", "get"},
		},
	}
}

func getPlatformAdminRules(version string) []rbacv1.PolicyRule {
	// version is not needed now, there is only one version
	// this parameter is added for future use
	glog.V(5).Infof("devops-platform-admin version is %s", version)

	return []rbacv1.PolicyRule{
		rbacv1.PolicyRule{
			APIGroups: []string{"*"},
			Resources: []string{"*"},
			Verbs:     []string{"*"},
		},
		rbacv1.PolicyRule{
			Verbs:           []string{"*"},
			NonResourceURLs: []string{"*"},
		},
	}
}

// func getProjectViewPolicies(projectName string) []rbacv1.PolicyRule {
// 	return []rbacv1.PolicyRule{
// 		// List namespaces
// 		rbacv1.PolicyRule{
// 			APIGroups: []string{
// 				"", // core
// 			},
// 			Resources: []string{
// 				"namespaces",
// 			},
// 			ResourceNames: []string{
// 				projectName,
// 			},
// 			Verbs: []string{"view", "watch"},
// 		},
// 		// List projects
// 		rbacv1.PolicyRule{
// 			APIGroups: []string{GroupName},
// 			Resources: []string{
// 				"projects",
// 			},
// 			ResourceNames: []string{
// 				projectName,
// 			},
// 			Verbs: []string{"view", "watch"},
// 		},
// 	}
// }

func getDevopsAnnotations() map[string]string {
	// TODO: Find a better way to:
	// - get product version
	// - get role version
	return map[string]string{
		AnnotationsKeyProduct:        ProductName,
		AnnotationsKeyProductVersion: "v0.2",
	}
}

func getDevopsBasicLabels() map[string]string {
	return map[string]string{
		LabelDevopsAlaudaIOKey: TrueString,
	}
}

func addAutoUpdate(annotations map[string]string) map[string]string {
	annotations["rbac.authorization.kubernetes.io/autoupdate"] = TrueString
	return annotations
}

func addRoleVersion(version string, ann map[string]string) map[string]string {
	ann[AnnotationsKeyRoleVersion] = version
	return ann
}

// RoleMatcher controls an expected Role set
type RoleMatcher interface {
	Add(role *rbacv1.ClusterRole)
	GetMissing() (missing []string)
	GetExpected() []string
	GetUpdates(ignore []string, expected map[string]*rbacv1.ClusterRole) (toUpdate []*rbacv1.ClusterRole)
}

// DevOpsRoleMatcher specific implementation for DevOps RoleMatcher
type DevOpsRoleMatcher struct {
	Matched         []string `protobuf:"bytes,1,rep,name=matched"`
	version         map[string]string
	expectedVersion map[string]string
	expected        []string
	currentRoles    map[string]*rbacv1.ClusterRole
}

var _ RoleMatcher = &DevOpsRoleMatcher{}

// newDevOpsRoleMatcher internal constructor, should be invoked by RoleFactory
func newDevOpsRoleMatcher() RoleMatcher {
	return &DevOpsRoleMatcher{
		Matched:         []string{},
		version:         make(map[string]string),
		expectedVersion: make(map[string]string),
		currentRoles:    make(map[string]*rbacv1.ClusterRole),
		expected: []string{
			RoleNameDeveloper, RoleNamePlatformAdmin,
			RoleNameProjectManager, RoleNameProjectUser,
		},
	}
}

// Add add a new role as a metch
func (d *DevOpsRoleMatcher) Add(role *rbacv1.ClusterRole) {
	obj := role.ObjectMeta
	d.Matched = append(d.Matched, obj.GetName())
	if len(obj.Annotations) > 0 {
		version, ok := obj.Annotations[AnnotationsKeyRoleVersion]
		if ok {
			d.version[obj.GetName()] = version
		}
	}
	d.currentRoles[obj.GetName()] = role.DeepCopy()
}

// GetMissing returns the missing names after comparing with expected
func (d *DevOpsRoleMatcher) GetMissing() (missing []string) {
	missing = []string{}
	for _, e := range d.expected {
		found := false
		for _, m := range d.Matched {
			if e == m {
				found = true
				break
			}
		}
		if !found {
			missing = append(missing, e)
		}
	}
	return
}

// GetExpected return the expected role names
func (d *DevOpsRoleMatcher) GetExpected() []string {
	return d.expected
}

// GetUpdates returns all the roles that needs to be updated
func (d *DevOpsRoleMatcher) GetUpdates(ignore []string, expected map[string]*rbacv1.ClusterRole) (toUpdate []*rbacv1.ClusterRole) {
	toUpdate = make([]*rbacv1.ClusterRole, 0, len(expected))
	for name, role := range expected {
		if isInSlice(name, ignore) {
			continue
		}
		currentRole := d.currentRoles[name]

		if currentRole == nil {
			toUpdate = append(toUpdate, role)
		} else if hasHigherVersion(role.ObjectMeta, currentRole.ObjectMeta) {
			glog.V(9).Infof("Role %s needs to be updated...", role.GetName())
			// we keep the resource but update the contents
			currentRole.Rules = role.Rules
			currentRole.AggregationRule = role.AggregationRule
			currentRole.Annotations[AnnotationsKeyRoleVersion] = role.Annotations[AnnotationsKeyRoleVersion]
			toUpdate = append(toUpdate, currentRole)
		}
	}
	return
}

func isInSlice(value string, slice []string) bool {
	if len(slice) == 0 {
		return false
	}
	for _, d := range slice {
		if d == value {
			return true
		}
	}
	return false
}

func hasHigherVersion(expected metav1.ObjectMeta, current metav1.ObjectMeta) bool {
	if len(expected.Annotations) == 0 || expected.Annotations[AnnotationsKeyRoleVersion] == "" {
		return false
	}
	expectedVer := strings.TrimPrefix(expected.Annotations[AnnotationsKeyRoleVersion], "v")
	if len(current.Annotations) == 0 || current.Annotations[AnnotationsKeyRoleVersion] == "" {
		return true
	}
	currentVer := strings.TrimPrefix(current.Annotations[AnnotationsKeyRoleVersion], "v")
	glog.V(7).Infof("Role \"%v\" current ver: %v expected ver: %v", expected.GetName(), currentVer, expectedVer)
	if strings.Count(expectedVer, ".") == 1 {
		expectedVer = expectedVer + ".0"
	}
	if strings.Count(currentVer, ".") == 1 {
		currentVer = currentVer + ".0"
	}
	semExp, err := semver.Make(expectedVer)
	if err != nil {
		glog.V(7).Infof("Expected ver \"%v\" err: %v", expectedVer, err)
		return false
	}
	semCurr, err := semver.Make(currentVer)
	if err != nil {
		glog.V(7).Infof("Current ver \"%v\" err: %v", currentVer, err)
		return true
	}
	glog.V(7).Infof("Semver GT result: %s > %s = %v", expectedVer, currentVer, semExp.GT(semCurr))
	return semExp.GT(semCurr)
}
