// Code generated by MockGen. DO NOT EDIT.
// Source: alauda.io/devops-apiserver/pkg/controller (interfaces: SubController)

// Package informers is a generated GoMock package.
package informers

import (
	controller "alauda.io/devops-apiserver/pkg/controller"
	gomock "github.com/golang/mock/gomock"
	cache "k8s.io/client-go/tools/cache"
	reflect "reflect"
)

// MockSubController is a mock of SubController interface
type MockSubController struct {
	ctrl     *gomock.Controller
	recorder *MockSubControllerMockRecorder
}

// MockSubControllerMockRecorder is the mock recorder for MockSubController
type MockSubControllerMockRecorder struct {
	mock *MockSubController
}

// NewMockSubController creates a new mock instance
func NewMockSubController(ctrl *gomock.Controller) *MockSubController {
	mock := &MockSubController{ctrl: ctrl}
	mock.recorder = &MockSubControllerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockSubController) EXPECT() *MockSubControllerMockRecorder {
	return m.recorder
}

// GetType mocks base method
func (m *MockSubController) GetType() string {
	ret := m.ctrl.Call(m, "GetType")
	ret0, _ := ret[0].(string)
	return ret0
}

// GetType indicates an expected call of GetType
func (mr *MockSubControllerMockRecorder) GetType() *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetType", reflect.TypeOf((*MockSubController)(nil).GetType))
}

// HasSynced mocks base method
func (m *MockSubController) HasSynced() []cache.InformerSynced {
	ret := m.ctrl.Call(m, "HasSynced")
	ret0, _ := ret[0].([]cache.InformerSynced)
	return ret0
}

// HasSynced indicates an expected call of HasSynced
func (mr *MockSubControllerMockRecorder) HasSynced() *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HasSynced", reflect.TypeOf((*MockSubController)(nil).HasSynced))
}

// Initialize mocks base method
func (m *MockSubController) Initialize(arg0 controller.Interface) {
	m.ctrl.Call(m, "Initialize", arg0)
}

// Initialize indicates an expected call of Initialize
func (mr *MockSubControllerMockRecorder) Initialize(arg0 interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Initialize", reflect.TypeOf((*MockSubController)(nil).Initialize), arg0)
}

// Shutdown mocks base method
func (m *MockSubController) Shutdown() {
	m.ctrl.Call(m, "Shutdown")
}

// Shutdown indicates an expected call of Shutdown
func (mr *MockSubControllerMockRecorder) Shutdown() *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Shutdown", reflect.TypeOf((*MockSubController)(nil).Shutdown))
}

// Start mocks base method
func (m *MockSubController) Start() {
	m.ctrl.Call(m, "Start")
}

// Start indicates an expected call of Start
func (mr *MockSubControllerMockRecorder) Start() *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Start", reflect.TypeOf((*MockSubController)(nil).Start))
}
