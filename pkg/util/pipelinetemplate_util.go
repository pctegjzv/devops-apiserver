package util

import (
	"fmt"
	"log"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	domain "bitbucket.org/mathildetech/jenkinsfilext/domain"
)

// GetPipelineTaskTemplate interface for get PipelineTaskTemplate
type GetPipelineTaskTemplate interface {
	GetPipelineTaskTemplate(name string) (taskTemplate *devops.PipelineTaskTemplate, err error)
}

// ConvertFromPipelineTemplate convert from PipelineTemplate to PipelineTemplateSpec domain
func ConvertFromPipelineTemplate(template *devops.PipelineTemplate) (pipeline *domain.PipelineTemplateSpec) {
	spec := template.Spec
	agent := map[string]interface{}{}

	if spec.Agent != nil {
		agent["Label"] = spec.Agent.Label
	}

	pipeline = &domain.PipelineTemplateSpec{
		Engine:    string(spec.Engine),
		Agent:     agent,
		WithSCM:   spec.WithSCM,
		Stages:    []*domain.Stage{},
		Arguments: domain.ArgSections{},
	}

	for _, stage := range spec.Stages {
		if len(stage.Tasks) <= 0 {
			continue
		}

		_stage := &domain.Stage{
			Name:  stage.Name,
			Tasks: []*domain.Task{},
		}

		for _, task := range stage.Tasks {
			agent := map[string]interface{}{}

			if task.Agent != nil {
				agent["Label"] = task.Agent.Label
			}

			_task := &domain.Task{
				Name:  task.Name,
				Type:  task.Type,
				Agent: agent,
			}

			_stage.Tasks = append(_stage.Tasks, _task)
		}

		pipeline.Stages = append(pipeline.Stages, _stage)
	}

	for _, argument := range spec.Arguments {
		displayName := argument.DisplayName

		argSection := domain.ArgSection{
			DisplayName: domain.MulitLangValue{
				ZH_CN: displayName.Zh,
				EN:    displayName.En,
			},
			Items: []domain.ArgItem{},
		}

		for _, item := range argument.Items {
			argItem := domain.ArgItem{
				Name:    item.Name,
				Binding: item.Binding,
				Schema: &domain.ArgItemSchema{
					Type: item.Schema.Type,
				},
				Required: item.Required,
				Default:  item.Default,
				DisplayInfo: &domain.ArgDisplayInfo{
					Name: domain.MulitLangValue{
						ZH_CN: item.Display.Name.Zh,
						EN:    item.Display.Name.En,
					},
					Type: item.Display.Type,
				},
			}

			argSection.Items = append(argSection.Items, argItem)
		}

		pipeline.Arguments = append(pipeline.Arguments, argSection)
	}

	return
}

// Convert from PipelineConfigTemplate to jenkinsfilext object
func Convert(template *devops.PipelineConfigTemplate) (pipeline *domain.PipelineTemplateSpec) {
	spec := template.Spec
	agent := map[string]interface{}{}

	if spec.Agent != nil {
		agent["Label"] = spec.Agent.Label
	}

	pipeline = &domain.PipelineTemplateSpec{
		Engine:    string(spec.Engine),
		Agent:     agent,
		WithSCM:   spec.WithSCM,
		Stages:    []*domain.Stage{},
		Arguments: domain.ArgSections{},
	}

	for _, stage := range spec.Stages {
		if len(stage.Tasks) <= 0 {
			continue
		}

		_stage := &domain.Stage{
			Name:  stage.Name,
			Tasks: []*domain.Task{},
		}

		for _, task := range stage.Tasks {
			agent = map[string]interface{}{}

			if task.Spec.Agent != nil {
				agent["Label"] = task.Spec.Agent.Label
			}

			_task := &domain.Task{
				Name:  task.Name,
				Type:  task.Spec.Type,
				Agent: agent,
			}

			_stage.Tasks = append(_stage.Tasks, _task)
		}

		pipeline.Stages = append(pipeline.Stages, _stage)
	}

	for _, argument := range spec.Arguments {
		displayName := argument.DisplayName

		argSection := domain.ArgSection{
			DisplayName: domain.MulitLangValue{
				ZH_CN: displayName.Zh,
				EN:    displayName.En,
			},
			Items: []domain.ArgItem{},
		}

		for _, item := range argument.Items {
			argItem := domain.ArgItem{
				Name:    item.Name,
				Binding: item.Binding,
				Schema: &domain.ArgItemSchema{
					Type: item.Schema.Type,
				},
				Required: item.Required,
				Default:  item.Default,
				DisplayInfo: &domain.ArgDisplayInfo{
					Name: domain.MulitLangValue{
						ZH_CN: item.Display.Name.Zh,
						EN:    item.Display.Name.En,
					},
					Type: item.Display.Type,
				},
			}

			argSection.Items = append(argSection.Items, argItem)
		}

		pipeline.Arguments = append(pipeline.Arguments, argSection)
	}

	return
}

// JenkinsfileRenderFromTemplate render jenkinsfile from PipelineTemplate
func JenkinsfileRenderFromTemplate(template *devops.PipelineTemplate, source *devops.PipelineSource, values map[string]string, taskClient GetPipelineTaskTemplate) (jenkinsfile string, err error) {
	spec := ConvertFromPipelineTemplate(template)

	configTemplate := &devops.PipelineConfigTemplate{
		Spec: devops.PipelineConfigTemplateSpec{
			Stages: []devops.PipelineStageInstance{},
		},
	}

	templateSpec := template.Spec
	for _, stage := range templateSpec.Stages {
		v1Stage := devops.PipelineStageInstance{}
		v1Stage.Name = stage.Name
		v1Stage.Tasks = []devops.PipelineTemplateTaskInstance{}

		for _, task := range stage.Tasks {
			v1Task := devops.PipelineTemplateTaskInstance{}

			v1Task.Name = task.Name

			taskTemplate, err := taskClient.GetPipelineTaskTemplate(v1Task.Name)
			if err != nil {
				log.Printf("can't find PipelineTaskTemplate name [%s]", v1Task.Name)
				continue
			}

			v1Task.Spec = devops.PipelineTemplateTaskInstanceSpec{
				Type:      task.Type,
				Arguments: []devops.PipelineTemplateArgument{},
				Body:      taskTemplate.Spec.Body,
			}

			for _, argument := range taskTemplate.Spec.Arguments {
				argBinding := fmt.Sprintf("%s.args.%s", task.Name, argument.Name)
				arg := findArg(argBinding, template.Spec.Arguments)
				if arg != nil {
					v1Task.Spec.Arguments = append(v1Task.Spec.Arguments, devops.PipelineTemplateArgument{
						Name:     arg.Name,
						Binding:  arg.Binding,
						Schema:   arg.Schema,
						Required: arg.Required,
						Display: devops.PipelineTaskArgumentDisplay{
							Name: devops.I18nName{
								Zh: arg.Display.Name.Zh,
								En: arg.Display.Name.En,
							},
							Type: arg.Display.Type,
						},
					})
				}
			}

			v1Stage.Tasks = append(v1Stage.Tasks, v1Task)
		}

		configTemplate.Spec.Stages = append(configTemplate.Spec.Stages, v1Stage)
	}

	return render(spec, configTemplate, source, values)
}

func findArg(arg string, argGroup []devops.PipelineTemplateArgumentGroup) *devops.PipelineTemplateArgumentValue {
	for _, group := range argGroup {
		for _, item := range group.Items {

			for _, bind := range item.Binding {
				if arg == bind {
					return &item
				}
			}
		}
	}

	return nil
}

func render(pipeline *domain.PipelineTemplateSpec, template *devops.PipelineConfigTemplate, source *devops.PipelineSource, argumentValues map[string]string) (jenkinsfile string, err error) {
	taskTemplates := map[string]domain.TaskTemplateSpec{}
	scmInfo := createSCM(source)

	// convert values
	argumentObjValues := map[string]interface{}{}
	for key, value := range argumentValues {
		argumentObjValues[key] = value
	}

	// convert TaskTemplate
	templateSpec := template.Spec
	for _, stage := range templateSpec.Stages {
		for _, task := range stage.Tasks {
			Arguments := []domain.ArgItem{}

			for _, argument := range task.Spec.Arguments {
				argItem := domain.ArgItem{
					Name:    argument.Name,
					Binding: argument.Binding,
					Schema: &domain.ArgItemSchema{
						Type: argument.Schema.Type,
					},
					Required: argument.Required,
					DisplayInfo: &domain.ArgDisplayInfo{
						Name: domain.MulitLangValue{
							ZH_CN: argument.Display.Name.Zh,
							EN:    argument.Display.Name.En,
						},
						Type: argument.Display.Type,
					},
				}

				Arguments = append(Arguments, argItem)
			}

			taskTemplates[task.Spec.Type] = domain.TaskTemplateSpec{
				Engine:    "gotpl", // TODO should replace by variable
				Body:      task.Spec.Body,
				Arguments: Arguments,
			}
		}
	}

	return pipeline.Render(taskTemplates, argumentObjValues, scmInfo)
}

// JenkinsfileRender render PipelineConfigTemplate to jenkinsfile
func JenkinsfileRender(pipelineConfigSpec *devops.PipelineConfigSpec) (jenkinsfile string, err error) {
	spec := pipelineConfigSpec
	pipeline := Convert(spec.Strategy.Template)

	source := &spec.Source
	templateSpec := spec.Strategy.Template.Spec

	// convert Arguments
	argumentValues := map[string]string{}
	for _, argument := range templateSpec.Arguments {
		for _, item := range argument.Items {
			fmt.Printf("name: %s, value: %s\n", item.Name, item.Value)
			argumentValues[item.Name] = item.Value
		}
	}

	return render(pipeline, spec.Strategy.Template, source, argumentValues)
}

func createSCM(source *devops.PipelineSource) *domain.SCMInfo {
	if source == nil || source.Git == nil {
		return nil
	}

	return &domain.SCMInfo{
		RepositoryPath: source.Git.URI,
		Type:           "GIT", // we only support git for now
		CredentialsID:  "",
		Branch:         source.Git.Ref,
	}
}
