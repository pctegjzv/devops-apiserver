package k8s

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeClientset "k8s.io/client-go/kubernetes"
)

func getDefaultDevopsConfigmap() *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      v1alpha1.SettingsConfigMapName,
			Namespace: v1alpha1.SettingsConfigMapNamespace,
		},
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.ConfigMapKindName,
			APIVersion: v1alpha1.ConfigMapAPIVersion,
		},
		Data: map[string]string{
			v1alpha1.SettingsKeyGithubCreated: "false",
		},
	}
}

func GetDevopsConfigmap(c kubeClientset.Interface) *corev1.ConfigMap {
	devopsConfigMap, err := c.CoreV1().ConfigMaps(v1alpha1.SettingsConfigMapNamespace).Get(v1alpha1.SettingsConfigMapName, metav1.GetOptions{})
	if err != nil && errors.IsNotFound(err) {
		glog.Infof("Configmap %s/%s does not exist, will create...", v1alpha1.SettingsConfigMapNamespace, v1alpha1.SettingsConfigMapName)
		devopsConfigMap = getDefaultDevopsConfigmap()
		devopsConfigMap, err = c.CoreV1().ConfigMaps(v1alpha1.SettingsConfigMapNamespace).Create(devopsConfigMap)
		if err != nil {
			glog.Errorf("Error creating Configmap %s/%s configuration configmap: %v", v1alpha1.SettingsConfigMapNamespace, v1alpha1.SettingsConfigMapName, err)
		}
	}
	if devopsConfigMap == nil {
		devopsConfigMap = getDefaultDevopsConfigmap()
	}
	if devopsConfigMap.Data == nil {
		devopsConfigMap.Data = make(map[string]string)
	}
	return devopsConfigMap
}
