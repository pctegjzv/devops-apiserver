package k8s

import (
	"encoding/json"
	"fmt"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"

	"github.com/golang/glog"
	"github.com/mitchellh/mapstructure"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeClientset "k8s.io/client-go/kubernetes"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// Do not import packages outside

func GetToolChains(c kubeClientset.Interface) (elements []*v1alpha1.ToolChainElement, err error) {
	elements = []*v1alpha1.ToolChainElement{}
	devopsConfigMap := GetDevopsConfigmap(c)

	domainMap := make(map[string]interface{})
	if domain, ok := devopsConfigMap.Data["_domain"]; ok && domain != "" {
		domainByte := []byte(domain)
		json.Unmarshal(domainByte, &domainMap)
	}

	if toolChains, ok := domainMap[v1alpha1.SettingsKeyToolChains]; ok && toolChains != nil {
		err = mapstructure.Decode(toolChains, &elements)
		if err != nil {
			glog.Errorf("decode the tool chain, err: %v", err)
			return
		}
	}
	return
}

func GetUsernameAndPasswordFromSecret(secretLister corev1listers.SecretLister, secretName, usernameKey, apiTokenKey, namespace string) (username, password string, err error) {
	if secretName == "" || apiTokenKey == "" || usernameKey == "" {
		return
	}

	secret, err1 := secretLister.Secrets(namespace).Get(secretName)
	if err1 != nil {
		err = err1
		return
	}
	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[usernameKey]))
		password = strings.TrimSpace(string(secret.Data[apiTokenKey]))
	}
	return
}

func GetValueInSecret(secret *corev1.Secret, key string) string {
	if secret == nil || secret.Data == nil || len(secret.Data) == 0 {
		return ""
	}

	if value, ok := secret.Data[key]; ok {
		return strings.TrimSpace(string(value))
	}
	return ""
}

func GetOwnerReferences(metadata metav1.ObjectMeta, kind string) (owners []*metav1.OwnerReference) {
	if metadata.OwnerReferences == nil || len(metadata.OwnerReferences) == 0 {
		return
	}

	owners = make([]*metav1.OwnerReference, 0)
	for _, ownerReference := range metadata.OwnerReferences {
		ownerReferenceCopy := ownerReference.DeepCopy()
		if ownerReference.Kind == kind {
			owners = append(owners, ownerReferenceCopy)
		}
	}
	return
}

func GetOwnerReference(metadata metav1.ObjectMeta, kind, name string) *metav1.OwnerReference {
	if metadata.OwnerReferences == nil || len(metadata.OwnerReferences) == 0 {
		return nil
	}

	for _, ownerReference := range metadata.OwnerReferences {
		if ownerReference.Kind == kind && ownerReference.Name == name {
			return &ownerReference
		}
	}
	return nil
}

func GetDataBasicAuthFromSecret(secret *corev1.Secret) *SecretDataBasicAuth {
	return &SecretDataBasicAuth{
		Username: GetValueInSecret(secret, corev1.BasicAuthUsernameKey),
		Password: GetValueInSecret(secret, corev1.BasicAuthPasswordKey),
	}
}

func GetDataOAuth2FromSecret(secret *corev1.Secret) *SecretDataOAuth2 {
	return &SecretDataOAuth2{
		ClientID:       GetValueInSecret(secret, devops.OAuth2ClientIDKey),
		ClientSecret:   GetValueInSecret(secret, devops.OAuth2ClientSecretKey),
		Code:           GetValueInSecret(secret, devops.OAuth2CodeKey),
		AccessTokenKey: GetValueInSecret(secret, devops.OAuth2AccessTokenKeyKey),
		SecretDataOAuth2Details: SecretDataOAuth2Details{
			AccessToken:  GetValueInSecret(secret, devops.OAuth2AccessTokenKey),
			Scope:        GetValueInSecret(secret, devops.OAuth2ScopeKey),
			RefreshToken: GetValueInSecret(secret, devops.OAuth2RefreshTokenKey),
			CreatedAt:    GetValueInSecret(secret, devops.OAuth2CreatedAtKey),
			ExpiresIn:    GetValueInSecret(secret, devops.OAuth2ExpiresInKey),
		},
	}
}

func GetOAuthRedirectUrl(c corev1listers.ConfigMapLister) string {
	return fmt.Sprintf("%s?is_secret_validate=true", getDevopsRedirectUrl(c))
}

func getDevopsRedirectUrl(c corev1listers.ConfigMapLister) (redirectUrl string) {
	redirectUrl = DefaultDevopsRedirectUrl
	authConfigMap, err := c.ConfigMaps(v1alpha1.SettingsConfigMapNamespace).Get(ConfigMapNameAuthConfig)
	if err != nil || authConfigMap == nil || authConfigMap.Data == nil {
		glog.Errorf("error when get configmap %s, err: %v", ConfigMapNameAuthConfig, err)
		return
	}

	if value, ok := authConfigMap.Data[ConfigMapKeyCustomRedirectURI]; ok && value != "" {
		var customRedirectURI ConfigMapDataCustomRedirectURI
		err = json.Unmarshal([]byte(value), &customRedirectURI)
		if err != nil {
			glog.Errorf("error when unmarshal data in configmap %s, data: %s, err: %s", ConfigMapNameAuthConfig, value, err)
			return
		}

		if customRedirectURI.Devops != "" {
			return customRedirectURI.Devops
		}
	}
	return
}
