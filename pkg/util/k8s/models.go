package k8s

import (
	"alauda.io/devops-apiserver/pkg/util/generic"
	"fmt"
	"github.com/golang/glog"
	"strconv"
	"time"
)

type SecretDataBasicAuth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (s *SecretDataBasicAuth) GetData() (data map[string]string) {
	if s == nil {
		return
	}

	data = map[string]string{
		"username": s.Username,
		"password": s.Password,
	}
	return
}

// SecretDataOAuth2Details is the data we can get from access_token response
type SecretDataOAuth2Details struct {
	AccessToken  string `json:"accessToken"`
	Scope        string `json:"scope"`
	RefreshToken string `json:"refreshToken"`
	CreatedAt    string `json:"createdAt"`
	ExpiresIn    string `json:"expiresIn"`
}

type SecretDataOAuth2 struct {
	ClientID       string `json:"clientID"`
	ClientSecret   string `json:"clientSecret"`
	Code           string `json:"code"`
	AccessTokenKey string `json:"accessTokenKey"`
	SecretDataOAuth2Details
}

func (s *SecretDataOAuth2) HasAccessToken() bool {
	return s.AccessToken != ""
}

func (s *SecretDataOAuth2) IsExpired() bool {
	if s.RefreshToken == "" || s.ExpiresIn == "" || s.AccessToken == "" {
		return false
	}

	fmt.Println(s.CreatedAt)
	createdAt := generic.ConvertStrToTime(s.CreatedAt)
	expiresIn, err := strconv.Atoi(s.ExpiresIn)
	if err != nil {
		glog.Errorf("expiresIn %v is not a valid type, err: %v", expiresIn, err)
		return false
	}

	fmt.Println("expiresIn: ", expiresIn, "createdAt: ", createdAt)
	return createdAt.Add(7200 * time.Second).Before(time.Now()) // todo use expiresIn
}

func (s *SecretDataOAuth2) GetData() (data map[string]string) {
	if s == nil {
		return
	}

	data = map[string]string{
		"clientID":       s.ClientID,
		"clientSecret":   s.ClientSecret,
		"code":           s.Code,
		"accessTokenKey": s.AccessTokenKey,
		"accessToken":    s.AccessToken,
		"scope":          s.Scope,
		"refreshToken":   s.RefreshToken,
		"createdAt":      s.CreatedAt,
		"expiresIn":      s.ExpiresIn,
	}
	return
}

type ConfigMapDataCustomRedirectURI struct {
	Devops    string `json:"devops"`
	Dashboard string `json:"dashboard"`
}
