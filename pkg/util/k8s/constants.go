package k8s

const (
	ConfigMapNameAuthConfig       = "auth-config"
	ConfigMapKeyCustomRedirectURI = "custom_redirect_uri"

	DefaultDevopsRedirectUrl = "http://localhost:4200"

	GithubAccessTokenKey    = "x-access-token"
	GitlabAccessTokenKey    = "oauth2"
	BitbucketAccessTokenKey = "x-token-auth"
	GiteeAccessTokenKey     = "oauth2"

	GithubAccessTokenScope    = "user,repo"
	GitlabAccessTokenScope    = "api read_repository"
	BitbucketAccessTokenScope = "account team repository"
	GiteeAccessTokenScope     = "user_info groups projects"
)
