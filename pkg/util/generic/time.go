package generic

import (
	"github.com/golang/glog"
	"time"
)

const LAYOUT = "2006-01-02 15:04:05"

func ConvertTimeToStr(t time.Time) string {
	return t.Format(LAYOUT)
}

func ConvertStrToTime(t string) time.Time {
	formatTime, err := time.Parse(LAYOUT, t)
	if err != nil {
		glog.Errorf("error time format %v", err)
		return time.Time{}
	}
	return formatTime
}

func ConvertTimestampToString(t int64) string {
	return time.Unix(t, 0).Format(LAYOUT)
}

func GetCurrentTimestamp() int64 {
	return time.Now().Unix()
}
