package generic

import (
	"encoding/base64"
	"strings"
)

const (
	HeaderKeyAccept            = "Accept"
	HeaderKeyContentType       = "Content-Type"
	HeaderValueContentTypeJson = "application/json"
	HeaderValueContentTypeForm = "application/x-www-form-urlencoded"
)

func GetSchemaAndHost(url string) (string, string) {
	var scheme string
	schemeSplit := strings.Split(url, "//")
	if len(schemeSplit) > 0 {
		scheme = schemeSplit[0]
		scheme = strings.Replace(scheme, ":", "", -1)
	}
	if len(schemeSplit) > 1 {
		url = schemeSplit[1]
	}
	return scheme, url
}

func BasicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
