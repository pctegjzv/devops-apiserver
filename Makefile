.PHONY: init build-base build unitest test
PROJ?=lauda.io/devops-apiserver
PWD=$(shell pwd)
CODEGENERATOR=${GOPATH}/src/k8s.io/code-generator
K8SCODE=${GOPATH}/src/k8s.io/kubernetes
TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/devops-apiserver
image=$(IMAGE)
tag=$(TAG)
PACKAGES = $(shell go list ./... | grep -v 'client' | grep -v 'mock' | grep -v vendor | grep -v 'test')
SET1=$(foreach pkg,$(shell ls ./pkg/ | grep -v client | grep -v mock), $(shell echo './pkg/'$(pkg)'/...'))
BASE_TOKEN=$(shell echo $(TAG))-int-dev
INT_TOKEN=$(shell echo $(BASE_TOKEN) | sed 's|\.|-|g')
PROTOC=$(shell which protoc)

test:
	go test -cover -v $(PACKAGES) -json > test.json

test-all:
	go test -cover -v $(PACKAGES)

init: init-protoc init-codegenerator init-gitversion init-k8scode
	
init-gitversion:
	go get -u github.com/alauda/gitversion

init-protoc: init-codegenerator
	echo $(PROTOC)
	cd ${CODEGENERATOR}/cmd/go-to-protobuf && go install
	cd ${CODEGENERATOR}/cmd/go-to-protobuf/protoc-gen-gogo && go install
	go get -u github.com/gogo/protobuf/protoc-gen-gofast

init-codegenerator:
	rm -rf ${CODEGENERATOR}
	go get -u k8s.io/code-generator || true
	cd ${CODEGENERATOR} && git checkout kubernetes-1.9.1

init-k8scode:
	rm -rf ${K8SCODE}
	go get -u k8s.io/kubernetes || true
	cd ${K8SCODE} && git checkout v1.9.1

build:
	GO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -a -installsuffix cgo -o ${PWD}/bin/devops-apiserver
	upx ${PWD}/bin/devops-apiserver

k8s-init:
	kubectl create -f artifacts/deploy/ns.yaml
	kubectl create -f artifacts/deploy/sa.yaml
	kubectl create -f artifacts/deploy/auth-delegator.yaml
	kubectl create -f artifacts/deploy/auth-reader.yaml
	kubectl create -f artifacts/deploy/secret.yaml
	kubectl create -f artifacts/deploy/etcd-pod.yaml
	kubectl create -f artifacts/deploy/deploy.yaml
	kubectl create -f artifacts/deploy/service.yaml
	kubectl create -f artifacts/deploy/apiservice.yaml
	$(MAKE) run

k8s-drop:
	kubectl delete -f artifacts/deploy/apiservice.yaml
	kubectl delete -f artifacts/deploy/sa.yaml
	kubectl delete -f artifacts/deploy/auth-delegator.yaml
	kubectl delete -f artifacts/deploy/auth-reader.yaml
	kubectl delete -f artifacts/deploy/secret.yaml
	kubectl delete -f artifacts/deploy/etcd-pod.yaml
	kubectl delete -f artifacts/deploy/deploy.yaml
	kubectl delete -f artifacts/deploy/service.yaml
	kubectl delete -f artifacts/deploy/ns.yaml


build-image: build
	cp artifacts/images/Dockerfile ./bin
	docker build -t ${IMAGE}:${TAG} -f ./bin/Dockerfile ./bin
	rm -rf ./bin

push-image: build-image
	docker push ${IMAGE}:${TAG}

run: push-image
	$(MAKE) image=${IMAGE} tag=${TAG} update

update:
	kubectl set image deploy/devops-apiserver -n alauda-system server=$(image):$(tag) controller=$(image):$(tag)
	make restart

controller: 
	go run main.go controller --kubeconfig ~/.kube/config --logtostderr -v 7

clean:
	rm -rf ./bin

.PHONY: gen-client
gen-client:
	./hack/update-codegen.sh

.PHONY: build-in-docker
build-in-docker:
	docker run --rm -v ${PWD}:/go/src/alauda.io/devops-apiserver -w /go/src/alauda.io/devops-apiserver index.alauda.cn/library/golang:alpine go install

.PHONY: cover
cover: collect-cover-data test-cover-html

collect-cover-data:
	echo "mode: count" > coverage-all.out
	@$(foreach pkg,$(PACKAGES),\
		go test -v -coverprofile=coverage.out -covermode=count $(pkg);\
		if [ -f coverage.out ]; then\
			tail -n +2 coverage.out >> coverage-all.out;\
		fi;)

test-cover-html:
	go tool cover -html=coverage-all.out -o coverage.html

test-cover-func:
	go tool cover -func=coverage-all.out

open-cover-html:
	open coverage.html

check: check-test check-coverage

check-full: check-lint check-test check-coverage

check-lint:
	gometalinter.v2 --checkstyle --exclude='(.)*/zz_(.)*go' --exclude='(.)*_test(.)go' --exclude='(.)*/generated(.)*go' --deadline 10m $(SET1)  > report.xml || true

check-coverage:
	gocov test $(PACKAGES) | gocov-xml > coverage.xml

check-test:
	go test -v $(PACKAGES) | go-junit-report > test.xml

set:
	echo $(PROTOC)

build-test-image:
	docker build -t index.alauda.cn/alaudak8s/devops-apiserver-tests:$(INT_TOKEN) -f artifacts/images/Dockerfile.test .
	docker push index.alauda.cn/alaudak8s/devops-apiserver-tests:$(INT_TOKEN)

integration: build-test-image int-cleanup
	cp test/integration-tests.yaml tmp.yaml
	sed -i '' "s|%TOKEN%|$(INT_TOKEN)|g" tmp.yaml
	sed -i '' "s|%IMAGE%|index.alauda.cn/alaudak8s/devops-apiserver-tests:$(INT_TOKEN)|g" tmp.yaml
	kubectl create -f tmp.yaml

local-integration:
	ginkgo -r --progress

int-cleanup:
	kubectl delete -f tmp.yaml || true
	curl -X DELETE -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/
	rm tmp.yaml || true

int-status:
	curl -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/ | python -m json.tool

gen-protobuf:
	./hack/generate-proto.sh

gen-mock:
	mockgen -destination ./pkg/mock/client-go/informers/factory.go -package informers k8s.io/client-go/informers SharedInformerFactory
	mockgen -destination ./pkg/mock/devops/informers/factory.go -package informers alauda.io/devops-apiserver/pkg/client/informers/externalversions SharedInformerFactory
	mockgen -destination ./pkg/mock/devops/controller/subcontroller.go -package informers alauda.io/devops-apiserver/pkg/controller SubController
	mockgen -destination ./pkg/mock/apiserver/registry/storage.go -package registry k8s.io/apiserver/pkg/registry/rest StandardStorage
	mockgen -destination ./pkg/mock/mhttp/roundtripper.go -package mhttp net/http RoundTripper
	mockgen -destination ./pkg/mock/devops/registry/url_builder.go -package registry alauda.io/devops-apiserver/pkg/registry/devops/pipeline URLBuilder
	mockgen -destination ./pkg/mock/printers/handler.go -package printers k8s.io/kubernetes/pkg/printers PrintHandler
	# mockgen -destination ./pkg/mock/apiserver/registry/generic.go -package registry k8s.io/apiserver/pkg/registry/generic RESTOptionsGetter
	
restart:
	kubectl scale deploy -n alauda-system devops-apiserver --replicas=0
	kubectl scale deploy -n alauda-system devops-apiserver --replicas=1